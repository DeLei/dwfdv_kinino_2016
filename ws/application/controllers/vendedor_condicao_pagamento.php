<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Vendedor_condicao_pagamento extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('vendedor_condicao_pagamento_model');
    }

	function exportar_get()
	{
		$dados = $this->vendedor_condicao_pagamento_model->exportar_vendedor_condicao_pagamento($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar a Regra de Vendedor x Condição de Pagamento!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->vendedor_condicao_pagamento_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total da Regra de Vendedor x Condição de Pagamento!'), 404);
        }
	}
	
}