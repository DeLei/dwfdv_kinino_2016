<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Logs extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('logs_pedidos_model');
    }

	function importar_post()
	{
		$dados = $this->logs_pedidos_model->salvar_logs();
		
		if($dados['sucesso'])
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Notícias!'), 404);
        }
	}
	
}