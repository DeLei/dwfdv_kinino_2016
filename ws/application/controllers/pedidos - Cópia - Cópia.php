<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Pedidos extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('pedidos_processados_model');
		$this->load->model('pedidos_pendentes_model');
    }
	
	function teste_get()
	{
		
		
		//Exemplo de Pedido em Objeto
		

		$pedido['codigo_do_cliente'] = '000013';
		$pedido['loja_do_cliente'] = '00';
		$pedido['codigo_do_representante'] = '000002';
		$pedido['codigo_do_produto'] = '11010';
		$pedido['tipo_de_frete'] = 'F';
		$pedido['codigo_da_tabela_de_preco'] = '001';
		$pedido['timestamp'] = '20120427';
		$pedido['id_pro'] = '';
		$pedido['raz_soc_pro'] = '';
		$pedido['codigo_da_forma_de_pagamento'] = '003';
		$pedido['codigo_da_transportadora'] = '000028';
		$pedido['ordem_de_compra'] = 'OC 01';
		$pedido['data_de_entrega'] = '20120501';
		$pedido['observacao'] = 'Observacao do pedido';
		$pedido['id_do_evento'] = '3';
		$pedido['preco'] = '250.5';
		$pedido['total_sem_ipi'] = '2799.96';
		$pedido['desconto'] = '0';
		$pedido['valor_st'] = '0';
		$pedido['quantidade'] = '25';
		$pedido['filial'] = '01';
		$pedido['tipo_venda'] = 'V';
		
		
		$_pedidos[] = (object) $pedido;
		
		

		$pedido['codigo_do_cliente'] = '000013';
		$pedido['loja_do_cliente'] = '00';
		$pedido['codigo_do_representante'] = '000002';
		$pedido['codigo_do_produto'] = '11010';
		$pedido['tipo_de_frete'] = 'F';
		$pedido['codigo_da_tabela_de_preco'] = '001';
		$pedido['timestamp'] = '20120427';
		$pedido['id_pro'] = '';
		$pedido['raz_soc_pro'] = '';
		$pedido['codigo_da_forma_de_pagamento'] = '003';
		$pedido['codigo_da_transportadora'] = '000028';
		$pedido['ordem_de_compra'] = 'OC 01';
		$pedido['data_de_entrega'] = '20120501';
		$pedido['observacao'] = 'Observacao do pedido';
		$pedido['id_do_evento'] = '3';
		$pedido['preco'] = '250.5';
		$pedido['total_sem_ipi'] = '2799.96';
		$pedido['desconto'] = '0';
		$pedido['valor_st'] = '0';
		$pedido['quantidade'] = '25';
		$pedido['filial'] = '01';
		$pedido['tipo_venda'] = 'V';
		
		$_pedidos[] = (object) $pedido;
		
		
		$pedidos = (object) $_pedidos;
	
		
	
		//$pedido = json_decode($this->input->post('pedido'));
		
		$pedido = $this->pedidos_pendentes_model->importar_pedido($pedidos);
		
		if($pedido)
        {
            $this->response($pedido, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pedidos!'), 404);
        }
	}
	
	function importar_post()
	{
		$pedido = json_decode($this->input->post('pedido'));
		
		$retorno_pedido = $this->pedidos_pendentes_model->importar_pedido($pedido);
		
		if($retorno_pedido)
        {
            $this->response($retorno_pedido, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível importar Pedido!'), 404);
        }
	}
	
	// Pedidos Processados
	function exportar_get()
	{
		$pedidos = $this->pedidos_processados_model->exportar_pedidos($this->get('id'));
		
		if($pedidos)
        {
            $this->response($pedidos, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pedidos!'), 404);
        }
	}
	
	// Pedidos Pendentes - Controle "ROTEADO" -> (pedidos_pendentes/exportar/format/json);
	function exportar_pedidos_pendentes_get()
	{
		$pedidos = $this->pedidos_pendentes_model->exportar_pedidos($this->get('id'));
		
		if($pedidos)
        {
            $this->response($pedidos, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Pedidos!'), 404);
        }
	}
	
}