<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Representante extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('representantes_model');
    }

	function exportar_get()
	{
		$dados = $this->representantes_model->exportar_representante($this->input->get('codigo_representante'));
		
		
		
		if($dados)
        {
			$response[] = $dados;
		
            $this->response($response, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Representante!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = 1;
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Representante!'), 404);
        }
	}
	
}