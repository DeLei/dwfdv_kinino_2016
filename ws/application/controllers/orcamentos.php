<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Orcamentos extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
		$this->load->model('orcamentos_model');
    }
	
	
	function exportar_get()
	{
		$orcamentos = $this->orcamentos_model->exportar_orcamentos($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'));
		
		if($orcamentos)
        {
            $this->response($orcamentos, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Orçamentos!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->orcamentos_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Orçamentos!'), 404);
        }
	}
	
	
	function importar_post()
	{
	
		if($this->input->post('retorno') && $this->input->post('id_usuario'))
		{
			$retorno = $this->orcamentos_model->importar($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'));

			if($retorno)
			{
				$this->response($retorno, 200);
			}
			else
			{
				$this->response(array('error' => 'Não foi possível enviar Orçamentos!'), 404);
			}
		}
		else
		{
			$this->response(array('error' => 'Não foi possível enviar Orçamentos!'), 404);
		}
	}
	
	/**
	* upload_post()
	*
	* Realiza o upload do pedido para o servidor no diretório:
	*
	* 	X:\....\dwfdv\ws\uploads\pedidos\sincronizar
	**/
	function upload_post()
	{
		//Carrega o mapeamento
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
		
		//Move o arquivo enviado para a pasta de uploads
		$arquivo = explode('_', $_FILES['file']['name']);
		$arquivo = $arquivo[0] . '.json';
		move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/orcamentos/sincronizar/' . $arquivo);
		
		
		//Começa a leitura do arquivo
		$caminho = getcwd() . '\uploads\orcamentos\sincronizar\\' . $arquivo;
		$f = fopen($caminho, 'r');
		if ($f)
		{
			// Ler cabecalho do arquivo
			$cabecalho = explode('||', fgets($f));
			
			// Enquanto nao terminar o arquivo
			$dados = array();
			while (!feof($f))
			{
				// Ler uma linha do arquivo
				$linha = explode('||', fgets($f));
				
				if ((!$linha) or (count($linha) <= 1))
				{
					continue;
				}
				
				// Montar registro com valores indexados pelo cabecalho
				$registro = array_combine($cabecalho, $linha);
				
				$dados[] = $registro;
			}
			fclose($f);
		}
		
		//Organiza os valores
		$valores = $this->obter_valores($dados);
		
		//Envia os dados para gravação
		$retorno = $this->orcamentos_model->salvar_orcamento($valores, $arquivo, $this->input->post('id_usuario'), $this->input->post('codigo_representante'));
		
		
		//Retorna a situação do pedido enviado
		if($retorno)
		{
			$this->response($retorno, 200);
		}
		else
		{
			$this->response(array('error' => 'Não foi possível enviar Orçamento!'), 404);
		}
	}
	
	/**
	* obter_valores
	*
	* Organiza os campos e valores para salvar nas tabelas SZW e SZT
	**/
	private function obter_valores($dados)
	{
		$desconsiderar_indice = array('filial', 'pedido_chave');
		
		$recno_pedidos_dw	= $this->orcamentos_model->obter_ultimo_recno('pedidos_dw');
		$recno_itens_troca	= $this->orcamentos_model->obter_ultimo_recno('itens_troca');
		$item = 1;
		foreach($dados as $key => $pedido)
		{
		
			//Faz a separação dos itens de troca
			$tabela = 'pedidos_dw';
			if($pedido['item_troca'] == 1)
			{
				$tabela = 'itens_troca';
			}
			
			
			$valores[$tabela][$key] = array();
			
			if($pedido['item_troca'] == 1) {
				$recno_itens_troca += 1;
				$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['chave']] = $recno_itens_troca;
			} else {
				$recno_pedidos_dw += 1;
				$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['chave']] = $recno_pedidos_dw;
			}
			foreach($pedido as $indice => $valor)
			{
			
				if(in_array($indice, $desconsiderar_indice))
				{
					continue;
				}
				
				$indice = str_replace('pedido_', '', $indice);
				
				if($pedido['item_troca'] == 1 && $indice == 'peso_total')
				{
					$valor = '0';
				}
				
				if(array_key_exists($indice, $this->mapeamento['campos'][$tabela]))
				{
					$valor = tratar_valores($indice, $valor);
					
					if($indice == 'numero_item'){
						$valor = $item;
						$item++;
					}
					
					$valores[$tabela][$key][$this->mapeamento['campos'][$tabela][$indice]] = $valor;
				}
				
				
				//Valores fixos
				$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['status']] = 'A';
				$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['delecao']] = '';
				
				
				//Tratamento de valores/campos para os itens de troca
				if($tabela == 'itens_troca')
				{
					$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['unidade_medida']] = $pedido['produto_unidade_medida'];
					$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segunda_unidade_medida']] = $pedido['produto_segunda_unidade_medida'];
					$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segundo_preco_venda']] = $pedido['pedido_preco_unitario'];
					$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['preco_venda']] = $pedido['pedido_valor_unidade'];
					
					$pedido['produto_tipo_converter'] = 'D';
					
					if($pedido['pedido_unidade_medida'] != $pedido['produto_segunda_unidade_medida']) //Troca pela 1ª UM
					{
						if($pedido['produto_tipo_converter'] == 'D') {
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segundo_preco_venda']] = number_format($pedido['pedido_preco_unitario'] * $pedido['produto_converter'], 3);
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['preco_unitario']]		= number_format($pedido['pedido_preco_unitario'] * $pedido['produto_converter'], 3);
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segunda_quantidade']] = number_format($pedido['pedido_quantidade'] / $pedido['produto_converter'], 3);
						} else {
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segundo_preco_venda']] = number_format($pedido['pedido_preco_unitario'] / $pedido['produto_converter'], 3);
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['preco_unitario']]		= number_format($pedido['pedido_preco_unitario'] / $pedido['produto_converter'], 3);
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segunda_quantidade']] = number_format($pedido['pedido_quantidade'] * $pedido['produto_converter'], 3);
						}
						$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['quantidade']] = $pedido['pedido_quantidade'];
					}
					else //Troca pela 2ª UM
					{
						if($pedido['produto_tipo_converter'] == 'D') {
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['quantidade']] = number_format($pedido['pedido_quantidade'] * $pedido['produto_converter'], 3);
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segundo_preco_venda']] = $pedido['pedido_preco_unitario'];
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['preco_unitario']]		= $pedido['pedido_preco_unitario'];
						} else {
							$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['quantidade']] = number_format($pedido['pedido_quantidade'] / $pedido['produto_converter'], 3);
						}
						$valores[$tabela][$key][$this->mapeamento['campos'][$tabela]['segunda_quantidade']] = $pedido['pedido_quantidade'];
					}
				}
				
			}
		}
		
		return $valores;
	}
	
}