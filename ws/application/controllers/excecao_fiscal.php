<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Excecao_fiscal extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('excecao_fiscal_model');
    }

	function exportar_get()
	{
		$dados = $this->excecao_fiscal_model->exportar_excecao_fiscal($this->input->get('id'));
		
		
		
		if($dados)
        {
			$response = $dados;
		
            $this->response($response, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar os dados de Excecão Fiscal!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->excecao_fiscal_model->retornar_total($this->input->get('id'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar total de Exceção Fiscal!'), 404);
        }
	}
	
}