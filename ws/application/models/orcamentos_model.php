<?php

class Orcamentos_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
	
	
	/**
	* Met�do:		exportar_orcamentos
	* 
	* Descri��o:	Fun��o Utilizada para pegar retornar dados dos Or�amentos
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function exportar_orcamentos($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
	
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['campos']['pedidos_dw']['id_pedido'], $parametros_consulta, TRUE);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw'], 'pedido');
		$select_cliente 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_prospect 			= select_all($this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects'], 'prospect');
		$select_produto 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		
		$select = array_merge(
			$select_pedido, 
			$select_cliente,
			$select_prospect,
			$select_produto,
			$select_formas_pagamento,
			$select_transportadoras
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = N�o Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		$select[] = "'0' as converter_pedido_orcamento";
		$select[] = "'0' as item_troca";// Marcar itens de troca
		$select[] = "'' as pedido_segunda_quantidade";
		$select[] = "'' as pedido_segunda_unidade_medida";
		$select[] = "'' as pedido_segundo_preco_venda";

		
		$select += formatar_euf($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial'], $codigo_empresa);

		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ' >', $id);
		}
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $codigo_representante);
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo_venda'], '*'); // Buscar Or�amentos
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['delecao'] . ' !=', '*');
		
		//-----------------------------------------------
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . " != '*'". ' AND ' .
			" ((".$this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'] . ' AND ' .
			$this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'] . ') OR ' .
			$this->_db_cliente['campos']['clientes']['cpf'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['id_prospects']. ' AND '.$this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'].' IS NULL)'			
		, 'left');
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['prospects'], 
			euf(
				$this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['cgc'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['id_prospects'] . ' AND ' .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['pedidos_dw']);
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['codigo_empresa']		 = NULL;
	
		return retornar_total($this, $parametros_consulta, TRUE);
	}
	
	//--------------------------------------------------------
	
	/**
	* Met�do:		importar
	* 
	* Descri��o:	Fun��o Utilizada para inserir or�amentos no banco, e inserir LOGS
	* 
	* Data:			16/11/2012
	* Modifica��o:	16/11/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW for�a de vendas
	* @param		string 		$id_usuario					- ID do usu�rio
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('orcamentos_dw', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$orcamentos = json_decode($dados);
			
			foreach($orcamentos as $orcamento)
			{

				//Item de Troca
				if($orcamento->item_troca == 1)
				{
					$this->importar_itens_troca($id_sincronizacao, $orcamento);
				}
				else
				{
					try{
					
						if($this->validar_orcamento($orcamento->pedido_id_pedido, $orcamento->pedido_codigo_produto))
						{
							$orcamento->editado = 1;
						}
					
						// Se editado == 1 - Editar o Pedidos, senao, inserir
						if($orcamento->editado == 1)
						{
							$valores = $this->obter_campos_valores($orcamento, FALSE);
							
							$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $orcamento->pedido_id_pedido);
							$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['codigo_produto'], $orcamento->pedido_codigo_produto);
							$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['status'] . ' !=', 'I');
							$this->db_cliente->update($this->mapeamento['tabelas']['pedidos_dw'], $valores);
						}
						else
						{
							$valores = $this->obter_campos_valores($orcamento);
							
							$this->db_cliente->insert($this->mapeamento['tabelas']['pedidos_dw'], $valores);
						}
					}
					catch(Exception $e)
					{
						$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($orcamento)); 
					}
				}
			}
		}
		
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
			
		if($dados_erros)
		{
			$codigos_erro = array();
			
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->pedido_id_pedido, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->pedido_id_pedido;
					$nome_erro[] = $dados_json->pedido_id_pedido . ' - ' . ($dados_json->cliente_nome ? $dados_json->cliente_nome : $dados_json->prospect_nome);
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');
		}
		
	}
	
	
	function importar_itens_troca($id_sincronizacao = NULL, $orcamento = NULL)
	{
		
		try{
					
			if($this->validar_orcamento($orcamento->pedido_id_pedido, $orcamento->pedido_codigo_produto, TRUE))
			{
				$orcamento->editado = 1;
			}
			
			// Se editado == 1 - Editar o Pedidos, senao, inserir
			if($orcamento->editado == 1)
			{
				$valores = $this->obter_campos_valores($orcamento, FALSE, TRUE);
			
				$this->db_cliente->where($this->mapeamento['campos']['itens_troca']['id_pedido'], $orcamento->pedido_id_pedido);
				$this->db_cliente->where($this->mapeamento['campos']['itens_troca']['codigo_produto'], $orcamento->pedido_codigo_produto);
				$this->db_cliente->update($this->mapeamento['tabelas']['itens_troca'], $valores);
			}else{
				$valores = $this->obter_campos_valores($orcamento, TRUE, TRUE);
			
				$this->db_cliente->insert($this->mapeamento['tabelas']['itens_troca'], $valores);
			}
		
		}
		catch(Exception $e)
		{
			$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($orcamento)); 
		}	
		
	}
	
	function validar_orcamento($id_pedido, $codigo_produto, $troca = FALSE)
	{
		$troca = 'pedidos_dw';
		if($troca)
		{
			$tabela = 'itens_troca';
		}
		
		$pedido = $this->db_cliente->select($this->mapeamento['campos'][$troca]['id_pedido'])
									->from($this->mapeamento['tabelas'][$troca])
									->where(array(
										$this->mapeamento['campos'][$troca]['id_pedido']		=> $id_pedido,
										$this->mapeamento['campos'][$troca]['codigo_produto']	=> $codigo_produto
									))
									->limit(1)->get()->result();
		
		return $pedido;
	}
	
	
	/**
	* Met�do:		obter_campos_valores
	* 
	* Descri��o:	Fun��o Utilizada para retornar os campo com valores
	* 
	* Data:			21/09/2012
	* Modifica��o:	21/09/2012
	* 
	* @access		public
	* @param		array 		$orcamento				- Dados dos Or�amentos 
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function obter_campos_valores($pedido, $gerar_recno = TRUE, $troca = FALSE)
	{
		$tabela = 'pedidos_dw';
		
		if($troca)
		{
			$tabela = 'itens_troca';
		}
		
	
		foreach($pedido as $indice => $valor)
		{

			if($indice == 'filial')
			{
				continue;
			}
			
			if(!$gerar_recno)
			{
				if($indice == 'pedido_chave')
				{
					continue;
				}
			}
		
			$indice = str_replace("pedido_", "", $indice, $count);
			
			if($count > 1)
			{
				$indice = "pedido_" . $indice;
			}
			
			if($gerar_recno)
			{
				if($indice == 'chave') // gerar Recno
				{
					if($troca)
					{
						$valor = $this->gerar_recno_itens_troca();
					}
					else
					{
						$valor = $this->gerar_recno();
					}
				}
			}

			if(!empty($this->mapeamento['campos'][$tabela][$indice]))
			{
				
				if(empty($valor)) // N�o podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if(strtoupper($valor) == 'UNDEFINED') // N�o podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if($indice == 'time_emissao')
				{
					$valor = (int) $valor;
				}
				
				if($indice == 'status')
				{
					$valor = 'A';
				}
				
				
				if($indice == 'total_desconto_item') //  Calculando o valor do desconto
				{
					
					//Aplicar desconto em cima do pre�o de tabela que pedido_preco_unitario
					if($pedido->pedido_desconto1 > 0)
					{
						$pedido->pedido_preco_unitario = $pedido->pedido_preco_unitario - ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto1 / 100));
					}
					
					//Calcular o valor do desconto
					$valor = $pedido->pedido_quantidade * ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto_item / 100));
				}
				
				$valores[$this->mapeamento['campos'][$tabela][$indice]] = $valor;
				
				if($troca)
				{
					$valores[$this->mapeamento['campos'][$tabela]['unidade_medida']] = $pedido->produto_unidade_medida;
					$valores[$this->mapeamento['campos'][$tabela]['segunda_unidade_medida']] = $pedido->produto_segunda_unidade_medida;
					$valores[$this->mapeamento['campos'][$tabela]['segundo_preco_venda']] = $pedido->pedido_preco_unitario;
					$valores[$this->mapeamento['campos'][$tabela]['preco_venda']] = $pedido->pedido_valor_unidade;
					
					$pedido->produto_tipo_converter = 'D';
					//Troca pela 1� UM
					if($pedido->pedido_unidade_medida != $pedido->produto_segunda_unidade_medida)
					{
						if($pedido->produto_tipo_converter == 'D')
						{
							$valores[$this->mapeamento['campos'][$tabela]['segunda_quantidade']] = number_format($pedido->pedido_quantidade / $pedido->produto_converter, 3);
						}
						else
						{
							$valores[$this->mapeamento['campos'][$tabela]['segunda_quantidade']] = number_format($pedido->pedido_quantidade * $pedido->produto_converter, 3);
						}
						$valores[$this->mapeamento['campos'][$tabela]['quantidade']] = $pedido->pedido_quantidade;
					}
					//Troca pela 2� UM
					else
					{
						if($pedido->produto_tipo_converter == 'D')
						{
							$valores[$this->mapeamento['campos'][$tabela]['quantidade']] = number_format($pedido->pedido_quantidade * $pedido->produto_converter, 3);
						}
						else
						{
							$valores[$this->mapeamento['campos'][$tabela]['quantidade']] = number_format($pedido->pedido_quantidade / $pedido->produto_converter, 3);
						}
						$valores[$this->mapeamento['campos'][$tabela]['segunda_quantidade']] = $pedido->pedido_quantidade;
					}
				}
			}
			
		}
		
		$valores[$this->mapeamento['campos'][$tabela]['delecao']] = ' ';
		
		return $valores;
	}
	
	function gerar_recno()
	{
	
		$dados = $this->db_cliente
			->select('MAX(' . $this->mapeamento['campos']['pedidos_dw']['chave'] . ')+1 AS chave')
			->from($this->mapeamento['tabelas']['pedidos_dw'])
			->get()->row_array();
			
	
		if($dados['chave'])
		{
			return $dados['chave'];
		}
		else
		{
			return 1;
		}
	
	}
	
	function gerar_recno_itens_troca()
	{
	
		$dados = $this->db_cliente
			->select('MAX(' . $this->mapeamento['campos']['itens_troca']['chave'] . ')+1 AS chave')
			->from($this->mapeamento['tabelas']['itens_troca'])
			->get()->row_array();
			
	
		if($dados['chave'])
		{
			return $dados['chave'];
		}
		else
		{
			return 1;
		}
	
	}
	
	
	/**
	* Met�do:		consulta_troca
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar dados referentes aos itens de troca
	* 
	* Data:			14/01/2014
	* Modifica��o:	14/01/2014
	* 
	* @access		public
	* @param		string 		$dados
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta_troca($dados = NULL)
	{
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca'], 'pedido');
		$select_cliente 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_prospect 			= select_all($this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects'], 'prospect');
		$select_produto 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		
		$select = array_merge(
			$select_pedido, 
			$select_cliente,
			$select_prospect,
			$select_produto,
			$select_formas_pagamento,
			$select_transportadoras
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = N�o Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		$select[] = "'0' as converter_pedido_orcamento";
		$select[] = "'1' as item_troca";// Marcar itens de troca
		$select[] = "'' as pedido_analitico_perc";
		$select[] = "'' as pedido_analitico_valor";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial'], $codigo_empresa);

		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['itens_troca']['id_pedido'] . ' >', $id);
		}
		
		$this->db_cliente->where($this->_db_cliente['campos']['itens_troca']['codigo_representante'], $codigo_representante);
		
		$this->db_cliente->where($this->_db_cliente['campos']['itens_troca']['tipo_venda'], '*'); // Buscar Or�amentos
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['itens_troca'] . '.' . $this->_db_cliente['campos']['itens_troca']['delecao'] . ' !=', '*');
		
		//-----------------------------------------------
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . " != '*'". ' AND ' .
			" ((".$this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['codigo_cliente'] . ' AND ' .
			$this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['loja_cliente'] . ') OR ' .
			$this->_db_cliente['campos']['clientes']['cpf'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['id_prospects']. ' AND '.$this->_db_cliente['campos']['itens_troca']['codigo_cliente'].' IS NULL)'			
		, 'left');
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['prospects'], 
			euf(
				$this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['cgc'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['id_prospects'] . ' AND ' .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['itens_troca']['codigo_produto'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['itens_troca']['condicao_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['campos']['itens_troca']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['itens_troca']);
	}
	
	
	/**
	*	obter_ultimo_recno(nome_da_tabela)
	*
	*		Obtem o �ltimo R_E_C_N_O_ da tabela informada
	**/
	function obter_ultimo_recno($tabela){
		$dados = $this->db_cliente
						->select('MAX(' . $this->mapeamento['campos'][$tabela]['chave'] . ') AS chave')
						->from($this->mapeamento['tabelas'][$tabela])
						->get()->row()->chave;
		
		if($dados) {
			return $dados;
		} else {
			return 0;
		}
	}
	
	/**
	*	salvar_orcamento( array('orcamentos' => array(valores), 'itens_troca' => array(valores) ) )
	*
	*		Salva o pedido na base
	**/
	function salvar_orcamento($valores, $arquivo, $id_usuario, $codigo_representante)
	{
		//Carrega o model
		$this->load->model('sincronizacoes_model');
		
		//Grava o log de sincroniza��o
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('orcamentos', json_encode($valores), $id_usuario, $codigo_representante);
		
		try
		{
			$this->db_cliente->trans_start(); //Inicia a transa��o com o BD
			
				$this->db_cliente->insert_batch($this->mapeamento['tabelas']['pedidos_dw'], $valores['pedidos_dw']); //Insere os itens de venda
				
				if(isset($valores['itens_troca'])) //Verifica se existem itens de troca no pedido
				{
					$this->db_cliente->insert_batch($this->mapeamento['tabelas']['itens_troca'], $valores['itens_troca']); //Insere os itens de troca
				}
				
			$this->db_cliente->trans_complete(); //Fecha a transa��o
			
			//Transa��o bem sucedida
			if($this->db_cliente->trans_status())
			{
				//Move o arquivo da pasta "sincronizar" para a pasta "sincronizados"
				rename('uploads/orcamentos/sincronizar/' . $arquivo, 'uploads/orcamentos/sincronizados/' . $arquivo);
				
				return array('sucesso' => 'ok');
			}
		}
		catch(Exception $e)
		{
			$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($valores)); 
		}
		
		//Obtem os registros de erro
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
		
		if($dados_erros)
		{
			$codigo_erro = str_replace('.json', '', $arquivo);
			
			$erros['erro'] = array($codigo_erro);
			$erros['erro_descricao'] = 'Erro ao enviar o or�amento ' . $codigo_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');
		}
		
	}
}