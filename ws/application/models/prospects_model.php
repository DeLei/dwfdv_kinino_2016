<?php

class Prospects_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
    
	/**
	* Met�do:		exportar_prospects
	* 
	* Descri��o:	Fun��o Utilizada para pegar retornar dados de Prospects
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function exportar_prospects($id = NULL, $pacote = NULL, $codigo_representante = NULL, $erros_prospects = NULL)
	{
		// * Retornar todos os campos
		
		// Ordena��o
		//$this->db_cliente->order_by($this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['codigo']);
		
		// Consulta com Pacote de Dados
		//$dados = pacote_dados($this->db_cliente, $this->consulta($id, $codigo_representante, $erros_prospects), $pacote);

		
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['erros_prospects'] 		= $erros_prospects;
		
		
		
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['campos']['prospects']['data_emissao'], $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados)
	{
		$id 					= NULL;
		$codigo_representante	= $dados['codigo_representante'];
		$erros_prospects 		= $dados['erros_prospects'];
		$codigo_empresa 		= $dados['codigo_empresa'];
	
		// Campos para o SELECT
		$select = select_all($this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects'], NULL, FALSE, 'filial');
		
		$select[] = $this->_db_cliente['campos']['cadastro_municipios']['nome'] . ' as nome_municipio';
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = N�o Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects']['filial'], $codigo_empresa);
		
		// Join
		$this->db_cliente->join(
				$this->_db_cliente['tabelas']['cadastro_municipios'], 
				euf(
					$this->_db_cliente['tabelas']['cadastro_municipios'], $this->_db_cliente['campos']['cadastro_municipios']['filial'], 
					$this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects']['filial']
				) .
				$this->_db_cliente['campos']['prospects']['codigo_municipio'] . ' = ' . $this->_db_cliente['campos']['cadastro_municipios']['codigo'] . ' AND ' . $this->_db_cliente['campos']['prospects']['estado'] . ' = ' . $this->_db_cliente['campos']['cadastro_municipios']['uf']
		, 'left');
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['codigo'] . ' >', $id);
		}
		
		if($erros_prospects)
		{
			$codigos_prospects = explode(',',$erros_prospects);
			$this->db_cliente->where_not_in($this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['codigo'], $codigos_prospects);
		}
		

		$this->db_cliente->where($this->_db_cliente['campos']['prospects']['codigo_representante'], $codigo_representante);
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['delecao'] . ' !=', '*');
	
	
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['prospects']);
		
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL, $erros_prospects = NULL)
	{	
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['erros_prospects'] = $erros_prospects;
		$parametros_consulta['codigo_empresa'] = NULL;
		
		return retornar_total($this, $parametros_consulta);
	}
	
	
	/**
	* Met�do:		importar
	* 
	* Descri��o:	Fun��o Utilizada para inserir prospects no banco, e inserir LOGS
	* 
	* Data:			21/09/2012
	* Modifica��o:	21/09/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Prospects enviados pelo DW for�a de vendas
	* @param		string 		$id_usuario					- ID do usu�rio
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('prospects', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$prospects = json_decode($dados);
			
			foreach($prospects as $prospect)
			{
				
				if($this->verificar_cgc($prospect->cgc) || ($prospect->editado == 1 && is_numeric($prospect->codigo)))
				{
					
					// Obter o Tipo de Pessoa
					$prospect->tipo_pessoa = verificar_tipo_pessoa($prospect->cgc);
				
					try{
					
						// Se editado == 1 - Editar o Prospects, senao, inserir
						if($prospect->editado == 1 && is_numeric($prospect->codigo))
						{
							// Obter Valores
							$valores = $this->obter_campos_valores($prospect, FALSE);
						
							$this->db_cliente->where($this->mapeamento['campos']['prospects']['codigo'], $prospect->codigo);
							$this->db_cliente->update($this->mapeamento['tabelas']['prospects'], $valores);
						}else{
						
							// Obter Valores
							$valores = $this->obter_campos_valores($prospect);
						
							$this->db_cliente->insert($this->mapeamento['tabelas']['prospects'], $valores);
						}
					}
					catch(Exception $e)
					{
						$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($prospect)); 
					}
				
				}
				else
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, 'Prospect ' . $prospect->nome . ' - ' . $prospect->cgc . ' j� existe.', json_encode($prospect)); 
				}
				

			}
			
			$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
			
			if($dados_erros)
			{
				foreach($dados_erros as $dado_erro)
				{
					$dados_json = json_decode($dado_erro->dados); 
					$codigos_erro[] = $dados_json->codigo;
					$nome_erro[] = $dado_erro->mensagem;
				}
				
				$erros['erro'] = $codigos_erro;
				$erros['erro_descricao'] = $nome_erro;
				
				return $erros;
			}
			else
			{
				return array('sucesso' => 'ok');
			}
		}
		
	}
	
	
	/**
	* Met�do:		obter_campos_valores
	* 
	* Descri��o:	Fun��o Utilizada para retornar os campo com valores
	* 
	* Data:			21/09/2012
	* Modifica��o:	17/09/2013
	* 
	* @access		public
	* @param		array 		$prospect				- Dados dos Prospects 
	* @param		boleano 	$gerar_recno			- gerar recno? TRUE/FALSE
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function obter_campos_valores($prospect, $gerar_recno = TRUE)
	{
		foreach($prospect as $indice => $valor)
		{
		
			if($gerar_recno)
			{
				if($indice == 'codigo') // gerar Recno
				{
					$valor = $this->gerar_recno();
				}
			}
	
			if(!empty($this->mapeamento['campos']['prospects'][$indice]))
			{
				
				if(empty($valor)) // N�o podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if(strtoupper($valor) == 'UNDEFINED') // N�o podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if(strtoupper($valor) == 'NULL') // N�o podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if($indice == 'time_emissao')
				{
					$valor = (int) $valor;
				}
				
				if($indice == 'pais')
				{
					$valor = (int) '105';
				}
				
				if($indice == 'status')
				{
					if($valor == ' ')
					{
						$valor = 'A';
					}
				}
				
				
				$valores[$this->mapeamento['campos']['prospects'][$indice]] = $valor;
				
				// Valores FIXOS
				//$valores[$this->mapeamento['campos']['prospects']['status']] = 'A';
			}

		}
		
		return $valores;
	}
	
	/**
	* Met�do:		verificar_cgc
	* 
	* Descri��o:	Fun��o Utilizada para verificar se existe um prospect com o mesmo c�digo
	* 
	* Data:			06/03/2013
	* Modifica��o:	06/03/2013
	* 
	* @access		public
	* @param		array 		$cgc				- CGC do prospects
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function verificar_cgc($cgc)
	{
		$dados = NULL;
		
		if($cgc)
		{
			$dados = $this->db_cliente
				->select($this->mapeamento['campos']['prospects']['cgc'] . ' AS cgc')
				->from($this->mapeamento['tabelas']['prospects'])
				->where($this->mapeamento['campos']['prospects']['cgc'], $cgc)
				->where($this->mapeamento['campos']['prospects']['delecao'] . ' !=', '*')
				->get()->row_array();
		}
		
		if(isset($dados['cgc']))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function gerar_recno()
	{
	
		$dados = $this->db_cliente
			->select('MAX(' . $this->mapeamento['campos']['prospects']['codigo'] . ')+1 AS codigo')
			->from($this->mapeamento['tabelas']['prospects'])
			->get()->row_array();
			
	
		if($dados['codigo'])
		{
			return $dados['codigo'];
		}
		else
		{
			return 1;
		}
	
	}
	
	

}