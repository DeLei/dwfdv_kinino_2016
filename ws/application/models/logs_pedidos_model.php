<?php

class Logs_pedidos_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }

	function salvar_logs()
	{
		$dados = array(
			'timestamp' => time(),
			'id_usuario' => $this->input->post('id_usuario'),
			'codigo_representante' => $this->input->post('codigo_representante'),
			'versao' => $this->input->post('versao'),
			'pedido' => $this->input->post('log')
		);
		
		if($this->db->insert('logs_pedidos', $dados)) {
			return array('sucesso' => 'ok');
		} else {
			return array('erro' => 'Não foi possível enviar o arquivo de log.');
		}
	}
}