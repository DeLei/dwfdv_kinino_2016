<?php

class Regra_desconto_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    
	/**
	* Met�do:		exportar_regra_desconto
	* 
	* Descri��o:	Fun��o Utilizada para retornar dados da Regras de Desconto
	* 
	* Data:			16/10/2012
	* Modifica��o:	16/10/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function exportar_regra_desconto($id = NULL, $pacote = NULL)
	{
		$parametros_consulta['id'] 					 = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'ACO_CODREG', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			16/10/2012
	* Modifica��o:	16/10/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 			= $dados['id'];
		$codigo_empresa	= $dados['codigo_empresa'];
	
		// Campos para o SELECT
		$select = select_all($this->_db_cliente['tabelas']['regra_desconto'], $this->_db_cliente['campos']['regra_desconto'], NULL, FALSE, 'filial');
		
		$select += formatar_euf($this->_db_cliente['tabelas']['regra_desconto'], $this->_db_cliente['campos']['regra_desconto']['filial'], $codigo_empresa);
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['regra_desconto']['codigo'] . ' >', $id);
		}
	
		$this->db_cliente->where($this->_db_cliente['campos']['regra_desconto']['delecao'] . ' !=', '*');
		
	
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['regra_desconto']);
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id)
	{	
	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_empresa']		 = NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	

}