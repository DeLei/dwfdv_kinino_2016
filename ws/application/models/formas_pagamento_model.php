<?php

class Formas_pagamento_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    	
	
	function exportar_formas_pagamento($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'E4_CODIGO', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
	
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];
	
		// Campos para o SELECT
		// * Retornar todos os campos
		$select = select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], NULL, FALSE, 'filial');
		
		$select += formatar_euf($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], $codigo_empresa);
		
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['codigo'] . ' >', $id);
		}
				
		$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['delecao'] . ' !=', '*');
		$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['palm'], '1');
		$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['filial'], '01');
	
	
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['formas_pagamento']);
		
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	

}