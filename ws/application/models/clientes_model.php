<?php

class Clientes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	function exportar_clientes($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'A1_COD', $parametros_consulta);
		
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{

		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
	
	
	
		//-----------------------------------------------------------------------------------
		//SubSQL
		$this->db_cliente
			->select_max('C5_NUM')
			->from($this->_db_cliente['tabelas']['pedidos'])
			->where($this->_db_cliente['campos']['pedidos']['codigo_cliente'], $this->_db_cliente['campos']['clientes']['codigo'], FALSE)
			->where($this->_db_cliente['campos']['pedidos']['loja_cliente'], $this->_db_cliente['campos']['clientes']['loja'], FALSE)
			->where($this->_db_cliente['campos']['pedidos']['delecao'] . ' !=', '*');
		$subSql_ultimo_pedido = $this->db_cliente->sub_sql('ultimo_pedido');
		
		//-----------------------------------------------------------------------------------
		
		//-----------------------------------------------------------------------------------
		//SubSQL - Data do T�tulo Vencido mais Antigo
		$this->db_cliente
			->select("ISNULL(MIN(" . $this->_db_cliente['campos']['titulos']['data_vencimento'] . "), '')")
			->from($this->_db_cliente['tabelas']['titulos'])
			->where($this->_db_cliente['campos']['titulos']['codigo_cliente'], $this->_db_cliente['campos']['clientes']['codigo'], FALSE)
			->where($this->_db_cliente['campos']['titulos']['loja_cliente'], $this->_db_cliente['campos']['clientes']['loja'], FALSE)
			->where($this->_db_cliente['campos']['titulos']['data_vencimento'] . ' <', date('Ymd'))
			//T�tulo parcialmente pagos
			->where('( '.$this->_db_cliente['campos']['titulos']['data_baixa'] ." = '' OR (".$this->_db_cliente['campos']['titulos']['data_baixa'] ." != '' AND ".$this->_db_cliente['campos']['titulos']['saldo']. " > 0  ))")
			
			->where_not_in($this->_db_cliente['campos']['titulos']['tipo'], array('CH', 'NCC')) // CHEQUE
			->where($this->_db_cliente['campos']['titulos']['delecao'] . ' !=', '*');
		$subSql_data_titulo_vencido = $this->db_cliente->sub_sql('data_titulo_vencido');
		//-----------------------------------------------------------------------------------
	
		// Campos para o SELECT
		// * Retornar todos os campos
		$select = select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], NULL, FALSE, 'filial');
		
		
		
		
		// * retornar o ultimo pedido
		$select[] = $subSql_ultimo_pedido;
		// *
		
		// * retornar a Data do T�tulo Vencido mais Antigo
		$select[] = $subSql_data_titulo_vencido;
		// *
		
		// * retornar o ultimo pedido
		
	 	$select[] = "(SELECT SUM(" . $this->_db_cliente['campos']['titulos']['saldo'] . ") FROM " . $this->_db_cliente['tabelas']['titulos'] . " WHERE " . $this->_db_cliente['campos']['titulos']['codigo_cliente'] . " = " . $this->_db_cliente['campos']['clientes']['codigo'] . " AND " . $this->_db_cliente['campos']['titulos']['loja_cliente'] . " = " . $this->_db_cliente['campos']['clientes']['loja'] . " AND " . $this->_db_cliente['campos']['titulos']['delecao'] . " != '*' AND " . $this->_db_cliente['campos']['titulos']['data_vencimento'] . " < '" . date('Ymd') . "' AND ( ".$this->_db_cliente['campos']['titulos']['data_baixa'] ." = '' OR (".$this->_db_cliente['campos']['titulos']['data_baixa'] ." != '' AND ".$this->_db_cliente['campos']['titulos']['saldo']. " > 0  )) AND " . $this->_db_cliente['campos']['titulos']['delecao'] . " != '*') AS total_titulos_ventidos";
		// *
		
		$select += formatar_euf($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], $codigo_empresa);
		
		
		// JOIN
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['verba_introdutoria'],
			euf(
				$this->_db_cliente['tabelas']['verba_introdutoria'], $this->_db_cliente['campos']['verba_introdutoria']['filial'], 
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial']
			) .
			$this->_db_cliente['campos']['verba_introdutoria']['codigo_cliente'] . " = " . $this->_db_cliente['campos']['clientes']['codigo'] . " AND " .
			$this->_db_cliente['campos']['verba_introdutoria']['status'] . " = 'L' AND " .
			$this->_db_cliente['campos']['verba_introdutoria']['loja_cliente'] . " = " . $this->_db_cliente['campos']['clientes']['loja'] . " AND " .
			$this->_db_cliente['tabelas']['verba_introdutoria'] . "." . $this->_db_cliente['campos']['verba_introdutoria']['delecao'] . " != '*'" . " AND " .
			$this->_db_cliente['tabelas']['verba_introdutoria'] . "." . $this->_db_cliente['campos']['verba_introdutoria']['filial'] . " = '01'"
		, 'left');
		
		$select[] = 'ISNULL(' . $this->_db_cliente['campos']['verba_introdutoria']['saldo'] . ' , 0) AS verba_introdutoria';
		
		
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['recno'] . ' >', $id);
		}

		$this->db_cliente->where($this->_db_cliente['campos']['clientes']['codigo_representante'], $codigo_representante);
		$this->db_cliente->where($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . ' !=', '*');
		$this->db_cliente->where($this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['situacao'], '2');
	
	
		// Consulta
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['clientes']);

		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
}