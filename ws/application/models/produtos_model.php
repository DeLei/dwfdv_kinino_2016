<?php

class Produtos_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_produtos($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] 					 = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'RTRIM(LTRIM(SB1010.B1_COD))', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_empresa 		= NULL;
		
	
		//Obter Tabelas Pre�os
		$tabelas_preco = $this->obter_tabelas_preco();
	
		$selectFields = array(
			'RTRIM(LTRIM(SB1010.B1_COD)) AS produto_codigo', 
			'RTRIM(LTRIM(SB1010.B1_FILIAL)) AS produto_filial', 
			'RTRIM(LTRIM(SB1010.B1_COD)) AS produto_codigo_real', 
			'RTRIM(LTRIM(SB1010.B1_GRUPO)) AS produto_codigo_grupo', 
			'RTRIM(LTRIM(SB1010.B1_DESC)) AS produto_descricao', 
			'RTRIM(LTRIM(SB1010.B1_QE)) AS produto_quantidade_embalagem', 
			'RTRIM(LTRIM(SB1010.B1_UM)) AS produto_unidade_medida', 
			'RTRIM(LTRIM(SB1010.B1_CONV)) AS produto_converter', 
			'RTRIM(LTRIM(SB1010.B1_TIPCONV)) AS produto_tipo_converter', 
			'RTRIM(LTRIM(SB1010.B1_IPI)) AS produto_ipi', 
			'RTRIM(LTRIM(SB1010.B1_PICM)) AS produto_icms', 
			'RTRIM(LTRIM(SB1010.B1_COFINS)) AS produto_cofins', 
			'RTRIM(LTRIM(SB1010.B1_PIS)) AS produto_pis', 
			'RTRIM(LTRIM(SB1010.B1_MSBLQL)) AS produto_inativo', 
			'RTRIM(LTRIM(SB1010.B1_PESO)) AS produto_peso', 
			'RTRIM(LTRIM(SB1010.B1_LOCPAD)) AS produto_locpad', 
			'RTRIM(LTRIM(SB1010.B1_TS)) AS produto_tipo_entrada_saida', 
			'RTRIM(LTRIM(SB1010.B1_SEGUM)) AS produto_segunda_unidade_medida', 
			'RTRIM(LTRIM(SB1010.B1_PESBRU)) AS produto_peso_total', 
			'RTRIM(LTRIM(SB1010.B1_CODPALM)) AS produto_codigo_palm', 
			'RTRIM(LTRIM(SB1010.B1_KLIM)) AS produto_limite_desconto', 
			'RTRIM(LTRIM(SB1010.B1_KACMAX)) AS produto_limite_acrescimo', 
			'RTRIM(LTRIM(SB1010.B1_GRTRIB)) AS produto_grupo_tributacao', 
			'RTRIM(LTRIM(SB2010.B2_FILIAL)) AS etq_filial', 
			'RTRIM(LTRIM(SB2010.B2_COD)) AS etq_codigo', 
			'\'00\' AS etq_local',
			'SUM(SB2010.B2_QATU) AS etq_quantidade_atual', 
			'SUM(SB2010.B2_QPEDVEN) AS etq_quantidade_pedidos_venda', 
			'SUM(SB2010.B2_QEMP) AS etq_quantidade_empenhada',
			'SUM(SB2010.B2_RESERVA) AS etq_quantidade_reservada', 
			'RTRIM(LTRIM(DA1010.DA1_CODTAB)) AS ptp_codigo_tabela_precos', 
			'RTRIM(LTRIM(DA1010.DA1_CODPRO)) AS ptp_codigo_produto', 
			'RTRIM(LTRIM(DA1010.DA1_PRCVEN)) AS ptp_preco', 
			'RTRIM(LTRIM(DA1010.DA1_FILIAL)) AS ptp_filial', 
			'RTRIM(LTRIM(DA0010.DA0_FILIAL)) AS tb_filial', 
			'RTRIM(LTRIM(DA0010.DA0_CODTAB)) AS tb_codigo', 
			'RTRIM(LTRIM(DA0010.DA0_DESCRI)) AS tb_descricao', 
			'RTRIM(LTRIM(DA0010.DA0_CONDPG)) AS tb_condicao_pagamento', 
			'RTRIM(LTRIM(DA0010.DA0_DATDE)) AS tb_vigencia_inicio', 
			'RTRIM(LTRIM(DA0010.DA0_DATATE)) AS tb_vigencia_final', 
			'RTRIM(LTRIM(DA0010.DA0_ATIVO)) AS tb_ativo', 
			'SUM(B2_QATU - (B2_QPEDVEN + B2_QEMP + B2_RESERVA)) AS quantidade_disponivel_estoque', 
			'SUBSTRING(B1_FILIAL, 0, 0) AS empresa', 
			'SUBSTRING(B1_FILIAL, 0, 0) AS unidade', 
			'SUBSTRING(B1_FILIAL, 0, 0) AS filial'
		);
		// * Retornar todos os campos
		$select_produto = select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_estoque = select_all($this->_db_cliente['tabelas']['produtos_estoque'], $this->_db_cliente['campos']['produtos_estoque'], 'etq');
		$select_produto_tabela_preco = select_all($this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos'], 'ptp');
		$select_tabela_preco = select_all($this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos'], 'tb');
		/*
		$select = array_merge(
				$select_produto, 
				$select_estoque,
				$select_produto_tabela_preco,
				$select_tabela_preco
			);

		
		// * Obter quantidade disponivel
		$select[] = '(' . $this->_db_cliente['campos']['produtos_estoque']['quantidade_atual'] . ' - (' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_pedidos_venda'] . ' + ' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_empenhada'] . ' + ' . 
			$this->_db_cliente['campos']['produtos_estoque']['quantidade_reservada'] . ')) AS quantidade_disponivel_estoque';
		//*
		
		$select += formatar_euf($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], $codigo_empresa);
		*/
		//Join
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_estoque'], 
			$this->_db_cliente['campos']['produtos_estoque']['filial'] . ' = \'01\' AND ' .
			$this->_db_cliente['campos']['produtos_estoque']['codigo'] . ' = ' . $this->_db_cliente['campos']['produtos']['codigo'] . ' AND ' .
			$this->_db_cliente['tabelas']['produtos_estoque'] . '.' . $this->_db_cliente['campos']['produtos_estoque']['delecao'] . ' = \'\''
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos_tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial']
			) .
			$this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_produto'] . ' = ' . $this->_db_cliente['campos']['produtos']['codigo'] . ' AND ' .
			$this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' = \'\''
		);
		
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['tabelas_precos'], 
			euf(
				$this->_db_cliente['tabelas']['tabelas_precos'], $this->_db_cliente['campos']['tabelas_precos']['filial'], 
				$this->_db_cliente['tabelas']['produtos_tabelas_precos'], $this->_db_cliente['campos']['produtos_tabelas_precos']['filial']
			) .
			$this->_db_cliente['campos']['tabelas_precos']['codigo'] . ' = ' . $this->_db_cliente['campos']['produtos_tabelas_precos']['codigo_tabela_precos'] . ' AND ' . $this->_db_cliente['campos']['tabelas_precos']['ativo'] . " = '1'" . ' AND ' .
			$this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' = \'\''
		);
	
		
		// Condi��es do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['recno'] . ' >', $id);
		}

		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['inativo'] . ' !=', '1');
		$this->db_cliente->where($this->_db_cliente['tabelas']['produtos'] . '.' . $this->_db_cliente['campos']['produtos']['delecao'] . ' !=', '*');
		
		//$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_estoque'] . '.' . $this->_db_cliente['campos']['produtos_estoque']['delecao'] . ' !=', '*');
		
		$this->db_cliente->where_in($this->_db_cliente['tabelas']['produtos_estoque'] . '.' . $this->_db_cliente['campos']['produtos_estoque']['local'], array('02', '04'));
		
		//$this->db_cliente->where($this->_db_cliente['tabelas']['produtos_tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' !=', '*');
		
		//$this->db_cliente->where($this->_db_cliente['tabelas']['tabelas_precos'] . '.' . $this->_db_cliente['campos']['produtos_tabelas_precos']['delecao'] . ' !=', '*');
		
		$this->db_cliente->where_in($this->_db_cliente['campos']['tabelas_precos']['codigo'], $tabelas_preco);
	
		$this->db_cliente->where($this->_db_cliente['campos']['produtos_estoque']['filial'], '01');
		
		$this->db_cliente->limit('450');
		// Consulta
		$this->db_cliente->select($selectFields)->from($this->_db_cliente['tabelas']['produtos']);
		
		$this->db_cliente->group_by(array(
			'RTRIM(LTRIM(SB1010.B1_COD))', 
			'RTRIM(LTRIM(SB1010.B1_FILIAL))', 
			'RTRIM(LTRIM(SB1010.B1_COD))', 
			'RTRIM(LTRIM(SB1010.B1_GRUPO))', 
			'RTRIM(LTRIM(SB1010.B1_DESC))', 
			'RTRIM(LTRIM(SB1010.B1_QE))', 
			'RTRIM(LTRIM(SB1010.B1_UM))', 
			'RTRIM(LTRIM(SB1010.B1_CONV))', 
			'RTRIM(LTRIM(SB1010.B1_TIPCONV))', 
			'RTRIM(LTRIM(SB1010.B1_IPI))', 
			'RTRIM(LTRIM(SB1010.B1_PICM))', 
			'RTRIM(LTRIM(SB1010.B1_COFINS))', 
			'RTRIM(LTRIM(SB1010.B1_PIS))', 
			'RTRIM(LTRIM(SB1010.B1_MSBLQL))', 
			'RTRIM(LTRIM(SB1010.B1_PESO))', 
			'RTRIM(LTRIM(SB1010.B1_LOCPAD))', 
			'RTRIM(LTRIM(SB1010.B1_TS))', 
			'RTRIM(LTRIM(SB1010.B1_SEGUM))', 
			'RTRIM(LTRIM(SB1010.B1_PESBRU))', 
			'RTRIM(LTRIM(SB1010.B1_CODPALM))', 
			'RTRIM(LTRIM(SB1010.B1_KLIM))', 
			'RTRIM(LTRIM(SB1010.B1_KACMAX))', 
			'RTRIM(LTRIM(SB1010.B1_GRTRIB))', 
			'RTRIM(LTRIM(SB2010.B2_FILIAL))', 
			'RTRIM(LTRIM(SB2010.B2_COD))', 
			'RTRIM(LTRIM(DA1010.DA1_CODTAB))', 
			'RTRIM(LTRIM(DA1010.DA1_CODPRO))', 
			'RTRIM(LTRIM(DA1010.DA1_PRCVEN))', 
			'RTRIM(LTRIM(DA1010.DA1_FILIAL))', 
			'RTRIM(LTRIM(DA0010.DA0_FILIAL))', 
			'RTRIM(LTRIM(DA0010.DA0_CODTAB))', 
			'RTRIM(LTRIM(DA0010.DA0_DESCRI))', 
			'RTRIM(LTRIM(DA0010.DA0_CONDPG))', 
			'RTRIM(LTRIM(DA0010.DA0_DATDE))', 
			'RTRIM(LTRIM(DA0010.DA0_DATATE))', 
			'RTRIM(LTRIM(DA0010.DA0_ATIVO))', 
			'SUBSTRING(B1_FILIAL, 0, 0)', 
			'SUBSTRING(B1_FILIAL, 0, 0)', 
			'SUBSTRING(B1_FILIAL, 0, 0)'
		));
		
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de clientes
	* 
	* Data:			11/09/2012
	* Modifica��o:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	/**
	* Met�do:		obter_tabelas_preco
	* 
	* Descri��o:	Fun��o Utilizada para retornar os codigos da tabelas de pre�o
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function obter_tabelas_preco()
	{
		// Load Tabelas de Pre�o
		$this->load->model('tabelas_preco_model');
	
		$this->tabelas_preco_model->_db_cliente = $this->_db_cliente;
	
		$parametros_consulta['id'] = NULL;
		$parametros_consulta['codigo_empresa'] = NULL;
	
		$tabelas_preco = $this->tabelas_preco_model->obter_dados($parametros_consulta);
		

		
		foreach($tabelas_preco as $tp)
		{
			$codigos_tabelas_preco[] = $tp['codigo'];
		}
		
		return $codigos_tabelas_preco;

	}
	

}