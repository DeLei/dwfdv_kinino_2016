<?php

class Pedidos_pendentes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
	
	
	/**
	* Met�do:		exportar_pedidos
	* 
	* Descri��o:	Fun��o Utilizada para pegar retornar dados dos Pedidos
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function exportar_pedidos($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
	
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['campos']['pedidos_dw']['id_pedido'], $parametros_consulta, TRUE);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modifica��o:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
		
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw'], 'pedido');
		$select_cliente 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_produto 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		
		$select = array_merge(
			$select_pedido, 
			$select_cliente,
			$select_produto,
			$select_formas_pagamento,
			$select_transportadoras
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = N�o Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		$select[] = "'0' as converter_pedido_orcamento";
		$select[] = "'0' as item_troca";// Marcar itens de troca
		$select[] = "'' as pedido_segunda_quantidade";
		$select[] = "'' as pedido_segunda_unidade_medida";
		$select[] = "'' as pedido_segundo_preco_venda";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial'], $codigo_empresa);

		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ' >', $id);
		}
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $codigo_representante);
		
		//$this->db_cliente->where_in($this->_db_cliente['campos']['pedidos_dw']['status'], array('A', 'R'));
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['status'], 'L'); // Customiza��o criada para retornar apenas pedidos "Liberados"
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo_venda'] . ' !=', '*'); // Buscar Pedidos (* � or�amento)
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['delecao'] . ' !=', '*');
		
		//-----------------------------------------------
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'] . ' AND ' .
			$this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'] . ' AND ' .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['pedidos_dw']);
		
	}
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['codigo_empresa']		 = NULL;
	
		return retornar_total($this, $parametros_consulta, TRUE);
	}
	
	
	
	//--------------
	//------------------
	//--------------------
	//------------------
	//--------------
	/**
	* Met�do:		importar
	* 
	* Descri��o:	Fun��o Utilizada para inserir pedidos no banco, e inserir LOGS
	* 
	* Data:			08/09/2012
	* Modifica��o:	08/09/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW for�a de vendas
	* @param		string 		$id_usuario					- ID do usu�rio
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pedidos_dw', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$pedidos = json_decode($dados);
			
			foreach($pedidos as $pedido)
			{
				
				//Item de Troca
				if($pedido->item_troca == 1)
				{
					$this->importar_itens_troca($id_sincronizacao, $pedido);
				}
				else
				{
					try{
					
						if($this->validar_pedido($pedido->pedido_id_pedido, $pedido->pedido_codigo_produto))
						{
							$pedido->editado = 1;
						}
						
						// Se editado == 1 - Editar o Pedidos, senao, inserir
						if($pedido->editado == 1)
						{
							$valores = $this->obter_campos_valores($pedido, FALSE);
						
							$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $pedido->pedido_id_pedido);
							$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['codigo_produto'], $pedido->pedido_codigo_produto);
							$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['status'] . ' !=', 'I');
							$this->db_cliente->update($this->mapeamento['tabelas']['pedidos_dw'], $valores);
							
						}else{
							$valores = $this->obter_campos_valores($pedido);
						
							$this->db_cliente->insert($this->mapeamento['tabelas']['pedidos_dw'], $valores);
						}
					
					}
					catch(Exception $e)
					{
						$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pedido)); 
					}	
				}
			}
		}
		
		// Marcando o or�amento convertido para pedido como deletado
		if($pedido->converter_pedido_orcamento)
		{
			try{
				$this->db_cliente->update($this->mapeamento['tabelas']['pedidos_dw'],
					array($this->mapeamento['campos']['pedidos_dw']['delecao'] => '*'),
					array($this->mapeamento['campos']['pedidos_dw']['id_pedido'] => $pedido->pedido_id_pedido,
						$this->mapeamento['campos']['pedidos_dw']['filial'] => $pedido->pedido_filial,
						$this->mapeamento['campos']['pedidos_dw']['tipo_venda'] => '*'));
						
				$this->db_cliente->update($this->mapeamento['tabelas']['itens_troca'],
					array($this->mapeamento['campos']['itens_troca']['delecao'] => '*'),
					array($this->mapeamento['campos']['itens_troca']['id_pedido'] => $pedido->pedido_id_pedido,
						$this->mapeamento['campos']['itens_troca']['filial'] => $pedido->pedido_filial,
						$this->mapeamento['campos']['itens_troca']['tipo_venda'] => '*'));
				
				/*
				$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['filial'], $pedido->pedido_filial);
				$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $pedido->pedido_id_pedido);
				$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['tipo_venda'], '*');
				$this->db_cliente->delete($this->mapeamento['tabelas']['pedidos_dw']);
				*/
			}
			catch(Exception $e)
			{
				$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pedido)); 
			}	
		}
		
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
			
		if($dados_erros)
		{
			$codigos_erro = array();
		
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->pedido_id_pedido, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->pedido_id_pedido;
					$nome_erro[] = $dados_json->pedido_id_pedido . ' - ' . $dados_json->cliente_nome;
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');
		}
		
	}
	
	
	function validar_pedido($id_pedido, $codigo_produto, $troca = FALSE)
	{
		$tabela = 'pedidos_dw';
		if($troca)
		{
			$tabela = 'itens_troca';
		}
		
		$pedido = $this->db_cliente->select($this->mapeamento['campos'][$tabela]['id_pedido'])
									->from($this->mapeamento['tabelas'][$tabela])
									->where(array(
										$this->mapeamento['campos'][$tabela]['id_pedido']		=> $id_pedido,
										$this->mapeamento['campos'][$tabela]['codigo_produto']	=> $codigo_produto
									))
									->limit(1)->get()->result();
		
		return $pedido;
	}
	
	/**
	* Met�do:		consulta_troca
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que sera executado para retornar dados referentes aos itens de troca
	* 
	* Data:			14/01/2014
	* Modifica��o:	14/01/2014
	* 
	* @access		public
	* @param		string 		$dados
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta_troca($dados = NULL)
	{
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca'], 'pedido');
		$select_cliente 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_produto 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		
		$select = array_merge(
			$select_pedido, 
			$select_cliente,
			$select_produto,
			$select_formas_pagamento,
			$select_transportadoras
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = N�o Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		$select[] = "'0' as converter_pedido_orcamento";
		$select[] = "'1' as item_troca";// Marcar itens de troca
		$select[] = "'' as pedido_analitico_perc";
		$select[] = "'' as pedido_analitico_valor";
		
		$select += formatar_euf($this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial'], $codigo_empresa);

		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['itens_troca']['id_pedido'] . ' >', $id);
		}
		
		$this->db_cliente->where($this->_db_cliente['campos']['itens_troca']['codigo_representante'], $codigo_representante);
		
		//$this->db_cliente->where_in($this->_db_cliente['campos']['pedidos_dw']['status'], array('L'));
		
		$this->db_cliente->where($this->_db_cliente['campos']['itens_troca']['status'], 'L'); // Customiza��o criada para retornar apenas pedidos "Liberados"
		
		$this->db_cliente->where($this->_db_cliente['campos']['itens_troca']['tipo_venda'] . ' !=', '*'); // Buscar Pedidos (* � or�amento)
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['itens_troca'] . '.' . $this->_db_cliente['campos']['itens_troca']['delecao'] . ' !=', '*');
		
		//-----------------------------------------------
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . " != '*'". ' AND ' .
			" ((".$this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['codigo_cliente'] . ' AND ' .
			$this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['loja_cliente'] . ') OR ' .
			$this->_db_cliente['campos']['clientes']['cpf'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['id_prospects']. ' AND '.$this->_db_cliente['campos']['itens_troca']['codigo_cliente'].' IS NULL)'			
		, 'left');
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['prospects'], 
			euf(
				$this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['cgc'] . ' = ' . $this->_db_cliente['campos']['itens_troca']['id_prospects'] . ' AND ' .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['itens_troca']['codigo_produto'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['campos']['itens_troca']['condicao_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " != '*'"
		, 'left');
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['itens_troca'], $this->_db_cliente['campos']['itens_troca']['filial']
			) .
			$this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['campos']['itens_troca']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " != '*'"
		, 'left');
		
		$this->db_cliente->select($select)->from($this->_db_cliente['tabelas']['itens_troca']);
	}
	
	/**
	*	obter_ultimo_recno(nome_da_tabela)
	*
	*		Obtem o �ltimo R_E_C_N_O_ da tabela informada
	**/
	function obter_ultimo_recno($tabela){
		$dados = $this->db_cliente
						->select('MAX(' . $this->mapeamento['campos'][$tabela]['chave'] . ') AS chave')
						->from($this->mapeamento['tabelas'][$tabela])
						->get()->row()->chave;
		
		if($dados) {
			return $dados;
		} else {
			return 0;
		}
	}
	
	/**
	*	salvar_pedido( array('pedidos_dw' => array(valores), 'itens_troca' => array(valores) ) )
	*
	*		Salva o pedido na base
	**/
	function salvar_pedido($valores, $arquivo, $id_usuario, $codigo_representante, $conversao_orcamento = FALSE)
	{
		//Carrega o model
		$this->load->model('sincronizacoes_model');
		
		//Grava o log de sincroniza��o
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pedidos_dw', json_encode($valores), $id_usuario, $codigo_representante);
		
		try
		{
			if(!$this->verificar_pedido_existente($valores, $conversao_orcamento)) {
				$this->db_cliente->trans_start(); //Inicia a transa��o com o BD
					
					//Remove o or�amento para salvar o novo pedido
					if($conversao_orcamento) {
						$codigo_pedido = $valores['pedidos_dw'][0][$this->mapeamento['campos']['pedidos_dw']['id_pedido']];
						$filial = $valores['pedidos_dw'][0][$this->mapeamento['campos']['pedidos_dw']['filial']];
						
						$this->db_cliente->update($this->mapeamento['tabelas']['pedidos_dw'],
							array($this->mapeamento['campos']['pedidos_dw']['delecao'] => '*'),
							array($this->mapeamento['campos']['pedidos_dw']['id_pedido'] => $codigo_pedido,
								$this->mapeamento['campos']['pedidos_dw']['filial'] => $filial,
								$this->mapeamento['campos']['pedidos_dw']['tipo_venda'] => '*'));
								
						$this->db_cliente->update($this->mapeamento['tabelas']['itens_troca'],
							array($this->mapeamento['campos']['itens_troca']['delecao'] => '*'),
							array($this->mapeamento['campos']['itens_troca']['id_pedido'] => $codigo_pedido,
								$this->mapeamento['campos']['itens_troca']['filial'] => $filial,
								$this->mapeamento['campos']['itens_troca']['tipo_venda'] => '*'));
					}
					
					$this->db_cliente->insert_batch($this->mapeamento['tabelas']['pedidos_dw'], $valores['pedidos_dw']); //Insere os itens de venda
					
					if(isset($valores['itens_troca'])) //Verifica se existem itens de troca no pedido
					{
						$this->db_cliente->insert_batch($this->mapeamento['tabelas']['itens_troca'], $valores['itens_troca']); //Insere os itens de troca
					}
					
				$this->db_cliente->trans_complete(); //Fecha a transa��o
				
				//Transa��o bem sucedida
				if($this->db_cliente->trans_status())
				{
					//Move o arquivo da pasta "sincronizar" para a pasta "sincronizados"
					rename('uploads/pedidos/sincronizar/' . $arquivo, 'uploads/pedidos/sincronizados/' . $arquivo);
					
					return array('sucesso' => 'ok');
				}
			} else {
				//Reenvio de pedido, move o arquivo da pasta "sincronizar" para a pasta "sincronizados"
				rename('uploads/pedidos/sincronizar/' . $arquivo, 'uploads/pedidos/sincronizados/' . $arquivo);
			}
		}
		catch(Exception $e)
		{
			$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($valores)); 
		}
		
		//Obtem os registros de erro
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
		
		if($dados_erros)
		{
			$codigo_erro = str_replace('.json', '', $arquivo);
			
			$erros['erro'] = array($codigo_erro);
			$erros['erro_descricao'] = 'Erro ao enviar o pedido ' . $codigo_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');
		}
		
	}
	
	private function verificar_pedido_existente($valores, $conversao_orcamento = FALSE) {
		$aux = array_shift($valores);
		
		$id_pedido = '';
		if(isset($aux[0][$this->mapeamento['campos']['pedidos_dw']['id_pedido']]))
		{
			$id_pedido = $aux[0][$this->mapeamento['campos']['pedidos_dw']['id_pedido']];
		}
		else
		{
			$id_pedido = $aux[0][$this->mapeamento['campos']['itens_troca']['id_pedido']];
		}
		
		if($conversao_orcamento) {
			$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['tipo_venda'] . '!=', '*');
		}
		$pedido = $this->db_cliente->select($this->mapeamento['campos']['pedidos_dw']['id_pedido'])
									->from($this->mapeamento['tabelas']['pedidos_dw'])
									->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $id_pedido)
									->where($this->mapeamento['campos']['pedidos_dw']['delecao'] . ' !=', '*')
									->limit(1)
									->get()->num_rows();
									
		return $pedido;
	}
	
}