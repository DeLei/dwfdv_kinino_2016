<?php

class Logs_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
        
        //Carregar helper
        $this->load->helper('date');
        //Carregar agentes
        $this->load->library('user_agent');
    }

	function salvar_informacoes()
	{
		//Dados para salvar
		$dados = array(
				'usuarios_id' 			=> $this->input->server('HTTP_DW_ID_USUARIO'),
				'usuarios_codigo' 		=> $this->input->server('HTTP_DW_CODIGO_REPRESENTANTE'),
				'key' 						=> $this->input->server('HTTP_DW_KEY_APP'),
				'latitude' 				=> $this->input->server('HTTP_DW_LATITUDE'),
				'longitude' 				=> $this->input->server('HTTP_DW_LONGITUDE'),
				'macAddress'			=> $this->input->server('HTTP_DW_MACADDRESS'),
				'versao'					=> $this->input->server('HTTP_DW_VERSAO'),
				'endereco_ip'			=> $this->input->ip_address(),
				'navegador'				=> $this->agent->browser(). ' ' . $this->agent->version(),
				'navegador_string'	=> $this->agent->agent_string(),
				'so' 						=> $this->agent->platform(),
				'data_hora' 			=> now()
            );

		return $this->db->insert('usuarios_aparelhos_localizacoes', $dados); 
	}
}