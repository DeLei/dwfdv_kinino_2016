<?php

class Vendedor_condicao_pagamento_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	function exportar_vendedor_condicao_pagamento($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;		
		$parametros_consulta['id'] 						= $id;

		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, FALSE, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Met�do:		consulta
	* 
	* Descri��o:	Fun��o Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id = $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
	
		$this->db->where('vendedor', $codigo_representante);
	
		// Selecionar
		$this->db->select('id');	
		$this->db->select('condicao_pagamento');	
		$this->db->select('vendedor');	
			
		// Consulta
		$this->db->from('vendedor_condicao_pagamento');
		
		//debug_pre($this->db->_compile_select());
	}
	
	
	/**
	* Met�do:		retornar_total
	* 
	* Descri��o:	Fun��o Utilizada para retornar o n�mero total de transportadoras
	* 
	* Data:			10/09/2012
	* Modifica��o:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Solu��es Web
	* 
	*/
	function retornar_total($id, $codigo_representante = NULL)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['codigo_representante'] 	= $codigo_representante;
	
		return retornar_total($this, $parametros_consulta);
	}

}