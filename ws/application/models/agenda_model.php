<?php

class Agenda_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_agenda
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de comprimissos da agenda
	* 
	* Data:				09/09/2013
	* Modificação:	09/09/2013
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_agenda($id = NULL, $pacote = NULL, $id_usuario = NULL)
	{
		$parametros_consulta['id'] 							= $id;
		$parametros_consulta['id_usuario'] 					= $id_usuario;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar dados de comprimissos da agenda
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{	
		$id 				= $dados['id'];
		$id_usuario 	= $dados['id_usuario'];
		
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Compromissos a partir deste ID
			$this->db->where(array('id >' => $id));
		}
		
		// Condições do SQL (WHERE)
		if($id_usuario)
		{
			// Travando por usuário
			$this->db->where(array('usuarios.id' => $id_usuario));
		}
	
		// Selecionar
		$this->db->select('usuarios.status as usuario_status');
		$this->db->select('usuarios.nome as usuario_nome');
		$this->db->select('compromissos.id as id');	
		$this->db->select('compromissos.timestamp as timestamp');	
		$this->db->select('compromissos.id_usuario as id_usuario');	
		$this->db->select('compromissos.cpf as cpf');	
		$this->db->select('compromissos.status as status');	
		$this->db->select('compromissos.nome as nome');	
		$this->db->select('compromissos.titulo as titulo');	
		$this->db->select('compromissos.descricao as descricao');	
		$this->db->select('compromissos.tipo as tipo');
		$this->db->select('compromissos.codigo_tipo as codigo_tipo');
		$this->db->select('compromissos.id_usuario_cad as id_usuario_cad');
		$this->db->select('compromissos.id_representante as id_representante');
		$this->db->select('compromissos.detalhes as detalhes');
		$this->db->select('compromissos.inicio_timestamp as inicio_timestamp');
		$this->db->select('compromissos.final_timestamp as final_timestamp');
		$this->db->select('compromissos.class as class');
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$this->db->select("'1' as exportado", false);
		$this->db->select("'0' as editado", false);
		$this->db->select("'0' as remover", false);
		$this->db->select("'0' as erro", false);
		
		//Join
		$this->db->join('usuarios', 'usuarios.id = compromissos.id_usuario_cad');
			
		// Consulta
		$this->db->from('compromissos');
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $id_usuario)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['id_usuario'] = $id_usuario;
	
		return retornar_total($this, $parametros_consulta);
	}

}