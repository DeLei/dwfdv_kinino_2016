$(document).ready(function() {
	
	$('#atualizar_estoque').click(function(e) {
			e.preventDefault();
	
			confirmar('Deseja atualizar o estoque dos produtos?', 
				function () {
					$(this).dialog('close');
					
					//------------------
					//---------------------------------
			
					location.href = "sincronizar.html?produtos=sincronizar";
					
					//---------------------------------
					//------------------
					
					$("#confirmar_dialog").remove();
				});
		
	});
	
	
	
	
	// obter produtos
	obter_produtos(sessionStorage['produtos_pagina_atual'], sessionStorage['produtos_ordem_atual']);
	
	// ordenação
	
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC')
		{
			obter_produtos(sessionStorage['produtos_pagina_atual'], $(this).data('campo') + ' DESC');
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
		}
		else
		{
			obter_produtos(sessionStorage['produtos_pagina_atual'], $(this).data('campo') + ' ASC');
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['produtos_' + name] != 'undefined' ? sessionStorage['produtos_' + name] : '');
	});
	
	// obter filiais
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT * FROM filiais', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=produto_filial]').append('<option value="">Todos</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=produto_filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
				}
				
				selecionar_filial_padrao('produto_filial');
				
			}
		);
	});
	
	// obter tabelas
	db.transaction(function(x) {	
		x.executeSql(
			'SELECT * FROM tabelas_preco', [], function(x, dados) {
				
				var total = dados.rows.length;
				
				$('select[name=tb_codigo]').append('<option value="">Todos</option>');
				
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=tb_codigo]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
				
			}
		);
	});
	
	$('#filtrar').click(function() {
		sessionStorage['produtos_pagina_atual'] = 1;
		sessionStorage['produtos_unidade'] = $('#unidade').val();
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			sessionStorage['produtos_' + name] = $(this).val();
		});
		
		obter_produtos(sessionStorage['produtos_pagina_atual'], sessionStorage['produtos_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['produtos_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['produtos_' + name] = '';
		});
		
		obter_produtos(sessionStorage['produtos_pagina_atual'], sessionStorage['produtos_ordem_atual']);
	});
});

function obter_produtos(pagina, ordem)
{
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'produto_descricao ASC';
	
	// setar pagina e ordem atual
	sessionStorage['produtos_pagina_atual'] = pagina;
	sessionStorage['produtos_ordem_atual'] = ordem;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['produtos_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC')
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'ASC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▲');
	}
	else
	{
		$('a[data-campo="' + ordem_atual[0] + '"]').data('ordem', 'DESC');
		
		$('a[data-campo="' + ordem_atual[0] + '"]').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['produtos_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	filial = sessionStorage['produtos_produto_filial'];
	codigo = sessionStorage['produtos_produto_codigo'];
	descricao = sessionStorage['produtos_produto_descricao'];
	tabela_de_precos = sessionStorage['produtos_tb_codigo'];
	
	var wheres = '';
	
	if (filial && filial != 'undefined')
	{
		//wheres += " AND produto_filial = '" + filial + "'";
		wheres += " AND (produto_filial = '" + filial + "' OR produto_filial = '')";
	}
	
	if (tabela_de_precos && tabela_de_precos != 'undefined')
	{
		wheres += " AND tb_codigo = '" + tabela_de_precos + "'";
	}
	
	if (codigo && codigo != 'undefined')
	{
		wheres += " AND produto_codigo LIKE '%" + codigo + "%'";
	}
	
	if (descricao && descricao != 'undefined')
	{
		wheres += " AND produto_descricao LIKE '%" + descricao + "%'";
	}
	
	if (info.empresa)
	{
		//wheres += " AND empresa = '" + info.empresa + "'";
	}
	
	

	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM produtos WHERE produto_codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['produtos_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
			
				if (dados.rows.length)
				{
				
					
					$('table tbody').empty();
			
					for (i = 0; i < dados.rows.length; i++)
					{
						var item = dados.rows.item(i);
						
						var itens = [];
						
						itens.push(item.produto_filial);
						itens.push(item.produto_codigo);
						itens.push(item.produto_descricao);
						itens.push(item.tb_codigo);
						itens.push(number_format(item.quantidade_disponivel_estoque, 0, ',', '.'));
						itens.push(number_format(item.ptp_preco, 3, ',', '.'));
						itens.push(number_format(item.produto_ipi, 3, ',', '.'));
						
						var html = concatenar_html('<td>', '</td>', itens);
						
						$('table tbody').append('<tr>' + html + '</tr>');
					}
					
					alterarCabecalhoTabelaResolucao();
				
				}
				else
				{
					$('table tbody').html('<tr><td colspan="7" style="color: #900; padding: 10px;"><strong>Nenhum produto encontrado.</strong></td></tr>');
				}
			}
		);
		
		// calcular totais
		x.executeSql(
			'SELECT COUNT(produto_codigo) AS total FROM produtos WHERE produto_codigo > 0 ' + wheres, [], function(x, dados) {
				
	
				var dado = dados.rows.item(0);

				
				$('#total').text(number_format(dado.total, 0, ',', '.'));
				
				// paginação
				
				$('#paginacao').html('');
				
				var total = ceil(dado.total / 20);
				
				if (total > 1)
				{
					if (sessionStorage['produtos_pagina_atual'] > 6)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(1, \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
					}
					
					if (sessionStorage['produtos_pagina_atual'] > 1)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['produtos_pagina_atual']) - 1) + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">&lt;</a> ');
					}
					
					for (i = intval(sessionStorage['produtos_pagina_atual']) - 6; i <= intval(sessionStorage['produtos_pagina_atual']) + 5; i++)
					{
						if (i <= 0 || i > total)
						{
							continue;
						}
						
						if (i == sessionStorage['produtos_pagina_atual'])
						{
							$('#paginacao').append('<strong>' + i + '</strong> ');
						}
						else
						{
							$('#paginacao').append('<a href="#" onclick="obter_produtos(' + i + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">' + i + '</a> ');
						}
					}
					
					if (sessionStorage['produtos_pagina_atual'] < total)
					{
						$('#paginacao').append('<a href="#" onclick="obter_produtos(' + (intval(sessionStorage['produtos_pagina_atual']) + 1) + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">&gt;</a> ');
					}
					
					if (sessionStorage['produtos_pagina_atual'] <= total - 6)
					{
						$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_produtos(' + total + ', \'' + sessionStorage['produtos_ordem_atual'] + '\'); return false;">Última Página</a> ');
					}
				}
			}
		);
	});
}
