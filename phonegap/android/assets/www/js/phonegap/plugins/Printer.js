if (typeof PhoneGap !== "undefined") {

	/**
	 * Empty constructor
	 */
	var PrinterPlugin = function() {
	};
	
	
	PrinterPlugin.prototype.imprimir = function(sucesso, falhou) 
	{	
		PhoneGap.exec(sucesso, falhou, "Printer", "imprimir",  new Array());		
	};
	
	PhoneGap.addConstructor(function() {
		
		if (!window.plugins) 
		{
			window.plugins = {};
		}
		
		window.plugins.printer = new PrinterPlugin();
		
	});
}