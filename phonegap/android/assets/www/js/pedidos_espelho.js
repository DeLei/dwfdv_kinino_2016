$(document).ready(function() {
	
	var itens_carregados	= false;
	
	var itens_troca			= false; //Utilizado para exibir a confirmação de impressão
	var codigo_e_empresa 	= parse_url(location.href).fragment;
	array_codigo_e_empresa 	= codigo_e_empresa.split('|');
	var codigo_do_pedido 	= array_codigo_e_empresa[0];
	var codigo_da_empresa 	= array_codigo_e_empresa[1];
	var tipo_pedido 		= array_codigo_e_empresa[2];
	var valor_total_troca	= 0;
	var exportado			= false;
	var status				= false;
	
	//Definindo se é um Orçamento ou Pedido
	if(tipo_pedido == 'O') {
		var tabela = 'orcamentos';
		var descricao = 'orçamento';
		$('.descricao_pedido').html('Orçamento');
	} else {
		var tabela = 'pedidos_pendentes';
		var descricao = 'pedido';
	}
	
	exibir_cabecalho();
	
	$('#enviar_pedido').click(function(e) {
		e.preventDefault();
		
		if(status == 'A' && exportado == '1') {
			confirmar('Este ' + descricao + ' <b>já foi enviado</b>!<p>Deseja realmente <b>reenviar</b> este ' + descricao + '?</p>', function () {
				$(this).dialog('close');
				
				location.href = "sincronizar.html?codigo_pedido=" + codigo_do_pedido;
				
				$("#confirmar_dialog").remove();
			});
		} else {
			confirmar('Deseja enviar este ' + descricao + '?', function () {
				$(this).dialog('close');
				
				location.href = "sincronizar.html?codigo_pedido=" + codigo_do_pedido;
				
				$("#confirmar_dialog").remove();
			});
		}
	});
	
	// Função utilizada para verificar se o cliente pertence ao representante
	function verificar_cliente_representante(cpfOuCnpj, callback) {
		// Obter Pedido Pendentes
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM clientes WHERE cpf = ?', [cpfOuCnpj], 
				function(x, dados) {
					if (dados.rows.length) {
						callback();
					} else {
						mensagem('O Pedido não pode ser copiado porque o cliente (' + formatar_cpfCNPJ(cpfOuCnpj) + ') não pertence ao representante.');
					}
				}
			)
		});
	}
	
	// Transformar orçemaneto em pedido 
	$('#transformar_em_pedido').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja transformar este orçamento em pedido?', function () {
			$(this).dialog('close');
			
			copiar_pedido(codigo_do_pedido, codigo_da_empresa, false, tabela, true);
			
			$("#confirmar_dialog").remove();
		});
	});
	
	// Copiar pedido
	$('#copiar').click(function(e) {
		e.preventDefault();
		
		var copiar_pedido_orcamento = function() {
			confirmar('Deseja copiar este ' + descricao + '?', function () {
				$(this).dialog('close');
				
				if(!tipo_pedido) {
					copiar_pedido_processado(codigo_do_pedido, codigo_da_empresa);
				} else {
					copiar_pedido(codigo_do_pedido, codigo_da_empresa, false, tabela);
				}
				
				$("#confirmar_dialog").remove();
			});
		}
		
		if(tipo_pedido == 'O') {
			copiar_pedido_orcamento();
		} else {
			verificar_cliente_representante($('#cpfOuCnpj').val(), copiar_pedido_orcamento);
		}
	});
	
	// Editar pedido
	$('#editar').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja editar este ' + descricao + '?', function () {
			$(this).dialog('close');
			
			copiar_pedido(codigo_do_pedido, codigo_da_empresa, true, tabela);
			
			$("#confirmar_dialog").remove();
		});
	});
	
	
	function construirPedidoImpressao(dadosPedido, callback) {
		
		var dados 	= dadosPedido.dados;
		var info 	= dadosPedido.info;
		
		var pedido = dados.rows.item(0);
		
		var conteudo = localStorage.getItem('pedidoImpressao');;
		
		var codigo_pedido 		= (pedido.pedido_id_pedido ? pedido.pedido_id_pedido : pedido.ip_codigo_pedido);
		var forma_pagamento 	= (pedido.forma_pagamento_descricao ? pedido.forma_pagamento_descricao : pedido.forma_pagamento_descricao); 
		var data_emissao		= (pedido.pedido_data_emissao ? pedido.pedido_data_emissao : pedido.pedido_data_emissao);
		
		// Número do Pedido
		//Definindo se é um Orçamento ou Pedido
		var titulo_impressao = 'P E D I D O';
		var titulo_impressao_menor = 'pedido';
		
		if(pedido.pedido_tipo_venda == '*') {
			titulo_impressao = 'O R C A M E N T O';
			titulo_impressao_menor = 'orçamento';
		}
		
		if(dadosPedido.troca) {
			titulo_impressao = 'T R O C A S';
			titulo_impressao_menor = 'trocas';
		}
		
		
		conteudo += "<dw-b><dw-fonteGrande>" + textoEsquerdaDireita(titulo_impressao, codigo_pedido, ' ', 32) + "\n";
		conteudo += "<dw-fonteGrande>--------------------------------\n";
		
		// Vendedor 
		conteudo += "HL DO BRASIL IND COM PROD ALIMENT LTDA\n";
		conteudo += textoEsquerdaDireita('Vendedor:', info.nome_representante, ' ', 42) + "\n"; 
		conteudo += textoEsquerdaDireita('Telefone:', info.telefone_representrante, ' ', 42) + "\n";
		conteudo += "<dw-fonteGrande>--------------------------------\n";
		
		// Cliente
		var nome_cliente = '';
		if(!isEmpty(pedido.cliente_codigo)) {
			nome_cliente = pedido.cliente_nome;
		} else {
			nome_cliente = pedido.prospect_nome;
		}
		
		conteudo += nome_cliente + "\n";
		conteudo += textoEsquerdaDireita('C. Pag.:', forma_pagamento, ' ', 42) + "\n";
		conteudo += textoEsquerdaDireita('Data da Emissao:', protheus_data2data_normal(data_emissao), ' ', 42) + "\n";
		conteudo += textoEsquerdaDireita('Data da Impressao:',  date('d/m/Y'), ' ', 42) + "\n";
		conteudo += "<dw-fonteGrande>--------------------------------\n";
		
		// Info. Produtos
		conteudo += textoEsquerdaDireita('Produto', 'Un/Qt Fr', ' ', 42) + "\n";
		conteudo += textoEsquerdaDireita('    Qtde X Pr. Unit', 'Total', '.', 42) + "\n";
		conteudo += "<dw-fonteGrande>--------------------------------\n";
		
		// Itens do Pedido
		var total_itens = dados.rows.length;
		
		var total_do_pedido 	= 0;
		var total_do_desconto 	= 0;
		var total_do_ipi 		= 0;
		var total_da_st			= 0;
		var total_geral			= 0;
		var total_sem_desconto  = 0;
		
		if(total_itens > 0) {
			for(i = 0; i < total_itens; i ++) {
				var item_pedido = dados.rows.item(i);
				
				var unidade_medida = (item_pedido.pedido_unidade_medida ? item_pedido.pedido_unidade_medida : item_pedido.ip_unidade_medida_produto);
				
				var quantidade 		= (item_pedido.pedido_quantidade ? item_pedido.pedido_quantidade : item_pedido.ip_quantidade_vendida_produto);
				var preco_unitario_sem_desconto = (item_pedido.pedido_preco_unitario ? item_pedido.pedido_preco_unitario : item_pedido.ip_preco_produto);
				var preco_unitario 	= (item_pedido.pedido_preco_venda ? item_pedido.pedido_preco_venda : item_pedido.ip_preco_produto);
				var ipi 			= (item_pedido.pedido_ipi ? item_pedido.pedido_ipi : item_pedido.inf_ipi);
				var st 				= (item_pedido.pedido_st ? item_pedido.pedido_st : item_pedido.inf_st);
				
				total_sem_desconto += preco_unitario_sem_desconto * quantidade;
				
				conteudo += textoEsquerdaDireita(item_pedido.produto_descricao, item_pedido.produto_segunda_unidade_medida + '/' + item_pedido.produto_converter + ' ' + item_pedido.produto_unidade_medida, ' ', 42) + "\n";
				conteudo += textoEsquerdaDireita('    ' + quantidade + ' ' + unidade_medida + ' x ' + number_format(preco_unitario, 2, ',', '.'), number_format(parseFloat(quantidade) * parseFloat(preco_unitario), 2, ',', '.'), '.', 42) + "\n";
				
				total_do_ipi 		+= (parseFloat(ipi) ? parseFloat(ipi) : 0);
				total_da_st			+= (parseFloat(st) ? parseFloat(st) : 0);
				total_do_pedido 	+= parseFloat(quantidade) * parseFloat(preco_unitario);
			}
			
			if(dadosPedido.troca){
				valor_total_troca = total_do_pedido;
			}
			
			if(pedido.pedido_desconto2 > 0) {
				var trocas = 0;
				if(valor_total_troca) {
					trocas = valor_total_troca;
				}
				
				total_do_desconto = ((total_do_pedido - trocas) * (pedido.pedido_desconto2 / 100));
			}
			
		}
		
		total_geral = total_do_pedido;
		total_geral_sem_desconto = total_geral;
		
		if(!dadosPedido.troca && itens_troca) {
			total_geral = total_geral - valor_total_troca;
		}
		
		if(pedido.pedido_desconto1) {
			total_geral = total_geral - (total_geral * (pedido.pedido_desconto1 / 100));
		}
		//Não considerar desconto de condição de pagamento para itens de troca
		if(pedido.pedido_desconto2 && !dadosPedido.troca) {
			total_geral = total_geral - (total_geral * (pedido.pedido_desconto2 / 100));
		}
		if(pedido.pedido_desconto3) {
			total_geral = total_geral - (total_geral * (pedido.pedido_desconto3 / 100));
		}
		if(pedido.pedido_desconto4) {
			total_geral = total_geral - (total_geral * (pedido.pedido_desconto4 / 100));
		}
		
		if(!dadosPedido.troca) {
			total_geral =  total_geral + total_do_ipi + total_da_st;
		}
		
		conteudo += "<dw-fonteGrande>--------------------------------\n";
		
		if(!dadosPedido.troca) {
			conteudo += "<dw-fonteGrande>" + textoEsquerdaDireita('Total ' + titulo_impressao_menor + ':', number_format(total_do_pedido, 2, ',', '.'), ' ', 32) + "\n";
			if(itens_troca) {
				conteudo += "<dw-fonteGrande>" + textoEsquerdaDireita('Trocas:', number_format(valor_total_troca, 2, ',', '.'), ' ', 32) + "\n";
			}
			conteudo += "<dw-fonteGrande>" + textoEsquerdaDireita('Desconto:', number_format(total_do_desconto, 2, ',', '.'), ' ', 32) + "\n";
			conteudo += "<dw-fonteGrande>" + textoEsquerdaDireita('IPI:', number_format(total_do_ipi, 2, ',', '.'), ' ', 32) + "\n";
			conteudo += "<dw-fonteGrande>" + textoEsquerdaDireita('ICMS subst trib:', number_format(total_da_st, 2, ',', '.'), ' ', 32) + "\n";
		}
		
		//Pedido normal desconta o valor total das trocas
		conteudo += "<dw-b><dw-fonteGrande>" + textoEsquerdaDireita('Total geral:', number_format(total_geral, 2, ',', '.'), ' ', 32) + "\n";
		conteudo += "<dw-fonteGrande>--------------------------------\n";
		
		//Observacao
		if(pedido.pedido_observacao_comercial) {
			conteudo += "<dw-b><dw-fonteGrande>OBSERVACAO: " + "\n";
			conteudo += pedido.pedido_observacao_comercial + "\n";
			conteudo += "<dw-fonteGrande>--------------------------------\n";
		}
		
		localStorage.setItem('pedidoImpressao', conteudo);
		
		callback();
	}
	
	
	$('#imprimir').click(function(e) {
		e.preventDefault();
		
		if(tipo_pedido == 'P' || tipo_pedido == 'O') {
			if(itens_troca) {
				criarDialog('impressao_troca', 'Deseja realizar a impressão dos itens de troca?');
				
				$(function() {
					$('#impressao_troca').dialog({
						title: '<span class="ui-icone-dialog icone-alerta"></span>Impressão',			
						minWidth: 600,
						modal: true,
						dialogClass: "no-close",
						open: function (event, ui) {
							$(".ui-widget-overlay").css('z-index', '0');
							$("input").blur();
				        },
						buttons: {
							'Sim': function() {
								$(this).dialog('close');
								imprimir('troca');
								$('#impressao_troca').remove();
							},	
							'Não': function(){
								$(this).dialog('close');
								imprimir();
								$('#impressao_troca').remove();
							}
						}
					});
				});
			} else {
				imprimir();
			}
		} else {
			imprimir();
		}
	});
	
	// Editar pedido
	$('#excluir').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja excluir este ' + descricao + '?', function () {
			$(this).dialog('close');
			
			db.transaction(function(x) {
				x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + codigo_do_pedido + '"', [], function(){
					if(tipo_pedido == 'O') {
						// Apagando pedido da sessao
						location.href="orcamentos_aguardando.html";
					} else {
						// Apagando pedido da sessao
						location.href="pedidos_aguardando.html";
					}

				});
			});
			
			$("#confirmar_dialog").remove();
		});
	});
	
	
	function obter_total_trocas(){
		valor_total_troca = 0;
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ? AND item_troca = ? ORDER BY produto_descricao, pedido_numero_item', [codigo_do_pedido, codigo_da_empresa, '1'], function(x, dados) {
				var total_itens = dados.rows.length;
				
				if(total_itens > 0) {
					for(i = 0; i < total_itens; i ++) {
						var item_pedido = dados.rows.item(i);
						
						var ipi = (parseFloat(item_pedido.pedido_ipi) ? parseFloat(item_pedido.pedido_ipi) : 0);
						var st	= (parseFloat(item_pedido.pedido_st) ? parseFloat(item_pedido.pedido_st) : 0);
						valor_total_troca += (parseFloat(item_pedido.pedido_preco_venda) * parseFloat(item_pedido.pedido_quantidade)) + (ipi + st);
					}
				}
			});
		});
	}
	
	
	function exibir_cabecalho(){
		
		// Se for "P" (Pedidos Pendentes) ou "O" (Orçamento) entra no IF, se não entra no IF dos Pedidos Processados
		if(tipo_pedido == 'P' || tipo_pedido == 'O') {
			
			// Obtem somente 1 registro referente ao pedido para montar o cabeçalho
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM ' + tabela + ' WHERE LOWER(pedido_id_pedido) = ? AND pedido_filial = ? ORDER BY exportado ASC, item_troca DESC LIMIT 1', [codigo_do_pedido.toLowerCase(), codigo_da_empresa], function(x, dados) {
					var pedido = dados.rows.item(0);
					
					status = pedido.pedido_status;
					exportado = pedido.exportado;
					
					if(status == 'L') {
						$('#botoes_espelho_pedido').hide();
					} else if(status == 'A' && exportado == '1'){
						$('#botoes_espelho_pedido').show();
					}
					
					//Verifica se o pedido contem itens de troca
					if(pedido.item_troca == '1'){
						itens_troca = true;
						obter_total_trocas();
					}
					
					if(!pedido.exportado) {
						$('#editar').show();
						$('#excluir').show();
					}
					
					if(tipo_pedido == 'P') {
						$('#enviar_pedido').show();
					}
					
					// informações gerais
					$('.codigo_e_representante').text(info.cod_rep + ' / ' + info.nome_representante);
					$('.numero_pedido').text(pedido.pedido_id_pedido);
					$('.filial').text(pedido.pedido_filial);
					$('.data_emissao').text(protheus_data2data_normal(pedido.pedido_data_emissao));
					$('.condicao_pagamento').text(pedido.forma_pagamento_descricao);
					
					// Descontos 
					$('.pedido_desconto1').text(pedido.pedido_desconto1 ? number_format(pedido.pedido_desconto1, 3, ',', '.') : 'N/A');
					$('.pedido_desconto2').text(pedido.pedido_desconto2 ? number_format(pedido.pedido_desconto2, 3, ',', '.') : 'N/A');
					$('.pedido_desconto4').text(pedido.pedido_desconto4 ? number_format(pedido.pedido_desconto4, 3, ',', '.') : 'N/A');
					
					obter_descricao('formas_pagamento_real', 'descricao', 'chave', pedido.pedido_formas_de_pagamento, '.forma_de_pagamento');
					
					$('.tipo_movimentacao').text(pedido.pedido_tipo_movimentacao);
					
					if(tipo_pedido == 'O' && !isEmpty(pedido.prospect_cgc)) {
						x.executeSql('SELECT * FROM clientes WHERE cpf = ?', [pedido.prospect_cgc], function(x, dados) {
							//Prospect convertido para cliente
							if (dados.rows.length) {
								var prospect_convertido = dados.rows.item(0);
								
								// Informações do Cliente
								$('.nome_cliente').text(prospect_convertido.nome);
								$('.codigo_loja_cliente').text(prospect_convertido.codigo + ' / ' + prospect_convertido.loja);
								$('.cpf_cliente').text(formatar_cpfCNPJ(prospect_convertido.cpf));
								$('#cpfOuCnpj').val(prospect_convertido.cpf);
								$('.contato').text(prospect_convertido.pessoa_contato ? prospect_convertido.pessoa_contato : 'N/A');
								
								//Tratamento telefone
								var telefone = '';
								if(prospect_convertido.ddd) {
									telefone = prospect_convertido.ddd + prospect_convertido.telefone;
								} else {
									if(prospect_convertido.telefone) {
										telefone = prospect_convertido.telefone;
									}						
								}
								//Tratamento telefone
								$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
								$('.email').text(prospect_convertido.email ? prospect_convertido.email : 'N/A');
								$('.cidade').text(prospect_convertido.cidade ? prospect_convertido.cidade : 'N/A');
								$('.estado').text(prospect_convertido.estado ? prospect_convertido.estado : 'N/A');
								$('.endereco').text(prospect_convertido.endereco ? prospect_convertido.endereco : 'N/A');
								
								$('#transformar_em_pedido').show();
							} else {
								// Informações do Prospect
								$('.descricao_cliente').html('Prospect');
								$('.nome_cliente').text(pedido.prospect_nome);
								$('.codigo_loja_cliente').text(pedido.prospect_codigo + ' / ' + pedido.prospect_codigo_loja);
								$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.prospect_cgc));
								$('#cpfOuCnpj').val(pedido.prospect_cgc);
								$('.contato').text(pedido.prospect_contato ? pedido.prospect_contato : 'N/A');
								
								//Tratamento telefone
								var telefone = '';
								if(pedido.cliente_ddd) {
									telefone = pedido.prospect_ddd + pedido.prospect_telefone;
								} else {
									if(pedido.prospect_telefone) {
										telefone = pedido.prospect_telefone;
									}						
								}
								$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
								$('.email').text(pedido.prospect_email ? pedido.prospect_email : 'N/A');
								$('.estado').text(pedido.prospect_estado ? pedido.prospect_estado : 'N/A');
								$('.endereco').text(pedido.prospect_endereco ? pedido.prospect_endereco : 'N/A');
								
								// -- Obter Municipio
								x.executeSql('SELECT * FROM municipios WHERE codigo = ? AND uf = ?', [pedido.prospect_codigo_municipio, pedido.prospect_estado], function(x, dados) {
									if (dados.rows.length) {
										var municipio = dados.rows.item(0);
										$('.cidade').text(municipio.nome);
									} else {
										$('.cidade').text('N/A');
									}
								});
							}
						});
					} else {
						// Informações do Cliente
						$('.nome_cliente').text(pedido.cliente_nome);
						$('.codigo_loja_cliente').text(pedido.cliente_codigo + ' / ' + pedido.cliente_loja);
						$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.cliente_cpf));
						$('#cpfOuCnpj').val(pedido.cliente_cpf);
						$('.contato').text(pedido.cliente_pessoa_contato ? pedido.cliente_pessoa_contato : 'N/A');
						
						//Tratamento telefone
						var telefone = '';
						if(pedido.cliente_ddd) {
							telefone = pedido.cliente_ddd + pedido.cliente_telefone;
						} else {
							if(pedido.cliente_telefone) {
								telefone = pedido.cliente_telefone;
							}						
						}
						//Tratamento telefone
						$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
						$('.email').text(pedido.cliente_email ? pedido.cliente_email : 'N/A');
						$('.cidade').text(pedido.cliente_cidade ? pedido.cliente_cidade : 'N/A');
						$('.estado').text(pedido.cliente_estado ? pedido.cliente_estado : 'N/A');
						$('.endereco').text(pedido.cliente_endereco ? pedido.cliente_endereco : 'N/A');
						
						if(tipo_pedido == 'O'){
							$('#transformar_em_pedido').show();
						}
					}
					
					// Informações do Pedido
					$('.pedido_cliente').text(pedido.pedido_pedido_cliente ? pedido.pedido_pedido_cliente : 'N/A');
					$('.data_entrega').text(pedido.pedido_data_entrega ? protheus_data2data_normal(pedido.pedido_data_entrega) : 'N/A');
					
					// -- obter evento
					x.executeSql('SELECT * FROM eventos WHERE id = ?', [pedido.pedido_id_feira], function(x, dados) {
						if (dados.rows.length) {
							var evento = dados.rows.item(0);
							$('.eventos').text(evento.id + ' - ' + evento.nome);
						}
					});
					
					$('.tipo_frete').text(obter_tipo_frete(pedido.pedido_tipo_frete));
					$('.transportadoras').text(pedido.transportadora_nome ? pedido.transportadora_nome : 'N/A');
					
					// --
					// Cliente de Entrega
					if(pedido.pedido_cliente_entrega && pedido.pedido_loja_entrega) {
						x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [pedido.pedido_cliente_entrega, pedido.pedido_loja_entrega], function(x, dados) {
							if(dados.rows.length) {
								var cliente_entrega = dados.rows.item(0);
								
								$('.cliente_entrega').empty();
								$('.cliente_entrega').append('<b>Cliente:</b> ' + cliente_entrega.nome + '<br />');
								$('.cliente_entrega').append('<b>Endereço:</b> ' + cliente_entrega.endereco + '<br />');
								$('.cliente_entrega').append('<b>Bairro:</b> ' + cliente_entrega.bairro + '<br />');
								$('.cliente_entrega').append('<b>CEP:</b> ' + formatar_cep(cliente_entrega.cep) + '<br />');
								$('.cliente_entrega').append('<b>Cidade:</b> ' + cliente_entrega.cidade + '<br />');
								$('.cliente_entrega').append('<b>Estado:</b> ' + cliente_entrega.estado + '<br />');
							} else {
								$('.cliente_entrega').text('Cliente de Entrega não foi encontrado.');
							}
						});
					}
					//--
					
					$('.observacao_comercial').text(pedido.pedido_observacao_comercial ? pedido.pedido_observacao_comercial : 'N/A');
				});
			});
			
		} else {
			
			// Obter Pedido Processado
			db.transaction(function(x) {
				// Obtem somente 1 registro referente ao pedido para montar o cabeçalho
				x.executeSql('SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ? ORDER BY ip_codigo_item DESC LIMIT 1', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
					if (dados.rows.length) {
						
						$('#atencao_impostos').hide();
						
						var pedido = dados.rows.item(0);
						
						// informações gerais
						$('.codigo_e_representante').text(info.cod_rep + ' / ' + info.nome_representante);
						$('.numero_pedido').text(pedido.pedido_codigo);
						$('.filial').text(pedido.pedido_filial);
						$('.data_emissao').text(protheus_data2data_normal(pedido.pedido_data_emissao));
						$('.condicao_pagamento').text(pedido.forma_pagamento_descricao);
						
						$('.tipo_movimentacao').text(pedido.pedido_tipo_movimentacao);
						
						// Informações do Cliente
						$('.nome_cliente').text(pedido.cliente_nome);
						$('.codigo_loja_cliente').text(pedido.cliente_codigo + ' / ' + pedido.cliente_loja);
						$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.cliente_cpf));
						$('#cpfOuCnpj').val(pedido.cliente_cpf);
						$('.contato').text(pedido.cliente_pessoa_contato ? pedido.cliente_pessoa_contato : 'N/A');
						
						// Descontos 
						$('.pedido_desconto1').text(pedido.pedido_desconto1 ? number_format(pedido.pedido_desconto1, 3, ',', '.') : 'N/A');
						$('.pedido_desconto4').text(pedido.pedido_desconto4 ? number_format(pedido.pedido_desconto4, 3, ',', '.') : 'N/A');
						
						// Descontos 
						$('.pedido_desconto2').text(pedido.pedido_desconto2 ? number_format(pedido.pedido_desconto2, 3, ',', '.') : 'N/A');
						
						obter_descricao('formas_pagamento_real', 'descricao', 'chave', pedido.pedido_formas_de_pagamento, '.forma_de_pagamento');
						
						$('.tipo_movimentacao').text(pedido.pedido_tipo_movimentacao);
						
						
						//Tratamento telefone
						var telefone = '';
						if(pedido.cliente_ddd) {
							telefone = pedido.cliente_ddd + pedido.cliente_telefone;
						} else {
							if(pedido.cliente_telefone) {
								telefone = pedido.cliente_telefone;
							}						
						}
						$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
						//Tratamento telefone		
						
						$('.email').text(pedido.cliente_email ? pedido.cliente_email : 'N/A');
						$('.cidade').text(pedido.cliente_cidade ? pedido.cliente_cidade : 'N/A');
						$('.estado').text(pedido.cliente_estado ? pedido.cliente_estado : 'N/A');
						$('.endereco').text(pedido.cliente_endereco ? pedido.cliente_endereco : 'N/A');
						
						// Informações do Pedido
						$('.pedido_cliente').text(pedido.ip_pedido_cliente ? pedido.ip_pedido_cliente : 'N/A');
						$('.data_entrega').text(pedido.pedido_data_entrega ? protheus_data2data_normal(pedido.pedido_data_entrega) : 'N/A');
						$('.eventos').text('N/A');
						$('.tipo_frete').text(obter_tipo_frete(pedido.pedido_tipo_frete));
						$('.transportadoras').text(pedido.transportadora_nome ? pedido.transportadora_nome : 'N/A');
						
						// --
						// Cliente de Entrega
						if(pedido.pedido_cliente_entrega && pedido.pedido_loja_entrega) {
							x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [pedido.pedido_cliente_entrega, pedido.pedido_loja_entrega], function(x, dados) {
								if(dados.rows.length) {
									var cliente_entrega = dados.rows.item(0);
									
									$('.cliente_entrega').empty();
									$('.cliente_entrega').append('<b>Cliente:</b> ' + cliente_entrega.nome + '<br />');
									$('.cliente_entrega').append('<b>Endereço:</b> ' + cliente_entrega.endereco + '<br />');
									$('.cliente_entrega').append('<b>Bairro:</b> ' + cliente_entrega.bairro + '<br />');
									$('.cliente_entrega').append('<b>CEP:</b> ' + formatar_cep(cliente_entrega.cep) + '<br />');
									$('.cliente_entrega').append('<b>Cidade:</b> ' + cliente_entrega.cidade + '<br />');
									$('.cliente_entrega').append('<b>Estado:</b> ' + cliente_entrega.estado + '<br />');
								} else {
									$('.cliente_entrega').text('Cliente de Entrega não foi encontrado.');
								}
							});
						}
						//--
						
						$('.mensagem_nota_fiscal').text(pedido.pedido_mensagem ? pedido.pedido_mensagem : 'N/A');
						
						// informações gerais
						$('.oc').text(pedido.ordem_de_compra);
						$('.numero').text(pedido.exportado ? pedido.codigo : 'N/D');
						$('.tp_frete').text(strtoupper(pedido.tipo_de_frete));
						$('.emissao').text(date('d/m/Y', pedido.timestamp));
						$('.forma_pgto').text(pedido.descricao_da_forma_de_pagamento);
						$('.observacao').text(pedido.observacao ? pedido.observacao : 'N/D');
						
						var dt_ent = pedido.data_de_entrega;
						if (dt_ent) {
							var dia = substr(dt_ent, 6, 2);
							var mes = substr(dt_ent, 4, 2);
							var ano = substr(dt_ent, 0, 4);
							
							$('.dt_ent').text(dia + '/' + mes + '/' + ano);
						} else {
							$('.dt_ent').text('N/D');
						}
						
						// prospect/cliente
						if (pedido.id_pro) {
							$('#cli').hide();
							$('#pro').show();
							
							x.executeSql('SELECT * FROM prospects WHERE id = ?', [pedido.id_pro], function(x, dados) {
								if (dados.rows.length) {
									var prospect = dados.rows.item(0);
	
									$('.rs_cli').text(prospect.nome);
									$('.cnpj_cli').text(prospect.cgc);
									$('.tel_cli').text(prospect.telefone);
									$('.email_cli').text(prospect.email);
									$('.end_cli').text(prospect.endereco);
									$('.bairro_cep_cli').text(prospect.bairro + (prospect.cep ? '/' + prospect.cep : ''));
									$('.contato_cli').text(prospect.contato);
									$('.mun_est_cli').text(prospect.municipio + (prospect.estado ? '/' + prospect.estado : ''));
								}
							});
						} else {
							$('#cli').show();
							$('#pro').hide();
							
							x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [pedido.codigo_do_cliente, pedido.loja_do_cliente], function(x, dados) {
								if (dados.rows.length){
									var cliente = dados.rows.item(0);
									
									$('.rs_cli').text(cliente.razao_social);
									$('.cnpj_cli').text(cliente.cnpj);
									$('.tel_cli').text(cliente.telefone);
									$('.cod_cli').text(cliente.codigo);
									$('.lj_cli').text(cliente.loja);
									$('.email_cli').text(cliente.email);
									$('.end_cli').text(cliente.endereco);
									$('.bairro_cep_cli').text(cliente.bairro + (cliente.cep ? '/' + cliente.cep : ''));
									$('.contato_cli').text(cliente.pessoa_de_contato);
									$('.mun_est_cli').text(cliente.municipio + (cliente.estado ? '/' + cliente.estado : ''));
								}
							});
						}
						
						// produtos
						
						x.executeSql('SELECT * FROM pedidos WHERE codigo = ? ORDER BY timestamp ASC', [pedido.codigo], function(x, dados) {
							if (dados.rows.length) {
								var qtd_ven = 0;
								var qtd_fat = 0;
								
								for (i = dados.rows.length - 1; i >= 0; i--) {
									var dado = dados.rows.item(i);
									
									qtd_ven += dado.quantidade;
									qtd_fat += dado.quantidade_faturada;
									
									$('.itens').append('<tr><td style="text-align: center;">' + str_pad(i + 1, 2, 0, 'STR_PAD_LEFT') + '</td><td>' + dado.codigo_do_produto + '</td><td>' + dado.descricao_do_produto + '</td><td class="nao_exibir_impressao">' + dado.um_do_produto + '</td><td class="nao_exibir_impressao">' + (dado.descricao_da_tabela_de_preco ? dado.descricao_da_tabela_de_preco : dado.codigo_da_tabela_de_preco) + '</td><td style="text-align: right;">' + number_format(dado.preco, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.preco - round(dado.preco * (dado.desconto / 100), 2), 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.quantidade, 0, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.quantidade_faturada, 0, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.total_sem_ipi, 2, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.ipi, 2, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.total_com_ipi, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.valor_desconto, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.total_final, 2, ',', '.') + '</td></tr>');
								}
								
								$('.quantidade_vendida').text(number_format(qtd_ven, 0, ',', '.'));
								$('.quantidade_faturada').text(number_format(qtd_fat, 0, ',', '.'));
								$('.valor_total_desconto').text(number_format(dado.desconto_total_pedido, 2, ',', '.'));
								$('.valor_total_do_pedido').text(number_format(dado.valor_total_do_pedido, 2, ',', '.'));
							}
						});
						
						

						//----------------
						//-- Notas Fiscais
						$('#notas_fiscais').show();
						
						db.transaction(function(x) {
							x.executeSql('SELECT DISTINCT item_codigo_nota_fiscal, nota_data_emissao, nota_valor_total_produto, nota_valor_total, nota_valor_icms, nota_valor_ipi, nota_valor_frete, transportadora_nome, transportadora_telefone, nota_serie, nota_codigo, nota_filial FROM notas_fiscais WHERE item_codigo_pedido = ?', [codigo_do_pedido], function(x, dados) {
								
								var total_itens = dados.rows.length;
								
								if (total_itens) {
									
									var html = '';
									
									for(i = 0; i < total_itens; i ++) {
										var item_nota = dados.rows.item(i);
										
										html += '<tr>';
											html += '<td>' + item_nota.item_codigo_nota_fiscal + '</td>';
											html += '<td>' + protheus_data2data_normal(item_nota.nota_data_emissao) + '</td>';
											html += '<td align="right">' + number_format(item_nota.nota_valor_total_produto, 3, ',', '.') + '</td>';
											html += '<td align="right">' + number_format(item_nota.nota_valor_icms, 3, ',', '.') + '</td>';
											html += '<td align="right">' + number_format(item_nota.nota_valor_ipi, 3, ',', '.') + '</td>';
											html += '<td align="right">' + number_format(item_nota.nota_valor_frete, 3, ',', '.') + '</td>';
											html += '<td>' + (item_nota.transportadora ? item_nota.transportadora_nome : 'N/A') + '</td>';
											html += '<td>' + (item_nota.transportadora_telefone ? item_nota.transportadora_telefone : 'N/A') + '</td>';
											html += '<td align="center"><a href="#" id="ver_titulos" data-serie="'+item_nota.nota_serie+'" data-codigo="'+item_nota.nota_codigo+'">Ver Títulos</a> | <a href="notas_fiscais_visualizar.html#'+item_nota.nota_codigo+'_'+item_nota.nota_serie+'_'+item_nota.nota_filial+'">Ver Itens</a></td>';
										html += '</tr>';
									}
									
									$('.itens_notas').html(html);
									
									alterarCabecalhoListagem('#notas_fiscais_tabela');
								}
							});
						});
						
					} else {
						
						window.location = 'index.html';
						
					}
				});
			});
		}
		
		alterarCabecalhoTabelaConteudo();
	}
	
	$(window).scroll(function () {
		
		//Topo da página
		if($(window).scrollTop() <= 100) {
			
			//Remove a listagem de produtos para evitar travamento
			$('.itens_pendentes').html('');
			$('.itens_troca').html('');
			$('.itens_processados').html('');
			itens_carregados = false;
			
		} else if (($(window).scrollTop() + $(window).height() + 300) >= $(document).height()) {
			
			//Verifica se os registros já foram carregados
			if(!itens_carregados) {
				
				/* Totais do pedido */
				var total_preco_unitario		= 0;
				var total_desconto_reais		= 0;
				var total_preco_venda 			= 0;
				var total_valor_unidade 		= 0;
				var total_quantidade 			= 0;
				var total			 			= 0;
				var total_ipi					= 0;
				var total_icms 					= 0;
				var total_st 					= 0;
				var total_com_ipi		 		= 0;
				var total_geral		 			= 0;
				var total_peso		 			= 0;
				var valor_analitico				= 0;
				var percentual_analitico		= 0;
					valor_total_troca			= 0;
				var pedido						= null;
				itens_carregados = true;
				
				// Se for "P" (Pedidos Pendentes) ou "O" (Orçamento) entra no IF, se não entra no IF dos Pedidos Processados
				if(tipo_pedido == 'P' || tipo_pedido == 'O') {
					
					$('.loading-pendentes').show();
					
					
					// Obtem os dados do pedido para montar a listagem de itens
					db.transaction(function(x) {
						x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ? GROUP BY pedido_codigo_produto, item_troca ORDER BY produto_descricao, pedido_numero_item', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
							var total_itens = dados.rows.length;
							
							if(total_itens > 0) {
								
								for(i = 0; i < total_itens; i ++) {
									var item_pedido = dados.rows.item(i);
										pedido = item_pedido;
										
									var preco_unitario = item_pedido.pedido_preco_unitario;
									if(pedido.pedido_desconto1 > 0) {
										preco_unitario = aplicar_descontos_cabecalho(item_pedido.pedido_preco_unitario, pedido.pedido_desconto1, '0');
									}
									
									var itens_pedido 		= [];
									var valor_ipi 			= item_pedido.ip_valor_total_item * (item_pedido.produto_ipi / 100);
									var valor_com_desconto 	= item_pedido.ip_valor_total_item - item_pedido.ip_total_desconto_item;
									
									var html = '';
										html += '<td align="center">' + (i+1) + '</td>';
										html += '<td align="center">' + item_pedido.pedido_codigo_produto + '</td>';
										html += '<td>' + item_pedido.produto_descricao + '</td>';
										html += '<td align="right">' + number_format(preco_unitario, 3, ',', '.') + '</td>';
										html += '<td align="right">' + number_format(item_pedido.pedido_valor_unidade, 3, ',', '.') + '</td>';
										
										html += '<td align="right">' + item_pedido.pedido_unidade_medida + '</td>';
										
										html += '<td align="right">' + number_format(item_pedido.pedido_desconto_item, 3, ',', '.') + ' %<br />(R$ ' + number_format((preco_unitario * (item_pedido.pedido_desconto_item / 100)), 3, ',', '.') + ')</td>';
										html += '<td align="right">' + number_format(item_pedido.pedido_preco_venda, 3, ',', '.') + '</td>';
										html += '<td align="right">' + item_pedido.pedido_quantidade + '</td>';
										
										html += '<td align="right">' + number_format(item_pedido.pedido_peso_total, 3, '.', '.') + '</td>';
										
										//Total sem impostos
										html += '<td align="right">' + number_format(item_pedido.pedido_preco_venda * item_pedido.pedido_quantidade, 3, ',', '.')  + '</td>';
										
										//Impostos
										html += '<td align="right">' + (item_pedido.pedido_ipi ? number_format(item_pedido.pedido_ipi, 3, ',', '.') : '0,000') + '</td>';
										html += '<td align="right">' + (item_pedido.pedido_icms ? number_format(item_pedido.pedido_icms, 3, ',', '.') : '0,000') + '</td>';
										html += '<td align="right">' + (item_pedido.pedido_st ? number_format(item_pedido.pedido_st, 3, ',', '.') : '0,000') + '</td>';			
										
										//Total com Impostos
										var ipi = (parseFloat(item_pedido.pedido_ipi) ? parseFloat(item_pedido.pedido_ipi) : 0);
										var st	= (parseFloat(item_pedido.pedido_st) ? parseFloat(item_pedido.pedido_st) : 0);
										var total_item_impostos = (parseFloat(item_pedido.pedido_preco_venda) * parseFloat(item_pedido.pedido_quantidade)) + (ipi + st);
										html += '<td align="right">' + number_format(total_item_impostos, 3, ',', '.') +'</td>';	
										
									$('.itens_pendentes').parent('table').show();
									
									if(item_pedido.item_troca == 1) {
										valor_total_troca += total_item_impostos;
										itens_troca = true;
										$('#corpo_itens_troca').show();
										$('.itens_troca').append('<tr>' + html + '</tr>');
									} else {
										$('.itens_pendentes').append('<tr>' + html + '</tr>');
										
										// Somando Totais
										total_preco_unitario		+= parseFloat(preco_unitario);
										total_desconto_reais		+= parseFloat(preco_unitario * (item_pedido.pedido_desconto_item / 100));
										total_preco_venda 			+= parseFloat(item_pedido.pedido_preco_venda);
										total_valor_unidade			+= parseFloat(item_pedido.pedido_valor_unidade);
										total_quantidade			+= parseFloat(item_pedido.pedido_quantidade);
										total_ipi					+= (parseFloat(item_pedido.pedido_ipi) ? parseFloat(item_pedido.pedido_ipi) : 0);
										total_icms					+= (parseFloat(item_pedido.pedido_icms) ? parseFloat(item_pedido.pedido_icms) : 0);
										total_st					+= (parseFloat(item_pedido.pedido_st) ? parseFloat(item_pedido.pedido_st) : 0);
										total						+= parseFloat(item_pedido.pedido_preco_venda * item_pedido.pedido_quantidade);
										total_geral					+= parseFloat(item_pedido.pedido_preco_venda * item_pedido.pedido_quantidade);// + (total_ipi + total_st);
										total_peso					+= parseFloat(item_pedido.pedido_peso_total);
									}
									
								}
							}
							
							//Exibindo Totais
							var totais_html = '';
							totais_html += '<td colspan="3">TOTAIS DOS ITENS </td>';
							totais_html += '<td align="right">' + number_format(total_preco_unitario, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_valor_unidade, 3, ',', '.') + '</td>';
							
							totais_html += '<td align="right"></td>';
							
							totais_html += '<td align="right">' + number_format(total_desconto_reais * 100 / total_preco_unitario, 3, ',', '.') + ' %<br />R$ ' + number_format(total_desconto_reais, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
							
							totais_html += '<td align="right">' + total_quantidade + '</td>';
							totais_html += '<td align="right">' + number_format(total_peso, 3, '.', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_ipi, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_icms, 3, ',', '.') + '</td>';
							totais_html += '<td align="right">' + number_format(total_st, 3, ',', '.') + '</td>';
							
							totais_html += '<td align="right">' + number_format(total_geral + total_ipi + total_st, 3, ',', '.') + '</td>';

							
							$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
							
							
							//Exibir Analitico
							var analitico_html = '';
								analitico_html += '<td colspan="13">ANALÍTICO</td>';
								analitico_html += '<td colspan="2" align="right">R$ ' + number_format(pedido.pedido_analitico_valor, 3, ',', '.') + '<br />(' + number_format(pedido.pedido_analitico_perc, 3, ',', '.') + '%)</td>';
								
							$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + analitico_html + '</tr>');
							
							
							var total_pedido = '';
								total_pedido += '<td colspan="13">TOTAIS DO ' + (tipo_pedido != 'O' ? 'PEDIDO' : 'ORÇAMENTO') + '</td>';
								
							var total_com_desconto = total_geral;
							if(pedido.pedido_desconto1 > 0) {
								total_com_desconto = total_geral - (total_geral * (pedido.pedido_desconto1 / 100));
							}
							if(pedido.pedido_desconto2 > 0) {
								total_com_desconto = total_geral - (total_geral * (pedido.pedido_desconto2 / 100));
							}
							if(pedido.pedido_desconto3 > 0) {
								total_com_desconto = total_geral - (total_geral * (pedido.pedido_desconto3 / 100));
							}
							if(pedido.pedido_desconto4 > 0) {
								total_com_desconto = total_geral - (total_geral * (pedido.pedido_desconto4 / 100))
							}
							
							total_pedido += '<td colspan="2" align="right">R$ ' + number_format(total_com_desconto + total_ipi + total_st, 3, ',', '.') + '</td>';
							
							$('.itens_pendentes').append('<tr class="novo_grid_rodape">' + total_pedido + '</tr>');
							
							
							$('.loading-pendentes').hide();
							
							alterarCabecalhoListagem('#itens_pendentes_tabela');
							alterarCabecalhoListagem('#itens_troca_tabela');
						});
					});
					
					
				} else { //Pedido Processado
					
					$('.loading-processados').show();
					
					//Obtem os dados do pedido para montar a listagem de itens
					db.transaction(function(x) {
						x.executeSql('SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ? ORDER BY ip_codigo_item DESC', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
							if (dados.rows.length) {
								var total_itens = dados.rows.length;
								
								if(total_itens > 0)
								{
									// Declarando variaveis totais
									var total_preco_venda 			= 0;
									var total_quantidade 			= 0;
									var total_quantidade_faturada 	= 0;
									var total			 			= 0;
									var total_faturado		 		= 0;
									var total_ipi   				= 0;
									var total_com_ipi		 		= 0;
									var total_desconto		 		= 0;
									var total_preco_com_desconto	= 0;
									var total_geral		 			= 0;
									var total_peso		 			= 0;
									
									var numero_item = 1;
									
									for(i = 0; i < total_itens; i ++)
									{
										var item_pedido = dados.rows.item(i);
										var pedido 		= item_pedido;
										
										var preco_unitario = item_pedido.ip_preco_produto;
										if(pedido.pedido_desconto1 > 0)
										{
											preco_unitario = aplicar_descontos_cabecalho(item_pedido.ip_preco_produto, pedido.pedido_desconto1, '0');
										}
										
										var itens_pedido = [];
										var valor_ipi = parseFloat((item_pedido.inf_ipi ? item_pedido.inf_ipi : 0));
										var valor_com_desconto = item_pedido.ip_valor_total_item - item_pedido.ip_total_desconto_item;
										
										var html = '';
										html += '<td align="center">' + (numero_item++)  + '</td>';
										html += '<td align="center">' + item_pedido.ip_codigo_produto + '</td>';
										html += '<td>' + item_pedido.ip_descricao_produto + '</td>';
										html += '<td>' + (item_pedido.ip_pedido_cliente ? item_pedido.ip_pedido_cliente : 'N/A') + '</td>';
										html += '<td align="right">' + number_format(item_pedido.ip_preco_unitario, 3, ',', '.') + '</td>';
										html += '<td align="right">' + number_format(item_pedido.ip_preco_produto, 3, ',', '.') + '</td>';
										html += '<td align="center">' + number_format(preco_unitario - (item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto), 3, ',', '.') + '</td>';
										html += '<td align="center">' + item_pedido.ip_quantidade_vendida_produto + '</td>';
										html += '<td align="right">' + number_format(item_pedido.ip_valor_total_item, 3, ',', '.') + '</td>';
										
										html += '<td align="center">' + item_pedido.ip_quantidade_faturada_produto + '</td>';
										
										html += '<td align="center">' + number_format(item_pedido.pedido_peso_total, 3, '.', '.') + '</td>';
										
										html += '<td align="right">' + number_format(item_pedido.ip_preco_produto * item_pedido.ip_quantidade_faturada_produto, 3, ',', '.') + '</td>';
										html += '<td align="right">' + number_format(parseFloat(valor_ipi), 3, ',', '.') + '</td>';
										html += '<td align="right">' + number_format(valor_ipi + parseFloat(item_pedido.ip_valor_total_item), 3, ',', '.') + '</td>';
			
										
										$('.itens_processados').parent('table').show();
										$('.itens_processados').append('<tr>' + html + '</tr>');
										
										// Somando Totais
										total_preco_venda 			+= parseFloat(preco_unitario);
										total_preco_com_desconto 	+= parseFloat(preco_unitario - (item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto));
										total_quantidade 			+= parseFloat(item_pedido.ip_quantidade_vendida_produto);
										total_quantidade_faturada	+= parseFloat(item_pedido.ip_quantidade_faturada_produto);
										total						+= parseFloat(item_pedido.ip_valor_total_item);
										total_faturado				+= parseFloat(item_pedido.ip_preco_produto * item_pedido.ip_quantidade_faturada_produto);
										total_ipi					+= parseFloat(valor_ipi);
										total_com_ipi				+= valor_ipi + parseFloat(item_pedido.ip_valor_total_item);
										total_desconto				+= parseFloat(item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto);
										total_geral					+= (valor_com_desconto * (item_pedido.produto_ipi / 100)) + parseFloat(valor_com_desconto);
										total_peso					+= item_pedido.pedido_peso_total;
									}
									
									//Exibindo Totais
									var totais_html = '';
									totais_html += '<td colspan="4">TOTAIS DO PEDIDO</td>';
									totais_html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
									totais_html += '<td align="right">' + number_format(total_desconto, 3, ',', '.') + '</td>';
									totais_html += '<td align="right">' + number_format(total_preco_com_desconto, 3, ',', '.') + '</td>';
									totais_html += '<td align="center">' + total_quantidade + '</td>';
									totais_html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
									totais_html += '<td align="center">' + total_quantidade_faturada + '</td>';
									
									totais_html += '<td align="right">' + number_format(total_peso, 3, '.', '.') + '</td>';
									
									totais_html += '<td align="right">' + number_format(total_faturado, 3, ',', '.') + '</td>';
									totais_html += '<td align="right">' + number_format(total_ipi, 3, ',', '.') + '</td>';
									totais_html += '<td align="right">' + number_format(total_com_ipi, 3, ',', '.') + '</td>';
									
									
									$('.itens_processados').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
									
								}
							}
							
							$('.loading-processados').hide();
						});
					});
				}
				
			}
			
	    }
	});
	
	
	function imprimir(troca) {
		localStorage.setItem('pedidoImpressao', '');
		
		//Pedido pendente/Orçamento
		if(tipo_pedido == 'P' || tipo_pedido == 'O') {
			if(troca) {
				db.transaction(function(x) {
					x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ? AND item_troca = ? ORDER BY produto_descricao, pedido_numero_item', [codigo_do_pedido, codigo_da_empresa, '1'], function(x, dados) {
						if (dados.rows.length) {
							construirPedidoImpressao({info: info, dados: dados, troca: true}, function(){
								
								db.transaction(function(x) {
									x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ? AND item_troca = ? ORDER BY produto_descricao, pedido_numero_item', [codigo_do_pedido, codigo_da_empresa, '0'], function(x, dados) {
										if (dados.rows.length) {
											construirPedidoImpressao({info: info, dados: dados}, function(){
												var imp = new Impressao();
												
												imp.imprimir(localStorage.getItem('pedidoImpressao'));
											});
										}
									});
								});
							});
						}
					})
				});
			} else {
				db.transaction(function(x) {
					x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ? AND item_troca = ? ORDER BY produto_descricao, pedido_numero_item', [codigo_do_pedido, codigo_da_empresa, '0'], function(x, dados) {
						if (dados.rows.length) {
							construirPedidoImpressao({info: info, dados: dados}, function(){
								var imp = new Impressao();
								
								imp.imprimir(localStorage.getItem('pedidoImpressao'));
							});
						}
					});
				});
			}
		} else { //Pedido processado
			// Obter Pedido Processado
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ? ORDER BY ip_codigo_item DESC', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
					if (dados.rows.length) {
						construirPedidoImpressao({info: info, dados: dados}, function(){
							var imp = new Impressao();
							
							imp.imprimir(localStorage.getItem('pedidoImpressao'));
						});
					}
				})
			});
		}
	}
});


function buscar_cliente_cnpj(dados_pedido, converter_pedido, callback) {
	if(dados_pedido.cliente_cpf) {
		var cpf = dados_pedido.cliente_cpf;
		var tabela = 'clientes';
		var where = ' WHERE cpf = "' + cpf + '"';
	} else {
		if(converter_pedido){
			var cgc = dados_pedido.prospect_cgc;
			var tabela = 'clientes';
			var where = ' WHERE cpf = "' + cgc + '"';
		} else {
			var cpf = dados_pedido.prospect_cgc;
			var tabela = 'prospects';
			var where = ' WHERE cgc = "' + cpf + '"';
		}
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM ' + tabela + where, [], function(x, dados){
			//Dados
			var dados_cliente = dados.rows.item(0);
			
			callback(dados_pedido, dados_cliente, tabela);
		})
	});
}

function copiar_pedido(codigo_do_pedido, codigo_da_empresa, editar, tabela, converter_pedido_orcamento) {
	
	setarCabecalho(codigo_do_pedido, codigo_da_empresa, editar, tabela, converter_pedido_orcamento);
	
}


function copiar_pedido_processado(codigo_do_pedido, codigo_da_empresa) {
	var tabela =  "pedidos_processados";
	setarCabecalhoProcessado(codigo_do_pedido, codigo_da_empresa, false, tabela);
	/*
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ?', [codigo_do_pedido, codigo_da_empresa], 
			function(x, dados) {
				
				var dado = dados.rows.item(0);
				
				//Cabeçalho pedido
				var pedido_copiado = [];
				
				pedido_copiado['estado_filial']					= 'SP';
				pedido_copiado['codigo_cliente'] 				= dado.cliente_codigo;
				pedido_copiado['loja_cliente']					= dado.cliente_loja;
				pedido_copiado['codigo_cliente_entrega']		= dado.pedido_cliente_entrega;
				pedido_copiado['loja_cliente_entrega']			= dado.pedido_loja_entrega;
				pedido_copiado['codigo_transportadora']			= dado.transportadora_codigo;
				pedido_copiado['condicao_pagamento']			= dado.cliente_condicao_pagamento;
				pedido_copiado['data_entrega']					= (dado.pedido_data_entrega ? protheus_data2data_normal(dado.pedido_data_entrega) : '');
				pedido_copiado['desconto_cliente']				= dado.cliente_desconto;
				pedido_copiado['descricao_cliente']				= dado.cliente_nome;
				pedido_copiado['grupo_tributacao_cliente']		= dado.cliente_grupo_tributacao;
				pedido_copiado['estado_cliente']				= dado.cliente_estado;
				pedido_copiado['descricao_cliente_entrega']		= dado.cliente_codigo + '/' + dado.cliente_loja + ' - ' + dado.cliente_nome + ' - ' + dado.cliente_cpf;
				pedido_copiado['filial']						= dado.pedido_filial;
				pedido_copiado['mennota']						= utf8_decode(dado.pedido_mensagem);
				pedido_copiado['pedido_cliente']				= dado.ip_pedido_cliente;
				pedido_copiado['tabela_precos']					= dado.pedido_tabela_precos;
				pedido_copiado['tipo_frete']					= dado.cliente_tipo_frete;
				pedido_copiado['tipo_pedido']					= 'N';
				
				pedido_copiado['produtos'] = new Array();
				
				for (var i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					pedido_copiado['produtos'][dado.ip_codigo_produto] = new Array();
					
					pedido_copiado['produtos'][dado.ip_codigo_produto]['codigo'] 					= dado.ip_codigo_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['desconto'] 					= 0;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['descricao'] 				= dado.ip_descricao_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['ipi'] 						= dado.produto_ipi;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['preco_unitario'] 			= round((dado.produto_converter > 0 ? dado.ip_preco_produto * dado.produto_converter : dado.ip_preco_produto), 3);
					pedido_copiado['produtos'][dado.ip_codigo_produto]['preco_venda'] 				= round((dado.produto_converter > 0 ? dado.ip_preco_produto * dado.produto_converter : dado.ip_preco_produto), 3);
					pedido_copiado['produtos'][dado.ip_codigo_produto]['quantidade'] 				= (dado.produto_converter > 0 ? dado.ip_quantidade_vendida_produto / dado.produto_converter : dado.ip_quantidade_vendida_produto);
					pedido_copiado['produtos'][dado.ip_codigo_produto]['local'] 					= dado.ip_local;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['peso_total'] 				= dado.produto_peso_total;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['valor_unidade'] 			= dado.ip_preco_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['valor_produto_converter'] 	= dado.produto_converter;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['preco_tabela']				= round(dado.ip_preco_produto, 3);
					pedido_copiado['produtos'][dado.ip_codigo_produto]['unidade_medida']		 	= dado.ip_unidade_medida_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['segunda_unidade_medida']	= dado.produto_segunda_unidade_medida;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['unidade_medida_venda']		= dado.produto_segunda_unidade_medida;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['grupo_tributacao']			= dado.produto_grupo_tributacao;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['item_troca']				= '0';
				}
				
				sessionStorage['sessao_pedido'] = serialize(pedido_copiado);
				
				window.location = "pedidos_adicionar.html";  
			}
		)
	});
	
	*/
}


/**
* Metódo:		aplicar_descontos_cabecalho
* 
* Descrição:	Função Utilizada para aplicar os descontos do cabeçalho (Desconto do Cliente, Regra de Desconto)
* 
* Data:			23/10/2013
* Modificação:	23/10/2013
* 
* @access		public
* @param		string 					preco_produto		- Preço do Produto
* @param		string 					desconto_cliente	- Desconto do cliente
* @param		string 					regra_desconto		- Desconto da regra
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function aplicar_descontos_cabecalho(preco_produto, desconto_cliente, regra_desconto) {
	// Aplicando Desconto do "Cliente" no preço do produto
	if(desconto_cliente > 0) {
		var preco = preco_produto - (preco_produto * (desconto_cliente / 100));
	} else {
		var preco = preco_produto;
	}

	// Aplicando Desconto da "Regra de desconto" no preço do produto
	if(regra_desconto  > 0) {
		var preco = preco - (preco * (regra_desconto / 100));
	}
	
	return preco;
}




//---- Pedido Processado

function setarCabecalhoProcessado(codigo_pedido, codigo_empresa, editar, tabela, converter_pedido_orcamento){
	
	plugins.waitingDialog.mostrarDialog("Aguarde carregando pedido...");
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_codigo = ? AND pedido_filial = ?', [codigo_pedido, codigo_empresa], function(x, dados) {
			var cabecalho = dados.rows.item(0);
			
			buscar_cliente_cnpj(cabecalho, converter_pedido_orcamento, function(dado, dadosClienteProspect, clienteProspect){
				dadosCabecalho = {
					
					converter: converter_pedido_orcamento ? codigo_pedido : '0',
					
					edicao: {
						editado: editar,
						codigo_pedido: codigo_pedido
					},
					
					analitico: {
						percentual: dado.pedido_analitico_perc,
						valor: dado.pedido_analitico_valor
					},
					
					autorizado: dado.cliente_autorizado,
					
					condicao_pagamento: {
						codigo: dado.forma_pagamento_codigo,
						descricao: dado.forma_pagamento_descricao,
						desconto: dado.pedido_desconto2
					},
					
					data_entrega: (dado.pedido_data_entrega ? protheus_data2data_normal(dado.pedido_data_entrega) : ''),
					
					evento: {
						id: dado.pedido_id_feira
					},
					
					filial: {
						codigo: dado.pedido_filial,
						estado: ''
					},
					
					forma_pagamento: {
						codigo: dado.pedido_formas_de_pagamento,
						descricao: ''
					},
					
					frete: {
						codigo: dado.pedido_tipo_frete,
						dirigido_redespacho: dado.pedido_dirigido_redespacho
					},
					
					observacao_comercial: utf8_decode(dado.pedido_mensagem),
					
					ordem_compra: dado.ip_pedido_cliente,
					
					tabela_precos: {
						codigo: dado.pedido_tabela_precos,
						descricao: ''
					},
					
					tipo_movimentacao: {
						codigo: dado.pedido_tipo_movimentacao,
						descricao: ''
					},
					
					tipo_pedido: {
						codigo: (tabela == 'orcamentos' ? '*' : dado.pedido_tipo),
						descricao: ''
					},
					
					transportadora: {
						codigo: dado.pedido_codigo_transportadora,
						nome: dado.transportadora_nome
					}
					
				};
				
				
				
				
				if(clienteProspect == 'clientes'){
					dadosCabecalho.cliente = dadosClienteProspect;
					dadosCabecalho.cliente_entrega = dadosClienteProspect;
				} else {
					dadosCabecalho.prospect = dadosClienteProspect;
					dadosCabecalho.prospect_entrega = dadosClienteProspect;
				}
				
				var Pedido = new Pedidos();
				Pedido.setCabecalho(dadosCabecalho, function(retorno){
					setarProdutosProcessados(dados, false, function(){
						var tipo_pedido = 'P';
						if(tabela == 'orcamentos' && !converter_pedido_orcamento){
							tipo_pedido = 'O';
						}
						plugins.waitingDialog.fecharDialog();
						
						window.location = "pedidos_adicionar.html#" + tipo_pedido;
					});
				});
			});
		});
	});
}

function setarProdutosProcessados(dados, i, callback){	
	if(!i){
		i = 0;
	}
	
	var dado = dados.rows.item(i);
	
	var Produto = new Produtos();
	Produto.getDadosProduto(dado.pedido_filial, dado.ip_codigo_produto, dado.pedido_tabela_precos, function(dadosProduto){
				
		var preco_tabela_unitario 	=	 dadosProduto.ptp_preco;
		var preco_venda_unitario 	= 	dado.ip_preco_produto;
		console.log(preco_tabela_unitario+' - '+preco_venda_unitario );
		
		var desconto = 100- ((preco_venda_unitario *100)/ preco_tabela_unitario)
		
		dadosProduto.item_troca				= '0';
		//dadosProduto.valor_unidade			= round((dado.produto_converter > 0 ? dado.ip_preco_produto * dado.produto_converter : dado.ip_preco_produto), 3);
		dadosProduto.quantidade				= (dado.produto_converter > 0 ? dado.ip_quantidade_vendida_produto / dado.produto_converter : dado.ip_quantidade_vendida_produto).toString();
		dadosProduto.desconto				= desconto;
		dadosProduto.unidade_medida_venda	= dado.produto_segunda_unidade_medida;
		dadosProduto.preco_venda			= round((dado.produto_converter > 0 ? dado.ip_preco_produto * dado.produto_converter : dado.ip_preco_produto), 3);
		
		var params = {
			estado_filial				: 'SP',
			codigo_filial				: dado.pedido_filial,
			estado_cliente				: dado.cliente_estado,
			grupo_tributacao_cliente	: dado.cliente_grupo_tributacao,
			produto						: dadosProduto
		};
		console.log('calculando impostos');
		Produto.getValorImpostos(params, function(impostos){
			dadosProduto.cf			= impostos.cf;
			dadosProduto.tes		= impostos.tes;
			dadosProduto.valor_st	= impostos.st;
			dadosProduto.valor_ipi	= impostos.ipi;
			dadosProduto.valor_icms	= impostos.icms;
			
			
			
			console.log(JSON.stringify(dadosProduto));
			
			Produto.setProduto(dadosCabecalho, dadosProduto, function(retorno){
				console.log(typeof(retorno)+' - '+ retorno);
				
				if(i + 1 == dados.rows.length){
					callback();
				} else {
					setarProdutosProcessados(dados, i + 1, callback);
				}
			});
		});
	});
}



function setarCabecalho(codigo_pedido, codigo_empresa, editar, tabela, converter_pedido_orcamento){
	
	plugins.waitingDialog.mostrarDialog("Aguarde carregando pedido...");
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ?', [codigo_pedido, codigo_empresa], function(x, dados) {
			var cabecalho = dados.rows.item(0);
			
			buscar_cliente_cnpj(cabecalho, converter_pedido_orcamento, function(dado, dadosClienteProspect, clienteProspect){
				dadosCabecalho = {
					
					converter: converter_pedido_orcamento ? codigo_pedido : '0',
					
					edicao: {
						editado: editar,
						codigo_pedido: codigo_pedido
					},
					
					analitico: {
						percentual: dado.pedido_analitico_perc,
						valor: dado.pedido_analitico_valor
					},
					
					autorizado: dado.pedido_autorizado,
					
					condicao_pagamento: {
						codigo: dado.forma_pagamento_codigo,
						descricao: dado.forma_pagamento_descricao,
						desconto: dado.pedido_desconto2
					},
					
					data_entrega: dado.pedido_data_entrega,
					
					evento: {
						id: dado.pedido_id_feira
					},
					
					filial: {
						codigo: dado.pedido_filial,
						estado: ''
					},
					
					forma_pagamento: {
						codigo: dado.pedido_formas_de_pagamento,
						descricao: ''
					},
					
					frete: {
						codigo: dado.pedido_tipo_frete,
						dirigido_redespacho: dado.pedido_dirigido_redespacho
					},
					
					observacao_comercial: dado.pedido_observacao_comercial,
					
					ordem_compra: dado.pedido_pedido_cliente,
					
					tabela_precos: {
						codigo: dado.pedido_tabela_precos,
						descricao: ''
					},
					
					tipo_movimentacao: {
						codigo: dado.pedido_tipo_movimentacao,
						descricao: ''
					},
					
					tipo_pedido: {
						codigo: (tabela == 'orcamentos' ? '*' : dado.pedido_tipo_venda),
						descricao: ''
					},
					
					transportadora: {
						codigo: dado.pedido_codigo_transportadora,
						nome: dado.transportadora_nome
					}
					
				};
				
				if(clienteProspect == 'clientes'){
					dadosCabecalho.cliente = dadosClienteProspect;
					dadosCabecalho.cliente_entrega = dadosClienteProspect;
				} else {
					dadosCabecalho.prospect = dadosClienteProspect;
					dadosCabecalho.prospect_entrega = dadosClienteProspect;
				}
				
				var Pedido = new Pedidos();
				Pedido.setCabecalho(dadosCabecalho, function(retorno){
					setarProdutos(dados, false, function(){
						var tipo_pedido = 'P';
						if(tabela == 'orcamentos' && !converter_pedido_orcamento){
							tipo_pedido = 'O';
						}
						plugins.waitingDialog.fecharDialog();
						
						window.location = "pedidos_adicionar.html#" + tipo_pedido;
					});
				});
			});
		});
	});
}

function setarProdutos(dados, i, callback){
	if(!i){
		i = 0;
	}
	
	var dado = dados.rows.item(i);
	
	var Produto = new Produtos();
	Produto.getDadosProduto(dado.pedido_filial, dado.pedido_codigo_produto, dado.pedido_tabela_precos, function(dadosProduto){
		
		dadosProduto.item_troca				= dado.item_troca;
		dadosProduto.valor_unidade			= dado.pedido_valor_unidade;
		dadosProduto.quantidade				= dado.pedido_quantidade;
		dadosProduto.desconto				= dado.pedido_desconto_item;
		dadosProduto.unidade_medida_venda	= dado.pedido_unidade_medida;
		dadosProduto.preco_venda			= dado.pedido_preco_venda;
		
		var params = {
			estado_filial				: 'SP',
			codigo_filial				: dado.pedido_filial,
			estado_cliente				: dado.cliente_estado,
			grupo_tributacao_cliente	: dado.cliente_grupo_tributacao,
			produto						: dadosProduto
		};
		
		Produto.getValorImpostos(params, function(impostos){
			dadosProduto.cf			= impostos.cf;
			dadosProduto.tes		= impostos.tes;
			dadosProduto.valor_st	= impostos.st;
			dadosProduto.valor_ipi	= impostos.ipi;
			dadosProduto.valor_icms	= impostos.icms;
			
			Produto.setProduto(dadosCabecalho, dadosProduto, function(retorno){
				if(i + 1 == dados.rows.length){
					callback();
				} else {
					setarProdutos(dados, i + 1, callback);
				}
			});
		});
	});
}