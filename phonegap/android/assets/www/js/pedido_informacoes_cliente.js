$(document).ready(function(){
	
	var orcamento = parse_url(location.href).fragment;
	
	if(!localStorage.getItem('data_atualizacao_estoque')) {
		localStorage.setItem('data_atualizacao_estoque', 0);
	}
	
	// Salvando o Tipo de Sessaão (Orçamento ou pedido)
	if(orcamento == 'O') {
		
		$('#adicionar_produto_troca').hide();
		
		sessionStorage['sessao_tipo'] = 'sessao_orcamento';
		
		// Atalho de criar orçamento no espelho do prospect (retornar prospect selecionado)
		if(sessionStorage[sessionStorage['sessao_tipo']]) {
			var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
			
			if(sessao_pedido.codigo_prospect) {
				$('select[name="tabela_precos"]').removeAttr('disabled');
			}
		}
		
		// Atalho de criar orçamento no espelho do prospect
		// Trocando a descrição de Pedido para Orçamento
		$('.descricao_pedido').html('Orçamento');
		
		// Exibindo as opções de Cliente e Prospect
		$('#cliente_prospect').show();
	} else {
		sessionStorage['sessao_tipo'] = 'sessao_pedido';
	}

	// Ativando Biblioteca "TABS"
	$( "#tabs" ).tabs();

	// Desativar todos os campos quando clicar em id_informacao_cliente
	$('#id_informacao_cliente').click(function(){
		
		$('#id_adicionar_produtos').addClass('tab_desativada');
		$('#id_outras_informacoes').addClass('tab_desativada');
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',0);
		
		ativar_tabs();
	});
	
	ativar_tabs();

	obter_filiais();
	
	obter_tipo_pedido();
	
	if(obter_valor_sessao('filial')) {
		obter_clientes(obter_valor_sessao('filial'));		
		obter_prospects(obter_valor_sessao('filial'));
	}
	
	/**
	* 
	* Classe:		select_gravar_sessao
	*
	* Descrição:	Classe utilizada para "select", com a finalizade de gravar na sessão o valor do campo quando o usuário usar o "change"
	*
	*/
	$('.select_gravar_sessao').live('change', function(){
		salvar_sessao($(this).attr('name'), $(this).val());
		
		if($(this).attr('name') == 'filial') {
			var estado_filial = $('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').data('estado');
			salvar_sessao('estado_filial', estado_filial);
			
			obter_clientes($(this).val());
			
			obter_prospects($(this).val());
		}
		
		if($(this).attr('name') == 'tipo_movimentacao'){
			obter_formas_pagamento();
		}
	});
	
	obter_tabela_precos();
	
	obter_condicao_pagamento();
	
	obter_formas_pagamento();
	
	$('select[name="tipo_movimentacao"]').change();
	
	remover_disabled();
	
	/**
	* 
	* ID:			trocar_cliente
	*
	* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
	*
	*/
	$('#trocar_cliente').live('click', function(){
		
		salvar_sessao('descricao_cliente', null);
		salvar_sessao('codigo_cliente', null);
		salvar_sessao('loja_cliente', null);
		
		$('input[name="cliente"]').val("");
		$('input[name="codigo_cliente"]').val("");
		$('input[name="loja_cliente"]').val("");
		
		rotina_cliente();
		
		$(this).hide();
		$('#info_cli').hide();
	});
	
	/**
	* 
	* ID:			trocar_prospect
	*
	* Descrição:	Utilizado para apagar todos os dados do prospect na sessão, e forçar que o representante selecione outro prospect
	*
	*/
	$('#trocar_prospect').live('click', function(){
		
		salvar_sessao('descricao_prospect', null);
		salvar_sessao('codigo_prospect', null);
		salvar_sessao('loja_prospect', null);
		
		$('input[name="prospect"]').val("");
		$('input[name="codigo_prospect"]').val("");
		$('input[name="loja_prospect"]').val("");
		
		//-----------------------------------------------
		
		$('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
		$('select[name="tabela_precos"]').attr('disabled', 'disabled');
		
		$('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
		$('select[name="condicao_pagamento"]').change();
		$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
		
		//-----------------------------------------------
		
		$(this).hide();
		$('#info_pro').hide();
	});
	
	$('select[name="cliente_prospect"]').live('change', function(){
			
		if($(this).val() == 'P') {
			$('#campo_cliente').hide();
			$('#campo_prospect').show();
			
			//Limpando Dados do Cliente e Prospect
			$('#trocar_cliente').click();
			$('#trocar_cliente_entrega').click();
		} else {
			$('#campo_prospect').hide();
			$('#campo_cliente').show();
			
			//Limpando Dados do Cliente e Prospect
			$('#trocar_prospect').click();
		}
	});

	$('select[name=tipo_pedido]').live('change', function(){
		var valor = $(this).val();
		
		if(valor == 'I' || valor == 'B') {
			$('select[name="condicao_pagamento"] option[value="099"]').attr('selected', 'selected');
			$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
		} else {
			$('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
			
			if(obter_valor_sessao('tabela_precos')) {
				$('select[name="condicao_pagamento"]').removeAttr('disabled');
			}
		}
		
		$('select[name="condicao_pagamento"]').change();
	});
	
	$('select[name="condicao_pagamento"]').live('change', function(){
		var valor = $(this).val();
		
		//Condição de Pgto (A VISTA ou 7 DIAS) permite o desconto de 2%
		if(valor == '001' || valor == '019') {	
			$('#desconto_condpag').show();
			$('select[name="desconto_condpag"] option[value="2,00"]').attr('selected', 'selected');
		} else {
			$('#desconto_condpag').hide();
			$('select[name="desconto_condpag"] option[value="0,00"]').attr('selected', 'selected');
		}
		
	});
	
	// Verificar Títulos de Vencimentos
	function verificar_titulos_vencidos(parametros, callback) {
		
		if(parametros.codigo != undefined && parametros.loja != undefined && orcamento != 'O') {
			
			db.transaction(function(x) {
				x.executeSql('SELECT nome, data_titulo_vencido, autorizado FROM clientes WHERE codigo = ? AND loja = ?', [parametros.codigo, parametros.loja], function(x, dados) {
					
					if(dados.rows.length) {
						var dado = dados.rows.item(0);
						var validar_bloqueio = true;
						var data_bloqueio = date('Ymd', diminuir_dias_uteis(time(), 3));
						
						//Chamado 002238 - Desabilitar regra de titutlos em aberto
						//Removida a validação termporariamente
						validar_bloqueio = false;
						
						
						//Nunca realiza o bloqueio para compras com CHEQUE, A VISTA e BONIFICAÇÃO
						if(in_array(parametros.formas_de_pagamento, ['CH', 'VIS', 'BON'])){
							validar_bloqueio = false;
						}else if(in_array(parametros.formas_de_pagamento, ['BOL', 'CAR', 'VA'])) {
							//Realiza o bloqueio somente 3 dias úteis após o vencimento
							if(data_bloqueio < dado.data_titulo_vencido) {
								validar_bloqueio = false;
							}
						}
						
						//Verifica se o cliente/pedido passa pela validação de bloqueio
						if(validar_bloqueio) {
							
							if(!dado.data_titulo_vencido) {
								salvar_sessao('autorizado', 'N');
								
								callback();
							} else if(dado.autorizado == 'S') { //Autorização de Emergência
								salvar_sessao('autorizado', 'S');
								
								callback();
							} else {
								mensagem('Não é possível incluir pedido para o cliente <strong>' + dado.nome + '</strong>. <br /><br />O cliente possui títulos vencidos.');
							}
							
						} else {
							
							salvar_sessao('autorizado', 'N');
							callback();
							
						}
						
					}
					
					//----------------------------
						
				});
			});
			
		} else {
			callback();
		}
	}
	
	//VERIFICAR VERBA INTRODUTORIA
	function verificar_verba_introdutoria(parametros, callback) {
		db.transaction(function(x) {
			x.executeSql('SELECT nome, verba_introdutoria FROM clientes WHERE codigo = ? AND loja = ?', [parametros.codigo, parametros.loja], function(x, dados) {
				
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					
					if(parseFloat(dado.verba_introdutoria) > 0) {
						callback();
					} else {
						mensagem('Não é possível incluir pedido do tipo <b>Verba Introdutória</b> para o cliente <strong>' + dado.nome + '</strong>. <br /><br />O cliente não possui saldo.');
					}
				}
			});
		});
	}
	
	//VERIFICAR VERBA MENSAL
	function verificar_verba_mensal(callback) {
		db.transaction(function(x) {
			x.executeSql('SELECT verba_mensal FROM representante WHERE codigo = ?', [info.cod_rep], function(x, dados) {
				
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					
					if(parseFloat(dado.verba_mensal) > 0) {
						callback();
					} else {
						mensagem('Não é possível incluir pedido do tipo <b>Verba Mensal</b>. <br /><br />Você não possui saldo.');
					}
				}
			});
		});
	}
	
	/**
	* 
	* CLASSE:		avancar
	*
	* Descrição:	Utilizado para validar os campos, se estiver tudo correto, passar para a proxima ABA
	*
	*/
	$('#avancar_passo_1').live('click', function(e){
		
		e.preventDefault();

		if(sessionStorage[sessionStorage['sessao_tipo']]) {
			var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		} else {
			var sessao_pedido = [];
		}
		
		// Verificar Títulos Vencidos
		verificar_titulos_vencidos({
			codigo: 				sessao_pedido.codigo_cliente,
			loja: 					sessao_pedido.loja_cliente,
			formas_de_pagamento : 	sessao_pedido.formas_de_pagamento
		}, function(){

			if(sessao_pedido.tipo_pedido == 'I') { // Introdutoria
				verificar_verba_introdutoria({
					codigo: sessao_pedido.codigo_cliente,
					loja: sessao_pedido.loja_cliente
				}, function(){
					avancar_passo_1(sessao_pedido);
				});
			} else if(sessao_pedido.tipo_pedido == 'B') { // Mensal
				verificar_verba_mensal(function(){
					avancar_passo_1(sessao_pedido);
				});
			} else { // Pedido Normal
				avancar_passo_1(sessao_pedido);
			}
			
		});
		
	});
	
	//Função com as verificacoes dos dados 
	function avancar_passo_1(sessao_pedido) {

		if(!sessao_pedido.filial) {
			mensagem('Selecione uma <strong>Filial</strong>.');
		} else if(!sessao_pedido.tipo_pedido) {
			mensagem('Selecione um <strong>Tipo de Pedido</strong>.');
		} else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido') {
			mensagem('Selecione um <strong>Cliente</strong>.');
		} else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null)) {
			mensagem('Selecione um <strong>Cliente</strong>.');
		} else if(!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P') {
			mensagem('Selecione um <strong>Prospect</strong>.');
		} else if(!sessao_pedido.tabela_precos) {
			mensagem('Selecione uma <strong>Tabela de Preços</strong>.');
		} else if(!sessao_pedido.condicao_pagamento) {
			mensagem('Selecione uma <strong>Condição de Pagamento</strong>.');
		} else if(!validar_decimal($('select[name=desconto_condpag]').val())) {
			mensagem('O valor "<b>' + $('select[name=desconto_condpag]').val() + '</b>" no campo <b>Desconto</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>1.100,250<b/>');
		} else if(!sessao_pedido.formas_de_pagamento) {
			mensagem('Selecione uma <strong>Forma de Pagamento</strong>.');
		} else if(!sessao_pedido.tipo_movimentacao) {
			mensagem('Selecione um <strong>Tipo de Movimentação</strong>.');
		} else {
			
			//-----------------------------------
			//	Desconto da Condição de Pagamento
			//Condição de Pgto (A VISTA ou 7 DIAS) permite o desconto de 2%
			if(sessao_pedido.condicao_pagamento == '001' || sessao_pedido.condicao_pagamento == '019') {
				salvar_sessao('desconto_condicao_pagamento', converter_decimal($('select[name=desconto_condpag]').val()));
			} else {
				salvar_sessao('desconto_condicao_pagamento', '0');
			}
			//	Desconto da Condição de Pagamento
			//-----------------------------------
			
			salvar_dados_forma_pagamento(sessao_pedido.condicao_pagamento);
			
			salvar_dados_cliente(sessao_pedido.codigo_cliente, sessao_pedido.loja_cliente);
			
			salvar_dados_prospect(sessao_pedido.codigo_prospect, sessao_pedido.loja_prospect);
			
			// Ativando Botao "Adicionar Produtos"
			$('#id_adicionar_produtos').removeClass('tab_desativada');
			
			ativar_tabs();
			
			//Chamando as funções do arquivo adicionar produtos
			adicionar_produtos();
			
			// Acionando (Click) botao "Adicionar Produtos"
			$("#id_adicionar_produtos").click();
			
			$('input[name="codigo_palm"]').focus();
		}
		
	};
	
	$('.cancelar').click(function(e){
		e.preventDefault();
		
		confirmar('Deseja cancelar esse pedido?', 
			function () {
				$(this).dialog('close');
				
				sessionStorage[sessionStorage['sessao_tipo']] = '';
				location.reload();
				
				$("#confirmar_dialog").remove();
			}
		);
	});
	

});


/**
* Metódo:		ativar_tabs
* 
* Descrição:	Função Utilizada ativar a biblioteca, e desativar tabs com a classe tab_desativada
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function ativar_tabs()
{
	// Desativando tabs com classe tab_desativada
	var tabs_desativar = [];
			
	$( "#tabs ul li a" ).each(function(e, i) {
		var classname = i.className;
		
		if(classname === 'tab_desativada') {
			tabs_desativar.push(e);
		}
	});
	
	$("#tabs").tabs({disabled: tabs_desativar});
}


/**
* Metódo:		obter_filiais
* 
* Descrição:	Função Utilizada para retornar as filiais
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_filiais() {
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM filiais', [], function(x, dados) {
			var total = dados.rows.length;
			
			$('select[name=filial]').append('<option value="">Selecione...</option>');
			
			for(i = 0; i < total; i++) {
				var dado = dados.rows.item(i);
				$('select[name=filial]').append('<option value="' + dado.codigo + '" data-estado="' + dado.estado + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
			}
			
			// Selecionar campo se existir na sessão
			if(obter_valor_sessao('filial')) {
				$('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
			} else {
				selecionar_filial_padrao();
			}
		});
	});
}



/**
* Metódo:		obter_tabela_precos
* 
* Descrição:	Função Utilizada para retornar Tabelas Preços
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tabela_precos() {
	var where = " WHERE codigo > 0 ";
	
	if(info.empresa) {
		where += " AND empresa = '" + info.empresa + "' ";
	}

	//DATA INICIO DE VIGENCIA DA TABELA DE PREÇO
	where += " AND vigencia_inicio <= '" + date('Ymd') + "'";
	
	//DATA FINAL DE VIGENCIA DA TABELA DE PREÇO
	where += " AND (vigencia_final >= '" + date('Ymd') + "' OR vigencia_final = '')";
	
	db.transaction(function(x) {	
		x.executeSql('SELECT * FROM tabelas_preco ' + where, [], function(x, dados) {
			
			var total = dados.rows.length;
			
			$('select[name=tabela_precos]').append('<option value="">Selecione...</option>');
			
			for(i = 0; i < total; i++) {
				var dado = dados.rows.item(i);
				$('select[name=tabela_precos]').append('<option value="' + dado.codigo + '" data-condicao_pagamento="' + dado.condicao_pagamento + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
			}
			
			rotina_tabelas_preco();
			
		});
	});
}


/**
* Metódo:		obter_condicao_pagamento
* 
* Descrição:	Função Utilizada para retornar Condições de pagamento
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_condicao_pagamento() {
	if(info.empresa) {
		var where = " WHERE empresa = '" + info.empresa + "'";
	}

	db.transaction(function(x) {	
		x.executeSql('SELECT * FROM formas_pagamento ' + where, [], function(x, dados) {
			
			var total = dados.rows.length;
			
			$('select[name=condicao_pagamento]').append('<option value="">Selecione...</option>');
			
			for(i = 0; i < total; i++) {
				var dado = dados.rows.item(i);
				$('select[name=condicao_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
			}
			
			// Selecionar campo se existir na sessão
			var condicao_pagamento = obter_valor_sessao('condicao_pagamento');
			if(condicao_pagamento) {
				$('select[name="condicao_pagamento"] option[value="' + condicao_pagamento + '"]').attr('selected', 'selected');
				$('select[name="condicao_pagamento"]').change();
			}
			
		});
	});
}


/**
* Metódo:		obter_formas_pagamento
* 
* Descrição:	Função Utilizada para retornar Formas de pagamento
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_formas_pagamento() {
	
	if(info.empresa) {
		var where = " WHERE empresa = '" + info.empresa + "'";
	}
	
	//Restrição de formas de pagamento de acordo com o tipo de movimentação
	if($('[name=tipo_movimentacao]').val() == '00') {
		where += ' AND chave IN ("VIS", "CH", "BON", "VA")';
	} else {
		where += ' AND chave IN ("VIS", "CH", "BOL", "BON", "CAR")';
	}
	
	db.transaction(function(x) {	
		x.executeSql('SELECT * FROM formas_pagamento_real ' + where, [], function(x, dados) {
			
			var total = dados.rows.length;
			
			$('select[name=formas_de_pagamento]').html('<option value="">Selecione...</option>');
			
			for(i = 0; i < total; i++) { 
				var dado = dados.rows.item(i);
				$('select[name=formas_de_pagamento]').append('<option value="' + dado.chave + '">' + dado.descricao + '</option>');
			}
			
			// Selecionar campo se existir na sessão
			var forma_pagamento = obter_valor_sessao('formas_de_pagamento');
			if(forma_pagamento) {
				$('select[name="formas_de_pagamento"] option[value="' + forma_pagamento + '"]').attr('selected', 'selected');
				$('select[name="formas_de_pagamento"]').change();
			}
			
		});
	});
}


/**
* Metódo:		obter_tipo_pedido
* 
* Descrição:	Função Utilizada para retornar Tipos de pedido
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tipo_pedido() {

	$('select[name=tipo_pedido]').html('');
	$('select[name=tipo_pedido]').append('<option value="">Selecione...</option>');
	
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento') {
		$('select[name=tipo_pedido]').append('<option value="*">Orçamento</option>');
	} else {
		$('select[name=tipo_pedido]').append('<option value="N">Venda Normal</option>');
		$('select[name=tipo_pedido]').append('<option value="I">Verba Introdutória</option>');
		$('select[name=tipo_pedido]').append('<option value="B">Verba Mensal</option>');
	}
	
	// Selecionar campo se existir na sessão
	var tipo_pedido = obter_valor_sessao('tipo_pedido');
	if(tipo_pedido)
	{
		$('select[name="tipo_pedido"] option[value="' + tipo_pedido + '"]').attr('selected', 'selected');
	}
}


/**
* Metódo:		obter_clientes
* 
* Descrição:	Função Utilizada para retornar Todos os Clientes
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_clientes(filial) {
	if(info.empresa) {
		var wheres = " WHERE empresa = '" + info.empresa + "'";
	}
	
	if(filial) {
		wheres += " AND (filial = '" + filial + "' OR filial = '')";
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM clientes ' + wheres, [], function(x, dados) {
			if (dados.rows.length) {
				var clientes = [];
				
				for (i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					clientes.push({ label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf, codigo: dado.codigo, loja: dado.loja, tabela_preco: dado.tabela_preco, desconto: dado.desconto, condicao_pagamento: dado.condicao_pagamento, estado: dado.estado, grupo_tributacao: dado.grupo_tributacao});
				}
				
				buscar_clientes(clientes);
				
				// Selecionar campo se existir na sessão
				var codigo_cliente = obter_valor_sessao('codigo_cliente');
				var loja_cliente = obter_valor_sessao('loja_cliente');
				if(codigo_cliente) {
					
					$('input[name=cliente]').val(obter_valor_sessao('descricao_cliente'));
					$('input[name=codigo_cliente]').val(codigo_cliente);
					$('input[name=loja_cliente]').val(loja_cliente);
					
					exibir_informacoes_cliente(codigo_cliente, loja_cliente);
					
					// Bloquear campo quando selecionar cliente
					$('input[name=cliente]').attr('disabled', 'disabled');
					$('#trocar_cliente').show();
				}
			}
		});
	});
}

/**
* Metódo:		obter_prospects
* 
* Descrição:	Função Utilizada para retornar Todos os Prospects
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_prospects(filial) {
	
	var wheres = "WHERE status = 'A' OR status IS NULL";
	
	if(info.empresa) {
		wheres += " AND empresa = '" + info.empresa + "' ";
	}
	
	if(filial) {
		wheres += " AND (filial = '" + filial + "' OR filial = '')";
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM prospects ' + wheres, [], function(x, dados) {
			if (dados.rows.length) {
				var prospects = [];
				
				for (i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					prospects.push({ label: dado.codigo + '/' + dado.codigo_loja + ' - ' + dado.nome + ' - ' + dado.cgc, codigo: dado.codigo, loja: dado.codigo_loja, estado: dado.estado});
				}
				
				buscar_prospects(prospects);
				
				// Selecionar campo se existir na sessão
				var codigo_prospect = obter_valor_sessao('codigo_prospect');
				var loja_prospect = obter_valor_sessao('loja_prospect');
				if(codigo_prospect) {
					$('input[name=prospect]').val(obter_valor_sessao('descricao_prospect'));
					$('input[name=codigo_prospect]').val(codigo_prospect);
					$('input[name=loja_prospect]').val(loja_prospect);
					
					exibir_informacoes_prospect(codigo_prospect, loja_prospect);
					
					// Bloquear campo quando selecionar cliente
					$('input[name=prospect]').attr('disabled', 'disabled');
					$('#trocar_prospect').show();
					
					
					//----------------
					// Selecionar a opção prospect se existir na sessao
					$('select[name="cliente_prospect"] option[value="P"]').attr('selected', 'selected');
					$('#campo_cliente').hide();
					$('#campo_prospect').show();
					
					if(obter_valor_sessao('tabela_precos')) {
						$('select[name="tabela_precos"]').removeAttr('disabled');
					}
					
				}
			
			}
		});
	});
	
	
	
	//--------------
	// Selecionar campo se existir na sessão
	if(obter_valor_sessao('codigo_prospect')) {
		$('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
	}

}



/**
* Metódo:		buscar_prospects
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_prospects(prospects) {
	
	$('input[name=prospect]').autocomplete({
		minLength: 3,
		source: prospects,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=prospect]').val(ui.item.label);
			$('input[name=codigo_prospect]').val(ui.item.codigo);
			$('input[name=loja_prospect]').val(ui.item.loja);
			$('input[name=estado_prospect]').val(ui.item.estado);
			
			salvar_sessao('descricao_prospect', ui.item.label);
			salvar_sessao('codigo_prospect', ui.item.codigo);
			salvar_sessao('loja_prospect', ui.item.loja);
			salvar_sessao('desconto_prospect', ui.item.desconto);
			salvar_sessao('estado_prospect', ui.item.estado);
			
			
			//--------------
			
			exibir_informacoes_prospect(ui.item.codigo, ui.item.loja);
			
			//--------------
			
			$('select[name=tabela_precos]').removeAttr('disabled', '');
			
			// Bloquear campo quando selecionar prospect
			$('input[name=prospect]').attr('disabled', 'disabled');
			$('#trocar_prospect').show();

			return false;
		}
	});
}


/**
* Metódo:		buscar_clientes
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_clientes(clientes) {
	$('input[name=cliente]').autocomplete({
		minLength: 3,
		source: clientes,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=cliente]').val(ui.item.label);
			$('input[name=codigo_cliente]').val(ui.item.codigo);
			$('input[name=loja_cliente]').val(ui.item.loja);
			$('input[name=estado_cliente]').val(ui.item.estado);
			$('input[name=grupo_tributacao_cliente]').val(ui.item.grupo_tributacao);
			
			salvar_sessao('descricao_cliente', ui.item.label);
			salvar_sessao('codigo_cliente', ui.item.codigo);
			salvar_sessao('loja_cliente', ui.item.loja);
			salvar_sessao('desconto_cliente', ui.item.desconto);
			salvar_sessao('estado_cliente', ui.item.estado);
			salvar_sessao('grupo_tributacao_cliente', ui.item.grupo_tributacao);
			
			//--------------
			// Salvando Cliente de Entrega
			salvar_sessao('descricao_cliente_entrega', ui.item.label);
			salvar_sessao('codigo_cliente_entrega', ui.item.codigo);
			salvar_sessao('loja_cliente_entrega', ui.item.loja);
			//--------------
			
			salvar_sessao('codigo_transportadora', ui.item.condicao_pagamento);
			
			//--------------
			
			exibir_informacoes_cliente(ui.item.codigo, ui.item.loja);
			
			rotina_cliente(ui.item.tabela_preco);
			
			// Bloquear campo quando selecionar cliente
			$('input[name=cliente]').attr('disabled', 'disabled');
			$('#trocar_cliente').show();

			return false;
		}
	});
}

/**
* Metódo:		exibir_informacoes_cliente
* 
* Descrição:	Função Utilizada para exibir informações do cliente
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_cliente(codigo, loja) {
	if(codigo && loja) {
		
		if(info.empresa) {
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
		db.transaction(function(x) {	
			x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ? ' + where, [codigo, loja], function(x, dados) {
				
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					
					var html = '';
					
					$('.info_nome').html(dado.nome);
					$('.info_cpf').html(dado.cpf);

					
					obter_ultimo_pedido_cliente(codigo, loja);

					$('.info_limite_credito').html(number_format(dado.limite_credito, 3, ',', '.'));
					$('.info_titulos_aberto').html(number_format(dado.total_titulos_aberto, 3, ',', '.'));
					$('.info_titulos_vencidos').html(number_format(dado.total_titulos_ventidos, 3, ',', '.'));
					$('.info_credito_disponivel').html(number_format(dado.limite_credito - dado.total_titulos_aberto, 3, ',', '.'));

					$('.info_endereco').html(dado.endereco);
					$('.info_bairro').html(dado.bairro);
					$('.info_cidade').html(dado.cidade);
					$('.info_estado').html(dado.estado);
					
					$('.info_cep').html(dado.cep);
					
					/*Tratamento telefone*/
					var telefone = '';
					if(dado.ddd) {
						telefone = '('+dado.ddd+') ' + dado.telefone;
					} else {
						if(dado.telefone) {
							telefone = dado.telefone;
						}						
					}
					/*Tratamento telefone*/
					$('.info_telefone').html(telefone);
					
					$('#info_cli').show();
					
				}
				
			});
		});
	}
}



/**
* Metódo:		exibir_informacoes_prospect
* 
* Descrição:	Função Utilizada para exibir informações do prospect
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Prospect
* @param		String 			var loja		- Loja do Prospect
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_prospect(codigo, loja) {

	if(codigo && loja) {
		if(info.empresa) {
			var where = " AND empresa = '" + info.empresa + "'";
		}
	
	
		db.transaction(function(x) {	
			x.executeSql('SELECT * FROM prospects WHERE codigo = ? AND codigo_loja = ? ' + where, [codigo, loja], function(x, dados) {
				
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					
					var html = '';
					
					$('.info_nome').html(dado.nome);
					$('.info_cpf').html(dado.cgc);

					$('.info_endereco').html(dado.endereco);
					$('.info_bairro').html(dado.bairro);
					$('.info_cidade').html(dado.nome_municipio);
					$('.info_estado').html(dado.estado);
					
					$('.info_cep').html(dado.cep);
					
					/*Tratamento telefone*/
					var telefone = '';
					if(dado.ddd) {
						telefone = '('+dado.ddd+') ' + dado.telefone;
					} else {
						if(dado.telefone) {
							telefone = dado.telefone;
						}						
					}
					/*Tratamento telefone*/
					$('.info_telefone').html(telefone);
					
					$('#info_pro').show();
					
				}
				
			});
		});
	}
}




/**
* Metódo:		salvar_sessao
* 
* Descrição:	Função Utilizada para salvar dados do pedido na sessão
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		String 			var nome_campo		- Nome do campo que será utilizado na sessao
* @param		String 			var valor			- Valor do campo que será utilizado na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_sessao(nome_campo, valor) {
	var sessao_pedido = [];
	
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	// Adicionar novo valor no array
	sessao_pedido[nome_campo] = valor;
	
	// Gravar dados do Array na sessão
	sessionStorage[sessionStorage['sessao_tipo']] = serialize(sessao_pedido);
	
	remover_disabled();
}

/**
* Metódo:		obter_valor_sessao
* 
* Descrição:	Função Utilizada para retornar o valor de campo da sessão
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var campo		- Nome do campo que será utilizado para retornar o valor na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_valor_sessao(campo) {
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		var sessao = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao) {
		if(sessao[campo]) {
			return sessao[campo];
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/**
* Metódo:		obter_ultimo_pedido_cliente
* 
* Descrição:	Função Utilizada para pegar o ultimo pedido do cliente e exibir os valores nas informações do cliente
* 
* Data:			10/10/2013
* Modificação:	10/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do Cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_ultimo_pedido_cliente(codigo, loja) {
	db.transaction(function(x) {	
		x.executeSql('SELECT SUM(ip_valor_total_item) AS valor_pedido, SUM(ip_total_desconto_item) AS valor_desconto, * FROM pedidos_processados WHERE cliente_codigo = ? AND cliente_loja = ? GROUP BY pedido_codigo ORDER BY pedido_codigo DESC', [codigo, loja], function(x, dados) {
			if(dados.rows.length) {
				$('#conteudo_ultimo_pedido ul').show();
				$('#conteudo_ultimo_pedido div').hide();
				
				var dado = dados.rows.item(0);
				
				$('.info_pedido_data_emissao').html(protheus_data2data_normal(dado.pedido_data_emissao));
				$('.info_pedido_codigo').html(dado.pedido_codigo);
				$('.info_pedido_codigo').attr('href', 'pedidos_espelho.html#' + dado.pedido_codigo + '|' + dado.pedido_filial);
				$('.info_forma_pagamento').html(dado.forma_pagamento_descricao);
				$('.info_valor_pedido').html('R$ ' + number_format(dado.valor_pedido, 3, ',', '.'));
				$('.info_valor_desconto').html('R$ ' + number_format(dado.valor_desconto, 3, ',', '.'));
			} else {
				$('#conteudo_ultimo_pedido ul').hide();
				$('#conteudo_ultimo_pedido div').show();
			}
		});
	});
}


//---------------------------------------------------------------------------
//----------  ----------  Rotinas (Regras) da Incusão do Pedido ----------  ----------
//---------------------------------------------------------------------------

/**
* Metódo:		rotina_cliente
* 
* Descrição:	Função Utilizada para selecionar a tabela de preços do cliente (Se existir, se não seleciona a opção vazia "Selecione..." e bloqueia o campo e condicao_pagamento)
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function rotina_cliente(tabela) {
	if(tabela) {
		$('select[name="tabela_precos"] option[value="' + tabela + '"]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
		
	} else {
		$('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
		$('select[name="tabela_precos"]').attr('disabled', 'disabled');
		
		if(obter_valor_sessao('tipo_pedido') == "N") {
			$('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
		}
		
		$('select[name="condicao_pagamento"]').change();
		$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
	}
}


/**
* Metódo:		rotina_tabelas_preco
* 
* Descrição:	Função Utilizada bucar a condição de pagamento da tabela de preços e depois selecionar a condição de pagamento e bloquea-la (Se não existir condição de pagamento na tabela de preços, o campo de condição de pagamento fica livre para seleção)
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function rotina_tabelas_preco() {
	$('select[name=tabela_precos]').change(function(){
	
		//rotina_tabelas_preco apenas para tipo de pedido "N"
		if(obter_valor_sessao('tipo_pedido') == "N") {
			// Obter condição de pagamento pela tabela de preços
			var codigo_tabela_precos = $(this).val();
			var codigo_condicao_pagamento = null;
			if(codigo_tabela_precos){
				codigo_condicao_pagamento =  trim($(this).children(":selected").data('condicao_pagamento'));
			}
			
			if(!codigo_condicao_pagamento) {
				var condicao_pagamento = obter_valor_sessao('condicao_pagamento');
				if(condicao_pagamento)
				{
					$('select[name="condicao_pagamento"] option[value="' + condicao_pagamento + '"]').attr('selected', 'selected');
				}
			}
			
			// Se existir condição de pagamento selecionar e bloquear campo
			if(codigo_condicao_pagamento) 
			{
				$('select[name="condicao_pagamento"] option[value="' + codigo_condicao_pagamento + '"]').attr('selected', 'selected');
				$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
				
				salvar_sessao('condicao_pagamento', codigo_condicao_pagamento);
				
				$('#aviso_condicao_pagamento').html('A condição de pagamento ' + codigo_tabela_precos + ' é fixa para a tabela de preços ' + codigo_condicao_pagamento + '.');
			} else {
				$('select[name="condicao_pagamento"]').removeAttr('disabled');
				$('#aviso_condicao_pagamento').html('');
			}
		} else {
			$('select[name="condicao_pagamento"]').removeAttr('disabled');
			$('#aviso_condicao_pagamento').html('');
		}
		
		// chamando regra de desconto para salvar na sessao
		regra_desconto();

	});
	
	// Selecionar campo se existir na sessão
	var tabela_precos = obter_valor_sessao('tabela_precos');
	if(tabela_precos) {
		$('select[name="tabela_precos"] option[value="' + tabela_precos + '"]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
	}
	
}

/**
* Metódo:		regra_desconto
* 
* Descrição:	Função Utilizada para salvar o valor da regra de desconto na sessao ( A regra padrão e pela tabela de preços)
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function regra_desconto() {
	db.transaction(function(x) {	
		x.executeSql('SELECT percentual_desconto FROM regra_desconto WHERE tabela_preco = ?', [obter_valor_sessao('tabela_precos')], function(x, dados) {
			if(dados.rows.length) {
				var dado = dados.rows.item(0);
				
				salvar_sessao('regra_desconto', dado.percentual_desconto);
			} else {
				salvar_sessao('regra_desconto', 0);
			}
		});
	});
}


/**
* Metódo:		remover_disabled
* 
* Descrição:	Função Utilizada para habilitar os botoes que estao bloqueados
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function remover_disabled() {
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		
		// Se existir Filial na sessao, habilitar TIPO DE PEDIDO
		if(sessao_pedido.filial) {
			$('select[name=tipo_pedido]').removeAttr('disabled');
		}
		
		// Se existir Tipo de Pedido na sessao, habilitar CLIENTE
		if(sessao_pedido.tipo_pedido && !sessao_pedido.codigo_cliente) {
			$('input[name=cliente]').removeAttr('disabled');
		}
		
		// Se existir Tipo de Pedido na sessao, habilitar Propect
		if(sessao_pedido.tipo_pedido && !sessao_pedido.codigo_prospect) {
			$('input[name=prospect]').removeAttr('disabled');
		}

		//-------------------------------------------------------------------------------------------------------------
		//------------ Desativar botao "Adicionar Produtos" se uma opção nao for selecionada --------------------------
		//-------------------------------------------------------------------------------------------------------------
		
		if(!sessao_pedido.filial) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.tipo_pedido) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		//----------------------------
		//------------------------------------------
		if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido') {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		} else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null)) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		} else if(!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P') {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		//------------------------------------------
		//----------------------------
		
		
		
		if(!sessao_pedido.tabela_precos) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.condicao_pagamento) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		ativar_tabs();
	}
}


/**
* Metódo:		salvar_dados_cliente
* 
* Descrição:	Função Utilizada para salvar dados do cliente
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_cliente(codigo, loja) { 

	if(codigo && loja) {
		db.transaction(function(x) {	
			x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados) {
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					salvar_sessao('dados_cliente', dado);	
				}
			});
		});
	}
}

/**
* Metódo:		salvar_dados_prospect
* 
* Descrição:	Função Utilizada para salvar dados do prospect
* 
* Data:			16/11/2013
* Modificação:	16/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Prospect
* @param		String 			var loja		- Loja do Prospect
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_prospect(codigo, loja) {
	if(codigo && loja) {
		db.transaction(function(x) {	
			x.executeSql('SELECT * FROM prospects WHERE codigo = ? AND codigo_loja = ?', [codigo, loja], function(x, dados) {
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					salvar_sessao('dados_prospect', dado);	
				}	
			});
		});
	}
}


/**
* Metódo:		salvar_dados_forma_pagamento
* 
* Descrição:	Função Utilizada para salvar dados da forma de pagamento
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo forma de pagamento
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_forma_pagamento(codigo) {
	if(codigo) {
		db.transaction(function(x) {	
			x.executeSql('SELECT * FROM formas_pagamento WHERE codigo = ?', [codigo], function(x, dados) {				
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					salvar_sessao('dados_forma_pagamento', dado);	
				}
			});
		});
	}
}

/**
* Metódo:		obter_descricao_pedido
* 
* Descrição:	Função Utilizada para obter a descrição do pedido
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_descricao_pedido(tipo) {
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento') {
		var descricao_tipo_pedido = 'Orçamento';
	} else {
		var descricao_tipo_pedido = 'Pedido';
	}
	
	if(tipo == 'upper') {
		return descricao_tipo_pedido.toUpperCase();
	} else if(tipo == 'lower') {
		return descricao_tipo_pedido.toLowerCase();
	} else {
		return descricao_tipo_pedido;
	}
}