$(document).ready(function () {
	
    document.addEventListener("deviceready", obterArquivos, false);
    
    $('#enviar_selecionados').live('click', function(){
    	
    	$.each($('.pedidos'), function(k, obj) {
    		var selecionado = $(obj).attr('checked');
    		
    		if(selecionado == 'checked') {
    			var codigo_pedido	= $(obj).data('codigo_pedido');
    	    	var data_criacao	= $(obj).data('data_criacao');
    	    	
    	    	enviarPedido(codigo_pedido, data_criacao);
    		}
    	});
    	
    	return false;
    });
    
    
    $('#enviar').live('click', function(){
    	var codigo_pedido	= $(this).data('codigo_pedido');
    	var data_criacao	= $(this).data('data_criacao');
    	
    	enviarPedido(codigo_pedido, data_criacao);
    	
    	return false;
    });
    
});

function obterArquivos() {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
		fileSystem.root.getDirectory(localStorage.getItem('caminho_local') + "Pedidos/", { create: true }, function(directory) {
			var directoryReader = directory.createReader();
			
			//Varre o diretório para verificar os arquivos existentes
	        directoryReader.readEntries(function(entries) {
	            var i;
	            var html = '';
	            
	            $('#lista_logs').empty();
	            
	            if(entries.length > 0) {
	            	
		            for (i = 0; i < entries.length; i++) {
		                var codigo_pedido = entries[i].name.split('_');
		                var data_criacao = codigo_pedido[1].split('.')[0];
		                
		        		html += '<tr>';
		        			html += '<td class="center"><span class="titulo"><input type="checkbox" data-codigo_pedido="' + codigo_pedido[0] + '" data-data_criacao="' + data_criacao + '" class="pedidos" name="enviar[' + codigo_pedido[0].toUpperCase() + ']" /></span></td>';
		        			html += '<td>' + codigo_pedido[0].toUpperCase() + '</td>';
		        			html += '<td class="center">' + protheus_data2data_normal(data_criacao) + '</td>';
		        			html += '<td class="center"><a href="" id="enviar" data-codigo_pedido="' + codigo_pedido[0] + '" data-data_criacao="' + data_criacao + '" class="btn btn-large btn-primary">Enviar</a></td>';
		        		html += '</tr>';
		            }
		            
		            $('#lista_logs').html(html);
	            } else {
	            	$('#lista_logs').html('<tr><td colspan="4">Nenhum arquivo de log encontrado.</td></tr>');
	            }
	        });
		});
	});
}

function enviarPedido(codigo_pedido, data_criacao) {
	var nome_arquivo = codigo_pedido + '_' + data_criacao + '.txt';
	
	$.ajax({
        url : localStorage.getItem('caminho_local') + "Pedidos/" + nome_arquivo,
        dataType: "text",
        success : function (data) {
        	
        	if(data) {
        		
            	$.ajax({
					url: config.ws_url + 'logs/importar',
					type: 'POST',
					data: {
						log: json_encode(data)
					},
					success: function(dados) {
						if(dados.sucesso) {
							mensagem('Log enviado com sucesso.');
						} else {
							mensagem('Falha ao enviar arquivo de Log do pedido <strong>' + codigo_pedido + '</strong>, favor tentar novamente.');
						}
					}
            	});
            	
        	} else {
        		mensagem('Falha ao obter o arquivo de Log do pedido <strong>' + codigo_pedido + '</strong>.');
        	}
        	
            return false;
        },
        error: function(x){
        	mensagem('Falha ao obter o arquivo de Log do pedido <strong>' + codigo_pedido + '</strong>.');
        }
    });
	return false;
}