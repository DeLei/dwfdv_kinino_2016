var url = parse_url(location.href).fragment;
	url = url.split('|');
var PedidoOrcamento = url[0];

var PrePreenchidoClienteProspect = url[1];
var PrePreenchidoCodigo = url[2];
var PrePreenchidoLoja = url[3];

var tabela = 'pedidos_pendentes';
if(PedidoOrcamento == 'O'){
	tabela = 'orcamentos';
}

$(document).ready(function(){
	
	var Cliente				=	new Clientes();
	var Prospect			=	new Prospects();
	var CondicaoPagamento	= 	new CondicoesPagamento();
	var Evento				=	new Eventos();
	var Frete				=	new Fretes();
	var Filial				=	new Filiais();
	var FormaPagamento		= 	new FormasPagamento();
	var Pedido				=	new Pedidos();
	var TabelaPreco			=	new TabelaPrecos();
	var TipoMovimentacao	=	new TiposMovimentacao();
	var TipoPedido			=	new TiposPedido();
	var Transportadora		=	new Transportadoras();
	var Representante		=	new Representantes();
	
	var Produto				=	new Produtos();
	
	var dadosCabecalho		=	{};
	
	if(PedidoOrcamento == 'O'){
		exibirCamposOrcamento();
	}
	
	/**
	 * Carrega as informações ao entrar na tela
	 */
	Filial.getFiliais(function(){
		Filial.setDefault();
		Cliente.getClientes(false, false, function(){
			Prospect.getProspects(false, false, function(){
				TabelaPreco.getTabelasPreco(function(){
					CondicaoPagamento.getCondicoesPagamento(function(){
						FormaPagamento.getFormasPagamento(function(){
							
							//carregarPedidoTemporario();
							
						});
					});
				});
			});
		});
	});
	
	function exibirCamposOrcamento(){
		$(".descricao_pedido").html('Orçamento');
		$("#caixa_cliente_prospect").show();
		$("#caixa_tipo_pedido").hide();
	}
	
	$("#cliente_prospect").change(function(){
		Cliente.trocarCliente();
		Prospect.trocarProspect();
		
		if($(this).val() == 'C'){
			$("#caixa_cliente").show();
			$("#caixa_prospect").hide();
			$(".cabecalho_coluna_cliente").html('CLIENTE');
		} else {
			$("#caixa_prospect").show();
			$("#caixa_cliente").hide();
			$(".cabecalho_coluna_cliente").html('PROSPECT');
		}
	});
	
	
	/**
	 * Carrega os dados do pedido temporário e seta os valores nos respectivos campos
	 */
	function carregarPedidoTemporario(){
		Pedido.getCabecalho(function(pedido){
			
			
			if(pedido) {
				Filial.setValue(pedido.pedido_filial);
				
				TipoPedido.setValue(pedido.pedido_tipo_venda);
				
				if(pedido.pedido_id_prospects){
					Pedido.setClienteProspect('P');
					$("#cliente_prospect").trigger('change');
					Prospect.setCodigo(pedido.prospect_codigo);
					Prospect.setLoja(pedido.prospect_codigo_loja);
					Prospect.setEstado(pedido.prospect_estado);
					Prospect.setLabel(pedido.prospect_codigo + '/' + pedido.prospect_codigo_loja + ' - ' + pedido.prospect_nome + ' - ' + pedido.prospect_cgc);
				} else {
					Pedido.setClienteProspect('C');
					$("#cliente_prospect").trigger('change');
					Cliente.setCodigo(pedido.pedido_codigo_cliente);
					Cliente.setLoja(pedido.pedido_loja_cliente);
					Cliente.setDesconto(parseFloat(pedido.cliente_desconto));
					Cliente.setEstado(pedido.cliente_estado);
					Cliente.setGrupoTributacao(pedido.cliente_grupo_tributacao);
					Cliente.setLabel(pedido.pedido_codigo_cliente + '/' + pedido.pedido_loja_cliente + ' - ' + pedido.cliente_nome + ' - ' + pedido.cliente_cpf);
					Cliente.getInformacoesCliente(pedido.pedido_codigo_cliente, pedido.pedido_loja_cliente);
				}
				
				TabelaPreco.regraTabelasPreco(pedido.pedido_tabela_precos);
				
				CondicaoPagamento.setValue(pedido.forma_pagamento_codigo);
				CondicaoPagamento.setValorDesconto(pedido.pedido_desconto2);
				CondicaoPagamento.getDescontoCabecalho();
				
				TipoMovimentacao.setValue(pedido.pedido_tipo_movimentacao);
				
				FormaPagamento.getFormasPagamento(function(){
					FormaPagamento.setValue(pedido.pedido_formas_de_pagamento);
				});
				
				
				Pedido.setOrdemCompra(pedido.pedido_pedido_cliente);
				Pedido.setDataEntrega(pedido.pedido_data_entrega);
				Frete.setFrete(pedido.pedido_tipo_frete);
				Frete.setDirigidoRedespacho(pedido.pedido_dirigido_redespacho);
				Pedido.setObservacaoComercial(pedido.pedido_observacao_comercial);
				
			} else {
				
				if(PrePreenchidoClienteProspect == 'P'){
					
					Prospect.getProspects(PrePreenchidoCodigo, PrePreenchidoLoja, function(clienteProspect){
						Pedido.setClienteProspect('P');
						$("#cliente_prospect").trigger('change');
						Prospect.setCodigo(clienteProspect.codigo);
						Prospect.setLoja(clienteProspect.codigo_loja);
						Prospect.setEstado(clienteProspect.estado);
						Prospect.setLabel(clienteProspect.codigo + '/' + clienteProspect.codigo_loja + ' - ' + clienteProspect.nome + ' - ' + clienteProspect.cgc);
						Prospect.getInformacoesProspect(clienteProspect.codigo, clienteProspect.codigo_loja);
					});
					
				} else if(PrePreenchidoClienteProspect == 'C') {
					
					Cliente.getClientes(PrePreenchidoCodigo, PrePreenchidoLoja, function(clienteProspect){
						Pedido.setClienteProspect('C');
						$("#cliente_prospect").trigger('change');
						Cliente.setCodigo(clienteProspect.codigo);
						Cliente.setLoja(clienteProspect.loja);
						Cliente.setDesconto(parseFloat(clienteProspect.desconto));
						Cliente.setEstado(clienteProspect.estado);
						Cliente.setGrupoTributacao(clienteProspect.grupo_tributacao);
						Cliente.setLabel(clienteProspect.codigo + '/' + clienteProspect.loja + ' - ' + clienteProspect.nome + ' - ' + clienteProspect.cpf);
						Cliente.getInformacoesCliente(clienteProspect.codigo, clienteProspect.loja);
					});
					
				}
			}
		});
	}
	
	/**
	 * Para pedidos do tipo "VERBA INTRODUTORIA" e "VERBA MENSAL" será fixado a condição de pgto "099 - BONIFICACAO"
	 */
	$("#tipo_pedido").change(function(){
		
		if( (TipoPedido.getValue() == 'I') || (TipoPedido.getValue() == 'B') ){
			CondicaoPagamento.getCondicoesPagamento(function(){
				CondicaoPagamento.setValorDesconto('0');
				CondicaoPagamento.setValue('099', true);
				
				FormaPagamento.setValue('BON', true);
			});
		} else if(TipoPedido.getValue() == 'N') {
			CondicaoPagamento.getCondicoesPagamento(function(){
				CondicaoPagamento.setValue('');
				FormaPagamento.setValue('');
			});
		}
		
	});
	
	$(".trocar_cliente_prospect").click(function(){
		
		if($(this).data('trocar') == 'cliente'){
			Cliente.trocarCliente();
		} else {
			Prospect.trocarProspect();
		}
		
	});
	
	/**
	 * Para as condições "001 - A VISTA", "019 - 7 DIAS" e "074 - 10 DIAS" permite a seleção do desconto de cabeçalho (2%)
	 */
	$("#condicao_pagamento").change(function(){
		
		CondicaoPagamento.getDescontoCabecalho();
		
	});
	
	/**
	 * Obtem as formas de pagamento de acordo com o tipo de movimentação
	 */
	$("#tipo_movimentacao").change(function(){
		
		FormaPagamento.getFormasPagamento();
		
	});
	
	
	
	
	
	
	
	
	
	
	
	
	$("#avancar_passo_1").click(function(){
		
		$("#retorno_adicionar_item").hide();
		
		if(validarCabecalho()){
			var param = {
				codigo: Cliente.getCodigo(),
				loja: Cliente.getLoja(),
				forma_pagamento: FormaPagamento.getValue(),
				pedido_orcamento: PedidoOrcamento
			};
			
			if(Pedido.getClienteProspect() == 'C'){
				
				Cliente.validarTitulosVencidos(param, function(retorno){ //Validar Títulos Vencidos
					if(retorno){
						
						mensagem('Não é possível incluir pedido para o cliente <strong>' + retorno.nome + '</strong>. <br /><br />O cliente possui títulos vencidos.');
						
					} else {
						
						var tipo_pedido = TipoPedido.getValue();
						
						if(tipo_pedido == 'I'){ //Validar Limite de Verba Introdutória
							
							Pedido.validarVerbaIntrodutoria(Cliente.getCodigo(), Cliente.getLoja(), function(retorno){
								if(parseFloat(retorno.verba_introdutoria) <= 0){
									mensagem('Não é possível incluir pedido do tipo <b>Verba Introdutória</b> para o cliente <strong>' + retorno.nome + '</strong>. <br /><br />O cliente não possui saldo.');
								} else {
									avancarEtapa1();
								}
							});
							
						} else if(tipo_pedido == 'B'){ //Validar Limite de Verba Mensal
							
							Pedido.validarVerbaMensal(function(retorno){
								if(parseFloat(retorno.verba_mensal) <= 0){
									mensagem('Não é possível incluir pedido do tipo <b>Verba Mensal</b>. <br /><br />Você não possui saldo.');
								} else {
									avancarEtapa1();
								}
							});
							
						} else {
							
							avancarEtapa1();
							
						}
					}
				});
				
			} else {
				
				avancarEtapa1();
				
			}
		}
		
	});
	
	function validarCabecalho(){
		var valido = false;
		
		if(!Filial.getValue()){
			mensagem('Selecione uma <b>Filial</b> para prosseguir.');
		} else if(!TipoPedido.getValue() && PedidoOrcamento != 'O') {
			mensagem('Selecione um <b>Tipo de Pedido</b> para prosseguir.');
		} else if(!Cliente.getCodigo() && Pedido.getClienteProspect() == 'C') {
			mensagem('Selecione um <b>Cliente</b> para prosseguir.');
		} else if(!Prospect.getCodigo() && (PedidoOrcamento == 'O' && Pedido.getClienteProspect() == 'P')) {
			mensagem('Selecione um <b>Prospect</b> para prosseguir.');
		} else if(!TabelaPreco.getValue()) {
			mensagem('Selecione uma <b>Tabela de Preços</b> para prosseguir.');
		} else if(!CondicaoPagamento.getValue()){
			mensagem('Selecione uma <b>Condição de Pagamento</b> para prosseguir.');
		} else if(!TipoMovimentacao.getValue()) {
			mensagem('Selecione um <b>Tipo de Movimentação</b> para prosseguir.');
		} else if(!FormaPagamento.getValue()) {
			mensagem('Selecione uma <b>Forma de Pagamento</b> para prosseguir.');
		} else {
			valido = true;
		}
		
		return valido;
	}
	
	function avancarEtapa1(){
		dadosCabecalho = {
				
			edicao: {
				editado: (Pedido.getEdicao() ? true : false),
				codigo_pedido: Pedido.getEdicao()
			},
			
			analitico: {
				percentual: Pedido.getPercentualAnalitico(),
				valor: Pedido.getValorAnalitico()
			},
			
			autorizado: Pedido.getAutorizado(),
			
			condicao_pagamento: {
				codigo: CondicaoPagamento.getValue(),
				descricao: CondicaoPagamento.getLabel(),
				desconto: CondicaoPagamento.getValorDesconto()
			},
			
			data_entrega: Pedido.getDataEntregaDefault(),
			
			evento: {
				id: Evento.getIdEvento()
			},
			
			filial: {
				codigo: Filial.getValue(),
				estado: Filial.getEstado()
			},
			
			forma_pagamento: {
				codigo: FormaPagamento.getValue(),
				descricao: FormaPagamento.getLabel()
			},
			
			frete: {
				codigo: Frete.getFrete(),
				dirigido_redespacho: Frete.getDirigidoRedespacho()
			},
			
			observacao_comercial: Pedido.getObservacaoComercial(),
			
			ordem_compra: Pedido.getOrdemCompra(),
			
			tabela_precos: {
				codigo: TabelaPreco.getValue(),
				descricao: TabelaPreco.getLabel()
			},
			
			tipo_movimentacao: {
				codigo: TipoMovimentacao.getValue(),
				descricao: TipoMovimentacao.getLabel()
			},
			
			tipo_pedido: {
				codigo: (PedidoOrcamento == 'O' ? '*' : TipoPedido.getValue()),
				descricao: (PedidoOrcamento == 'O' ? 'ORÇAMENTO' : TipoPedido.getLabel())
			},
			
			transportadora: {
				codigo: Transportadora.getCodigo(),
				nome: Transportadora.getLabel()
			}
			
		};

		if(Pedido.getClienteProspect() == 'C'){
			Cliente.getClientes(Cliente.getCodigo(), Cliente.getLoja(), function(objCliente){
				
				dadosCabecalho.cliente = objCliente;
				dadosCabecalho.cliente_entrega = objCliente;
				
				Pedido.setCabecalho(dadosCabecalho, function(retorno){
					carregarEtapa2();
				});
				
			});
		} else {
			Prospect.getProspects(Prospect.getCodigo(), Prospect.getLoja(), function(objProspect){
				
				dadosCabecalho.prospect = objProspect;
				dadosCabecalho.prospect_entrega = objProspect;
				
				Pedido.setCabecalho(dadosCabecalho, function(retorno){
					carregarEtapa2();
				});
				
			});
		}
		
	}
	
	//////// ETAPA 2 ////////
	
	function carregarEtapa2() {
		
		Produto.getProdutosAutocomplete(Filial.getValue(), TabelaPreco.getValue());
		
		$('#id_adicionar_produtos').removeClass('tab_desativada');
		
		ativar_tabs();
		
		$("#id_adicionar_produtos").click();
		
		exibirCabecalho();
		
		exibirTotais();
		
	}
	
	function exibirCabecalho(){
		$(".cabecalho_filial").html(Filial.getLabel());
		$(".cabecalho_tipo_pedido").html((PedidoOrcamento == 'O' ? 'ORÇAMENTO' : TipoPedido.getLabel()));
		if(Pedido.getClienteProspect() == 'C'){
			$(".cabecalho_cliente").html(Cliente.getLabel());
		} else {
			$(".cabecalho_cliente").html(Prospect.getLabel());
		}
		$(".cabecalho_tabela_precos").html(TabelaPreco.getLabel());
		$(".cabecalho_condicao_pagamento").html(CondicaoPagamento.getLabel());
		$(".cabecalho_formas_de_pagamento").html(FormaPagamento.getLabel());
		$(".cabecalho_tipo_movimentacao").html(TipoMovimentacao.getLabel());
		$(".cabecalho_desconto_cliente").html(number_format(Cliente.getDesconto(), 2, ',', '.') + '%');
		$(".cabecalho_desconto_condicao_pagamento").html(number_format(CondicaoPagamento.getValorDescontoCabecalho(), 2, ',', '.') + '%');
		
		if(Pedido.getOrdemCompra()){
			$(".cabecalho_pedido_cliente").html(Pedido.getOrdemCompra());
		}
		$(".cabecalho_tipo_frete").html(Frete.getLabel());
		$(".cabecalho_dirigido_redespacho").html(Frete.getLabelDirigidoRedespacho());
		if(Pedido.getObservacaoComercial()){
			$(".cabecalho_observacao_comercial").html(Pedido.getObservacaoComercial());
		}
	}
	
	function exibirTotais(){
		Pedido.getTotaisPedido(function(totais){
			$(".total_produtos_adicionados").html(totais.qtd_itens);
			var percentual_analitico = parseFloat(totais.percentual_analitico) - CondicaoPagamento.getValorDesconto();
			var corAnalitico = Pedido.getCorAnalitico(percentual_analitico);
			$(".total_analitico").html('<span style="color: ' + corAnalitico + ';">' + number_format(percentual_analitico, 3, ',', '.') + "% <br/> R$ " + number_format(totais.valor_analitico, 3, ',', '.') + '</span>');
			$(".total_sem_impostos").html("R$ " + number_format(totais.total_sem_impostos, 2, ',', '.'));
			$(".total_com_impostos").html("R$ " + number_format(totais.total_com_impostos, 2, ',', '.'));
			Pedido.getTotaisTroca(function(troca){
				$(".total_trocas").html("R$ " + number_format(troca.total_sem_impostos, 2, ',', '.'));
			});
			var total_geral = totais.total_com_impostos - (totais.total_com_impostos * (CondicaoPagamento.getValorDesconto() / 100));
			$(".total_geral").html("R$ " + number_format(total_geral, 2, ',', '.'));
			
			alterarCabecalhoTabelaResolucao();
		});
	}
	
	function exibirTotaisAnalitico(somarDesconto)
	{
		Pedido.getTotaisPedidoAnalitico(somarDesconto, function(totais)
		{
			$(".total_produtos_adicionados").html(totais.qtd_itens);
			//mensagem(somarDesconto+' - somar desconto');
			if(somarDesconto == "S"){
				var percentual_analitico = parseFloat(totais.percentual_analitico) - CondicaoPagamento.getValorDesconto();
			}else{
				var percentual_analitico = 0;
			}
			var corAnalitico = Pedido.getCorAnalitico(percentual_analitico);
			$(".total_analitico").html('<span style="color: ' + corAnalitico + ';">' + number_format(percentual_analitico, 3, ',', '.') + "% <br/> R$ " + number_format(totais.valor_analitico, 3, ',', '.') + '</span>');
			$(".total_sem_impostos").html("R$ " + number_format(totais.total_sem_impostos, 2, ',', '.'));
			$(".total_com_impostos").html("R$ " + number_format(totais.total_com_impostos, 2, ',', '.'));
			Pedido.getTotaisTroca(function(troca)
			{
				$(".total_trocas").html("R$ " + number_format(troca.total_sem_impostos, 2, ',', '.'));
			});
			var total_geral = totais.total_com_impostos - (totais.total_com_impostos * (CondicaoPagamento.getValorDesconto() / 100));
			$(".total_geral").html("R$ " + number_format(total_geral, 2, ',', '.'));
			
			alterarCabecalhoTabelaResolucao();
		});
	}
	
	$("#adicionar_produto").click(function(e){
		e.preventDefault();
		
		validarProduto(function(valido){
			if(valido) {
				var codigo_produto = Produto.getCodigoSelecionado();
				var item_troca = Produto.getItemTrocaProdutoSelecionado();
				
				Produto.verificarProdutoExiste(codigo_produto, item_troca, function(retorno){
					if(retorno) {
						confirmar('O item <b>' + Produto.getLabelProdutoSelecionado() + '</b> já foi adicionado ao pedido.<br><br>Deseja alterar os valores informados anteriormente?', function () {
							$(this).dialog('close');
							
							adicionar_produto();
							
							$("#confirmar_dialog").remove();
						});
					} else {
						adicionar_produto();
					}
				});
			}
			
			$("#codigo_reduzido").focus();
		});
		$("#codigo_reduzido").focus();
	});
	
	function validarProduto(callback) {
		var valido = false;
		var validarDescAcrescItem = true;
		
		Cliente.getLimiteDesconto(Cliente.getCodigo(), Cliente.getLoja(), function(dadosCliente)
		{
			//Não validar desconto do item caso o cliente possua desconto e acrescimo definido
			if(parseFloat(dadosCliente.limite_desconto) > 0 && parseFloat(dadosCliente.limite_acrescimo) > 0) {
				validarDescAcrescItem = false;
			}
			
			//Chamado 002958 - Validação item
			if(Produto.getItemTrocaProdutoSelecionado() == '1') {
				validarDescAcrescItem = false;
			}
			
			Produto.getLimiteDescontoAcrescimo(TabelaPreco.getValue(), Produto.getCodigoSelecionado(), function(dadosProduto) 
			{
				Representante.getInformacoesRepresentante(function(dadosRepresentante) 
				{
					Representante.getDescontoPeriodoGrupo(dadosCliente.grupo_cliente, function(descontoGrupo) 
					{
						Representante.getDescontoPeriodoCliente(Cliente.getCodigo(), function(descontoCliente) 
						{
							Representante.getDescontoPeriodoRepresentante(dadosRepresentante.codigo, function(descontoRepresentante) 
							{
								//mensagem(descontoGrupo+' - desconto Grupo, '+ descontoCliente+' - desconto Cliente, '+ descontoRepresentante+' - desconto Representante');

								var descontoPeriodo = 0;
								if(descontoGrupo != null)
								{
									if(descontoGrupo.desconto_maximo > 0)
									{
										descontoPeriodo = descontoGrupo.desconto_maximo; 
									}
								}
								else if(descontoCliente != null)
								{
									if(descontoCliente.desconto_maximo > 0)
									{
										descontoPeriodo = descontoCliente.desconto_maximo;
									}
								}
								else if(descontoRepresentante != null)
								{
									if(descontoRepresentante.desconto_maximo > 0)
									{
										descontoPeriodo = descontoRepresentante.desconto_maximo;
									}
								}
								
								//mensagem(descontoPeriodo+' - desconto Período, '+Cliente.getCodigo()+' - codigo cliente');
								
								if(!Produto.getCodigoSelecionado()){
									mensagem('Selecione um <b>Produto</b> para adicionar.');
								} else if(!Produto.getPrecoVendaProdutoSelecionado() || Produto.getPrecoVendaProdutoSelecionado() <= 0){
									mensagem('Informe um <b>Preço de Venda</b> para adicionar o produto.');
								} else if(!validar_decimal(Produto.getPrecoVendaProdutoSelecionado(true)) || isNaN(Produto.getPrecoVendaProdutoSelecionado())){
									mensagem('O <b>Preço de Venda</b> informado é inválido.');
								} else if(!Produto.getQuantidadeProdutoSelecionado() || Produto.getQuantidadeProdutoSelecionado() <= 0){
									mensagem('Informe a <b>Quantidade</b> desejada para adicionar o produto.');
								} else if(!validar_digitos(Produto.getQuantidadeProdutoSelecionado())) {
									mensagem('A <b>Quantidade</b> informado é inválida.');
								} else if(!validar_decimal(Produto.getValorUnidadeProdutoSelecionado(true)) || isNaN(Produto.getValorUnidadeProdutoSelecionado())){
									mensagem('O <b>Valor da Unidade</b> informado é inválido.');
								} else if(descontoPeriodo == 0 && dadosRepresentante.tipo_liberacao == 'T' && !Produto.validarDesconto(dadosProduto.limite_desconto) && validarDescAcrescItem) {
									mensagem('O <b>Desconto</b> aplicado para este item está acima do limite permitido.<p>O limite de desconto para este item é de: <b>' + number_format(dadosProduto.limite_desconto, 2, ',', '.') + '%</b></p>');
								} 
								//nova regra de desconto
								else if(descontoPeriodo > 0 && dadosRepresentante.tipo_liberacao == 'T' && !Produto.validarDesconto(descontoPeriodo)) 
								{
									mensagem('O <b>Desconto</b> aplicado para este item está acima do limite permitido.<p>O limite de desconto para este item é de: <b>' + number_format(descontoPeriodo, 2, ',', '.') + '%</b></p>');
								} 
								else if(dadosRepresentante.tipo_liberacao == 'T' && !Produto.validarAcrescimo(dadosProduto.limite_acrescimo) && validarDescAcrescItem) {
									mensagem('O <b>Acréscimo</b> aplicado para este item está acima do limite permitido.<p>O limite de acréscimo para este item é de: <b>' + number_format(dadosProduto.limite_acrescimo, 2, ',', '.') + '%</b></p>');
								} else {
									valido = true;
								}
								
								callback(valido);
							});
						});
					});
					
				});
			});
		});
	}
	
	function adicionar_produto()
	{
		var codigo_filial	= Filial.getValue();
		var codigo_produto	= Produto.getCodigoSelecionado();
		var codigo_tabela	= TabelaPreco.getValue();
		
		Produto.getProduto(codigo_filial, codigo_produto, codigo_tabela, function(dadosProduto)
		{
			Cliente.getLimiteDesconto(Cliente.getCodigo(), Cliente.getLoja(), function(dadosCliente)
			{
				Representante.getInformacoesRepresentante(function(dadosRepresentante) 
				{
					Representante.getDescontoPeriodoGrupo(dadosCliente.grupo_cliente, function(descontoGrupo) 
					{
						Representante.getDescontoPeriodoCliente(Cliente.getCodigo(), function(descontoCliente) 
						{
							Representante.getDescontoPeriodoRepresentante(dadosRepresentante.codigo, function(descontoRepresentante) 
							{
								var descontoPeriodo = 0;
								var somarDesconto = '';
								if(descontoGrupo != null)
								{
									if(descontoGrupo.desconto_maximo > 0)
									{
										descontoPeriodo = descontoGrupo.desconto_maximo; 
										somarDesconto = descontoGrupo.somar_desconto;
										alert(descontoPeriodo+' - desconto período 1');
										alert(somarDesconto+' - somarDesconto 1');
									}
								}
								else if(descontoCliente != null)
								{
									if(descontoCliente.desconto_maximo > 0)
									{
										descontoPeriodo = descontoCliente.desconto_maximo;
										somarDesconto = descontoCliente.somar_desconto;
										alert(descontoPeriodo+' - desconto período 2');
										alert(somarDesconto+' - somarDesconto 2');
									}
								}
								else if(descontoRepresentante != null)
								{
									if(descontoRepresentante.desconto_maximo > 0)
									{
										descontoPeriodo = descontoRepresentante.desconto_maximo;
										somarDesconto = descontoRepresentante.somar_desconto;
										alert(descontoPeriodo+' - desconto período 3');
										alert(somarDesconto+' - somarDesconto 3');
									}
								}
								//mensagem(descontoPeriodo+' - desconto Período, ' + somarDesconto+' - somar Desconto ');
								
								Produto.setProduto(dadosCabecalho, dadosProduto, descontoPeriodo, somarDesconto, function(retorno)
								{
									if(retorno == true)
									{	
										$("#retorno_adicionar_item").html("Produto adicionado com sucesso.");
										$("#retorno_adicionar_item").show();
										
										$("#trocar_produto").trigger('click');
										$("#item_troca").trigger('change');
										
										exibirTotaisAnalitico(somarDesconto);	
									} 
									else 
									{
										mensagem("Atenção! Erro ao adicionar produto.");	
									}
								});
							});
						});
					});
				});
			});
		});
	}
	
	$("#item_troca").change(function(){
		if(Produto.getItemTrocaProdutoSelecionado() == '1'){
			Produto.getValoresProdutoItemTroca();
			$("#adicionar_produto").html('Adicionar Troca');
			$("#adicionar_produto").removeClass('btn-primary');
			$("#adicionar_produto").addClass('btn-warning');
		} else {
			Produto.getValoresProdutoItemVenda();
			$("#adicionar_produto").html('Adicionar Produto');
			$("#adicionar_produto").removeClass('btn-warning');
			$("#adicionar_produto").addClass('btn-primary');
		}
	});
	
	$("#trocar_produto").click(function(){
		
		Produto.limparProdutoSelecionado(true);
		
	});
	
	$(".selecionar_valor").click(function(){
		
		$(this).select();
		
	});
	
	$(".calcular_valores").keyup(function(){
		
		Produto.calcularValoresProdutoSelecionado($(this).attr('id'));
		
	});
	
	$("#dropdown_unidade_medida").change(function(){
		
		Produto.getValoresProdutoItemTroca();
		
	});
	
	$("#avancar_passo_2").click(function()
	{
		$("#retorno_adicionar_item").hide();
		Cliente.getLimiteDesconto(Cliente.getCodigo(), Cliente.getLoja(), function(dadosCliente)
		{
			Representante.getInformacoesRepresentante(function(dadosRepresentante) 
			{
				Representante.getDescontoPeriodoGrupo(dadosCliente.grupo_cliente, function(descontoGrupo) 
				{
					Representante.getDescontoPeriodoCliente(Cliente.getCodigo(), function(descontoCliente) 
					{
						Representante.getDescontoPeriodoRepresentante(dadosRepresentante.codigo, function(descontoRepresentante) 
						{
							var descontoPeriodo = 0;
							var somarDesconto = '';
							if(descontoGrupo != null)
							{
								if(descontoGrupo.desconto_maximo > 0)
								{
									descontoPeriodo = descontoGrupo.desconto_maximo; 
									somarDesconto = descontoGrupo.somar_desconto;
									//mensagem(somarDesconto+' - somar desconto grupo');
								}
							}
							else if(descontoCliente != null)
							{
								if(descontoCliente.desconto_maximo > 0)
								{
									descontoPeriodo = descontoCliente.desconto_maximo;
									somarDesconto = descontoCliente.somar_desconto;
									//mensagem(somarDesconto+' - somar desconto cliente');
								}
							}
							else if(descontoRepresentante != null)
							{
								if(descontoRepresentante.desconto_maximo > 0)
								{
									descontoPeriodo = descontoRepresentante.desconto_maximo;
									somarDesconto = descontoRepresentante.somar_desconto;
									//mensagem(descontoPeriodo+' - desconto representante');
									//mensagem(somarDesconto+' - somar desconto representante');
								}
							}
		
							Pedido.getTotaisPedido(function( pedido)
							{
								Pedido.validarVerbaIntrodutoria(Cliente.getCodigo(), Cliente.getLoja(), function(cliente)
								{
									Pedido.validarVerbaMensal(function(representante)
									{		
										
										
										
										if(PedidoOrcamento != 'O'){
											if( TipoPedido.getValue() == 'I' && pedido.total_com_impostos > parseFloat(cliente.verba_introdutoria) ){
												mensagem('O valor do pedido ultrapassa a <b>Verba Introdutória</b> disponível para este cliente.<p>Valor disponível: R$ ' + number_format(cliente.verba_introdutoria, 2, ',', '.') + '</p>');
												return false;
											}
											
											if(TipoPedido.getValue() == 'B' && pedido.total_com_impostos > parseFloat(representante.verba_mensal) ){
												mensagem('O valor do pedido ultrapassa sua <b>Verba Mensal</b> disponível.<p>Valor disponível: R$ ' + number_format(representante.verba_mensal, 2, ',', '.') + '</p>');
												return false;
											}
										}
										if(pedido.qtd_itens > 0){
											
											if(PedidoOrcamento != 'O'){
												
												Cliente.getLimiteDesconto(Cliente.getCodigo(), Cliente.getLoja(), function(cliente){
													var limite_desconto = cliente.limite_desconto;
													var limite_acrescimo = cliente.limite_acrescimo;
													var mensagem_limite_desconto = 'O limite de desconto para este cliente';
													var mensagem_limite_acrescimo = 'O limite de acréscimo para este cliente';
													
													Representante.getLimiteDesconto(function(representante){
														if(limite_desconto <= 0){
															limite_desconto = representante.limite_desconto;
															mensagem_limite_desconto = 'Seu limite de desconto';
														}
														if(limite_acrescimo <= 0){
															limite_acrescimo = representante.limite_acrescimo;
															mensagem_limite_acrescimo = 'Seu limite de acréscimo';
														}
														
														if(descontoPeriodo <= 0 && PedidoOrcamento != 'O' && pedido.percentual_analitico < 0 && round(abs(pedido.percentual_analitico) + pedido.desconto_condicao, 2) > limite_desconto){
															
															mensagem('O desconto de <b>' + number_format(abs(pedido.percentual_analitico) + pedido.desconto_condicao, 3, ',', '.') + '%</b> não é permitido.<p>' + mensagem_limite_desconto + ' é de <b>' + number_format(limite_desconto, 3, ',', '.') + '%</b>.</p>');
														
														} else if (descontoPeriodo <= 0 && PedidoOrcamento != 'O' && pedido.percentual_analitico > 0 && round(abs(pedido.percentual_analitico) + pedido.desconto_condicao, 2) > limite_acrescimo) {
															
															mensagem('O acréscimo de <b>' + number_format(abs(pedido.percentual_analitico) + pedido.desconto_condicao, 3, ',', '.') + '%</b> não é permitido.<p>' + mensagem_limite_acrescimo + ' é de <b>' + number_format(limite_acrescimo, 3, ',', '.') + '%</b>.</p>');
														
														} else {
															Pedido.getTotaisTroca(function(troca){
																if( (troca.qtd_itens > 0) && (troca.total_com_impostos >= pedido.total_com_impostos) ){
																	mensagem('O valor dos itens de troca deve ser inferior ao valor dos itens de venda.')
																} else {
																	avancarEtapa2();
																}
															});
														}
													});
												});
												
											}else{
												Representante.getLimiteDesconto(function(representante){
													
													
													if(PedidoOrcamento != 'O' && pedido.percentual_analitico < 0 && round(abs(pedido.percentual_analitico) + pedido.desconto_condicao, 2) > representante.limite_desconto){
														mensagem('O desconto de <b>' + number_format(abs(pedido.percentual_analitico) + pedido.desconto_condicao, 3, ',', '.') + '%</b> não é permitido.<p>' + mensagem_limite_desconto + ' é de <b>' + number_format(limite_desconto, 3, ',', '.') + '%</b>.</p>');
													} else {
														Pedido.getTotaisTroca(function(troca){
															if( (troca.qtd_itens > 0) && (troca.total_com_impostos >= pedido.total_com_impostos) ){
																mensagem('O valor dos itens de troca deve ser inferior ao valor dos itens de venda.')
															} else {
																avancarEtapa2();
															}
														});
													}
												});
												
											}
										} else {
											mensagem('Adicione um produto no pedido!');
										}
										
									});
								});
							});
						});
					});
				});
			});
		});
	});
	
	function avancarEtapa2() {
		
		$('#id_adicionar_produtos').removeClass('tab_desativada');
		$('#id_outras_informacoes').removeClass('tab_desativada');
		
		ativar_tabs();
		
		$("#id_outras_informacoes").click();
		
		exibirCabecalho();
		
		Evento.getLista(function(){
			Evento.setIdEvento(dadosCabecalho.evento.id);
		});
		Pedido.setOrdemCompra(dadosCabecalho.ordem_compra);
		Pedido.setDataEntrega(dadosCabecalho.data_entrega);
		Pedido.setObservacaoComercial(dadosCabecalho.observacao_comercial);
		if(Pedido.getClienteProspect() == 'C'){
			Cliente.exibirClienteEntrega(dadosCabecalho.cliente_entrega.codigo, dadosCabecalho.cliente_entrega.loja);
		} else {
			Prospect.exibirProspectEntrega(dadosCabecalho.prospect_entrega.codigo, dadosCabecalho.prospect_entrega.codigo_loja);
		}
	}
	
	$("#avancar_passo_3").click(function(){
		
		dadosCabecalho.ordem_compra 				= Pedido.getOrdemCompra();
		dadosCabecalho.data_entrega					= Pedido.getDataEntrega();
		dadosCabecalho.evento.id					= Evento.getIdEvento();
		dadosCabecalho.frete.codigo					= Frete.getFrete();
		dadosCabecalho.frete.dirigido_redespacho	= Frete.getDirigidoRedespacho();
		dadosCabecalho.observacao_comercial			= Pedido.getObservacaoComercial();
		dadosCabecalho.observacao_comercial			= utf8_encode(str_replace("\n", " ", dadosCabecalho.observacao_comercial));
		dadosCabecalho.nome 						= Transportadora.getLabel;
		dadosCabecalho.transportadora.codigo 		= Transportadora.getCodigo();
		
		Pedido.setCabecalho(dadosCabecalho, function(retorno){
			avancarEtapa3();
		});
	});
	
	function avancarEtapa3(){
		$('#id_adicionar_produtos').removeClass('tab_desativada');
		$('#id_outras_informacoes').removeClass('tab_desativada');
		$('#id_finalizar').removeClass('tab_desativada');
		
		ativar_tabs();
		
		$("#id_finalizar").click();
		
		exibirCabecalho();
		
		exibirTotais();
	}
	
	
	$("#finalizar_pedido").click(function(event){
		confirmar('Deseja realmente finalizar o pedido?', function () {
			$(this).dialog('close');
			
			$("#confirmar_dialog").remove();
			
			plugins.waitingDialog.mostrarDialog("Aguarde salvando pedido...");
			
			Pedido.finalizar(function(pedido){
				if(!pedido){
					mensagem('Atenção! Não foi possível finalizar o pedido!');
				} else {
					Pedido.gerarTxt(pedido, function(codigo_pedido){
						plugins.waitingDialog.fecharDialog();
						
						// tarefa 2848 lentidão nos pedidos
						//aqui limpamos o cache ao finalizar cada pedido
						
						var page = jQuery(event.target);
						page.remove();
						
						//fim chamado 2848
						
						location.href = "pedidos_espelho.html#" + codigo_pedido + "|" + pedido.pedido_filial + "|" + PedidoOrcamento;
					});
				}
			});
		});
	});
	
	
	
	
	
	
	$(".visualizar_produtos").click(function(){
		
		visualizarProdutos();
		
	});
	
	function visualizarProdutos(){
		
		$("#retorno_adicionar_item").hide();
		
		plugins.waitingDialog.mostrarDialog("Aguarde carregando produtos...");
		
		visualizarProdutosVenda(function(){
			visualizarProdutosTroca(function(){
				$("#geral").hide();
				
				$("#listagem_produtos").show();
				
				$("#barra_inferior").show();
				
				plugins.waitingDialog.fecharDialog();
				
				$(document).scrollTop(0);
			});
		});
	}
	
	$(".voltar_visualizacao_produtos").click(function(){
		$("#listagem_itens_venda_conteudo").html('');
		$("#listagem_itens_troca_conteudo").html('');
		$("#listagem_produtos").hide();
		$("#barra_inferior").hide();
		
		$("#geral").show();
		$(document).scrollTop(0);
		return false;
	});
	
	function visualizarProdutosVenda(callback){
		Produto.getItensVenda(function(dados){
			var html = '<h2><img src="img/produtos.png" width="20">  Itens de Venda</h2>';
			html += '<div style="clear:both;"></div><br/>';
			html += '<table class="cabecalho_pedido">';
				html += '<thead>';
					html += '<tr>';
						html += '<th>PRODUTO</th>';
						html += '<th>PREÇO UNIT. (R$)</th>';
						html += '<th>DESC. (%)</th>';
						html += '<th>PREÇO VENDA (R$)</th>';
						html += '<th>VALOR DA UNI. (R$)</th>';
						html += '<th>QUANT.</th>';
						html += '<th>PESO (KG)</th>';
						html += '<th>TOTAL (R$)</th>';
						html += '<th>IPI<br/>ICMS<br/>ST<br/>(R$)</th>';
						html += '<th>TOTAL GERAL (R$)</th>';
					html += '</tr>';
				html += '</thead>';
				html += '<tbody>';
				
			if(dados){
				var total = dados.rows.length;
				
				var total_geral_st = 0;
				var total_geral_ipi = 0;
				var total_geral_icms = 0;
				var total_geral_peso = 0;
				var total_geral_sem_impostos = 0;
				var total_geral_com_impostos = 0;
				var total_analitico = 0;
				var total_porcentagem_analitico = 0;
				
				for(i = 0; i < total; i++){
					var produto = dados.rows.item(i);
					
					var st			= parseFloat(produto.pedido_st);
					var ipi			= parseFloat(produto.pedido_ipi);
					var icms		= parseFloat(produto.pedido_icms);
					var peso		= parseFloat(produto.pedido_peso_total);
					var quantidade	= parseInt(produto.pedido_quantidade);
					var preco_venda	= parseFloat(produto.pedido_preco_venda);
					
					var valor_total_sem_impostos = quantidade * preco_venda;
					var valor_total_com_impostos = (quantidade * preco_venda) + st + ipi;
					html += '<tr class="editar_produto" data-codigo="' + produto.pedido_codigo_produto + '" data-troca="' + produto.item_troca + '">';
						html += '<td class="label_produto">' + produto.pedido_codigo_produto + ' - ' + produto.produto_descricao + '</td>';
						html += '<td align="right">' + number_format(produto.pedido_preco_unitario, 2, ',', '.') + '</td>';
						html += '<td align="center">' + number_format(produto.pedido_desconto_item, 2, ',', '.') + '</td>';
						html += '<td align="right">' + number_format(preco_venda, 2, ',', '.') + '</td>';
						html += '<td align="right">' + number_format(produto.pedido_valor_unidade, 2, ',', '.') + '</td>';
						html += '<td align="center">' + produto.pedido_quantidade + ' ' + produto.pedido_unidade_medida + '</td>';
						html += '<td align="right">' + number_format(peso, 3, ',', '.') + '</td>';
						html += '<td align="right">' + number_format(valor_total_sem_impostos, 2, ',', '.') + '</td>';
						html += '<td align="right">' + number_format(ipi, 2, ',', '.') + '<br/>' + number_format(icms, 2, ',', '.') + '<br/>' + number_format(st, 2, ',', '.') + '</td>';
						html += '<td align="right">' + number_format(valor_total_com_impostos, 2, ',', '.') + '</td>';
					html += '</tr>';
					
					total_geral_st += st;
					total_geral_ipi += ipi;
					total_geral_icms += icms;
					total_geral_peso += peso;
					total_geral_sem_impostos += valor_total_sem_impostos;
					total_geral_com_impostos += valor_total_com_impostos;
				}
				
				html += '<tr>';
					html += '<td colspan="6"><strong>TOTAIS DOS ITENS</strong></td>';
					html += '<td align="right">' + number_format(total_geral_peso, 3, ',', '.') + '</td>';
					html += '<td align="right">' + number_format(total_geral_sem_impostos, 2, ',', '.') + '</td>';
					html += '<td align="right">' + number_format(total_geral_ipi, 2, ',', '.') + '<br/>' + number_format(total_geral_icms, 2, ',', '.') + '<br/>' + number_format(total_geral_st, 2, ',', '.') + '</td>';
					html += '<td align="right">' + number_format(total_geral_com_impostos, 3, ',', '.') + '</td>';
				html += '</tr>';
			
				Pedido.getTotaisPedido(function(totais){
						
						var percentual_analitico = parseFloat(totais.percentual_analitico) - CondicaoPagamento.getValorDesconto();
						var corAnalitico = Pedido.getCorAnalitico(percentual_analitico);
						var total_geral = total_geral_com_impostos - (total_geral_com_impostos * (CondicaoPagamento.getValorDesconto() / 100));
						html += '<tr>';
							html += '<td colspan="6"><strong>ANALÍTICO</strong></td>';
							html += '<td colspan="4" align="center"><span style="color: ' + corAnalitico + ';">' + number_format(percentual_analitico, 3, ',', '.') + "% <br/> R$ " + number_format(totais.valor_analitico, 3, ',', '.') + '</span></td>';
						html += '</tr>';
						
						html += '<tr>';
							html += '<td colspan="6"><strong>TOTAIS DO PEDIDO</strong></td>';
							html += '<td colspan="4" align="center"><strong>R$ ' + number_format(total_geral, 3, ',', '.') + '</strong></td>';
						html += '</tr>';
						html += '</tbody>';
					html += '</table>';
					$("#listagem_itens_venda_conteudo").html(html);
					
					callback();
				});
			} else {
				html += '<tr>';
					html += '<td colspan="10" style="color: #900;">Nenhum produto adicionado</td>';
				html += '</tr>';
				
				$("#listagem_itens_venda_conteudo").html(html);
				
				callback();
			}
		});
	}
	
	function visualizarProdutosTroca(callback){
		Produto.getItensTroca(function(dados){
			if(dados){
				var total = dados.rows.length;
				
				var html = '<h2><img src="img/produtos.png" width="20">  Itens de Troca</h2>';
					html += '<div style="clear:both;"></div><br/>';
					html += '<table class="cabecalho_pedido">';
						html += '<thead>';
							html += '<tr>';
								html += '<th>PRODUTO</th>';
								html += '<th>PREÇO UNIT. (R$)</th>';
								html += '<th>DESCONTO</th>';
								html += '<th>PREÇO VENDA (R$)</th>';
								html += '<th>VALOR DA UNI. (R$)</th>';
								html += '<th>U.M.</th>';
								html += '<th>QUANT.</th>';
								html += '<th>PESO (kg)</th>';
								html += '<th>TOTAL (R$)</th>';
							html += '</tr>';
						html += '</thead>';
						html += '<tbody>';
				
				var total_geral_st = 0;
				var total_geral_ipi = 0;
				var total_geral_icms = 0;
				var total_geral_peso = 0;
				var total_geral_sem_impostos = 0;
				var total_geral_com_impostos = 0;
				
				for(i = 0; i < total; i++){
					var produto = dados.rows.item(i);
					
					var peso		= parseFloat(produto.pedido_peso_total);
					var quantidade	= parseInt(produto.pedido_quantidade);
					var preco_venda	= parseFloat(produto.pedido_preco_venda);
					
					var valor_total_sem_impostos = quantidade * preco_venda;
					html += '<tr class="editar_produto" data-codigo="' + produto.pedido_codigo_produto + '" data-troca="' + produto.item_troca + '">';
						html += '<td class="label_produto">' + produto.pedido_codigo_produto + ' - ' + produto.produto_descricao + '</td>';
						html += '<td>' + number_format(produto.pedido_preco_unitario, 2, ',', '.') + '</td>';
						html += '<td>' + number_format(produto.pedido_desconto_item, 2, ',', '.') + '</td>';
						html += '<td>' + number_format(preco_venda, 2, ',', '.') + '</td>';
						html += '<td>' + number_format(produto.pedido_valor_unidade, 2, ',', '.') + '</td>';
						html += '<td>' + produto.pedido_unidade_medida + '</td>';
						html += '<td>' + produto.pedido_quantidade + '</td>';
						html += '<td>' + number_format(peso, 3, ',', '.') + '</td>';
						html += '<td>' + number_format(valor_total_sem_impostos, 2, ',', '.') + '</td>';
					html += '</tr>';
					
					total_geral_peso += peso;
					total_geral_sem_impostos += valor_total_sem_impostos;
				}
				
						html += '<tr class="novo_grid_rodape">';
							html += '<td colspan="7">TOTAIS DO PEDIDO</td>';
							html += '<td align="right">' + number_format(total_geral_peso, 3, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(total_geral_sem_impostos, 2, ',', '.') + '</td>';
						html += '</tr>';
					html += '</tbody>';
				html += '</table>';
				$("#listagem_itens_troca_conteudo").html(html);
				
			} else {
				
				$("#listagem_itens_troca_conteudo").html('');
				
			}
			
			callback();
		});
	}
	
	$(".editar_produto").live('click', function(){
		var descricaoProduto = $(this).children('.label_produto').html();
		
		criarDialog('acoesProduto', descricaoProduto, 'Ações');
		var elemProduto = $(this);
		
		$("#acoesProduto").dialog({
			title: '<span class="ui-icone-dialog icone-alerta"></span> Ações',
			bgiframe: true,			
			minWidth: 600,
			resizable: true,
			draggable: true,
			zIndex: 9999,
			modal: true,
			dialogClass: "no-close",
			open: function (event, ui) {
				$(".ui-widget-overlay").css('z-index', '0');
				$("input").blur();
	        },
			buttons: {
				'Editar': function() {
					$(this).dialog('close');
					$("#acoesProduto").remove();
					editarProduto(elemProduto);
					$("#id_adicionar_produtos").click();
					$("#barra_inferior").hide();
				},
				'Excluir': function(){
					$(this).dialog('close');
					$("#acoesProduto").remove();
					excluirProduto(elemProduto);
				},
				'Cancelar': function(){
					$(this).dialog('close');
					$("#acoesProduto").remove();
				}
			}
		});
	});
	
	function editarProduto(elemProduto){
		var codigo_filial			= Filial.getValue();
		var codigo_tabela_precos	= TabelaPreco.getValue();
		var codigo_produto			= $(elemProduto).data('codigo');
		var item_troca				= $(elemProduto).data('troca');
		
		Produto.setItemTrocaProdutoSelecionado(item_troca);
		Produto.getDadosProduto(codigo_filial, codigo_produto, codigo_tabela_precos, function(dadosProduto){
			var produto = {
				dados: dadosProduto,
				label: dadosProduto.produto_codigo_palm + ' - ' + dadosProduto.produto_descricao
			};
			
			Produto.setValoresProdutoSelecionado(produto);
			Produto.bloquearProdutoSelecionado();
			
			$("#listagem_itens_venda_conteudo").html('');
			$("#listagem_itens_troca_conteudo").html('');
			$("#listagem_produtos").hide();
			
			$("#geral").show();
		});
	}
	
	function excluirProduto(elemProduto){
		var codigo_filial			= Filial.getValue();
		var codigo_tabela_precos	= TabelaPreco.getValue();
		var codigo_produto			= $(elemProduto).data('codigo');
		var item_troca				= $(elemProduto).data('troca');
		
		Produto.excluir(codigo_filial, codigo_produto, item_troca, function(){
			mensagem('Produto excluído com sucesso!');
			
			visualizarProdutos();
			
			exibirTotais();
		});
	}
	
	$('#id_informacao_cliente').click(function(){
		
		$('#id_adicionar_produtos').addClass('tab_desativada');
		$('#id_outras_informacoes').addClass('tab_desativada');
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select', 0);
		
		ativar_tabs();
	}).trigger('click');
	
	
	
	
	
	
	
	$(".cancelar").click(function(e){
		e.preventDefault();
		
		var descricao = (PedidoOrcamento == 'O' ? 'orçamento' : 'pedido');
		
		confirmar('Deseja cancelar esse <b>' + descricao + '</b>?', function (){
			Pedido.removerPedidoTemporario(function(){
				$(this).dialog('close');
				
				if(PedidoOrcamento == 'O') {
					url = 'orcamentos_menu.html';
				} else {
					url = 'pedidos.html';
				}
				
				window.location.href = url;
				
				$("#confirmar_dialog").remove();
			});
		});
	});
});




function ativar_tabs(){
	// Desativando tabs com classe tab_desativada
	var tabs_desativar = [];
	
	$( "#tabs ul li a" ).each(function(e, i){
		var classname = i.className;
		
		if(classname === 'tab_desativada'){
			tabs_desativar.push(e);
		}
	});
	
	$("#tabs").tabs({disabled: tabs_desativar});
}