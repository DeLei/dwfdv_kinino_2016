var LogPedidos = function(){
	
	var conteudo = "";
	var codigo_pedido = "";
	
	/**
	* Metódo:		salvar_arquivo
	* 
	* Descrição:	Salva arquivo de backup contendo o pedido
	**/
	this.salvar_arquivo = function(parametro_conteudo, id_pedido){
		conteudo = parametro_conteudo;
		codigo_pedido = id_pedido;
		
		removerArquivosAntigos();
		carregarDevice();
	}
	
	var removerArquivosAntigos = function(){
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
			fileSystem.root.getDirectory(localStorage.getItem('caminho_local') + "Pedidos/", { create: true }, function(directory) {
				var directoryReader = directory.createReader();
				
				//Varre o diretório para verificar os arquivos existentes
		        directoryReader.readEntries(function(entries) {
		            var i;
		            for (i = 0; i < entries.length; i++) {
		                var file = entries[i].name;
		                	file = file.split('_');
		                
		                //Obtem a data de criação do arquivo
		                var data_criacao = file[1].split('.');
		                
		                //Obtem 10 dias atras a partir da data atual
		                var data_atual = date('Ymd', strtotime('-10 days'));
		               	
		                //Se o arquivo for criado a mais de 10 dias exclui o arquivo
		                if(parseInt(data_criacao[0]) <= parseInt(data_atual)){
		                	entries[i].remove();
		                }
		            }
		        });
			});
		});
	}
	
	/**
	* Metódo:		imprimir
	* 
	* Descrição:	Aguarda carregar dispositivos
	**/
	var carregarDevice = function(){
		
		console.log('[IMPRIMIR] - <this.carregarDevice>');
		
		document.addEventListener("deviceready", dispositivoCarregado, false);

	};
	
	var dispositivoCarregado = function(){
		
		console.log('[IMPRIMIR] - <this.dispositivoCarregado>');
		
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, criarArquivo, this.falha);
		
	};
	
	var criarArquivo = function(fileSystem) {
		
		console.log('[IMPRIMIR] - <this.criarArquivo>');
		
        fileSystem.root.getFile(localStorage.getItem('caminho_local') + "Pedidos/" + codigo_pedido + "_" + date('Ymd', time()) + ".txt", {create: true, exclusive: false},  chamarArquivo,  this.falha);
        
    }
	
	var chamarArquivo = function(fileEntry) {
		
		console.log('[IMPRIMIR] - <this.chamarArquivo>');
		
        fileEntry.createWriter(escreverArquivo, this.falha);
    }
	
	var escreverArquivo = function (writer) {
		
		var conteudo_impressao = conteudo;
		
        writer.write(conteudo_impressao);
        
    }
	
	var falha = function (error) {
		console.log('[IMPRIMIR] - ERRO');
		console.log(JSON.stringify(error));
		
		mensagem('<strong style="color: #900;">ERRO!</strong><br><br>Não foi possível criar o arquivo.', function() {
			
		});
		
	}



	

}