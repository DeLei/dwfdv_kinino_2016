var prospect;

$(document).ready(function() {
	
	//$('.caixa ul li a').button();
	
	db.transaction(function(x) {
		var id = parse_url(location.href).fragment;
		
		x.executeSql(
			'SELECT prospects.*, eventos.nome AS nome_evento FROM prospects LEFT JOIN eventos ON eventos.id = prospects.id_feira WHERE codigo = ? ', [id], function(x, dados) {
				if (dados.rows.length)
				{
					prospect = dados.rows.item(0);
					
					if(!prospect.exportado)
					{
						
						$('#excluir_prospect').live('click', function(){

							apprise('Você deseja excluir esse prospect?', {
								'verify': true,
								'textYes': 'Sim',
								'textNo': 'Não'
							}, function (dado) {
								if (dado)
								{
									db.transaction(function(x) {
										x.executeSql('DELETE FROM prospects WHERE codigo = ?', [prospect.codigo], function(){
											window.location = 'prospects_aguardando.html';
										});
									});
								}
							});
						
						});
						
						
						$('#excluir_prospect').show();
	
					}
					else if(prospect.status == 'A')
					{
						$('#solicitar_positivacao').live('click', function(){

							apprise('Você deseja positivar esse prospect?', {
								'verify': true,
								'textYes': 'Sim',
								'textNo': 'Não'
							}, function (dado) {
								if (dado)
								{
									db.transaction(function(x) {
										
										var tmp = new Array();
							
										tmp.push("editado = '1'");
										tmp.push("exportado = NULL");
										tmp.push("status = 'L'");
										
										console.log('Solicitando Positivação...');
										
										x.executeSql('UPDATE prospects SET ' + tmp.join(', ') + ' WHERE cgc = ?', [prospect.cgc], function(x, dados){
									
											console.log('Positivação Solicitada = ' + prospect.cgc);
											
											window.location = 'prospects_aguardando.html';
										
										});
					
										
									});
								}
							});
						
						});
						
						$('#solicitar_positivacao').show();
					}
					
					$('#editar_prospect').attr('href', 'prospects_editar.html#' + prospect.codigo);
					$('#criar_orcamento_prospect').attr('href', 'javascript: obter_prospect_orcamento(\''+prospect.codigo+'\');');
					
					
					$('td[id]').each(function() {
						
						var html = (prospect[$(this).attr('id')] ? prospect[$(this).attr('id')] : 'N/A');
					
						if ($(this).attr('id') == 'tipo')
						{
							html = obter_tipo_cliente(html);
						}
						
						if ($(this).attr('id') == 'id_feira')
						{
							if(prospect.id_feira)
							{
								html = prospect.id_feira + ' - ' + prospect.nome_evento;
							}
							else
							{
								html = 'N/A';
							}
						}
						
						// Verificação para Prospects Aguardando Sincronização
						if($(this).attr('id') == 'telefone')
						{
							var telefone 	= prospect.telefone;
							var ddd 		= prospect.ddd;
						
							if(telefone.length >= 8)
							{
								html = formatar_telefone(ddd+telefone);								
							}
							else
							{
								html = 'N/A';
							}
						}
						
						// Verificação para Prospects Aguardando Sincronização
						if($(this).attr('id') == 'fax')
						{
							var fax = prospect.fax;
							var ddd = prospect.ddd;
						
							if(fax.length >= 8)
							{
								html = formatar_telefone(ddd+fax);
							}
							else
							{
								html = 'N/A';
							}
						}
						
					
						
						//--------------
						
						if($(this).attr('id') == 'data_nascimento')
						{
							if(prospect.data_nascimento)
							{
								html = protheus_data2data_normal(prospect.data_nascimento);
							}
							else
							{
								html = 'N/A';
							}
						}
						
						$(this).html(html);

					});
					
					$('.nome').html(prospect.nome);
					$('.cgc').html(prospect.cgc);
					
					
					$('input[name=pessoa_contato]').val(prospect.contato);
					$('input[name=email]').val(prospect.email);
					
					
					var cgc_prospect = prospect.cgc;
					
					x.executeSql(
						'SELECT * FROM historico_prospects WHERE cpf = ? ORDER BY id DESC', [cgc_prospect], function(x, dados) {
							if (dados.rows.length)
							{
								for (i = 0; i < dados.rows.length; i++)
								{
									var dado = dados.rows.item(i);
									
									$('table:last').append('<tr><td>' + date('d/m/Y', dado.timestamp) + '</td><td>' + dado.pessoa_contato + '</td><td>' + (dado.cargo ? dado.cargo : '-') + '</td><td>' + (dado.email ? dado.email : '-') + '</td><td>'+ (dado.protocolo ? dado.protocolo : '-') +'</td><td><a href="#" id="ver_historico" data-id="' + dado.id + '">Ver Detalhes</a></td></tr>');
									
									
								}
							}
							
							alterarCabecalhoTabelaConteudo();	
							alterarCabecalhoTabelaResolucao();
						}
					);
				}
				else
				{
					window.location = 'index.html';
				}
			}
		);
	});
	
	// ver histórico
	
	$('#ver_historico').live('click', function() {
		var id = $(this).attr('data-id');
		
		console.log('id = ' + id);
		
		db.transaction(function(x) {
			x.executeSql(
				'SELECT * FROM historico_prospects WHERE id = ?', [id], function(x, dados) {
					var historico = dados.rows.item(0);
					
					var html = '<table cellspacing="5">';
					html += '<tr><td><strong>Prospect:</strong></td><td>' + prospect.nome + '</td></tr>';
					html += '<tr><td><strong>Contato:</strong></td><td>' + historico.pessoa_contato + '</td></tr>';
					html += '<tr><td><strong>Cargo:</strong></td><td>' + (historico.cargo ? historico.cargo : '-') + '</td></tr>';
					html += '<tr><td><strong>E-mail:</strong></td><td>' + (historico.email ? historico.email : '-') + '</td></tr>';
					html += '</table>';
					
					html+= '<p>' + nl2br(historico.descricao) + '</p>';
					
					$.colorbox({ html: html });
				}
			);
		});
		
		return false;
	});
	
	// novo histórico
	
	$('#link_novo_historico').click(function() {
		$(this).parents('p').hide();
		
		$('#novo_historico').show();
		
		return false;
	});
	
	// cancelar
	
	$('#cancelar').click(function() {
		$('#link_novo_historico').parents('p').show();
		
		$('#novo_historico').hide();
		
		return false;
	});
	
	// enviar form
	
	$('form').submit(function() {
		if (!$('input[name=pessoa_contato]').val())
		{
			mensagem('Digite uma pessoa de contato.');
		}
		else if ($('input[name=email]').val() && !validar_email($('input[name=email]').val()))
		{
			mensagem('Digite um e-mail válido.');
		}
		else if (!$('textarea[name=descricao]').val())
		{
			mensagem('Digite uma descrição para o histórico.');
		}
		else
		{
			db.transaction(function(x) {
				var id = parse_url(location.href).fragment;
				
				x.executeSql(
					'SELECT id FROM historico_prospects ORDER BY id DESC LIMIT 1', [], function(x, dados) {

						var id = uniqid();
						
						var tmp_1 = ['id', 'timestamp', 'id_prospect', 'cpf'];
						var tmp_2 = ['?', '?', '?', '?'];
						var tmp_3 = [id, String(time()), prospect.codigo, prospect.cgc];
						
						$('input[type!=submit], textarea').each(function() {
							tmp_1.push($(this).attr('name'));
							tmp_2.push('?');
							tmp_3.push($(this).val());
						});
						
						x.executeSql('INSERT INTO historico_prospects (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3);
						
						mensagem('Histórico adicionado com sucesso.');
						
						$('table:last').prepend('<tr><td>' + date('d/m/Y', time()) + '</td><td>' + $('input[name=pessoa_contato]').val() + '</td><td>' + ($('input[name=cargo]').val() ? $('input[name=cargo]').val() : 'N/A') + '</td><td>' + ($('input[name=email]').val() ? $('input[name=email]').val() : 'N/A') + '</td><td>' + ($('input[name=protocolo]').val() ? $('input[name=protocolo]').val() : 'N/A') + '</td><td><a href="#" id="ver_historico" data-id="' + id + '">Ver Detalhes</a></td></tr>');

						
						$('input[name=cargo], textarea[name=descricao]').val('');
						
						$('#cancelar').trigger('click');
						
						scroll('h3:last');
						
						alterarCabecalhoListagem('#historicos .novo_grid');
					}
				);
			});
		}
		
		return false;
	});
});

function obter_prospect_orcamento(codigo_proscpet)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM prospects WHERE codigo = ?', [codigo_proscpet], 
			function(x, dados) 
			{				
				var dado = dados.rows.item(0);
				
				window.location = "pedidos_adicionar.html#O|P|" + dado.codigo + "|" + dado.codigo_loja;
			
			}
		)
	});
}