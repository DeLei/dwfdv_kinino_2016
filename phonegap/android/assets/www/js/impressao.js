var Impressao = function(){
	
	var conteudo = "";
	
	/**
	* Metódo:		imprimir
	* 
	* Descrição:	Iniciar Impressao
	**/
	this.imprimir = function(parametro_conteudo){
		
		console.log('[IMPRIMIR] - <this.imprimir>');
		
		conteudo = parametro_conteudo;
		
		carregarDevice();
		
	}
	
	/**
	* Metódo:		imprimir
	* 
	* Descrição:	Aguarda carregar dispositivos
	**/
	var carregarDevice = function(){
		
		console.log('[IMPRIMIR] - <this.carregarDevice>');
		
		document.addEventListener("deviceready", dispositivoCarregado, false);

	};
	
	var dispositivoCarregado = function(){
		
		console.log('[IMPRIMIR] - <this.dispositivoCarregado>');
		
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, criarArquivo, this.falha);
		
	};
	
	var criarArquivo = function(fileSystem) {
		
		console.log('[IMPRIMIR] - <this.criarArquivo>');
		
        fileSystem.root.getFile(localStorage.getItem('caminho_local') + "impressao.txt", {create: true, exclusive: false},  chamarArquivo,  this.falha);
        
    }
	
	var chamarArquivo = function(fileEntry) {
		
		console.log('[IMPRIMIR] - <this.chamarArquivo>');
		
        fileEntry.createWriter(escreverArquivo, this.falha);
    }
	
	var escreverArquivo = function (writer) {
		
		console.log('[IMPRIMIR] - <this.escreverArquivo>');
		
		var conteudo_impressao = "\n<dw-fonteGrande>--------------------------------";
		conteudo_impressao += "\n" + conteudo;
		conteudo_impressao += "\n<dw-fonteGrande>--------------------------------";
		conteudo_impressao += "\r\n\r\n\r";
		
        writer.write(conteudo_impressao);
        
        //-------------------------------------
        
        document.addEventListener("deviceready", function(){
			var imprimir = {};
			
			// Get network interface   
			imprimir = window.plugins.printer.imprimir(function(){
				console.log('[PRINTER] SUCESSO');
			}, function(){
				
				confirmar('Para imprimir você precisa da aplicação de impressão. Deseja fazer download da aplicação?', 
				function () {
						$(this).dialog('close');
					
						//------------------
						//---------------------------------
						
						window.plugins.webintent.startActivity({
					        action: window.plugins.webintent.ACTION_VIEW,
					        url: config.dw_fdv + '../../dwprint/pagina_impressao',
					        },
					        function(){
					        },
					        function(e){
					        }
					    );     
						
						//location.href = "index.html";
						
						//---------------------------------
						//------------------
						
						$("#confirmar_dialog").remove();
				});
				
			});	
		}, false);
    }
	
	var falha = function (error) {
		console.log('[IMPRIMIR] - ERRO');
		console.log(JSON.stringify(error));
		
		mensagem('<strong style="color: #900;">ERRO!</strong><br><br>Não foi possível imprimir.', function() {
			
		});
		
	}



	

}