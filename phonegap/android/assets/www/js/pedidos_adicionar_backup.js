$(document).ready(function() {

	$('#editar_novo').live('click',function(){
	
		var aux 		= 0;
		var codigo 		= 0;
		var tabela 		= 0;
		var editar 		= true;
		
		var codigo_do_pedido	= $(this).attr('data-codigo_do_pedido');
		var codigo 				= $(this).attr('data-cod_prod');
		var descricao 			= $(this).attr('data-desc_prod');
		var pro_st	 			= $(this).attr('data-pro-st');
		var preco 				= $(this).attr('data-preco');
		var desconto 			= $(this).attr('data-desconto');
		var qtde 				= $(this).attr('data-qtde');
		var preco_prod			= $(this).attr('data-preco-prod');
		var peso				= $(this).attr('data-peso');
		var ipi					= $(this).attr('data-ipi');
		var tabela_preco		= $(this).attr('data-tabela-preco');
		

		$('#e_descricao').html('Item: '+descricao);
		$('#e_pre_total').html('');
		
		$('#e_preco').val(preco);
		$('#e_preco_prod').val(preco_prod);
		$('#e_desconto').val(desconto);
		$('#e_quantidade').val(qtde);
		$('#e_cod_prod').val(codigo);
		$('#e_peso').val(peso);
		$('#e_ipi').val(ipi);
		$('#e_prod_st').val(pro_st);
		$('#e_tabela_preco').val(tabela_preco);
		
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 380,
			width: 380,
			minWidth: 600,
			modal: true,
			buttons: {
				"Editar": function() {
						
						var cod_prod 			= $('#e_cod_prod').val();
						var cod_tabela_preco	= $('#e_cod_tabela_preco').val();
						var preco_editado 		= $('#e_preco').val();
						var qtde_editado		= $('#e_quantidade').val();
						var desconto_editado 	= $('#e_desconto').val();
						var preco_prod 			= $('#e_preco_prod').val();
						var preco_prod 			= $('#e_ipi').val();
				
						//--------------------------------------------------------------
						// Editar preço
						//--------------------------------------------------------------
						if(preco !== preco_editado && editar)
						{
						}

						//--------------------------------------------------------------
						// Editar desconto
						//--------------------------------------------------------------
						// se o usuário não cancelou...
						if (desconto !== desconto_editado && editar)
						{
							// devemos usar o ^ e $, senão a expressão não valida corretamente
							if (!desconto_editado.match(/^[0-9]+(,[0-9]+)?$/))
							{
								mensagem('Digite um desconto válido. Para decimais utilize a vírgula.');
							}
							else
							{
								desconto = prc2sql(desconto_editado);
								
								var codigo_do_pedido 			= '';								
								var codigo_do_produto 			= cod_prod;
								var codigo_da_tabela_de_preco 	= tabela_preco;
								
								console.log('UPDATE pedidos SET desconto = ? WHERE codigo = ? AND codigo_do_produto = ? AND codigo_da_tabela_de_preco = ? AND orc = ?');
								
								console.log(desconto+ ' - ' +codigo_do_pedido+ ' - ' +codigo_do_produto+ ' - ' +codigo_da_tabela_de_preco+ ' - ' +$('[name=orc]').val());
								
								db.transaction(function(x) {
									x.executeSql('UPDATE pedidos SET desconto = ? WHERE codigo = ? AND codigo_do_produto = ? AND codigo_da_tabela_de_preco = ? AND orc = ?', [desconto, codigo_do_pedido, codigo_do_produto, codigo_da_tabela_de_preco, $('[name=orc]').val()]);
					
									exibir_itens_do_pedido_offline();
								});
												
							}
						}
						
						//--------------------------------------------------------------
						// Editar quantidade
						//--------------------------------------------------------------
						// se o usuário não cancelou...
						if (qtde !== qtde_editado && editar)
						{
							// devemos usar o ^ e $, senão a expressão não valida corretamente
							if (!qtde_editado.match(/^[0-9]+$/))
							{
								mensagem('Digite uma quantidade válida.');
							}
							else
							{
								quantidade = intval(qtde_editado);
								
								if (!quantidade)
								{
									mensagem('Digite uma quantidade maior que 0.');
								}
								else
								{
									var codigo_do_pedido 			= '';
									var codigo_do_produto 			= cod_prod;
									var codigo_da_tabela_de_preco 	= tabela_preco;
									
									db.transaction(function(x) {
										x.executeSql('UPDATE pedidos SET quantidade = ? WHERE codigo = ? AND codigo_do_produto = ? AND codigo_da_tabela_de_preco = ? AND orc = ?', [quantidade, codigo_do_pedido, codigo_do_produto, codigo_da_tabela_de_preco, $('[name=orc]').val()]);
										
										exibir_itens_do_pedido_offline();
									});
								}
							}
						}
						
						if(editar == true)
						{
							$( this ).dialog( "close" );
						}
						
				},
				Cancel: function() {					
					$( this ).dialog( "close" );
				}
			}
		});
		$( "#dialog-form" ).dialog( "open" );
		$('#dialog-form').animate({scrollTop: $("#dialog-form").offset().top - 50},1000);
	});	
	
	$('input[name=e_quantidade], input[name=e_desconto]').keydown(function(e) {
			
		if (e.keyCode == 13)
		{
			return false;
		}
		
		// recalcular preço final
		
		setTimeout(function () 
		{
			
			var qtde		= $('[name=e_quantidade]').val();
			var prc 		= prc2sql($('[name=e_preco]').val());
			var prc			= prc * qtde;			
			
			if($('[name=e_prod_st]').val() > 0){
				var val_st 		= round(prc * ($('[name=e_prod_st]').val() / 100), 2);
			}else{
				var val_st 		= round(prc * (0 / 100), 2);
			}
			
			var val_ipi 	= round(prc * ($('[name=e_ipi]').val() / 100), 2);
			var val_desc 	= round(prc * (prc2sql($('[name=e_desconto]').val()) / 100), 2);				
			var prc_fin 	= prc + val_ipi + val_st - val_desc;
			
			$('#e_pre_total').html('R$ '+number_format(prc_fin, 2, ',', '.'));
			
		}, 100);
	});
	
	// ** orçamentos
	
	$('[name=pro_cli]').click(function () {
		if ($('[name=pro_cli]:checked').val() == 'pro')
		{
			$('#pro').show();
			$('#cli').hide().find('input').val('');
		}
		else
		{
			$('#pro').hide().find('input').val('');
			$('#cli').show();
		}
	});
	
	// orçamentos **
	
	//
	
	// focar campo cliente
	
	$('input[name=_codigo_e_loja_do_cliente]').focus();
	
	// popular autocompletes e selects
	
	db.transaction(function(x) {
		// popular autocompletes e selects
		
		// obter unidades
		
		x.executeSql(
			'SELECT unidades FROM informacoes_do_representante', [], function(x, dados) {
				var dado = dados.rows.item(0);
				
				var unidades = unserialize(dado.unidades);
				
				$.each(unidades, function(i, v) {
					$('select[name=unidade]').append('<option value="' + i + '">' + i + ' - ' + v + '</option>');
				});
			}
		);
		
		// obter prospects
		
		db.transaction(function(x) {
			x.executeSql(
				'SELECT id, codigo, nome, cgc FROM prospects', [], function(x, dados) {
					if (!dados.rows.length)
					{
						$('[name=pro_cli]').parents('p').hide();
						
						$('[name=pro_cli][value=pro]').attr('checked', false);
						$('[name=pro_cli][value=cli]').attr('checked', true);
						
						$('#pro').hide();
						$('#cli').show();
					}
					else
					{
						var prospects = [];
						
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							prospects.push({ label: dado.nome + ' - ' + dado.cgc, value: dado.id });
						}
						
						$('input[name=_id_pro]').autocomplete({
							minLength: 3,
							source: prospects,
							focus: function(e, ui) {
								// no caso de foco, somente o label será alterado
								$('input[name=_id_pro]').val(ui.item.label);
								
								return false;
							},
							select: function( event, ui ) {
								$('input[name=_id_pro]').val(ui.item.label).attr('disabled', 'disabled');
								$('input[name=id_pro]').val(ui.item.value);
								obter_cgc_pro(ui.item.value);
								
								$('[name=pro_cli]').attr('disabled', 'disabled');
								
								obter_info_pro(ui.item.value);
								
								$('.adicionar_produtos').show();
								$('select[name=codigo_da_tabela_de_preco]').focus();
								
								scroll('select[name=codigo_da_tabela_de_preco]');
								
								return false;
							}

						});
					}
				}
			);
		});
		
		// obter clientes
		
		x.executeSql(
			'SELECT codigo, loja, razao_social, cnpj FROM clientes', [], function(x, dados) {
				if (dados.rows.length)
				{
					var clientes = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						clientes.push({ label: dado.codigo + ' - ' + dado.razao_social + ' - ' + dado.cnpj, value: dado.codigo + '|' + dado.loja });
					}
					
					$('input[name=_codigo_e_loja_do_cliente]').autocomplete({
						minLength: 3,
						source: clientes,
						position : { my : "left bottom", at: "left top", collision : "none"},
						focus: function(e, ui) {
							// no caso de foco, somente o label será alterado
							$('input[name=_codigo_e_loja_do_cliente]').val(ui.item.label);
							
							return false;
						},
						select: function( event, ui ) {
							$('input[name=_codigo_e_loja_do_cliente]').val(ui.item.label).attr('disabled', 'disabled');
							$('input[name=codigo_e_loja_do_cliente]').val(ui.item.value);
							
							$('[name=pro_cli]').attr('disabled', 'disabled');
							
							var valor = ui.item.value;
							valor = valor.split('|');
							
							var codigo = valor[0];
							var loja = valor[1];
							
							obter_info_cli(codigo, loja);

							$('.adicionar_produtos').show();
							$('select[name=codigo_da_tabela_de_preco]').focus();

							scroll('select[name=codigo_da_tabela_de_preco]');
							
							return false;
						}
					});
				}
			}
		);
		
		// obter tabelas de preço
		
		x.executeSql(
			'SELECT DISTINCT codigo, descricao FROM tabelas_de_preco ORDER BY descricao ASC', [], function(x, dados) {
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=codigo_da_tabela_de_preco]').append('<option value="' + dado.codigo + '">' + dado.descricao + '</option>');
					}
				}
			}
		);
		
		// obter transportadoras
		
		x.executeSql(
			'SELECT DISTINCT codigo, nome FROM transportadoras ORDER BY nome ASC', [], function(x, dados) {
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=codigo_da_transportadora]').append('<option value="' + dado.codigo + '">' + dado.nome + '</option>');
					}
				}
			}
		);

		// obter eventos
		
		x.executeSql(
			'SELECT DISTINCT id, nome FROM eventos ORDER BY nome ASC', [], function(x, dados) {
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=id_do_evento]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
					}
				}
			}
		);
		
		// verificar se o usuário está tentando editar um pedido
		
		var codigo_do_pedido = parse_url(location.href).fragment;
		
		console.log('Número: '+codigo_do_pedido);
		
		if (codigo_do_pedido)
		{
			x.executeSql(
				'SELECT * FROM pedidos WHERE codigo = ? OR num = ?', [codigo_do_pedido, codigo_do_pedido], function(x, dados) {
					
					console.log('Pedidos: '+dados.rows.length);
					
					if (dados.rows.length)
					{
						x.executeSql('DELETE FROM pedidos WHERE codigo = "" AND orc = ?', [$('[name=orc]').val()]);
						
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							var tmp_1 = ['id', 'exportado', 'codigo', 'pedido_sendo_editado'];
							var tmp_2 = ['?', '?', '?', '?'];
							var tmp_3 = [0, 0, '', 1];
							
							$.each(dado, function(indice, valor) {
								if (indice != 'id' && indice != 'exportado' && indice != 'codigo')
								{
									tmp_1.push(indice);
									tmp_2.push('?');
									tmp_3.push(valor);
								}
							});
							
							x.executeSql('INSERT INTO pedidos (' + tmp_1.join(', ') + ') VALUES (' + tmp_2.join(', ') + ')', tmp_3);
							
							//
							
							localStorage['codigo_do_pedido_sendo_editado'] = dado.codigo;
							
							if (dado.orc)
							{
								$('h2').text('Editar Orçamento');
							}
							else
							{
								$('h2').text('Editar Pedido');
							}
							
							exibir_itens_do_pedido_offline();
							
							$('.adicionar_produtos, .adicionar_produto, .itens, .outras_informacoes').show();
							
							if (dado.codigo_do_cliente)
							{
								$('input[name=_codigo_e_loja_do_cliente]').val(dado.codigo_do_cliente + ' - ' + dado.razao_social_do_cliente + ' - ' + dado.cnpj_do_cliente).attr('disabled', 'disabled');
								$('input[name=codigo_e_loja_do_cliente]').val(dado.codigo_do_cliente + '|' + dado.loja_do_cliente);
								
								$('[name=pro_cli]').parents('p').hide();
								
								$('[name=pro_cli][value=pro]').attr('checked', false);
								$('[name=pro_cli][value=cli]').attr('checked', true);
								
								$('#pro').hide();
								$('#cli').show();
							}
							else
							{
								$('input[name=_id_pro]').val(dado.raz_soc_pro + ' - ' + dado.cpf_pro).attr('disabled', 'disabled');
								$('input[name=id_pro]').val(dado.id_pro);
								
								$('[name=pro_cli]').parents('p').hide();
								
								$('[name=pro_cli][value=pro]').attr('checked', true);
								$('[name=pro_cli][value=cli]').attr('checked', false);
								
								$('#pro').show();
								$('#cli').hide();
							}
							
							// a forma de pgto só será setada após 1 segundo
							// porque o sistema precisa processar a unidade selecionada, para depois exibir o select de formas de pgto.
							setTimeout(function () {
								$('select[name=codigo_da_forma_de_pagamento]').val(dado.codigo_da_forma_de_pagamento);
							}, 1000);
							
							$('select[name=id_do_evento]').val(dado.id_do_evento);
							
							$('select[name=tipo_de_frete]').val(dado.tipo_de_frete).trigger('change');
							$('select[name=codigo_da_transportadora]').val(dado.codigo_da_transportadora);
							
							if (dado.valor_do_frete_alterado)
							{
								 $('.editar_frete').css('textDecoration', 'line-through').next().val(dado.valor_do_frete_do_pedido).show();
							}
							
							$('input[name=ordem_de_compra]').val(dado.ordem_de_compra);
							
							var dt_ent = dado.data_de_entrega;
							
							if (dt_ent)
							{
								var dia = substr(dt_ent, 6, 2);
								var mes = substr(dt_ent, 4, 2);
								var ano = substr(dt_ent, 0, 4);
								
								$('input[name=data_de_entrega]').val(dia + '/' + mes + '/' + ano);
							}
							
							$('textarea[name=observacao]').val(dado.observacao);
							
							$('select[name=unidade]').val(dado.unidade);
							
							$('select[name=tipo_de_pedido]').val(dado.tipo_de_pedido);
							
							$('input[name=pedido_autorizado]').val(dado.pedido_autorizado);
							
						}
					}
				}
			);
		}
		
		// o usuário não está tentando editar o pedido
		else
		{
			localStorage['codigo_do_pedido_sendo_editado'] = '';
			
			x.executeSql('DELETE FROM pedidos WHERE pedido_sendo_editado = 1 AND orc = ?', [$('[name=orc]').val()]);
			
			// verificar se existe um pedido não concluído
			
			x.executeSql(
				'SELECT * FROM pedidos WHERE codigo = "" AND orc = ? AND (pedido_sendo_editado = "" OR pedido_sendo_editado IS NULL OR pedido_sendo_editado = 0)', [$('[name=orc]').val()], function(x, dados) {
					// existia um pedido não concluído
					
					if (dados.rows.length)
					{
						for (i = 0; i < dados.rows.length; i++)
						{
							var dado = dados.rows.item(i);
							
							if (dado.orc)
							{
								$('h2').text('Novo Orçamento');
								
								$('h2').after('<div class="ajuda"></p>Este orçamento foi iniciado ' + date('d/m/Y H:i:s', dado.timestamp) + ' e não foi concluído. <a href="#" class="cancelar_pedido" style="color: #900;">Clique aqui</a> para cancelá-lo.</p></div>');
							}
							else
							{
								$('h2').text('Novo Pedido');
								
								$('h2').after('<div class="ajuda"></p>Este pedido foi iniciado ' + date('d/m/Y H:i:s', dado.timestamp) + ' e não foi concluído. <a href="#" class="cancelar_pedido" style="color: #900;">Clique aqui</a> para cancelá-lo.</p></div>');
							}
							
							exibir_itens_do_pedido_offline();
							
							$('.adicionar_produtos, .adicionar_produto, .itens, .outras_informacoes').show();
							
							if (dado.codigo_do_cliente)
							{
								$('input[name=_codigo_e_loja_do_cliente]').val(dado.codigo_do_cliente + ' - ' + dado.razao_social_do_cliente + ' - ' + dado.cnpj_do_cliente).attr('disabled', 'disabled');
								$('input[name=codigo_e_loja_do_cliente]').val(dado.codigo_do_cliente + '|' + dado.loja_do_cliente);
								
								$('[name=pro_cli]').parents('p').hide();
								
								$('[name=pro_cli][value=pro]').attr('checked', false);
								$('[name=pro_cli][value=cli]').attr('checked', true);
								
								$('#pro').hide();
								$('#cli').show();
							}
							else
							{
								$('input[name=_id_pro]').val(dado.raz_soc_pro + ' - ' + dado.cpf_pro).attr('disabled', 'disabled');
								$('input[name=id_pro]').val(dado.id_pro);
								
								$('[name=pro_cli]').parents('p').hide();
								
								$('[name=pro_cli][value=pro]').attr('checked', true);
								$('[name=pro_cli][value=cli]').attr('checked', false);
								
								$('#pro').show();
								$('#cli').hide();
							}
							
							// a forma de pgto só será setada após 1 segundo
							// porque o sistema precisa processar a unidade selecionada, para depois exibir o select de formas de pgto.
							setTimeout(function () {
								$('select[name=codigo_da_forma_de_pagamento]').val(dado.codigo_da_forma_de_pagamento);
							}, 1000);
							
							$('select[name=id_do_evento]').val(dado.id_do_evento);
							
							$('select[name=tipo_de_frete]').val(dado.tipo_de_frete).trigger('change');
							$('select[name=codigo_da_transportadora]').val(dado.codigo_da_transportadora);
							
							if (dado.valor_do_frete_alterado)
							{
								 $('.editar_frete').css('textDecoration', 'line-through').next().val(dado.valor_do_frete_do_pedido).show();
							}
							
							$('input[name=ordem_de_compra]').val(dado.ordem_de_compra);
							
							var dt_ent = dado.data_de_entrega;
							
							if (dt_ent)
							{
								var dia = substr(dt_ent, 6, 2);
								var mes = substr(dt_ent, 4, 2);
								var ano = substr(dt_ent, 0, 4);
								
								$('input[name=data_de_entrega]').val(dia + '/' + mes + '/' + ano);
							}
							
							$('textarea[name=observacao]').val(dado.observacao);
							
							$('select[name=unidade]').val(dado.unidade);
							
							$('select[name=tipo_de_pedido]').val(dado.tipo_de_pedido);
							
							$('input[name=pedido_autorizado]').val(dado.pedido_autorizado);
							
						}
					}
					
					// não  existia um pedido não concluído
					
					else
					{
						$('[name=tipo_de_frete]').val('cif').trigger('change');
				
						$('[name=codigo_da_transportadora]').val('001');
						
						$('[name=data_de_entrega]').val(date('d/m/Y', time() + (3600 * 24 * 14)));
					}
				}
			);
		}
	});
	
	// quando uma unidade é selecionada as formas de pagamento devem ser atualizadas
	
	obter_formas_de_pagamento();
	
	$('[name=unidade]').live('change', function () {
		obter_formas_de_pagamento();
	});
	
	// obter produtos
	
	$('select[name=codigo_da_tabela_de_preco]').change(function() {
		var codigo_da_tabela = $(this).val();
		
		if (codigo_da_tabela == 0)
		{
			$('.adicionar_produto').hide();
		}
		else
		{
			$('.adicionar_produto').show();
			
			
			var where = '';
			
			db.transaction(function(x) {
				x.executeSql(
					'SELECT DISTINCT produtos.codigo, produtos.descricao, produtos.qe, produtos.ipi, tabelas_de_preco.descricao AS descricao_da_tabela_de_preco, tabelas_de_preco.preco_do_produto AS prc, estoque.qatu AS qatu FROM produtos INNER JOIN tabelas_de_preco ON tabelas_de_preco.codigo_do_produto = produtos.codigo LEFT OUTER JOIN estoque ON estoque.cod = produtos.codigo AND estoque.fil = "' + $('[name=unidade]').val() + '" WHERE tabelas_de_preco.codigo = ? ' + where + ' ORDER BY produtos.descricao ASC', [codigo_da_tabela], function(x, dados) {
						if (dados.rows.length)
						{
							var produtos = [];
							
							for (i = 0; i < dados.rows.length; i++)
							{
								var dado = dados.rows.item(i);
								
								produtos.push({ label: dado.codigo + ' - ' + dado.descricao + ' - Estoque: ' + number_format(dado.qatu, 0, ',', '.') + ' - Emb.: ' + number_format(dado.qe, 0, ',', '.') + ' - Prç.: R$ ' + number_format(dado.prc, 2, ',', '.') + ' - IPI: ' + number_format(dado.ipi, 0, ',', '.'), value: dado.codigo, prc: dado.prc, ipi: dado.ipi });
							}
							
							$('input[name=_codigo_do_produto]').autocomplete({
								minLength: 3,
								source: produtos,
								position : { my : "left bottom", at: "left top", collision : "none"},
								focus: function(e, ui) {
									// no caso de foco, somente o label será alterado
									$('input[name=_codigo_do_produto]').val(ui.item.label);
									
									var prc = ui.item.prc;
									var val_ipi = round(prc * (ui.item.ipi / 100), 2);
									var val_st = 0;
									var val_desc = round(prc * (prc2sql($('[name=desconto]').val()) / 100), 2);
									
									var prc_fin = prc + val_ipi + val_st - val_desc;
									
									$('[name=prc]').val(number_format(prc, 2, ',', '.'));
									$('#prc_fin').text(number_format(prc_fin, 2, ',', '.'));
									
									$('[name=prod_ipi]').val(ui.item.ipi);
									$('[name=prod_st]').val(ui.item.st);
									
									return false;
								},
								select: function( event, ui ) {
									$('input[name=_codigo_do_produto]').val(ui.item.label);
									$('input[name=codigo_do_produto]').val(ui.item.value);
									
									var prc = ui.item.prc;
									var val_ipi = round(prc * (ui.item.ipi / 100), 2);
									var val_st = 0;
									var val_desc = round(prc * (prc2sql($('[name=desconto]').val()) / 100), 2);
									
									var prc_fin = prc + val_ipi + val_st - val_desc;
									
									$('[name=prc]').val(number_format(prc, 2, ',', '.'));
									$('#prc_fin').text(number_format(prc_fin, 2, ',', '.'));
									
									$('[name=prod_ipi]').val(ui.item.ipi);
									$('[name=prod_st]').val(ui.item.st);
									
									$('input[name=quantidade]').focus();
									
									return false;
								}
							});
						}
					}
				);
			});
		}
		
		$('input[name=_codigo_do_produto]').val('').focus();
		$('input[name=codigo_do_produto]').val('');
		$('input[name=quantidade]').val('');
	});
	
	// adicionar produto
	
	$('input[name=adicionar_produto]').click(function() {
		adicionar_produto($('input[name=codigo_e_loja_do_cliente]').val(), $('select[name=codigo_da_tabela_de_preco]').val(), $('input[name=codigo_do_produto]').val(), $('input[name=quantidade]').val(), $('input[name=desconto]').val());
		
		scroll('.adicionar_produtos');
	});
	
	// editar quantidade
	
	$('.editar_quantidade').live('click', function() {
		var _this = this;
		
		apprise('Digite a nova quantidade para o produto "' + $(this).data('descricao_do_produto') + '":', {
			'input': true,
			'textOk': 'Ok',
			'textCancel': 'Cancelar'
		}, function (quantidade) {
			// se o usuário não cancelou...
			if (quantidade !== false)
			{
				// devemos usar o ^ e $, senão a expressão não valida corretamente
				if (!quantidade.match(/^[0-9]+$/))
				{
					mensagem('Digite uma quantidade válida.');
				}
				else
				{
					quantidade = intval(quantidade);
					
					if (!quantidade)
					{
						mensagem('Digite uma quantidade maior que 0.');
					}
					else
					{
						var codigo_do_pedido = $(_this).data('codigo_do_pedido');
						// "_" faz com que o código seja obrigatoriamente uma string
						// se não for dessa forma, um inteiro pode ser retornando provocando erros
						var codigo_do_produto = str_replace('_', '', $(_this).data('codigo_do_produto'));
						var codigo_da_tabela_de_preco =  str_replace('_', '', $(_this).data('codigo_da_tabela_de_preco'));
						
						db.transaction(function(x) {
							x.executeSql('UPDATE pedidos SET quantidade = ? WHERE codigo = ? AND codigo_do_produto = ? AND codigo_da_tabela_de_preco = ? AND orc = ?', [quantidade, codigo_do_pedido, codigo_do_produto, codigo_da_tabela_de_preco, $('[name=orc]').val()]);
							
							exibir_itens_do_pedido_offline();
						});
					}
				}
			}
		});
		
		return false;
	});
	
	// editar desconto
	$('.editar_desconto').live('click', function() {
		var _this = this;
		
		apprise('Digite o desconto em porcentagem (%) para o produto "' + $(this).data('descricao_do_produto') + '":', {'input': true, 'textOk': 'Ok', 'textCancel': 'Cancelar'}, function (desconto) 
		{
			// se o usuário não cancelou...
			
			if (desconto !== false)
			{
			
				// devemos usar o ^ e $, senão a expressão não valida corretamente
				if (!desconto.match(/^[0-9]+(,[0-9]+)?$/))
				{
					mensagem('Digite um desconto válido. Para decimais utilize a vírgula.');
				}
				else
				{
					desconto = prc2sql(desconto);
					
					var codigo_do_pedido = $(_this).data('codigo_do_pedido');
					// "_" faz com que o código seja obrigatoriamente uma string
					// se não for dessa forma, um inteiro pode ser retornando provocando erros
					var codigo_do_produto = str_replace('_', '', $(_this).data('codigo_do_produto'));
					var codigo_da_tabela_de_preco =  str_replace('_', '', $(_this).data('codigo_da_tabela_de_preco'));
					
					db.transaction(function(x) {
						x.executeSql('UPDATE pedidos SET desconto = ? WHERE codigo = ? AND codigo_do_produto = ? AND codigo_da_tabela_de_preco = ? AND orc = ?', [desconto, codigo_do_pedido, codigo_do_produto, codigo_da_tabela_de_preco, $('[name=orc]').val()]);
		
						exibir_itens_do_pedido_offline();
					});
									
				}
			}
			
		});
		
		return false;
	});
	
	// excluir item
	
	$('.excluir_item').live('click', function() {
		var _this = this;
		
		apprise('Tem certeza que deseja excluir o produto "' + $(this).data('descricao_do_produto') + '"?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				var codigo_do_pedido = $(_this).data('codigo_do_pedido');
				
				// "_" faz com que o código seja obrigatoriamente uma string
				// se não for dessa forma, um inteiro pode ser retornando provocando erros
				var codigo_do_produto = str_replace('_', '', $(_this).data('codigo_do_produto'));
				var codigo_da_tabela_de_preco =  str_replace('_', '', $(_this).data('codigo_da_tabela_de_preco'));
				
				db.transaction(function(x) {
					x.executeSql('DELETE FROM pedidos WHERE codigo = ? AND codigo_do_produto = ? AND codigo_da_tabela_de_preco = ? AND orc = ?', [codigo_do_pedido, codigo_do_produto, codigo_da_tabela_de_preco, $('[name=orc]').val()]);
					
					exibir_itens_do_pedido_offline();
				});
			}
		});
		
		return false;
	});
	
	// chamar adicionar_produto() através da quantidade
	
	$('input[name=quantidade], input[name=desconto]').keydown(function(e) {
			
		if (e.keyCode == 13)
		{
			$('input[name=adicionar_produto]').trigger('click');
		}
		
		// recalcular preço final
		
		setTimeout(function () 
		{
			
			var qtde		= $('[name=quantidade]').val();
			var prc 		= prc2sql($('[name=prc]').val());
			var prc			= prc * qtde;			
			
			var val_ipi 	= round(prc * ($('[name=prod_ipi]').val() / 100), 2);
			var val_st 		= round(prc * ($('[name=prod_st]').val() / 100), 2);
			var val_desc 	= round(prc * (prc2sql($('[name=desconto]').val()) / 100), 2);			
			
			var prc_fin 	= prc + val_ipi + val_st - val_desc;
			
			$('#prc_fin').text(number_format(prc_fin, 2, ',', '.'));
			
		}, 100);
	});
	
	// tipo de frete
	
	$('select[name=tipo_de_frete]').change(function(e) {
		switch ($(this).val())
		{
			case 'redespacho':
			case 'fob':
				$('select[name=codigo_da_transportadora]').show();
			break;
			
			default:
				$('select[name=codigo_da_transportadora]').hide();
			break;
		}
	});

	// editar frete
	
	$('.editar_frete').live('click', function() {
		var frete = $('input[name=valor_do_frete]');
		
		if (frete.is(':visible'))
		{
			$(this).css('textDecoration', 'none').next().hide().val('0,00');
		}
		else
		{
			$(this).css('textDecoration', 'line-through').next().show().focus();
		}
		
		return false;
	});
	
	// data de entrega
	
	$('input[name=data_de_entrega]').datepicker({ dateFormat: 'dd/mm/yy', minDate: 'now' });
	
	// cancelar pedido
	
	$('.cancelar_pedido').live('click', function() {
		apprise('Tem certeza que deseja cancelar o ' + ($('[name=orc]').val() == 1 ? 'orçamento' : 'pedido') + '?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				db.transaction(function(x) {
					x.executeSql('DELETE FROM pedidos WHERE codigo = "" AND orc = ?', [$('[name=orc]').val()]);
					
					window.location = 'pedidos_analisados.html';
				});
			}
		});
		
		return false;
	});
	
	// finalizar pedido
	
	$('input[name=finalizar_pedido]').click(function() {
		apprise('Tem certeza que deseja finalizar o ' + ($('[name=orc]').val() == 1 ? 'orçamento' : 'pedido') + '?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				if ($('[name=orc]').val() == 0 && !$('input[name=codigo_e_loja_do_cliente]').val())
				{
					mensagem('Selecione um cliente.');
				}
				else if ($('[name=orc]').val() == 1 && (!$('input[name=codigo_e_loja_do_cliente]').val() && !$('input[name=id_pro]').val()))
				{
					mensagem('Selecione um prospect ou cliente.');
				}
				else if ($('select[name=tipo_de_pedido]').val() != 'V' && !$('input[name=pedido_autorizado]').val())
				{
					mensagem('Preencha o campo "Pedido autorizado por".');
				}
				else
				{
					db.transaction(function(x) {
						x.executeSql(
							'SELECT * FROM pedidos WHERE codigo = "" AND orc = ? LIMIT 1', [$('[name=orc]').val()], function(x, dados) {
								if (!dados.rows.length)
								{
									mensagem('Adicione pelo menos um item no pedido.');
								}
								else
								{
									var data_de_entrega = $('input[name=data_de_entrega]').val();
									
									if (data_de_entrega && !data_de_entrega.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/))
									{
										mensagem('Digite uma data de entrega válida.');
									}
									else
									{
										var codigo_da_forma_de_pagamento = $('select[name=codigo_da_forma_de_pagamento]').val();
										var descricao_da_forma_de_pagamento = $('select[name=codigo_da_forma_de_pagamento] option:selected').text();
										
										var tipo_de_frete = $('select[name=tipo_de_frete]').val();
										
										var codigo_da_transportadora = '';
										var nome_da_transportadora = '';
										
										if (tipo_de_frete != 'cif')
										{
											codigo_da_transportadora = $('select[name=codigo_da_transportadora]').val();
											nome_da_transportadora = $('select[name=codigo_da_transportadora] option:selected').text();
										}
										
										var id_do_evento = intval($('select[name=id_do_evento]').val());
										var nome_do_evento = $('select[name=id_do_evento] option:selected').text();
										
										var ordem_de_compra = $('input[name=ordem_de_compra]').val();
										
										if (data_de_entrega)
										{
											data_de_entrega = data_de_entrega.split('/');
											data_de_entrega = data_de_entrega[2] + data_de_entrega[1] + data_de_entrega[0];
										}
										else
										{
											data_de_entrega = '';
										}
										
										var observacao = $('textarea[name=observacao]').val();
										
										var unidade = $('select[name=unidade]').val();

										var tipo_de_pedido = $('select[name=tipo_de_pedido]').val();

										var pedido_autorizado = $('input[name=pedido_autorizado]').val();

	
										
										x.executeSql('UPDATE pedidos SET codigo_da_forma_de_pagamento = ?, descricao_da_forma_de_pagamento = ?, tipo_de_frete = ?, codigo_da_transportadora = ?, nome_da_transportadora = ?, id_do_evento = ?, nome_do_evento = ?, ordem_de_compra = ?, data_de_entrega = ?, observacao = ?, unidade = ?, tipo_de_pedido = ?, pedido_autorizado = ? WHERE codigo = "" AND orc = ?', [codigo_da_forma_de_pagamento, descricao_da_forma_de_pagamento, tipo_de_frete, codigo_da_transportadora, nome_da_transportadora, id_do_evento, nome_do_evento, ordem_de_compra, data_de_entrega, observacao, unidade, tipo_de_pedido, pedido_autorizado, $('[name=orc]').val()]);
										
										// ** verificar se o frete foi alterado
										
										var valor_do_frete_do_pedido = $('input[name=valor_do_frete]').val();
										
										if (valor_do_frete_do_pedido != '0,00')
										{
											valor_do_frete_do_pedido = floatval(str_replace(',', '.', valor_do_frete_do_pedido));
											
											x.executeSql('UPDATE pedidos SET valor_do_frete_do_pedido = ?, valor_do_frete_alterado = 1 WHERE codigo = "" AND orc = ?', [valor_do_frete_do_pedido, $('[name=orc]').val()]);
											
											x.executeSql(
												'SELECT valor_dos_produtos_do_pedido FROM pedidos WHERE codigo = "" AND orc = ?', [$('[name=orc]').val()], function(x, dados) {
													if (dados.rows.length)
													{
														var dado = dados.rows.item(0);
														
														var valor_total_do_pedido = round(dado.valor_dos_produtos_do_pedido + valor_do_frete_do_pedido, 2);
														
														x.executeSql('UPDATE pedidos SET valor_total_do_pedido = ? WHERE codigo = "" AND orc = ?', [valor_total_do_pedido, $('[name=orc]').val()]);
													}
												}
											);
										}
										else
										{
											x.executeSql('UPDATE pedidos SET valor_do_frete_alterado = 0 WHERE codigo = "" AND orc = ?', [$('[name=orc]').val()]);
										}
										
										// verificar se o frete foi alterado **
										
										if (localStorage['codigo_do_pedido_sendo_editado'])
										{
											var codigo_do_pedido = localStorage['codigo_do_pedido_sendo_editado'];
											
											x.executeSql('DELETE FROM pedidos WHERE codigo = ?', [codigo_do_pedido], function(x, dados) {
												x.executeSql('UPDATE pedidos SET codigo = ?, pedido_sendo_editado = 0 WHERE codigo = "" AND orc = ?', [codigo_do_pedido, $('[name=orc]').val()]);
												
												localStorage['codigo_do_pedido_sendo_editado'] = '';
												
												// google chrome bug
												// sem o timeout, por algum motivo o pedido é deletado
												setTimeout(function() {
													window.location = 'pedidos_espelho.html#' + codigo_do_pedido;
												}, 1000);
											});
										}
										else
										{
											// ** se existia um pedido sendo editado, vamos excluí-lo para previnir erros
											
											var codigo_do_pedido_sendo_editado = localStorage['codigo_do_pedido_sendo_editado'];
											
											if (codigo_do_pedido_sendo_editado)
											{
												x.executeSql('DELETE FROM pedidos WHERE codigo = ?', [codigo_do_pedido_sendo_editado], function(x, dados) {
													localStorage['codigo_do_pedido_sendo_editado'] = '';
												});
											}
											
											// se existia um pedido sendo editado, vamos excluí-lo para previnir erros **
											
											//var codigo_do_pedido = uniqid(info.cod_rep + '-');
											var codigo_do_pedido = info.cod_rep + '-' + new Date().getTime();
											
											x.executeSql(
												'SELECT id FROM pedidos WHERE codigo = ?', [codigo_do_pedido], function(x, dados) {
													// se um pedido foi encontrado com o uniqid gerado, vamos usar outro método para gerar o id
													if (dados.rows.length)
													{
														codigo_do_pedido = info.cod_rep + '-' + new Date().getTime();
														
														x.executeSql('UPDATE pedidos SET codigo = ? WHERE codigo = "" AND orc = ?', [codigo_do_pedido, $('[name=orc]').val()]);
														
														// google chrome bug
														// sem o timeout, por algum motivo o pedido é deletado
														setTimeout(function() {
															window.location = 'pedidos_espelho.html#' + codigo_do_pedido;
														}, 1000);
													}
													
													// se um pedido nao foi encontrado com o uniqid gerado vamos usá-lo
													else
													{
														x.executeSql('UPDATE pedidos SET codigo = ? WHERE codigo = "" AND orc = ?', [codigo_do_pedido, $('[name=orc]').val()]);
														
														// google chrome bug
														// sem o timeout, por algum motivo o pedido é deletado
														setTimeout(function() {
															window.location = 'pedidos_espelho.html#' + codigo_do_pedido;
														}, 1000);
													}
												}
											);
										}
									}
								}
							}
						);
					});
				}
			}
		});
		
		return false;
	});
	
	// verificar tipo de pedido
	
	verificar_tipo_de_pedido();
	
	
	$('select[name=tipo_de_pedido]').change(function() {
		verificar_tipo_de_pedido();
		
		//$(this).attr('disabled', true);
	});
});

function verificar_tipo_de_pedido()
{
	var tp = $('select[name=tipo_de_pedido]').val();
	
	if (tp == 'V')
	{
		$('input[name=pedido_autorizado]').val('').parents('p').hide();
	}
	else
	{
		$('input[name=pedido_autorizado]').parents('p').show();
	}
	
	//
	
	if (tp == 'X' || tp == 'B')
	{
		$('[name=prc]').attr('disabled', false);
	}
	else
	{
		$('[name=prc]').attr('disabled', true);
	}
	
	//
	
	if (tp == 'B')
	{
		$('[name=codigo_da_forma_de_pagamento]').val('001');
	}
}

function obter_info_cli(codigo, loja)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT razao_social, cnpj, endereco, bairro, cep, municipio, estado, limite_de_credito, telefone, titulos_em_aberto, titulos_vencidos FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados) {
				if (dados.rows.length && $('input[name=codigo_e_loja_do_cliente]').val())
				{
					var dado = dados.rows.item(0);
					
					$('#info_cli').html('<p><strong>' + dado.razao_social + '</strong> - ' + dado.cnpj +  '</p><ul style="float: left; margin-right: 10px;"> <li><strong>Limite de crédito:</strong> R$ ' + number_format(dado.limite_de_credito, 2, ',', '.') + '</li> <li><strong>Títulos em aberto:</strong> R$ ' + number_format(dado.titulos_em_aberto, 2, ',', '.') + '</li> <li><strong>Títulos vencidos:</strong> <span style="color: #cc0000"><strong>R$ ' + number_format(dado.titulos_vencidos, 2, ',', '.') + '</strong> </span></li> </ul><ul style="float: left; margin-right: 10px;"><li><strong>CEP:</strong> ' + dado.cep + '</li><li><strong>Cidade:</strong> ' + dado.municipio +  '</li><li><strong>Estado:</strong> ' + dado.estado + '</li></ul><ul style="float: left; margin-right: 10px;"><li><strong>Endereço:</strong> ' + dado.endereco + '</li><li><strong>Bairro:</strong> ' + dado.bairro + '</li><li><strong>Telefone:</strong> ' + dado.telefone + '</li></ul><div style="clear: both;"></div>').slideDown();
				}
				else
				{
					$('#info_cli').slideUp();
				}
			}
		);
	});
}

function obter_info_pro(id)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT nome, cpf, endereco, numero, complemento, bairro, cep, cidade, estado FROM prospects WHERE id = ?', [id], function(x, dados) {
				if (dados.rows.length && $('input[name=id_pro]').val())
				{
					var dado = dados.rows.item(0);
					
					$('#info_cli').html('<p><strong>' + dado.nome + '</strong> - ' + dado.cpf +  '</p><ul style="float: left; margin-right: 10px;"><li><strong>Endereço:</strong> ' + (dado.endereco + (dado.numero ? ', ' + dado.numero : '') + (dado.complemento ? ', ' + dado.complemento : '')) + '</li><li><strong>Bairro:</strong> ' + dado.bairro + '</li><li><strong>CEP:</strong> ' + dado.cep + '</li></ul><ul style="float: left; margin-right: 10px;"><li><strong>Cidade:</strong> ' + dado.cidade +  '</li><li><strong>Estado:</strong> ' + dado.estado + '</li></ul></ul><div style="clear: both;"></div>').slideDown();
				}
				else
				{
					$('#info_cli').slideUp();
				}
			}
		);
	});
}

function adicionar_produto(codigo_do_cliente, codigo_da_tabela, codigo_do_produto, quantidade, desconto)
{
	desconto = prc2sql(desconto);
	
	var total_itens = $('#total_itens').val();
	
	if ($('[name=orc]').val() == 0)
	{
		if (!codigo_do_cliente)
		{
			mensagem('Selecione um cliente.');
		}
		else if (codigo_da_tabela == 0)
		{
			mensagem('Selecione uma tabela de preços.');
		}
		else if (!codigo_do_produto)
		{
			mensagem('Selecione um produto.');
		}
		else if (prc2sql($('[name=prc]').val()) <= 0)
		{
			mensagem('Digite um preço.');
		}
		else if (!quantidade || quantidade <= 0)
		{
			mensagem('Digite uma quantidade.');
		}
		else if (desconto > 100)
		{
			mensagem('O desconto não pode ser maior que 100%.');
		}
		else if(total_itens >= 30)
		{
			mensagem('A quantidade máxima por pedido é de 30 itens.');
		}
		else
		{
			var codigo_e_loja_do_cliente = codigo_do_cliente;
			codigo_e_loja_do_cliente = codigo_e_loja_do_cliente.split('|');
			
			var codigo_do_cliente = codigo_e_loja_do_cliente[0];
			var loja_do_cliente = codigo_e_loja_do_cliente[1];
			
			var quantidade = intval(str_replace('.', '', quantidade));
			
			db.transaction(function(x) {
				x.executeSql(
					'SELECT produtos.codigo AS codigo_do_produto, produtos.descricao AS descricao_do_produto, produtos.unidade_de_medida AS um_do_produto, produtos.peso AS peso_do_produto, produtos.ipi AS ipi, produtos.peso AS peso, tabelas_de_preco.codigo AS codigo_da_tabela_de_preco, tabelas_de_preco.descricao AS descricao_da_tabela_de_preco, tabelas_de_preco.preco_do_produto AS preco, clientes.codigo AS codigo_do_cliente, clientes.loja AS loja_do_cliente, clientes.razao_social AS razao_social_do_cliente, clientes.cnpj AS cnpj_do_cliente FROM produtos INNER JOIN tabelas_de_preco ON tabelas_de_preco.codigo_do_produto = produtos.codigo INNER JOIN clientes ON clientes.codigo = ? AND clientes.loja = ? WHERE produtos.codigo = ? AND tabelas_de_preco.codigo = ?', [codigo_do_cliente, loja_do_cliente, codigo_do_produto, codigo_da_tabela], function(x, dados) {
						if (dados.rows.length)
						{
							var dado = dados.rows.item(0);
							
						
							x.executeSql(
								'SELECT quantidade FROM pedidos WHERE codigo = "" AND codigo_da_tabela_de_preco = ? AND codigo_do_produto = ? AND orc = ?', [codigo_da_tabela, codigo_do_produto, $('[name=orc]').val()], function(x, dados_1) {
									if (dados_1.rows.length > 0)
									{
										var dado_1 = dados_1.rows.item(0);
										
										quantidade = dado_1.quantidade + quantidade;
										
										x.executeSql('UPDATE pedidos SET quantidade = ? WHERE codigo = "" AND codigo_da_tabela_de_preco = ? AND codigo_do_produto = ? AND orc = ?', [quantidade, codigo_da_tabela, codigo_do_produto, $('[name=orc]').val()]);
										
										mensagem('Produto já adicionado. As quantidades serão somadas.');
									}
									else
									{
										var preco = prc2sql($('[name=prc]').val());
										
										x.executeSql('INSERT INTO pedidos (id, timestamp, exportado, situacao, codigo, codigo_do_cliente, loja_do_cliente, razao_social_do_cliente, cnpj_do_cliente, codigo_da_tabela_de_preco, descricao_da_tabela_de_preco, codigo_do_produto, descricao_do_produto, um_do_produto, peso_do_produto, quantidade, quantidade_faturada, preco, ipi, desconto, st, valor_st, orc) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [0, mktime(0, 0, 0), 0, 'aguardando_faturamento', '', dado.codigo_do_cliente, dado.loja_do_cliente, dado.razao_social_do_cliente, dado.cnpj_do_cliente, dado.codigo_da_tabela_de_preco, dado.descricao_da_tabela_de_preco, dado.codigo_do_produto, dado.descricao_do_produto, dado.um_do_produto, dado.peso_do_produto, quantidade, 0, preco, dado.ipi, desconto, dado.st, 0, $('[name=orc]').val()]);
									}
								}
							);
							
							exibir_itens_do_pedido_offline();
							
							$('.itens, .outras_informacoes').show();
							
							//$('input[name=_codigo_do_produto]').val(dado.descricao_do_produto).focus();
							$('input[name=_codigo_do_produto]').val('');
							//$('input[name=codigo_do_produto]').val('');
							$('input[name=quantidade]').val('');
							$('input[name=desconto]').val('');
							//$('input[name=prc]').val('');
							$('#prc_fin').val('00,00');
							
							//-------------------------------------------------------------------------------
							$('#tabela_preco').attr('disabled', 'disabled');
							//-------------------------------------------------------------------------------
							
							obter_total_itens();
								
							
						}
						else
						{
							mensagem('Selecione um produto.');
						}
					}
				);
			});
		}
	}
	// orçamento
	else
	{
		if (!codigo_do_cliente && !$('[name=id_pro]').val())
		{
			mensagem('Selecione um prospect ou cliente.');
		}
		else if (codigo_da_tabela == 0)
		{
			mensagem('Selecione uma tabela de preços.');
		}
		else if (!codigo_do_produto)
		{
			mensagem('Selecione um produto.');
		}
		else if (!quantidade)
		{
			mensagem('Digite uma quantidade.');
		}
		else
		{
			var id_pro = $('[name=id_pro]').val();
			
			// prospect selecionado
			if (id_pro)
			{
				var quantidade = intval(str_replace('.', '', quantidade));
				
				db.transaction(function(x) {
					x.executeSql(
						'SELECT produtos.codigo AS codigo_do_produto, produtos.descricao AS descricao_do_produto, produtos.unidade_de_medida AS um_do_produto, produtos.peso AS peso_do_produto, produtos.ipi AS ipi, produtos.peso AS peso, tabelas_de_preco.codigo AS codigo_da_tabela_de_preco, tabelas_de_preco.descricao AS descricao_da_tabela_de_preco, tabelas_de_preco.preco_do_produto AS preco, prospects.id AS id_pro, prospects.nome AS raz_soc_pro, prospects.cgc AS cpf_pro FROM produtos INNER JOIN tabelas_de_preco ON tabelas_de_preco.codigo_do_produto = produtos.codigo INNER JOIN prospects ON prospects.id = ? WHERE produtos.codigo = ? AND tabelas_de_preco.codigo = ?', [id_pro, codigo_do_produto, codigo_da_tabela], function(x, dados) {
							if (dados.rows.length)
							{
								var dado = dados.rows.item(0);
								
								if (desconto && desconto > dado.desc_max && $('select[name=tipo_de_pedido]').val() == 'V')
								{
									mensagem('O desconto máximo para este produto é ' + number_format(dado.desc_max, 2, ',', '.') + '%.');
								}
								else
								{
									x.executeSql(
										'SELECT quantidade FROM pedidos WHERE codigo = "" AND codigo_da_tabela_de_preco = ? AND codigo_do_produto = ? AND orc = ?', [codigo_da_tabela, codigo_do_produto, $('[name=orc]').val()], function(x, dados_1) {
											if (dados_1.rows.length > 0)
											{
												var dado_1 = dados_1.rows.item(0);
												
												quantidade = dado_1.quantidade + quantidade;
												
												x.executeSql('UPDATE pedidos SET quantidade = ? WHERE codigo = "" AND codigo_da_tabela_de_preco = ? AND codigo_do_produto = ? AND orc = ?', [quantidade, codigo_da_tabela, codigo_do_produto, $('[name=orc]').val()]);
												
												mensagem('Produto já adicionado. As quantidades serão somadas.');
											}
											else
											{
												var preco = prc2sql($('[name=prc]').val());
												
												x.executeSql('INSERT INTO pedidos (id, timestamp, exportado, situacao, codigo, id_pro, cgc_pro, raz_soc_pro, cpf_pro, codigo_da_tabela_de_preco, descricao_da_tabela_de_preco, codigo_do_produto, descricao_do_produto, um_do_produto, peso_do_produto, quantidade, quantidade_faturada, preco, ipi, desconto, st, valor_st, orc) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [0, mktime(0, 0, 0), 0, 'aguardando_faturamento', '', dado.id_pro, $('[name=cgc_pro]').val(), dado.raz_soc_pro, dado.cpf_pro, dado.codigo_da_tabela_de_preco, dado.descricao_da_tabela_de_preco, dado.codigo_do_produto, dado.descricao_do_produto, dado.um_do_produto, dado.peso_do_produto, quantidade, 0, preco, dado.ipi, desconto, dado.st, 0, $('[name=orc]').val()]);
											}
										}
									);
									
									exibir_itens_do_pedido_offline();
									
									$('.itens, .outras_informacoes').show();
									
									//$('input[name=_codigo_do_produto]').val(dado.descricao_do_produto).focus();
									$('input[name=_codigo_do_produto]').val('');
									//$('input[name=codigo_do_produto]').val('');
									$('input[name=quantidade]').val('');
									$('input[name=desconto]').val('');
									//$('input[name=prc]').val('');
									$('#prc_fin').val('00,00');
									
									//-------------------------------------------------------------------------------
									$('#tabela_preco').attr('disabled', 'disabled');
									//-------------------------------------------------------------------------------
									
								}
							}
							else
							{
								mensagem('Selecione um produto.');
							}
						}
					);
				});
			}
			
			// cliente selecionado
			else
			{
				var codigo_e_loja_do_cliente = codigo_do_cliente;
				codigo_e_loja_do_cliente = codigo_e_loja_do_cliente.split('|');
				
				var codigo_do_cliente = codigo_e_loja_do_cliente[0];
				var loja_do_cliente = codigo_e_loja_do_cliente[1];
				
				var quantidade = intval(str_replace('.', '', quantidade));
				
				db.transaction(function(x) {
					x.executeSql(
						'SELECT produtos.codigo AS codigo_do_produto, produtos.descricao AS descricao_do_produto, produtos.unidade_de_medida AS um_do_produto, produtos.peso AS peso_do_produto, produtos.ipi AS ipi, produtos.peso AS peso, tabelas_de_preco.codigo AS codigo_da_tabela_de_preco, tabelas_de_preco.descricao AS descricao_da_tabela_de_preco, tabelas_de_preco.preco_do_produto AS preco, tabelas_de_preco.st AS st, tabelas_de_preco.desc_max AS desc_max, clientes.codigo AS codigo_do_cliente, clientes.loja AS loja_do_cliente, clientes.razao_social AS razao_social_do_cliente, clientes.cnpj AS cnpj_do_cliente FROM produtos INNER JOIN tabelas_de_preco ON tabelas_de_preco.codigo_do_produto = produtos.codigo INNER JOIN clientes ON clientes.codigo = ? AND clientes.loja = ? WHERE produtos.codigo = ? AND tabelas_de_preco.codigo = ?', [codigo_do_cliente, loja_do_cliente, codigo_do_produto, codigo_da_tabela], function(x, dados) {
							if (dados.rows.length)
							{
								var dado = dados.rows.item(0);
								
								if (desconto && desconto > dado.desc_max && $('select[name=tipo_de_pedido]').val() == 'V')
								{
									mensagem('O desconto máximo para este produto é ' + number_format(dado.desc_max, 2, ',', '.') + '%.');
								}
								else
								{
									x.executeSql(
										'SELECT quantidade FROM pedidos WHERE codigo = "" AND codigo_da_tabela_de_preco = ? AND codigo_do_produto = ? AND orc = ?', [codigo_da_tabela, codigo_do_produto, $('[name=orc]').val()], function(x, dados_1) {
											if (dados_1.rows.length > 0)
											{
												var dado_1 = dados_1.rows.item(0);
												
												quantidade = dado_1.quantidade + quantidade;
												
												x.executeSql('UPDATE pedidos SET quantidade = ? WHERE codigo = "" AND codigo_da_tabela_de_preco = ? AND codigo_do_produto = ? AND orc = ?', [quantidade, codigo_da_tabela, codigo_do_produto, $('[name=orc]').val()]);
												
												mensagem('Produto já adicionado. As quantidades serão somadas.');
											}
											else
											{
												var preco = prc2sql($('[name=prc]').val());
												
												x.executeSql('INSERT INTO pedidos (id, timestamp, exportado, situacao, codigo, codigo_do_cliente, loja_do_cliente, razao_social_do_cliente, cnpj_do_cliente, codigo_da_tabela_de_preco, descricao_da_tabela_de_preco, codigo_do_produto, descricao_do_produto, um_do_produto, peso_do_produto, quantidade, quantidade_faturada, preco, ipi, desconto, st, valor_st, orc) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [0, mktime(0, 0, 0), 0, 'aguardando_faturamento', '', dado.codigo_do_cliente, dado.loja_do_cliente, dado.razao_social_do_cliente, dado.cnpj_do_cliente, dado.codigo_da_tabela_de_preco, dado.descricao_da_tabela_de_preco, dado.codigo_do_produto, dado.descricao_do_produto, dado.um_do_produto, dado.peso_do_produto, quantidade, 0, preco, dado.ipi, desconto, dado.st, 0, $('[name=orc]').val()]);
											}
										}
									);
									
									exibir_itens_do_pedido_offline();
									
									$('.itens, .outras_informacoes').show();
									
									$('input[name=_codigo_do_produto]').val(dado.descricao_do_produto).focus();
									//$('input[name=codigo_do_produto]').val('');
									$('input[name=quantidade]').val('');
									$('input[name=desconto]').val('');
								}
							}
							else
							{
								mensagem('Selecione um produto.');
							}
						}
					);
				});
			}
		}
	}
}

function exibir_itens_do_pedido_offline()
{
	// calcular totais do pedido offline
	calcular_totais_do_pedido("");
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedidos WHERE codigo = "" AND orc = ? ORDER BY id ASC', [$('[name=orc]').val()], function(x, dados) {
				$('.itens_do_pedido tbody').empty();
				
				console.log('Total de itens: '+dados.rows.length);
				if (dados.rows.length)
				{
					//Se já existir produto adicionado então travar tabela de preço
					$('#tabela_preco').attr('disabled', 'disabled');
					
					for (i = dados.rows.length - 1; i >= 0; i--)
					{
						var dado = dados.rows.item(i);
												
						//<td>' + dado.descricao_da_tabela_de_preco + '</td>
						console.log(dado);
						
						$('.itens_do_pedido tbody').append('<tr><td style="text-align: center;">' + str_pad(i + 1, 2, 0, 'STR_PAD_LEFT') + '</td><td>' + dado.codigo_do_produto + '</td><td>' + dado.descricao_do_produto + '</td><td>' + dado.um_do_produto + '</td><td style="text-align: right;">' + number_format(dado.preco, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.preco - round(dado.preco * (dado.desconto / 100), 2), 2, ',', '.') + '</td><td style="text-align: right;"><a href="#" class="editar_quantidade" data-codigo_do_pedido="' + dado.codigo + '" data-codigo_do_produto="_' + dado.codigo_do_produto + '" data-descricao_do_produto="' + dado.descricao_do_produto + '" data-codigo_da_tabela_de_preco="_' + dado.codigo_da_tabela_de_preco + '">' + number_format(dado.quantidade, 0, ',', '.') + '</a></td><td style="text-align: right;">' + number_format(dado.total_sem_ipi, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.ipi, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.total_com_ipi, 2, ',', '.') + '</td><td style="text-align: right;"><a href="#" class="editar_desconto" data-desconto="' + number_format(dado.desconto, 2, ',', '') + '" data-codigo_do_pedido="' + dado.codigo + '" data-codigo_do_produto="_' + dado.codigo_do_produto + '" data-descricao_do_produto="' + dado.descricao_do_produto + '" data-codigo_da_tabela_de_preco="_' + dado.codigo_da_tabela_de_preco + '">' + number_format(dado.valor_desconto, 2, ',', '.') + '</a></td><td style="text-align: right;">' + number_format(dado.total_final, 2, ',', '.') + '</td><td width="90"><a id="editar_novo" data-peso="' + dado.peso_do_produto + '" data-preco-prod="' + dado.preco + '" data-preco="' + number_format(dado.preco, 2, ',', '.') + '" data-qtde="' + number_format(dado.quantidade, 0, ',', '.') + '" data-cod_prod="' + dado.codigo_do_produto + '" data-pro-st="' + dado.st + '" data-ipi="' + dado.ipi + '" data-desconto="' + number_format(dado.desconto, 2, ',', '') + '" data-codigo_do_pedido="' + dado.codigo + '" data-tabela-preco="' + dado.codigo_da_tabela_de_preco + '" data-desc_prod="' + dado.descricao_do_produto + '" href="#tabela_itens" style="margin-right: 0; padding: 0 5px;">Editar</a> - <a href="#" class="excluir_item" data-codigo_do_pedido="' + dado.codigo + '" data-codigo_do_produto="_' + dado.codigo_do_produto + '" data-descricao_do_produto="' + dado.descricao_do_produto + '" data-codigo_da_tabela_de_preco="_' + dado.codigo_da_tabela_de_preco + '">Excluir</a></td></tr>');
					}
					
					$('#valor_total_desconto').text(number_format(dado.desconto_total_pedido, 2, ',', '.'));
					$('#valor_total_do_pedido').text(number_format(dado.valor_total_do_pedido, 2, ',', '.'));
					
					$('#valor_do_frete_2').text(number_format(dado.valor_do_frete_do_pedido, 2, ',', '.'));
					
				}else{
					console.log('Destravar tabela de preço.');
					//Se não existir produto adicionado então destravar tabela de preço
					$('#tabela_preco').removeAttr('disabled');					
				}
			}
		);
	});
}

function calcular_totais_do_pedido(codigo_do_pedido)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedidos WHERE codigo = "" AND orc = ?', [$('[name=orc]').val()], function(x, dados) {
				if (dados.rows.length)
				{
					var peso_total_do_pedido = 0;
					var valor_dos_produtos_do_pedido = 0;
					var desconto_total_pedido = 0;
					
					for (i = dados.rows.length - 1; i >= 0; i--)
					{
						var dado = dados.rows.item(i);
						
						// atualizar valores do item atual
						
						var total_sem_ipi = round(dado.preco * dado.quantidade, 2);
						
						var total_com_ipi = round(total_sem_ipi + ((dado.preco * (dado.ipi / 100)) * dado.quantidade), 2);
						var peso_total = round(dado.peso_do_produto * dado.quantidade, 3);
						
						var valor_desconto = round(total_sem_ipi * (dado.desconto / 100), 2);
						
						var total_final = total_com_ipi - valor_desconto;
						
						x.executeSql('UPDATE pedidos SET total_sem_ipi = ?, total_com_ipi = ?, peso_total = ?, valor_desconto = ?, total_final = ? WHERE codigo = ? AND codigo_da_tabela_de_preco = ? AND codigo_do_produto = ? AND orc = ?', [total_sem_ipi, total_com_ipi, peso_total, valor_desconto, total_final, codigo_do_pedido, dado.codigo_da_tabela_de_preco, dado.codigo_do_produto, $('[name=orc]').val()]);

						// calcular valores totais do pedido
						
						peso_total_do_pedido += peso_total;
						valor_dos_produtos_do_pedido += total_com_ipi;
						desconto_total_pedido += valor_desconto;
					}
					
					peso_total_do_pedido = round(peso_total_do_pedido, 2);
					valor_dos_produtos_do_pedido = round(valor_dos_produtos_do_pedido, 2);
					desconto_total_pedido = round(desconto_total_pedido, 2);
					
					var preco_medio_por_kg_do_pedido = round(valor_dos_produtos_do_pedido / peso_total_do_pedido, 2);
					
					var valor_total_do_pedido = round(valor_dos_produtos_do_pedido - desconto_total_pedido, 2);
					
					x.executeSql('UPDATE pedidos SET peso_total_do_pedido = ?, preco_medio_por_kg_do_pedido = ?, valor_dos_produtos_do_pedido = ?, desconto_total_pedido = ?, valor_total_do_pedido = ?, nro_itens = ? WHERE codigo = ? AND orc = ?', [peso_total_do_pedido, preco_medio_por_kg_do_pedido, valor_dos_produtos_do_pedido, desconto_total_pedido, valor_total_do_pedido, dados.rows.length, codigo_do_pedido, $('[name=orc]').val()]);
				}
			}
		);
	});
}

function obter_total_itens()
{
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedidos WHERE codigo = "" AND orc = ? ORDER BY id ASC', [$('[name=orc]').val()], function(x, dados) {
				
				if (dados.rows.length)
				{
					
					$('#total_itens').val(parseInt(dados.rows.length));
					
				}
			}
		);
	});
	
}

function obter_formas_de_pagamento()
{
	$('select[name=codigo_da_forma_de_pagamento]').empty();
	
	db.transaction(function(x) {
		x.executeSql(
			'SELECT codigo, descricao FROM formas_de_pagamento WHERE filial = ? ORDER BY descricao ASC', [$('[name=unidade]').val()], function(x, dados) {
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=codigo_da_forma_de_pagamento]').append('<option value="' + dado.codigo + '">' + dado.descricao + '</option>');
					}
				}
			}
		);
	});
}

function obter_cgc_pro(id)
{
	db.transaction(function(x) {
		x.executeSql(
			'SELECT cgc FROM prospects WHERE id = ?', [id], function(x, dados) {
				
				var dado = dados.rows.item(0);
				
				var valor_cgc_pro = dado.cgc;
				var valor_cgc_pro_replace = valor_cgc_pro.replace(/[^0-9]+/g,'');  

				$('input[name=cgc_pro]').val(valor_cgc_pro_replace);
			
			}
		);
	});
}