var por_pagina = 10;
$(document).ready(function(){
	
	montar_paginacao();
	sessionStorage['pagina_itens'] = sessionStorage['pagina_itens'] || 1;
	sessionStorage['pagina_itens_troca'] = sessionStorage['pagina_itens_troca'] || 1;
	
	//Seleciona o valor atual ao entrar no campo
	$('input[name=quantidade], input[name=valor_unidade], input[name=desconto], input[name=preco_venda], input[name=codigo_palm]').live('click', function(e){
		$(this).select();
	});
	
	$('#erro_item').hide();
	$('#carregando_produtos').hide();
	
	$('[name=troca]').change(function(){
		if($(this).val() == 'S') {
			$('#adicionar_produto_troca').show();
			$('#adicionar_produto').hide();
			$('#unidade_medida_normal').hide();
			$('#unidade_medida_troca').show();
			
			//Sugere a 1ª Unidade de Medida para itens de troca
			$('select[name=unidade_medida_venda] option[value="'+$('input[name=unidade_medida]').val()+'"]').attr('selected', 'selected');
			
			//Se for um pedido de troca para a 1ª unidade de medida, utiliza o preço por unidade do produto
			if($('select[name=unidade_medida_venda]').val() == $('input[name=unidade_medida]').val()) {
				$('input[name=preco_venda]').val($('input[name=valor_unidade]').val());
			} else {
				$('input[name=preco_venda]').val($('input[name=preco_unitario]').val());
			}
		} else {
			$('#adicionar_produto_troca').hide();
			$('#adicionar_produto').show();
			$('#unidade_medida_normal').show();
			$('#unidade_medida_troca').hide();
			$('select[name=unidade_medida_venda] option[value="'+$('input[name=segunda_unidade_medida]').val()+'"]').attr('selected', 'selected');
			$('input[name=preco_venda]').val($('input[name=preco_unitario]').val());
		}
		
		$('select[name=unidade_medida_venda]').trigger('change');
		$('[name=desconto]').trigger('keyup');
	}).trigger('change');
	
	// Desativar todos os campos quando clicar em id_informacao_cliente
	
	$('#id_adicionar_produtos').click(function(){
		
		$('#id_outras_informacoes').addClass('tab_desativada');
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',1);
		
		ativar_tabs();
	});
	
	$('.salvar_sessao_edicao_dropdown').live('change', function(){
		var codigo_produto = $(this).data('codigo_produto');
		var sessao_produtos = obter_produtos_troca_sessao();
		
		var unidade_medida = converter_decimal($('select[name=editar_unidade_medida_venda_' + codigo_produto + ']').val());
		
		var indice = remover_zero_esquerda(str_replace('troca_', '', codigo_produto));
		sessao_produtos[indice]['unidade_medida_venda'] = unidade_medida;
		
		var produtos = serialize(sessao_produtos);
		salvar_produto_troca_sessao(produtos);
	});
	
	$('.salvar_sessao_edicao').live('keyup', 'focusout', function(e){
		var codigo_produto = $(this).data('codigo_produto');
		
		var desconto = converter_decimal($('input[name=editar_desconto_' + codigo_produto + ']').val());
		var quantidade = converter_decimal($('input[name=editar_quantidade_' + codigo_produto + ']').val());
		var preco_venda = converter_decimal($('input[name=editar_preco_venda_' + codigo_produto + ']').val());
		
		var indice = remover_zero_esquerda(str_replace('troca_', '', codigo_produto));
		
		if(strstr(codigo_produto, 'troca_')) {
			var sessao_produtos = obter_produtos_troca_sessao();
		} else {
			var sessao_produtos = obter_produtos_sessao();
		}
		
		sessao_produtos[indice]['desconto'] = desconto;
		sessao_produtos[indice]['quantidade'] = quantidade;
		sessao_produtos[indice]['preco_venda'] = preco_venda;
		
		var produtos = serialize(sessao_produtos);
		
		if(strstr(codigo_produto, 'troca_')) {
			salvar_produto_troca_sessao(produtos);
		} else {
			salvar_produto_sessao(produtos);
		}
	});
	
	/**
	* 
	* ID:			trocar_produto
	*
	* Descrição:	Utilizado para apagar todos os valores dos campos ligados ao produto par aque o representante possa selecionar um novo produto
	*
	*/
	$('#trocar_produto').live('click', function(){
		
		$('input[name=produto]').css('background-color', '#FFF');
		$('[name=codigo_palm]').val("");
		limpar_dados_produtos();
		$('[name=codigo_palm]').focus();
	});

	
	/**
	* 
	* ID:			adicionar_produto
	*
	* Descrição:	Utilizado para adicionar um produto e seus valores na sessao
	*
	*/
	$('#adicionar_produto, #adicionar_produto_troca').live('click', function(e){
		e.preventDefault();

		// TIPO DE PRODUTO
		var tipo_produto = '';
		if($(this).attr('id') == 'adicionar_produto_troca') {
			tipo_produto = 'troca';
		}
		// TIPO DE PRODUTO
		
		
		var campos = [];
		campos['codigo_produto'] 	= 'input[name=codigo_produto]';
		campos['preco_venda'] 		= 'input[name=preco_venda]';
		campos['quantidade'] 		= 'input[name=quantidade]';
		campos['desconto'] 			= 'input[name=desconto]';
		campos['preco_tabela']		= converter_decimal($('input[name=preco_tabela]').val());
		campos['valor_unidade']		= converter_decimal($('input[name=valor_unidade]').val());
		campos['fator_conversao']	= $('input[name=valor_produto_converter]').val();
		campos['tipo_converter']	= $('input[name=tipo_converter]').val();
		campos['tipo_produto']		= tipo_produto;
		
		
		if(validar_campos(null, serialize(campos))) {
			if(verificar_existe($('input[name=codigo_produto]').val(), tipo_produto)) {
				confirmar('O item <b>' + $('input[name=codigo_produto]').val() + ' - ' + $('input[name=descricao_produto]').val() + '</b> já foi adicionado ao pedido.<br><br>Deseja alterar os valores informados anteriormente?', function () {
					$(this).dialog('close');
					
					var codigo_produto = $('input[name=codigo_produto]').val();
					
					//Adicionar o novo item
					adicionar_item(tipo_produto);
					
					$("#confirmar_dialog").remove();
				});
			} else {
				adicionar_item(tipo_produto);
			}
			
			$('input[name=produto]').css('background-color', '#FFF');
			$('[name=codigo_palm]').val("");
			$('[name=codigo_palm]').focus();
		}


	});
	
	function adicionar_item(tipo_produto) {
		
		var codigo_produto 			= $('input[name=codigo_produto]').val();
		var descricao_produto 		= $('input[name=descricao_produto]').val();
		var preco_tabela 			= converter_decimal($('input[name=preco_tabela]').val());
		var preco_unitario 			= converter_decimal($('input[name=preco_unitario]').val());
		var preco_venda 			= converter_decimal($('input[name=preco_venda]').val());
		var quantidade 				= $('input[name=quantidade]').val();
		var desconto 				= converter_decimal($('input[name=desconto]').val());
		var ipi 					= converter_decimal($('input[name=ipi]').val());
		var unidade_medida 			= $('input[name=unidade_medida]').val();
		var segunda_unidade_medida 	= $('input[name=segunda_unidade_medida]').val();
		var local 					= $('input[name=local]').val();
		var valor_unidade 			= converter_decimal($('input[name=valor_unidade]').val());
		var valor_produto_converter = converter_decimal($('input[name=valor_produto_converter]').val());
		var peso_total 				= $('input[name=peso_total]').val();
		var tipo_converter			= $('input[name=tipo_converter]').val();
		var grupo_tributacao		= $('input[name=grupo_tributacao_produto]').val(); //CUSTOM E0184/2014 - Calculo de impostos offline
		
		//UM selecionada na venda quando for item de troca
		if($('select[name=troca]').val() == 'S') {
			var unidade_medida_venda = $('select[name=unidade_medida_venda]').val();
		} else {
			var unidade_medida_venda = $('input[name=unidade_medida_venda]').val();
		}
		
		if(tipo_produto == 'troca') {
			var sessao_produtos 	= obter_produtos_troca_sessao();
		} else {
			var sessao_produtos 	= obter_produtos_sessao();
		}
		
		var produtos = new Array();
		produtos[codigo_produto] = new Array();
		produtos[codigo_produto]['codigo'] 							= codigo_produto;
		produtos[codigo_produto]['descricao'] 						= descricao_produto;
		produtos[codigo_produto]['preco_tabela'] 					= number_format(preco_tabela, 2);
		produtos[codigo_produto]['preco_unitario'] 					= number_format(preco_unitario, 2);
		produtos[codigo_produto]['preco_venda'] 					= number_format(preco_venda, 2);
		produtos[codigo_produto]['quantidade'] 						= quantidade;
		produtos[codigo_produto]['desconto'] 						= number_format(desconto, 2);
		produtos[codigo_produto]['ipi'] 							= ipi;
		produtos[codigo_produto]['local'] 							= local;
		produtos[codigo_produto]['valor_unidade'] 					= number_format(valor_unidade, 2);
		produtos[codigo_produto]['valor_produto_converter'] 		= valor_produto_converter;
		produtos[codigo_produto]['peso_total'] 						= peso_total;
		produtos[codigo_produto]['unidade_medida'] 					= unidade_medida; //1ª UM
		produtos[codigo_produto]['segunda_unidade_medida']			= segunda_unidade_medida; //2ª UM
		produtos[codigo_produto]['unidade_medida_venda'] 			= unidade_medida_venda;	//U.M. de venda
		produtos[codigo_produto]['tipo_converter']					= tipo_converter; //Tipo de conversão (D ou M)
		produtos[codigo_produto]['grupo_tributacao']				= grupo_tributacao; //CUSTOM E0184/2014 - Calculo de impostos offline
		
		if(tipo_produto == 'troca') {
			produtos[codigo_produto]['item_troca'] 					= '1';
		} else {
			produtos[codigo_produto]['item_troca'] 					= '0';
		}
		var add_produto = produtos[codigo_produto];
		var produtos = serialize(produtos);
		
		//-------------------------------------------------
		// Salvando Produtos na Sessao
		if(tipo_produto == 'troca') {
			salvar_produto_troca_sessao(produtos);
		} else {
			salvar_produto_sessao(produtos);
		}
		
		montar_paginacao(tipo_produto);
		exibir_produtos(tipo_produto);
		
		$('#erro_item').hide('fast');
		
		limpar_dados_produtos();
		
		$('input[name=quantidade]').focus();
		
		$('select[name="troca"] option[value="N"]').attr('selected', 'selected');
		$('select[name="troca"]').trigger('change');
		
		$('input[name="codigo_palm"]').focus();
	}
	
	
	/**
	* 
	* ID:			avancar_passo_2
	*
	* Descrição:	Utilizado para salvar os dados editados e passar para o proximo passo
	*
	*/
	function validar_desconto(sessao_produtos, callback) {
		
		var total_preco_unitario				= 0;
		var total_preco_venda 					= 0;
		var total_desconto 						= 0;
		var desconto_condicao_pagamento 		= obter_valor_sessao('desconto_condicao_pagamento');
		var desconto_geral						= 0;
		
		$.each(sessao_produtos, function(key, objeto){
			total_preco_unitario 	+= parseFloat(number_format(objeto.preco_unitario, 3))  * parseFloat(number_format(objeto.quantidade, 3));
			total_preco_venda		+= parseFloat(number_format(objeto.preco_venda, 3))  * parseFloat(number_format(objeto.quantidade, 3));
		});
		
		total_desconto 			= (total_preco_unitario - total_preco_venda) * 100 / total_preco_unitario;
		total_desconto			= parseFloat((total_desconto > 0 ? total_desconto : 0)) + parseFloat(desconto_condicao_pagamento);
		
		// --------------------
		// Consultando Desconto
		
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM informacoes_do_representante', [], function(x, dados) {
				if (dados.rows.length) {
					var dado = dados.rows.item(0);
					
					if(parseFloat(number_format(total_desconto, 2)) <= parseFloat(dado.limite_desconto)) {
						callback()
					} else {
						mensagem('O desconto de <b>' + number_format(total_desconto, 3, ',', '.') + '%</b> não é permitido.<br />Seu limite de desconto é de <b>' + number_format(dado.limite_desconto, 3, ',', '.') + '%</b>.');
					}
				}
			});
		});
	}
	
	$('#avancar_passo_2').live('click', function(e){
	
		e.preventDefault();
	
		$('.erro_itens').remove();
		
		var sessao_produtos 		= obter_produtos_sessao();
		var sessao_produtos_troca 	= obter_produtos_troca_sessao();
		
		
		if(sessao_produtos) {
			var total_produtos 		= Object.keys(sessao_produtos).length;
		} else {
			var total_produtos 		= 0;
		}
		
		if(total_produtos > 0) {
			
			validar_desconto(sessao_produtos, function(){
			
				validar_itens_troca(function(retorno){
					
					if(retorno) {
						// Ativando Botao "Adicionar Produtos"
						$('#id_outras_informacoes').removeClass('tab_desativada');
						
						ativar_tabs();
						
						//Chamando as funções do outras_informacoes
						outras_informacoes();
						
						// Acionando (Click) botao "Adicionar Produtos"
						$("#id_outras_informacoes").click();
					} else {
						mensagem('O valor dos itens de troca deve ser inferior ao valor dos itens de venda.')
					}
				});
			});
		} else {
			mensagem('Adicione um produto no ' + obter_descricao_pedido('lower') + '.');
		}
	});
	
	function validar_itens_troca(callback) {
		var produtos = obter_produtos_sessao();
		var trocas = obter_produtos_troca_sessao();
		
		var total_produtos = 0;
		var total_trocas = 0;
		
		//Totalizar total do pedido
		$.each(produtos, function(x, obj){
			total_produtos += obj.quantidade * obj.preco_venda;
		});
		
		//Totalizar total de trocas
		$.each(trocas, function(x, obj){
			total_trocas += obj.quantidade * obj.preco_venda;
		});
		
		if(total_produtos <= total_trocas) {
			callback(false);
		} else {
			callback(true);
		}
	}
	
	/**
	* 
	* CLASSE:			excluir_item
	*
	* Descrição:	Utilizado para excluir um item na sessao
	*
	*/
	$('.excluir_item').live('click', function(e){
		e.preventDefault();
		
		var codigo_produto = $(this).attr('href');
		var indice_produto = remover_zero_esquerda(codigo_produto);
		
		var sessao_produtos = obter_produtos_sessao();
		
		var produto = sessao_produtos[indice_produto];
		
		confirmar('Deseja excluir o item <b>' + produto.codigo + ' - ' + produto.descricao + '</b>?', function () {
			$(this).dialog('close');
			
			delete sessao_produtos[indice_produto]; 
			
			salvar_produtos_excluidos(serialize(sessao_produtos));
			
			exibir_produtos(null);
			montar_paginacao(null);
			
			$("#confirmar_dialog").remove();
			
		});
		
	});
	
	/**
	* 
	* CLASSE:			excluir_item_troca
	*
	* Descrição:	Utilizado para excluir um item de troca na sessao
	*
	*/
	$('.excluir_item_troca').live('click', function(e){
		e.preventDefault();
		
		var codigo_produto = $(this).attr('href');
		var indice_produto = remover_zero_esquerda(codigo_produto);
		
		var sessao_produtos = obter_produtos_troca_sessao();
		
		var produto = sessao_produtos[indice_produto];
		
		confirmar('Deseja excluir o item <b>' + produto.codigo + ' - ' + produto.descricao + '</b>?', function () {
			$(this).dialog('close');
		
			delete sessao_produtos[indice_produto]; 
	
			salvar_produtos_excluidos(serialize(sessao_produtos), 'troca');
			
			montar_paginacao('troca');
			exibir_produtos('troca');
			
			$("#confirmar_dialog").remove();
		});
		
	});
	
	$('.pagina').live('click', function(){
		var pagina = $(this).data('page');
		var tipo_produto = $(this).data('tipo');
		
		if(tipo_produto == 'troca') {
			sessionStorage['pagina_itens_troca'] = pagina;
		} else {
			sessionStorage['pagina_itens'] = pagina;
		}
		
		montar_paginacao(tipo_produto);
		exibir_produtos(tipo_produto);
		
		return false;
	});

	//eduardo
	$('[name=codigo_palm]').keyup(function(){
		var term = $(this).val();
		var tamanho = term.length;
		var codigo_filial = obter_valor_sessao('filial');
		
		// Filtros necessários para obter os produtos corretamente			
		var wheres = '';
			wheres += ' AND ((substr(produto_codigo_palm, 1, ' + tamanho + ') LIKE "' + term + '%" AND length(produto_codigo_palm) <= ' + tamanho + ') OR produto_descricao LIKE "' + term + '%" )';

		if (codigo_filial && codigo_filial != 'undefined') {
			wheres += " AND (produto_filial = '" + codigo_filial + "' OR produto_filial = '')";
		}
		
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ?' + wheres, [obter_valor_sessao('tabela_precos')], function(x, dados) {
				if (dados.rows.length == 1) {
					var dado = dados.rows.item(0);
					
					var preco = dado.ptp_preco;
					var ptp_preco = dado.ptp_preco;
					var valor_unidade = 0;
					
					if(dado.produto_converter > 0) {
						if(dado.produto_tipo_converter == 'D') {
							valor_unidade = preco * dado.produto_converter;
							ptp_preco = ptp_preco * dado.produto_converter;
						} else if(dado.produto_tipo_converter == 'M') {
							valor_unidade = preco / dado.produto_converter;
							ptp_preco = ptp_preco / dado.produto_converter;
						} else {
							valor_unidade = preco;
						}
					} else {
						valor_unidade = preco;
					}
					
					var produto = {
						item : { 
							label: 							dado.produto_codigo_palm + ' - ' + dado.produto_descricao,
							codigo: 						dado.produto_codigo, 
							preco: 							valor_unidade, 
							preco_tabela: 					ptp_preco, 
							ipi: 							dado.produto_ipi, 
							descricao: 						dado.produto_descricao, 
							unidade_medida: 				dado.produto_unidade_medida, 
							local: 							dado.produto_locpad,
							valor_unidade:					preco,
							valor_produto_converter:		dado.produto_converter,
							peso_total:						dado.produto_peso_total,
							produto_segunda_unidade_medida:	dado.produto_segunda_unidade_medida,
							tipo_converter:					dado.produto_tipo_converter,
							grupo_tributacao:				dado.produto_grupo_tributacao
						}
					};
					
					$('input[name=produto]').attr('disabled', 'disabled');
					$('input[name=produto]').css('background-color', '#F2F2F2');
					$('#trocar_produto').show();
					verificar_produto_sessao(produto);
				} else {
					limpar_dados_produtos();
				}
			});
		});
	});
});

function montar_paginacao(tipo_produto) {
	if(tipo_produto == 'troca') {
		var produtos = obter_produtos_troca_sessao();
	} else {
		var produtos = obter_produtos_sessao();
	}
	
	if(produtos) {
		var total = Object.keys(produtos).length;
	}else{
		var total = 0;
	}
	
	var html = '';
	html += '<div class="pagination" id="pagination">';
	for(i = 0; i < ceil(total / por_pagina); i++){
		
		var classe = '';
		if(tipo_produto == 'troca') {
			if((i+1) == sessionStorage['pagina_itens_troca']) {
				classe = 'active';
			}
		} else {
			if((i+1) == sessionStorage['pagina_itens']) {
				classe = 'active';
			}
		}
		
		html += '<ul>';
		html += '<li><a href="" class="pagina ' + classe + '" data-tipo="' + tipo_produto + '" data-page="' + (i+1) + '">' + (i+1) + '</a></li>';
		html += '</ul>';
	}
	html += '</div>';
	
	if(tipo_produto == 'troca') {
		$('#page-selection-troca').html(html);
	} else {
		$('#page-selection').html(html);
	}
}

function adicionar_produtos() {

	// Recalcular valores caso ouver alteração no cliente ou tabela de preços
	//recalcular_precos();

	// Exibir valores do cabeçalho
	exibir_cabecalho();
	
	buscar_produtos();
	
	exibir_produtos(null);
	exibir_produtos('troca');
	
	// Função para os calculos do KEY UP
	calculos_automaticos({
		campo_preco_venda: 				'input[name=preco_venda]', 
		campo_preco_unitario:			'input[name=preco_unitario]', 
		campo_desconto:					'input[name=desconto]', 
		campo_quantidade:				'input[name=quantidade]', 
		campo_ipi:						'input[name=ipi]', 
		campo_total_item:				'input[name=total_item]', 
		campo_valor_unidade:			'input[name=valor_unidade]', 
		campo_valor_produto_converter:	'input[name=valor_produto_converter]',
		campo_unidade_medida_venda:		'select[name=unidade_medida_venda]',
		campo_unidade_medida:			'input[name=unidade_medida]',
	});
	
	alterarCabecalhoTabelaConteudo();
}

/**
* Metódo:		recalcular_precos
* 
* Descrição:	Função Utilizada para para recalcular preços e descontos quando a tabela de preços ou cliente é alterado
* 
* Data:			22/10/2013
* Modificação:	22/10/2013
* 
* @access		public
* @param		string 			codigo 	- codigo do produto

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function recalcular_precos() {

	var sessao_produtos 	= obter_produtos_sessao();
	if(sessao_produtos) {
		var total_produtos 		= Object.keys(sessao_produtos).length;
	}
	var indice = 0;

	
	if(sessao_produtos) {
		$.each(sessao_produtos, function(key, produto){
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ? AND produto_codigo = ?', [obter_valor_sessao('tabela_precos'), produto.codigo], function(x, dados) {
					if (dados.rows.length) {

						var dado = dados.rows.item(0);
						
						var ptp_preco = dado.ptp_preco;//teste aplicar_descontos_cabecalho(dado.ptp_preco);
						var preco = ptp_preco;
						
						if(dado.produto_converter > 0) {
							if(dado.produto_tipo_converter == 'D') {
								preco = ptp_preco * dado.produto_converter;
							} else if(dado.produto_tipo_converter == 'M') {
								preco = ptp_preco / dado.produto_converter;
							}
						}
						
						var codigo_produto 	= produto.codigo;
						
						//------------------------
						
						var produtos = new Array();
						produtos[codigo_produto] = new Array();
						produtos[codigo_produto]['codigo'] 			= produto.codigo;
						produtos[codigo_produto]['descricao'] 		= produto.descricao;
						produtos[codigo_produto]['preco_tabela'] 	= preco;
						produtos[codigo_produto]['preco_unitario'] 	= preco;
						//produtos[codigo_produto]['preco_venda'] 	= preco - (preco * (produto.desconto / 100));
						produtos[codigo_produto]['preco_venda'] 	= produto.preco_venda;
						produtos[codigo_produto]['quantidade'] 		= produto.quantidade;
						produtos[codigo_produto]['desconto'] 		= produto.desconto;
						produtos[codigo_produto]['ipi'] 			= produto.ipi;
						produtos[codigo_produto]['local'] 			= produto.local;
						produtos[codigo_produto]['TES'] 			= produto.TES;
						produtos[codigo_produto]['CF'] 				= produto.CF;
						produtos[codigo_produto]['ST'] 				= produto.ST;
						produtos[codigo_produto]['IPI'] 			= produto.IPI;
						produtos[codigo_produto]['ICMS'] 			= produto.ICMS;
						produtos[codigo_produto]['peso_total'] 		= produto.peso_total;
						produtos[codigo_produto]['valor_unidade'] 	= produto.valor_unidade;
						produtos[codigo_produto]['valor_produto_converter'] 	= produto.valor_produto_converter;
						
						produtos[codigo_produto]['unidade_medida'] 			= produto.unidade_medida;
						produtos[codigo_produto]['segunda_unidade_medida'] 	= produto.segunda_unidade_medida;
						produtos[codigo_produto]['unidade_medida_venda'] 	= produto.unidade_medida_venda;
						produtos[codigo_produto]['item_troca']				= produto.item_troca;
						produtos[codigo_produto]['grupo_tributacao']		= produto.grupo_tributacao;
						
						var produtos = serialize(produtos);
						
						salvar_produto_sessao(produtos);
						
						//------------------------

					} else {
					
						var codigo_produto 	= remover_zero_esquerda(produto.codigo);
						
						var produtos = new Array();
						produtos[codigo_produto] = new Array();
						produtos[codigo_produto]['codigo'] 			= produto.codigo;
						produtos[codigo_produto]['descricao'] 		= produto.descricao;
						produtos[codigo_produto]['preco_tabela'] 	= produto.preco_tabela;
						produtos[codigo_produto]['preco_unitario'] 	= produto.preco_unitario;
						produtos[codigo_produto]['preco_venda'] 	= produto.preco_venda
						produtos[codigo_produto]['quantidade'] 		= produto.quantidade;
						produtos[codigo_produto]['desconto'] 		= produto.desconto;
						produtos[codigo_produto]['ipi'] 			= produto.ipi;
						produtos[codigo_produto]['local'] 			= produto.local;
						produtos[codigo_produto]['TES'] 			= produto.TES;
						produtos[codigo_produto]['CF'] 				= produto.CF;
						produtos[codigo_produto]['ST'] 				= produto.ST;
						produtos[codigo_produto]['IPI'] 			= produto.IPI;
						produtos[codigo_produto]['ICMS'] 			= produto.ICMS;
						produtos[codigo_produto]['peso_total'] 		= produto.peso_total;
						produtos[codigo_produto]['valor_unidade'] 	= produto.valor_unidade;
						
						produtos[codigo_produto]['unidade_medida'] 			= produto.unidade_medida;
						produtos[codigo_produto]['segunda_unidade_medida'] 	= produto.segunda_unidade_medida;
						produtos[codigo_produto]['unidade_medida_venda'] 	= produto.unidade_medida_venda;
						
						produtos[codigo_produto]['grupo_tributacao'] 	= produto.grupo_tributacao;
						
						var produtos = serialize(produtos);
						
						salvar_produto_removido_sessao(produtos);
						
						delete sessao_produtos[codigo_produto]; 
	
						salvar_produtos_excluidos(serialize(sessao_produtos));
					}
					
					indice++;
					
					if(indice == total_produtos) {
						var sessao_produtos 	= obter_produtos_sessao();
						
						exibir_produtos(null);
					}
					
				});
			});
		});
	}
}

/**
* Metódo:		verificar_existe
* 
* Descrição:	Função Utilizada para saber se existe o produto na sessão
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo 	- codigo do produto

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function verificar_existe(codigo, tipo) {
	var codigo_produto 		= remover_zero_esquerda(codigo);
	
	if(tipo == 'troca') {
		var sessao_itens_troca 		= obter_produtos_troca_sessao();
		
		if(sessao_itens_troca[codigo_produto]) {
			return true;
		}
	} else {
		var sessao_itens_pedido 	= obter_produtos_sessao();
		
		if(sessao_itens_pedido[codigo_produto]) {
			return true;
		}
	}
	
	return false;
}

/**
* Metódo:		limpar_dados_produtos
* 
* Descrição:	Função Utilizada para apagar todos os campos ligados ao produto
* 
* Data:			19/10/2013
* Modificação:	23/10/2013
* 
* @access		public

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function limpar_dados_produtos() {
	$('input[name="produto"]').val("");
	$('input[name="codigo_produto"]').val("");
	$('input[name="descricao_produto"]').val("");
	$('input[name="preco_tabela"]').val("");
	$('input[name="preco_unitario"]').val("");
	$('input[name="preco_venda"]').val("");
	$('input[name="quantidade"]').val("");
	$('input[name="desconto"]').val("");
	$('input[name="ipi"]').val("");
	$('input[name="total_item"]').val("");
	$('input[name="local"]').val("");
	$('input[name="valor_unidade"]').val("");
	$('input[name="valor_produto_converter"]').val("");
	$('input[name="unidade_medida"]').val("");
	$('input[name="segunda_unidade_medida"]').val("");
	$('input[name="unidade_medida_venda"]').val("");
	$('input[name="grupo_tributacao_produto"]').val("");
	
	$('#trocar_produto').hide();
	$('input[name="produto"]').removeAttr('disabled');
	$('#erro_item').hide();
	
	$('select[name="troca"] option[value="N"]').attr('selected', 'selected');
	$('select[name="troca"]').trigger('change');
}

/**
* Metódo:		calculos_automaticos
* 
* Descrição:	Função Utilizada realizar os calculos automaticos de desconto e preço via keyup para tudo 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			campo_preco_venda		-	campo input do preco de venda
* @param		string 			campo_preco_unitario	-	campo input do preco unitario
* @param		string 			campo_desconto			-	campo input do desconto
* @param		string 			campo_quantidade		-	campo input do quantidade
* @param		string 			campo_ipi				-	campo input do ipi
* @param		string 			campo_total_geral		-	campo input do total geral
* @param		string 			campo_total_item		-	campo input do ptotal do item
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function calculos_automaticos(objeto_campos) {
	
	var campo_preco_venda =				objeto_campos.campo_preco_venda;
	var campo_preco_unitario =			objeto_campos.campo_preco_unitario;
	var campo_desconto =				objeto_campos.campo_desconto; 
	var campo_quantidade =				objeto_campos.campo_quantidade; 
	var campo_ipi =						objeto_campos.campo_ipi; 
	var campo_total_geral =				objeto_campos.campo_total_geral; 
	var campo_total_item =				objeto_campos.campo_total_item; 
	var campo_valor_unidade =			objeto_campos.campo_valor_unidade; 
	var campo_valor_produto_converter =	objeto_campos.campo_valor_produto_converter;
	var campo_peso_total =				objeto_campos.campo_peso_total;
	var campo_peso_unitario =			objeto_campos.campo_peso_unitario;
	
	var campo_unidade_medida =			objeto_campos.campo_unidade_medida;
	var campo_unidade_medida_venda =	objeto_campos.campo_unidade_medida_venda;
	
	$(campo_valor_unidade).keyup(function(){
		var preco_unidade		= converter_decimal($(this).val());
		var preco_venda			= converter_decimal($(campo_preco_venda).val());
		var percentual_desconto = converter_decimal($(campo_desconto).val());
		var valor_produto_converter =  converter_decimal($(campo_valor_produto_converter).val());
		
		var preco_unitario 			=  converter_decimal($(campo_preco_unitario).val());
		
		//Troca
		if($(campo_unidade_medida_venda).val() == $(campo_unidade_medida).val()) {
			$(campo_preco_venda).val($(this).val());
			
			var preco_unidade_original	= preco_unitario / valor_produto_converter;
			var valor_diferenca			= preco_unidade_original - preco_venda;
			var porcentagem 			= valor_diferenca / preco_unidade_original * 100;
		} else {
			var preco_venda = preco_unidade * valor_produto_converter;
			$(campo_preco_venda).val(number_format(preco_venda, 2, ',', '.'));
			
			var valor_diferenca 		= preco_unitario - preco_venda;
			var porcentagem 			= valor_diferenca / preco_unitario * 100;
		}
		
		if(porcentagem < 0) {
			porcentagem = 0;
		}
		
		// Iserindo porcentagem no campo
		$(campo_desconto).val(number_format(porcentagem, 2, ',', '.'));
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
	});
	
	$(campo_unidade_medida_venda).change(function(){
		//Se for um pedido de troca para a 1ª unidade de medida, utiliza o preço por unidade do produto
		if($(campo_unidade_medida_venda).val() == $(campo_unidade_medida).val()) {
			$(campo_preco_venda).val($(campo_valor_unidade).val());
		} else {
			$(campo_preco_venda).val($(campo_preco_unitario).val());
		}
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
	});
	
	// Calcular Desconto
	$(campo_preco_venda).keyup(function(){
		var preco_unitario 			=  converter_decimal($(campo_preco_unitario).val());
		var preco_venda 			=  converter_decimal($(campo_preco_venda).val());
		var preco_unidade			=  converter_decimal($(campo_valor_unidade).val());
		var valor_produto_converter =  converter_decimal($(campo_valor_produto_converter).val());
		
		if($(campo_unidade_medida_venda).val() == $(campo_unidade_medida).val()) {
			var preco_unidade_original	= preco_unitario / valor_produto_converter;
			var valor_diferenca			= preco_unidade_original - preco_venda;
			var percentagem 			= valor_diferenca / preco_unidade_original * 100;
		} else {
			var valor_diferenca 		= preco_unitario - preco_venda;
			var percentagem 			= valor_diferenca / preco_unitario * 100;
		}
		
		if(percentagem < 0) {
			percentagem = 0;
		}
		
		// Iserindo percentagem no campo
		$(campo_desconto).val(number_format(percentagem, 2, ',', '.'));
		
		//-----------------------------------------
		// Calculando valor da unidade com desconto
		var valor_unidade = 0;
		
		if(valor_produto_converter > 0) {
			if($(campo_unidade_medida_venda).val() == $(campo_unidade_medida).val()) {
				valor_unidade =  preco_venda;
			} else {
				valor_unidade =  preco_venda / valor_produto_converter;
			}
		}
		
		$(campo_valor_unidade).val(number_format(valor_unidade, 2, ',', '.'));
		// Calculando valor da unidade com desconto
		//-----------------------------------------
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
	});
	
	// Calcular Preço de Venda pelo Desconto
	$(campo_desconto).keyup(function(){
		var preco_unitario 			= converter_decimal($(campo_preco_unitario).val());
		var desconto 				= converter_decimal($(this).val());
		var valor_produto_converter = converter_decimal($(campo_valor_produto_converter).val());
		var unidade_medida_venda	= $(campo_unidade_medida_venda).val();
		var unidade_medida			= $(campo_unidade_medida).val();
		
		
		if(unidade_medida_venda == unidade_medida) { //Item de troca
			preco_unitario = preco_unitario / valor_produto_converter;
		} else {
			preco_unitario = preco_unitario;
		}
		
		var percentagem 			= desconto / 100;
		var valor_diferenca 		= preco_unitario * percentagem;
		var preco_venda 			= preco_unitario - valor_diferenca;
		
		if(preco_venda < 0) {
			preco_venda = 0;
		}
		
		// Iserindo preco_venda no campo
		$(campo_preco_venda).val(number_format(preco_venda, 2, ',', '.'));
		
		//-----------------------------------------
		// Calculando valor da unidade com desconto
		var valor_unidade = 0;
		
		if(valor_produto_converter > 0) {
			if(unidade_medida_venda == unidade_medida) {
				valor_unidade =  preco_venda;
			} else {
				valor_unidade =  preco_venda / valor_produto_converter;
			}
		}
		
		$(campo_valor_unidade).val(number_format(valor_unidade, 2, ',', '.'));
		// Calculando valor da unidade com desconto
		//-----------------------------------------
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
	});
	
	// Calcular Preço Total pela quantidade
	$(campo_quantidade).keyup(function(){
		if(campo_peso_unitario && campo_quantidade)
		{
			var peso_unitario 	= $(campo_peso_unitario).val();
			var quantidade 		= $(campo_quantidade).val();
			
			$(campo_peso_total).val(number_format(peso_unitario * quantidade, 2, '.', '.'));
		}
		
		calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item);
	});
}


/**
* Metódo:		validar_campos
* 
* Descrição:	Função Utilizada para exibir os erros 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
* @param		string 			mensagem_erro	- mensagem do erro
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function validar_campos(codigo_produto, campos) {
	var campos = unserialize(campos);
	var sucesso = true;
	
	// Se desconto for vazio, inserir o valor 0
	if(!$(campos['desconto']).val()) {
		$(campos['desconto']).val(number_format(0, 3, ',', '.'));
	}
	
	//------------------
	
	if(!$(campos['codigo_produto']).val() && campos['codigo_produto']) {// Validação do Produto
		erro_itens('Selecione um Produto.');
		
		sucesso = false;
	} else if(!validar_decimal($(campos['preco_venda']).val())) {
		if($(campos['preco_venda']).val()) {
			erro_itens('O valor "<b>' + $(campos['preco_venda']).val() + '</b>" no campo <b>Preço de Venda</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>1.100,250<b/>', codigo_produto);
			
			sucesso = false;
		} else {
			erro_itens('Digite um valor para o <b>Preço de Venda</b>.', codigo_produto);
			
			sucesso = false;
		}
	} else if(converter_decimal($(campos['preco_venda']).val()) <= 0) {
		erro_itens('Digite um valor para o <b>Preço de Venda</b>.', codigo_produto);
		
		sucesso = false;
	} else if(!validar_digitos($(campos['quantidade']).val()) || $(campos['quantidade']).val() <= 0) { // Validação da Quantidade
		if(!$(campos['quantidade']).val()) {
			erro_itens('Digite uma quantidade.', codigo_produto);
			
			sucesso = false;
		} else {
			erro_itens('O valor "<b>' + $(campos['quantidade']).val() + '</b>" no campo <b>Quantidade</b> não contém apenas digitos.', codigo_produto);
			
			sucesso = false;
		}
	} else if(!validar_decimal($(campos['desconto']).val())) { // Validação do Desconto
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não é um valor decimal válido.<br /><br />Exemplo de valor decimal válido: <b>50,250<b/>', codigo_produto);
		
		sucesso = false;
	} else if(converter_decimal($(campos['desconto']).val()) > 99.990) { // Validação do Desconto
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não pode ser maior que 99.990%.', codigo_produto);
		
		sucesso = false;
	} else if(converter_decimal($(campos['desconto']).val()) > 100) { // Validação do Desconto
		erro_itens('O valor "<b>' + $(campos['desconto']).val() + '</b>" no campo <b>Desconto</b> não pode ser maior que 100.', codigo_produto);
		
		sucesso = false;
	}
	
	//Validar preço de venda para itens de troca
	/*
	//Removido por solicitação do Carlos/Reginaldo
	if(campos['tipo_produto'] == 'troca') {
		var preco_unidade_tabela = campos['preco_tabela'];
		
		if(campos['fator_conversao'] > 0) {
			if(campos['tipo_converter'] == 'D') {
				preco_unidade_tabela = campos['preco_tabela'] / campos['fator_conversao'];
			} else if(campos['tipo_converter'] == 'M') {
				preco_unidade_tabela = campos['preco_tabela'] * campos['fator_conversao'];
			}
		}
		
		if(number_format(preco_unidade_tabela, 3) < number_format(campos['valor_unidade'], 3)) {
			erro_itens('O valor do <b>item de troca</b> deve ser inferior ao <b>preço de tabela</b>!', codigo_produto);
			
			sucesso = false;
		}
	}
	*/
	
	return sucesso;
}


/**
* Metódo:		erro_itens
* 
* Descrição:	Função Utilizada para exibir os erros 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
* @param		string 			mensagem_erro	- mensagem do erro
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function erro_itens(mensagem_erro, codigo_produto) {
	var icone = '<img src="img/warning.png" style="vertical-align: middle" /> ';

	if(codigo_produto) {
		$('<tr class="erro_itens" style="background-color: #F0798E"><td colspan="10">' + icone + mensagem_erro + '</td></tr>').insertAfter($('.produto_' + codigo_produto));
	
		$('.produto_' + codigo_produto).css('background-color', '#F0798E');
	} else {
		$('#erro_item').html(icone + mensagem_erro);
		$('#erro_item').show('fast');
	}
}


/**
* Metódo:		obter_produtos_sessao
* 
* Descrição:	Função Utilizada para obter os itens inseridos na sessão
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos_sessao() {
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos']) {
		return sessao_pedido['produtos'];
	} else {
		return false;
	}
}

function obter_produtos_troca_sessao() {
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_troca']) {
		return sessao_pedido['produtos_troca'];
	} else {
		return false;
	}
}


/**
* Metódo:		obter_produtos_removidos_sessao
* 
* Descrição:	Função Utilizada para obter os itens removidos na sessão
* 
* Data:			22/10/2013
* Modificação:	22/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos_removidos_sessao() {
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_removidos']) {
		return sessao_pedido['produtos_removidos'];
	} else {
		return false;
	}
}

/**
* Metódo:		exibir_totais
* 
* Descrição:	Função Utilizada para exibir os totais dos itens
* 
* Data:			29/10/2013
* Modificação:	29/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_totais() {

	var total_preco_unitario 	= 0;
	var total_desconto 			= 0;
	var total_desconto_reais 	= 0;
	var total_preco_venda 		= 0;
	var total_valor_unidade		= 0;
	var total_quantidade		= 0;
	var total					= 0;
	var total_ipi				= 0;
	var total_geral				= 0;
	var total_peso				= 0;
	var total_desconto_geral	= 0;
	
	var total_aumento			= 0;
	
	var total_preco_tabela		= 0;
	
	var total_preco_venda_quantidade = 0;
	
	
	var sessao_produtos = obter_produtos_sessao();
	
	if(sessao_produtos) {

		$.each(sessao_produtos, function(key, objeto){

			total_preco_unitario 	+= parseFloat(objeto.preco_unitario);
			total_preco_venda		+= parseFloat(objeto.preco_venda);
			total_valor_unidade		+= parseFloat(objeto.valor_unidade);
			total_quantidade		+= parseFloat(objeto.quantidade);
			total					+= parseFloat(objeto.quantidade * objeto.preco_venda);
			total_geral				+= parseFloat(objeto.quantidade * objeto.preco_venda);
			total_peso				+= parseFloat(objeto.peso_total * objeto.quantidade);
			
			total_preco_tabela				+= parseFloat(objeto.preco_tabela) * parseFloat(objeto.quantidade);
			total_preco_venda_quantidade	+= parseFloat(objeto.preco_venda) * parseFloat(objeto.quantidade);

			total_desconto_reais 	+= (parseFloat(objeto.preco_venda) - parseFloat(objeto.preco_tabela)) * parseFloat(objeto.quantidade);
			
		});
		
		total_desconto 			= (total_preco_venda_quantidade - total_preco_tabela) * 100 / total_preco_tabela;
		total_desconto_geral    = total_desconto;
		total_desconto			= (total_desconto > 0 ? 0 : total_desconto);
		total_ipi				= (total_geral - total) * 100 / total_geral;
	}

	var descricao_tipo_pedido = obter_descricao_pedido('upper');
	
	// Totais
	var html = '';
	html += '<td colspan="2">TOTAIS DO ' + descricao_tipo_pedido + '</td>';
	html += '<td align="right">' + number_format(total_preco_unitario, 3, ',', '.') + '</td>';
	html += '<td align="right">' + number_format(Math.abs(total_desconto), 3, ',', '.') + ' %<br />R$ ' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
	
	html += '<td align="right"></td>';
	
	html += '<td></td>';
	
	html += '<td align="right">' + total_quantidade + '</td>';
	html += '<td align="right">' + number_format(total_peso, 3, '.', '.') + '</td>';
	html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
	html += '<td align="right"></td>';

	$('#rodape_pedido').html(html);
	
	// Totais
	var total_do_desconto = parseFloat(total_desconto_geral) + (-1 * parseFloat(obter_valor_sessao('desconto_condicao_pagamento')));
	
	salvar_sessao('analitico_reais', parseFloat(total_desconto_reais));
	salvar_sessao('analitico_porcentagem', parseFloat(total_do_desconto));
	
	var html = '';
	html += '<td colspan="8">ANALÍTICO</td>';
	html += '<td align="right" style="color:red">R$ '  + number_format(total_desconto_reais, 3, ',', '.') + '<br />(' + number_format(total_do_desconto, 3, ',', '.') + '%)</td>';
	html += '<td align="right"></td>';

	$('#rodape_analitico').html(html);
}

/**
* Metódo:		exibir_produtos
* 
* Descrição:	Função Utilizada para exibir os itens dos produtos inseridos na sessao e Os itens excluídos (Um item é excluido quando o representante altera a tabela de preços e o item não existe nessa tabela)
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_produtos(tipo_produto) {
	
	if(tipo_produto == 'troca') {
		var sessao_produtos 	= obter_produtos_troca_sessao();
		var classe_itens 		= '.itens_pedido_troca';
		var classe_rodape		= classe_itens;//'.rodape_itens_troca';
	} else {
		var sessao_produtos 	= obter_produtos_sessao();
		var classe_itens 		= '.itens_pedido';
		var classe_rodape		= classe_itens;//'.rodape_itens';
	}
	
	
	
	if(sessao_produtos) {
		var total_produtos 		= Object.keys(sessao_produtos).length;
	} else {
		var total_produtos 		= 0;
	}
	
	if(total_produtos > 0) {
		
		if(tipo_produto == 'troca') {
			$('#corpo_troca').show();
		}
		
		$(classe_itens).empty();
		
		if(tipo_produto == 'troca') {
			var inicio_listagem = (sessionStorage['pagina_itens_troca'] - 1) * por_pagina;
			var final_listagem = sessionStorage['pagina_itens_troca'] * por_pagina;
		} else {
			var inicio_listagem = (sessionStorage['pagina_itens'] - 1) * por_pagina;
			var final_listagem = sessionStorage['pagina_itens'] * por_pagina;
		}
		var i = 0;
		var desconto_condicao_pagamento = obter_valor_sessao('desconto_condicao_pagamento');
		$.each(sessao_produtos, function(key, objeto){
			
			if(i >= inicio_listagem && i < final_listagem) {
				if(tipo_produto == 'troca') {
					var name_codigo_produto = 'troca_' + objeto.codigo;
				} else {
					var name_codigo_produto = objeto.codigo;
				}
				
				var html = '';
				html += '<td align="center" class="codigo_produto' + (tipo_produto ? '_' + tipo_produto : '') + '">' + objeto.codigo + '</td>';
				html += '<td>' + objeto.descricao + '</td>';
				
				html += '<td align="right">';
				if(tipo_produto != 'troca')
				{
					html += number_format(desconto_condicao_pagamento, 3, ',', '.') + '%<br />';
				}
				html += '<b>R$ ' + number_format(objeto.preco_unitario, 3, ',', '.') + '</b><input type="hidden" name="editar_preco_unitario_' + name_codigo_produto + '" value="' + number_format(objeto.preco_unitario, 3, ',', '.') + '" /></td>';			
				
				
				html += '<td align="center">';
				html += number_format(objeto.desconto, 3, ',', '.') + '<br/>'; 
				html += number_format(objeto.preco_venda, 3, ',', '.');
				html += '</td>';
				
				html += '<td align="center">';
				html += number_format(objeto.valor_unidade, 3, ',', '.');
				html += '<input name="editar_valor_produto_converter_' + name_codigo_produto + '" type="hidden" value="' + objeto.valor_produto_converter + '" />';
				html += '</td>';
				
				html += '<td align="center">';
				html += '<input type="hidden" name="editar_unidade_medida_' + name_codigo_produto + '" value="' + objeto.unidade_medida + '" />';
				if(classe_itens == '.itens_pedido_troca') {
					html += objeto.unidade_medida_venda;
				} else {
					html += objeto.segunda_unidade_medida;
				}
				html += '</td>';
				
				html += '<td align="center">' + objeto.quantidade + '</td>';
				
				html += '<td align="right">';
				html += number_format(objeto.peso_total * objeto.quantidade, 3, '.', '.');
				html += '<input name="editar_peso_unitario_' + name_codigo_produto + '" type="hidden" value="' + objeto.peso_total + '" />';
				html += '</td>';
				
				html += '<td align="right">' + number_format(objeto.preco_venda * objeto.quantidade, 3, ',', '.') + '</td>';
				html += '<td align="center">';
				if(tipo_produto == 'troca') {
					html += '<a class="excluir_item_troca botao_c_grid" href="' + objeto.codigo + '">Excluir</a>';
				} else {
					html += '<a class="excluir_item botao_c_grid" href="' + objeto.codigo + '">Excluir</a>';
				}
				html += '</td>';
				
				$(classe_itens).append('<tr class="produto_' + name_codigo_produto + '">' + html + '</tr>');
			} else if(i >= final_listagem) {
				//Quando terminar de exibir a listagem sai do foreach
				return false;
			}
			
			i++;
		});
		
		//Exibir Totais
		if(tipo_produto != 'troca') {
			$(classe_rodape).append('<tr class="novo_grid_rodape" id="rodape_pedido"></tr><tr class="novo_grid_rodape" id="rodape_analitico"></tr></tr>');
			
			exibir_totais();
		}
		
	} else {
		$(classe_itens).empty();
	
		$(classe_itens).append('<tr><td colspan="10" style="color:red">Nenhum produto adicionado.</td></tr>');
	
		//Exibir Totais
		if(tipo_produto != 'troca') {
			$(classe_rodape).append('<tr class="novo_grid_rodape" id="rodape_pedido"></tr><tr class="novo_grid_rodape" id="rodape_analitico"></tr></tr>');
			
			exibir_totais();
		}
	}
	
	//-------------------
	//-- Itens Excluidos
	//-------------------
	
	var sessao_produtos_removidos = obter_produtos_removidos_sessao();
	if(sessao_produtos_removidos) {
		$('.itens_removidos').empty();
		var titulo = '<b>Produto removido por não existir na tabela de preços.</b>';
		$('.itens_removidos').append(titulo);
		
		$.each(sessao_produtos_removidos, function(key, objeto){
			
			var html = '';
			html += '<li>' + objeto.codigo + ' - ' + objeto.descricao + '</li>';
			
			$('.itens_removidos').append('<ul>' + html + '</ul>');
		});
	}
	
	alterarCabecalhoListagem('#itens_pedido_tabela');
	alterarCabecalhoListagem('#itens_pedido_troca_tabela');
	
}

/**
* Metódo:		calcular_total
* 
* Descrição:	Função Utilizada para calcular o valor total dos itens e exibi-los
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			campo_preco_venda - input do preco de venda
* @param		string 			campo_ipi
* @param		string 			campo_quantidade
* @param		string 			campo_total_geral
* @param		string 			campo_total_item
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function calcular_total(campo_preco_venda, campo_ipi, campo_quantidade, campo_total_geral, campo_total_item) {
	var preco_venda 			= converter_decimal($(campo_preco_venda).val());
	var ipi 					= converter_decimal($(campo_ipi).val());
	var valor_ipi				= 0;
	var quantidade 				= $(campo_quantidade).val();
	
	if(!quantidade) {
		quantidade = 1;
	}
	
	// Iserindo total_item no campo
	if(campo_total_item) {
		$(campo_total_item).val(number_format(parseFloat(preco_venda) * quantidade, 2, ',', '.'));
	}
	
	$(campo_total_geral).val(number_format((parseFloat(preco_venda) + parseFloat(valor_ipi)) * quantidade, 2, ',', '.'));
}

/**
* Metódo:		buscar_produtos
* 
* Descrição:	Função Utilizada para pesquisar os produtos pelo autocomplete e adicionar os valores nos campso quando o produto for selecionado
* 
* Data:			17/10/2013
* Modificação:	17/10/2013
* 
* @access		public
* @param		array 			var produtos		- Todos os produtos
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_produtos(produtos) {
	var codigo_filial = obter_valor_sessao('filial');
	
	$('input[name=produto]').autocomplete({
		minLength: 2,
		//source: produtos,
		source: function(request, response) {
			// Foi alterada a consulta para obter os produtos dinamicamente, para ganho de desempenho
		
			//Oculta o erro exibido anteriormente caso tenha ocorrido.
			$('#erro_item').hide();
			//Exibe loading
			$('#carregando_produtos').show();
			
			var tamanho = request.term.length;
			// Filtros necessários para obter os produtos corretamente			
			var wheres = '';
				wheres += ' AND ((substr(produto_codigo_palm, 1, ' + tamanho + ') LIKE "' + request.term + '%" AND length(produto_codigo_palm) <= ' + tamanho + ') OR produto_descricao LIKE "' + request.term + '%" )';

			if (codigo_filial && codigo_filial != 'undefined') {
				wheres += " AND (produto_filial = '" + codigo_filial + "' OR produto_filial = '')";
			}
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ?' + wheres, [obter_valor_sessao('tabela_precos')], function(x, dados) {
					if (dados.rows.length) {
						var produtos = [];
						
						for (i = 0; i < dados.rows.length; i++) {
							var dado = dados.rows.item(i);
							
							//var preco = aplicar_descontos_cabecalho(dado.ptp_preco);
							//teste
							var preco = dado.ptp_preco;
							var ptp_preco = dado.ptp_preco;
							var valor_unidade = 0;
							
							if(dado.produto_converter > 0) {
								if(dado.produto_tipo_converter == 'D') {
									valor_unidade = preco * dado.produto_converter;
									ptp_preco = ptp_preco * dado.produto_converter;
								} else if(dado.produto_tipo_converter == 'M') {
									valor_unidade = preco / dado.produto_converter;
									ptp_preco = ptp_preco / dado.produto_converter;
								} else {
									valor_unidade = preco;
								}
							} else {
								valor_unidade = preco;
							}
							
							produtos.push({ 
								//label: 							dado.produto_codigo + ' - ' + dado.produto_descricao + ' - ESTOQUE ATUAL: ' + number_format(dado.etq_quantidade_atual, 0, ',', '.') + ' - PREÇO: R$ ' + number_format(preco, 3, ',', '.')+ ' - VALOR DA UNI.: R$ ' + number_format(valor_unidade, 3, ',', '.'),
								label: 							dado.produto_codigo_palm + ' - ' + dado.produto_descricao,
								codigo: 						dado.produto_codigo, 
								preco: 							valor_unidade, 
								preco_tabela: 					ptp_preco, 
								ipi: 							dado.produto_ipi, 
								descricao: 						dado.produto_descricao, 
								unidade_medida: 				dado.produto_unidade_medida, 
								local: 							dado.produto_locpad,
								valor_unidade:					preco,
								valor_produto_converter:		dado.produto_converter,
								peso_total:						dado.produto_peso_total,
								produto_segunda_unidade_medida:	dado.produto_segunda_unidade_medida,
								tipo_converter:					dado.produto_tipo_converter,
								grupo_tributacao:				dado.produto_grupo_tributacao //CUSTOM E0184/2014 - Calculo de impostos offline
							});
						}
						
						$('#carregando_produtos').hide();
						
						response(produtos);

					} else {
						$('#erro_item').text('Nenhum produto encontrado.');
						$('#carregando_produtos').hide();
						$('#erro_item').show();
					}
				});
			});
		},	
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
			$('#erro_item').hide();
			
			//Verifica se o produto selecionado já encontra-se na sessão e exibe os valores setados anteriormente
			verificar_produto_sessao(ui, true);
			
			calcular_total();
			
			// Bloquear campo quando selecionar cliente
			$('input[name=produto]').attr('disabled', 'disabled');
			$('input[name=produto]').css('background-color', '#F2F2F2');
			$('#trocar_produto').show();
			
			return false;
		}
	});
}

function verificar_produto_sessao(ui, focusquantidade) {
	if($('select[name=troca]').val() == 'S') {
		var sessao_produtos = obter_produtos_troca_sessao();
	} else {
		var sessao_produtos = obter_produtos_sessao();
	}
	
	codigo_produto = remover_zero_esquerda(ui.item.codigo);
	
	//Obtem os valores da sessão para exibir nos inputs
	if(sessao_produtos[codigo_produto]) {
		var produto = sessao_produtos[codigo_produto];
			ui.item.quantidade		= produto.quantidade;		//Quantidade
			ui.item.preco			= produto.preco_venda;		//Preço de Venda
			ui.item.ipi				= produto.ipi;				//IPI
			ui.item.local			= produto.local;			//Local
			ui.item.desconto		= produto.desconto;			//Desconto
			ui.item.peso_total		= produto.peso_total;		//Peso Total
			ui.item.valor_unidade	= produto.valor_unidade;	//Valor da Unidade
			ui.item.tipo_converter	= produto.tipo_converter;	//Tipo de Conversão (D/M)
			ui.item.unidade_medida	= produto.unidade_medida;	//1ª Unidade de Medida
			ui.item.produto_segunda_unidade_medida = produto.segunda_unidade_medida;	//2ª Unidade de Medida
			ui.item.grupo_tributacao = produto.grupo_tributacao //Grupo de tributação - //CUSTOM E0184/2014 - Calculo de impostos offline
	}
	
	$('input[name=quantidade]').val((ui.item.quantidade ? ui.item.quantidade : '1'));
	if(focusquantidade){
		$('input[name=quantidade]').focus();
		$('input[name=quantidade]').select();
	}
	
	$('input[name=produto]').val(ui.item.label);
	$('input[name=descricao_produto]').val(ui.item.descricao);
	$('input[name=codigo_produto]').val(ui.item.codigo);
	
	$('input[name=preco_tabela]').val(number_format(ui.item.preco_tabela, 2, ',', '.'));
	$('input[name=preco_unitario]').val(number_format(ui.item.preco_tabela, 2, ',', '.'));
	$('input[name=preco_venda]').val(number_format(ui.item.preco, 2, ',', '.'));
	
	$('input[name=ipi]').val(number_format(ui.item.ipi, 2, ',', ''));
	$('input[name=unidade_medida]').val(ui.item.unidade_medida);
	$('input[name=segunda_unidade_medida]').val(ui.item.produto_segunda_unidade_medida);
	$('input[name=local]').val(ui.item.local);
	
	$('input[name=valor_unidade]').val(number_format(ui.item.valor_unidade, 2, ',', ''));
	
	$('input[name=valor_produto_converter]').val(ui.item.valor_produto_converter);
	
	$('input[name=unidade_medida_venda]').val(ui.item.produto_segunda_unidade_medida);
	
	$('input[name=tipo_converter]').val(ui.item.tipo_converter);
	
	$('input[name=peso_total]').val(ui.item.peso_total);
	
	$('input[name=desconto]').val(number_format(ui.item.desconto, 2, ',', '.'));
	
	$('input[name=grupo_tributacao_produto]').val(ui.item.grupo_tributacao); //CUSTOM E0184/2014 - Calculo de impostos offline
	
	$('#dropdown_unidade_medida').html('');
	$('#dropdown_unidade_medida').append('<option value="' + ui.item.produto_segunda_unidade_medida+ '">' + ui.item.produto_segunda_unidade_medida + '</option>');
	$('#dropdown_unidade_medida').append('<option value="' + ui.item.unidade_medida+ '">' + ui.item.unidade_medida + '</option>');
	
	//$('input[name=preco_venda]').trigger('keyup');
}

/**
* Metódo:		aplicar_descontos_cabecalho
* 
* Descrição:	Função Utilizada para aplicar os descontos do cabeçalho (Desconto do Cliente, Regra de Desconto)
* 
* Data:			23/10/2013
* Modificação:	23/10/2013
* 
* @access		public
* @param		string 					preco_produto		- Preço do Produto
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function aplicar_descontos_cabecalho(preco_produto) {
	// Aplicando Desconto do "Cliente" no preço do produto
	var desconto_cliente = obter_valor_sessao('desconto_cliente');
	if(desconto_cliente > 0) {
		var preco = preco_produto - (preco_produto * (desconto_cliente / 100));
	} else {
		var preco = preco_produto;
	}
	
	// Aplicando Desconto da "Condição de Pagamento" no preço do produto
	var desconto_condicao_pagamento = obter_valor_sessao('desconto_condicao_pagamento');
	if(desconto_condicao_pagamento > 0) {
		var preco = preco_produto - (preco_produto * (desconto_condicao_pagamento / 100));
	} else {
		var preco = preco_produto;
	}
	
	// Aplicando Desconto da "Regra de desconto" no preço do produto
	var regra_desconto = obter_valor_sessao('regra_desconto');
	if(regra_desconto > 0) {
		var preco = preco - (preco * (regra_desconto / 100));
	}
	
	return preco;
}


/**
* Metódo:		exibir_cabecalho
* 
* Descrição:	Função Utilizada para exibir os valores do cabeçalho
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_cabecalho() {
	// Obter descrição da FILIAL e adicionar no cabeçalho
	obter_descricao('filiais', 'razao_social', 'codigo', obter_valor_sessao('filial'), '.filial');
	
	// ------
	// Obter descrição da TIPO DE PEDIDO e adicionar no cabeçalho
	var tipo_pedido = obter_valor_sessao('tipo_pedido');
	if(tipo_pedido == 'N') {
		$('.tipo_pedido').html('Venda Normal');
	} else if(tipo_pedido == '*') {
		$('.tipo_pedido').html('Orçamento');
	} else {
		$('.tipo_pedido').html('N/A');
	}
	
	// Obter descrição do PROSPECT e adicionar no cabeçalho
	var descricao_prospect = obter_valor_sessao('descricao_prospect');
	if(descricao_prospect) {
		$('.cabecalho_cliente_prospect').html('Prospect');
		$('.cliente').html(descricao_prospect);
	}
	
	// Obter descrição do CLIENTE e adicionar no cabeçalho
	var descricao_cliente = obter_valor_sessao('descricao_cliente');
	if(descricao_cliente) {
		$('.cabecalho_cliente_prospect').html('Cliente');
		$('.cliente').html(descricao_cliente);
	}
	
	// Obter descrição da TABELA DE PREÇOS e adicionar no cabeçalho
	obter_descricao('tabelas_preco', 'descricao', 'codigo', obter_valor_sessao('tabela_precos'), '.tabela_precos');
	
	// Obter descrição da CONDIÇÃO DE PAGAMENTO e adicionar no cabeçalho
	obter_descricao('formas_pagamento', 'descricao', 'codigo', obter_valor_sessao('condicao_pagamento'), '.condicao_pagamento');
	
	// Obter descrição da CONDIÇÃO DE PAGAMENTO e adicionar no cabeçalho
	obter_descricao('formas_pagamento_real', 'descricao', 'chave', obter_valor_sessao('formas_de_pagamento'), '.formas_de_pagamento');
	
	// Obter descrição da Tipo de Movimentação
	$('.tipo_movimentacao').html(obter_valor_sessao('tipo_movimentacao'));
	
	// Obter DESCONTO do CLIENTE e adicionar no cabeçalho
	$('.desconto_cliente').html(number_format(obter_valor_sessao('desconto_cliente'), 3, ',', '.') + ' %');
	
	// Obter REGRA DE DESCONTO e adicionar no cabeçalho
	$('.desconto_condicao_pagamento').html(number_format(obter_valor_sessao('desconto_condicao_pagamento'), 3, ',', '.') + ' %');
	
	// Obter REGRA DE DESCONTO e adicionar no cabeçalho
	$('.regra_desconto').html(number_format(obter_valor_sessao('regra_desconto'), 3, ',', '.') + ' %');
}

/**
* Metódo:		salvar_produto_sessao
* 
* Descrição:	Função Utilizada para salvar produtos adicionados
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produto_sessao(produtos_paramentro) {
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos']) {
		var sessao_produtos = sessao_pedido['produtos'];
	
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
	} else {
		var produtos = produtos_paramentro;
	}
	
	salvar_sessao('produtos', produtos);
}

//* Metódo:		salvar_produto_troca_sessao
function salvar_produto_troca_sessao(produtos_paramentro) {
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_troca']) {
		var sessao_produtos = sessao_pedido['produtos_troca'];
	
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
	} else {
		var produtos = produtos_paramentro;
	}
	
	salvar_sessao('produtos_troca', produtos);
}


/**
* Metódo:		salvar_produto_sessao
* 
* Descrição:	Função Utilizada para salvar produtos removidos
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produto_removido_sessao(produtos_paramentro) {
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_removidos']) {
		var sessao_produtos = sessao_pedido['produtos_removidos'];
	
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
	} else {
		var produtos = produtos_paramentro;
	}
	
	salvar_sessao('produtos_removidos', produtos);
}


/**
* Metódo:		salvar_produtos_excluidos
* 
* Descrição:	Função Utilizada para salvar produtos (Sem os removidos)
* 
* Data:			20/10/2013
* Modificação:	20/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produtos_excluidos(produtos_paramentro, troca) {
	var produtos_paramentro = unserialize(produtos_paramentro);

	var produtos = produtos_paramentro;
	
	if(troca) {
		salvar_sessao('produtos_troca', produtos);
	} else {
		salvar_sessao('produtos', produtos);	
	}
}