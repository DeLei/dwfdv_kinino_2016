var prospect;

$(document).ready(function() {	
	db.transaction(function(x) {
		// obter feiras
		x.executeSql(
			'SELECT DISTINCT id, nome FROM eventos ORDER BY nome ASC', [], function(x, dados) {
				
				$('select[name=id_feira]').append('<option value> Nenhum </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=id_feira]').append('<option value="' + dado.id + '">' + dado.nome + '</option>');
					}
				}
			}
		);
		
		//Obter Filiais
		x.executeSql(
			'SELECT DISTINCT codigo, razao_social FROM filiais ORDER BY codigo ASC', [], function(x, dados) {
				
				$('select[name=filial]').append('<option value> Selecione... </option>');
				if (dados.rows.length)
				{
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
					}
				}
			}
		);
		
		// obter prospect
		var codigo = parse_url(location.href).fragment;
		
		x.executeSql(
			'SELECT * FROM prospects WHERE codigo = ?', [codigo], function(x, dados) {
				if (dados.rows.length)
				{
				
					prospect = dados.rows.item(0);
					
					
					
					$('input[name=cgc]').attr('readonly', 'readonly');
					
					
					
					$('#cancelar').attr('href', 'prospects_visualizar.html#' + prospect.codigo);
					
					var municipio = '';
					
					$.each(prospect, function(i, v) {
					
						if ($('[name=' + i + ']').length)
						{
							if(i == 'estado')
							{
								estado = v;
								
								//correcao  
								if(prospect.codigo_municipio)
								{
									municipio = prospect.codigo_municipio;
									
									$('#trocar_municipio').show();
								}								
								//fim correcao
								
								obter_municipios(estado);
							}
							
	
							
							if(i == 'data_nascimento')
							{
								if(v)
								{
									v = protheus_data2data_normal(v);
								}
							}
							
							if(i == 'telefone')
							{
								if(v)
								{
									v = remover_caracteres_telefone(v);
								}

							}
							
							if(i == 'fax')
							{
								if(v)
								{
									v = remover_caracteres_telefone(v);
								}
							}
							
							
						
							$('[name=' + i + ']').val(v);
						}
					});
				}
				else
				{
					window.location = 'index.html';
				}
			}
		);
	});
	
	// copiar endereço
	
	var inputs = ['cep', 'endereco', 'numero', 'complemento', 'bairro', 'cidade', 'estado'];
	
	$('#copiar_end_cob').click(function() {
		var _this = this;
		
		$.each(inputs, function(i, v) {
			if ($(_this).is(':checked'))
			{
				$('[name=' + v + ']').val($('[name=' + v + '_cobranca]').val());
			}
			else
			{
				$('[name=' + v + ']').val('');
			}
		});
	});
	
	// Obter Estados
	$('select[name*=estado]').html('<option value> Selecione... </option>' + ufs);
	
	
	// Obter Municipios
	$('select[name=estado]').live('change', function(){
		$('#nome_municipio').attr('disabled', 'disabled');
		$('#nome_municipio').html('Carregando...');
		
		limpar_municipio();
		
		var estado = $(this).val();
		
		obter_municipios(estado);
	});
	
	function obter_municipios(estado)
	{
		// obter Municipios
		db.transaction(function(x) {
			x.executeSql('SELECT DISTINCT codigo, nome FROM municipios WHERE uf = ? ORDER BY nome ASC', [estado], function(x, dados) {
				var municipios = [];
				
				for (i = 0; i < dados.rows.length; i++)
				{
					var dado = dados.rows.item(i);
					
					municipios.push({ label: dado.nome, codigo: dado.codigo });
				}
				
				buscar_municipios(municipios)
			});
		});
	}
	
	$('#trocar_municipio').click(function(){
		limpar_municipio();
	});
	
	function limpar_municipio()
	{
		$('[name=codigo_municipio]').val('');
		$('#nome_municipio').val('');
		$('#nome_municipio').removeAttr('disabled');
		$('#trocar_municipio').hide();
	}
	
	function buscar_municipios(municipios)
	{
		$('input[name=nome_municipio]').autocomplete({
			minLength: 2,
			source: municipios,
			position : { my : "left bottom", at: "left top", collision : "none"},
			select: function( event, ui ) {
				$('[name=codigo_municipio]').val(ui.item.codigo);
				$('#nome_municipio').val(ui.item.label);
				
				$('#nome_municipio').attr('disabled', 'disabled');
				$('#trocar_municipio').show();
				return false;
			}
		});
	}
	
	$('select[name=tipo]').live('change', function(){
		
		if($(this).val() == 'L')
		{
			$('input[name=inscricao_rural]').addClass('obrigatorio');
		}
		else
		{
			$('input[name=inscricao_rural]').removeClass('obrigatorio');
		}
	});
	
	//Cancelar
	$('#cancelar').click(function() {
		document.location = $(this).attr('href');
	});
	
	// enviar form
	
	
	$('form').submit(function() {
	
		if (!$('select[name=filial]').val())
		{
			mensagem('Selecione uma <strong>Filial</strong>.');
		}
		else if (!$('select[name=tipo]').val())
		{
			mensagem('Selecione uma <strong>Tipo</strong> de Prospect.');
		}
		else if (!$('input[name=nome]').val())
		{
			mensagem('Preencha o campo <strong>Nome / Razão social</strong>.');
		}
		else if (!$('input[name=nome_fantasia]').val())
		{
			mensagem('Preencha o campo <strong>Nome Fantasia</strong>.');
		}
		else if (!$('input[name=cgc]').val() || !validar_cpf_cnpj($('input[name=cgc]').val()))
		{
			mensagem('Preencha um <strong>CPF / CNPJ</strong> válido.');
		}
		else if ($('select[name=tipo]').val() == 'L' && !$('input[name=inscricao_rural]').val())
		{
			mensagem('Preencha o campo <strong>Inscrição Rural</strong>.');
		}
		else if (!$('input[name=ddd]').val())
		{
			mensagem('Preencha o campo <strong>DDD</strong>.');
		}
		else if (!validar_ddd($('input[name=ddd]').val()))
		{
			mensagem('Preencha um <strong>DDD</strong> válido.');
		}
		else if (!$('input[name=telefone]').val())
		{
			mensagem('Preencha o campo <strong>Telefone</strong>.');
		}
		else if (!validar_telefone($('input[name=telefone]').val()))
		{
			mensagem('Preencha um <strong>Telefone</strong> válido.');
		}
		else if ($('input[name=fax]').val() && !validar_telefone($('input[name=fax]').val()))
		{
			mensagem('Preencha um <strong>Fax</strong> válido.');
		}
		else if (!validar_email($('input[name=email]').val()))
		{
			mensagem('Preencha um <strong>E-mail NF-e</strong> válido.');
		}
		else if ($('input[name=email_contato]').val() && !validar_email($('input[name=email_contato]').val()))
		{
			mensagem('Preencha um <strong>E-mail</strong> válido.');
		}
		else if (!validar_cep($('input[name=cep]').val()))
		{
			mensagem('Digite um CEP válido.');
		}
		else if (!$('input[name=endereco]').val())
		{
			mensagem('Preencha um <strong>Endereço</strong> válido.');
		}
		else if (!$('input[name=bairro]').val())
		{
			mensagem('Preencha um <strong>Bairro</strong> válido.');
		}
		else if (!$('select[name=estado]').val())
		{
			mensagem('Preencha um <strong>Estado</strong> válido.');
		}
		else if (!$('input[name=codigo_municipio]').val())
		{
			mensagem('Preencha um <strong>Município</strong> válido.');
		}
		else
		{
			// -----------
			// Isentos
			if (!$('input[name=inscricao_estadual]').val()){
				$('input[name=inscricao_estadual]').val('ISENTO');
			}
			
			if (!$('input[name=inscricao_municipal]').val()){
				$('input[name=inscricao_municipal]').val('ISENTO');
			}
			
			if (!$('input[name=inscricao_rural]').val()){
				$('input[name=inscricao_rural]').val('ISENTO');
			}
			// Isentos
			// -----------
			
			db.transaction(function(x) {
				x.executeSql(
					'SELECT codigo FROM prospects WHERE cgc = ? AND codigo != ?', [$('input[name=cgc]').val(), prospect.codigo], function(x, dados) {
						if ($('input[name=cgc]').val() && dados.rows.length)
						{
							mensagem('O <strong>CPF / CNPJ</strong> digitado já existe.');
						}
						else
						{
							x.executeSql(
								'SELECT codigo FROM prospects ORDER BY codigo DESC LIMIT 1', [], function(x, dados) {
							
									var tmp = new Array();
							
									tmp.push("editado = '1'");
									tmp.push("exportado = NULL");
									
									$('input[name], textarea[name], select[name]').each(function() {
										
										if(($(this).attr('name') != 'empresas') && ($(this).attr('name') != 'data_nascimento'))
										{
											tmp.push($(this).attr('name') + ' = "' + $(this).val() + '"');
										}
										
										
										if($(this).attr('name') == 'data_nascimento')
										{
											if($(this).val())
											{
												tmp.push($(this).attr('name') + ' = "' + data_normal2protheus($(this).val()) + '"');
											}
										}
										
										
										
										
										
										
									});
									
									tmp.push('codigo_empresa = "' + info.empresa + '"');
									
									x.executeSql('UPDATE prospects SET ' + tmp.join(', ') + ' WHERE codigo = ?', [prospect.codigo], function(x, dados){
									
										window.location = 'prospects_visualizar.html#' + prospect.codigo;
									
									});
							
									
								}
							);
						}
					}
				);
			});
		}
		
		return false;
	});
	
});
