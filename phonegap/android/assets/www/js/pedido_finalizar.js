var por_pagina = 10;
$(document).ready(function(){
	
	sessionStorage['pagina_itens_finalizar'] = sessionStorage['pagina_itens_finalizar'] || 1;
	sessionStorage['pagina_itens_troca_finalizar'] = sessionStorage['pagina_itens_troca_finalizar'] || 1;

	$('.pagina-finalizar').live('click', function(){
		var pagina = $(this).data('page');
		var tipo_produto = $(this).data('tipo');
		
		if(tipo_produto == 'troca') {
			sessionStorage['pagina_itens_troca_finalizar'] = pagina;
		} else {
			sessionStorage['pagina_itens_finalizar'] = pagina;
		}
		
		montar_paginacao_finalizar(tipo_produto);
		exibir_produtos_finalizar(tipo_produto);
		
		return false;
	});
	

	//----------------------------
	// Exibir botão impostos
	//----------------------------
	if(config.ws_url_impostos)
	{
		$('#botao_impostos').show();
	}
	//----------------------------
	// Exibir botão impostos
	//----------------------------
	
	//----------------------------
	// BOTÃO SIMULAR IMPOSTOS
	//----------------------------
	$('#botao_impostos').click(function(e){
		e.preventDefault();
		
		/**
		 * CUSTOM E0184/2014 - Calculo de impostos offline
		 */
		simular_impostos(function(){
			$('#msgImpostos').addClass('success');
			$('#msgImpostos').html('Simulação de impostos realizado com sucesso.');
			$('#botao_impostos').removeAttr('disabled');
			
			exibir_produtos_finalizar();
		});
	});
	
	
	
	
	$('#botao_impostos_ws').click(function(e){
		e.preventDefault();
		
		//Sessão do pedido
		var sessao_pedido = [];

		//Obter sessão do pedido
		if(sessionStorage[sessionStorage['sessao_tipo']]) {
			sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		}
				
		$.ajax({
			url: config.ws_url_impostos,
			type: 'POST',			
			data: {
				retorno: json_encode(sessao_pedido)
			},
			beforeSend: function(){			
				$('#msgImpostos').addClass('info');
				$('#msgImpostos').html('Aguarde simulação impostos em andamento...');
				$('#botao_impostos').attr('disabled', 'disabled');
			},
			success: function(dados) {	
				
				var sessao_produtos 	= obter_produtos_sessao();
				if(sessao_produtos) {
					var total_produtos 		= Object.keys(sessao_produtos).length;
				}
				var indice = 0;
				//-------------------------------
				// OBTER SESSÃO DOS PRODUTOS
				//-------------------------------
				
				if(dados.sucesso) {
					//Verificar itens retornados pelo WS - impostos
					$.each(dados.retorno.produtos, function(keyPedido, imposto){						
						
						//Verificar itens na sessão do pedido
						$.each(sessao_produtos, function(key, produto){
							
							//Verificar se é o mesmo código
							if(imposto.PRODUTO == produto.codigo) {
								var codigo_produto 	= remover_zero_esquerda(produto.codigo);
								
								var produtos = new Array();
								produtos[codigo_produto] = new Array();
								produtos[codigo_produto]['codigo'] 			= produto.codigo;
								produtos[codigo_produto]['descricao'] 		= produto.descricao;
								produtos[codigo_produto]['preco_tabela'] 	= produto.preco_tabela;
								produtos[codigo_produto]['preco_unitario'] 	= produto.preco_unitario;
								produtos[codigo_produto]['preco_venda'] 	= produto.preco_venda
								produtos[codigo_produto]['quantidade'] 		= produto.quantidade;
								produtos[codigo_produto]['desconto'] 		= produto.desconto;
								produtos[codigo_produto]['segunda_unidade_medida'] = produto.segunda_unidade_medida;
								produtos[codigo_produto]['unidade_medida_venda'] = produto.unidade_medida_venda
								produtos[codigo_produto]['unidade_medida'] 	= produto.unidade_medida;
								produtos[codigo_produto]['tipo_converter']	= produto.tipo_converter;
								produtos[codigo_produto]['local'] 			= produto.local;
								//Impostos
								produtos[codigo_produto]['TES'] 			= imposto.TES;
								produtos[codigo_produto]['CF'] 				= imposto.CF;
								produtos[codigo_produto]['ST'] 				= imposto.ST;
								produtos[codigo_produto]['IPI'] 			= imposto.IPI;
								produtos[codigo_produto]['ICMS'] 			= imposto.ICMS;
								
								produtos[codigo_produto]['valor_unidade'] 				= produto.valor_unidade;
								produtos[codigo_produto]['valor_produto_converter'] 	= produto.valor_produto_converter;
								
								produtos[codigo_produto]['peso_total'] 						= produto.peso_total;
								produtos[codigo_produto]['peso_unitario'] 					= produto.peso_unitario;
								produtos[codigo_produto]['item_troca'] 						= produto.item_troca;
								
								var produtos = serialize(produtos);
								
								salvar_produto_sessao(produtos);
								
								indice++;
								
								if(indice == total_produtos) {
									exibir_produtos_finalizar();
									$('#msgImpostos').addClass('success');
									$('#msgImpostos').html('Simulação de impostos realizado com sucesso.');
									$('#botao_impostos').removeAttr('disabled');
								}
							}
						});
					});
				}
			},
			error: function(jqXHR, textStatus,errorThrown) {
				$('#msgImpostos').addClass('warning');
				$('#msgImpostos').html('Não foi possível realziar simulação de impostos.');
				$('#botao_impostos').removeAttr('disabled');				
			}
		});
	});
	
	
	
	
	
	//----------------------------
	// BOTÃO FINALIZAR PEDIDO
	//----------------------------	
	$('#botao_finalizar').click(function(e){
	
		e.preventDefault();
		
		var descricao_tipo_pedido = obter_descricao_pedido();
		
		confirmar('Tem certeza que deseja FINALIZAR o ' + descricao_tipo_pedido + ' ?', function () {
			plugins.waitingDialog.mostrarDialog("Salvando Pedido...");
			
			$(this).dialog('close');
			
			salvar_pedido();
			
			$("#confirmar_dialog").remove();	
		});
	});
});

function finalizar() {

	exibir_produtos_finalizar();
	exibir_produtos_finalizar('troca');
	
	montar_paginacao_finalizar();
	
	exibir_outras_informacoes();
	
	
	/**
	 * CUSTOM E0184/2014 - Calculo de impostos offline
	 */
	simular_impostos(function(){
		$('#msgImpostos').addClass('success');
		$('#msgImpostos').html('Simulação de impostos realizado com sucesso.');
		$('#botao_impostos').removeAttr('disabled');
		
		exibir_produtos_finalizar();
	});
}

function exibir_produtos_finalizar(tipo_produto) {
	
	if(tipo_produto == 'troca') {
		var sessao_produtos 	= obter_produtos_troca_sessao();
		var classe_itens 		= '.itens_pedido_troca_finalizar';
	} else {
		var sessao_produtos 	= obter_produtos_sessao();
		var classe_itens 		= '.itens_pedido_finalizar';
	}
	
	if(sessao_produtos) {
		
		$(classe_itens).empty();
		
		if(tipo_produto == 'troca') {
			var inicio_listagem = (sessionStorage['pagina_itens_troca_finalizar'] - 1) * por_pagina;
			var final_listagem = sessionStorage['pagina_itens_troca_finalizar'] * por_pagina;
		} else {
			var inicio_listagem = (sessionStorage['pagina_itens_finalizar'] - 1) * por_pagina;
			var final_listagem = sessionStorage['pagina_itens_finalizar'] * por_pagina;
		}
		var i = 0;
		
		var total_preco_unitario 	= 0;
		var total_desconto 			= 0;
		var total_preco_venda 		= 0;
		var total_valor_unidade		= 0;
		var total_quantidade		= 0;
		var total					= 0;
		var total_ipi				= 0;
		var total_icms 				= 0;
		var total_st 				= 0;
		var total_geral				= 0;
		var total_peso				= 0;
		
		$.each(sessao_produtos, function(key, objeto){
			if(tipo_produto == 'troca' && objeto.item_troca){
				$('#corpo_itens_troca').show();
			}
			
			if(i >= inicio_listagem && i < final_listagem){
				
				var html = '';
					html += '<td align="center">' + objeto.codigo + '</td>';
					html += '<td>' + objeto.descricao + '</td>';
					html += '<td align="right">' + number_format(objeto.preco_unitario, 3, ',', '.') + '<input type="hidden" name="editar_preco_unitario_' + objeto.codigo + '" value="' + number_format(objeto.preco_unitario, 3, ',', '.') + '" /></td>';
					html += '<td align="right">' + number_format(objeto.desconto, 3, ',', '.') + '</td>';
					html += '<td align="right">' + number_format(objeto.preco_venda, 3, ',', '.') + '</td>';
					html += '<td align="right">' + number_format(objeto.valor_unidade, 3, ',', '.') + '</td>';
					html += '<td align="right">' + (objeto.unidade_medida_venda ? objeto.unidade_medida_venda : objeto.unidade_medida)+ '</td>';
					html += '<td align="right">' + objeto.quantidade + '</td>';
					html += '<td align="right">' + number_format(objeto.peso_total, 3, '.', '.') + '</td>';
					
					//Total sem impostos
					html += '<td align="right">' + number_format(objeto.preco_venda * objeto.quantidade, 3, ',', '.')  + '</td>';
					
					//Impostos
					html += '<td align="right">' + (objeto.IPI ? number_format(objeto.IPI, 3, ',', '.') : '0,000') + '</td>';
					html += '<td align="right">' + (objeto.ICMS ? number_format(objeto.ICMS, 3, ',', '.') : '0,000') + '</td>';
					html += '<td align="right">' + (objeto.ST ? number_format(objeto.ST, 3, ',', '.') : '0,000') + '</td>';			
					
					//Total com Impostos
					var ipi = (parseFloat(objeto.IPI) ? parseFloat(objeto.IPI) : 0);
					var st = (parseFloat(objeto.ST) ? parseFloat(objeto.ST) : 0);
					var total_item_impostos = (objeto.preco_venda * objeto.quantidade) + (ipi + st);
					html += '<td align="right">' + number_format(total_item_impostos, 3, ',', '.')  + '</td>';	
					
					$(classe_itens).append('<tr class="produto_' + objeto.codigo + '">' + html + '</tr>');
			}
			
			i++;
				
			// variaveis para os TOTAIS
			total_preco_unitario 	+= parseFloat(objeto.preco_unitario);
			total_preco_venda		+= parseFloat(objeto.preco_venda);
			total_valor_unidade		+= parseFloat(objeto.valor_unidade);
			total_quantidade		+= parseFloat(objeto.quantidade);
			total_ipi				+= (parseFloat(objeto.IPI) ? parseFloat(objeto.IPI) : 0);
			total_icms				+= (parseFloat(objeto.ICMS) ? parseFloat(objeto.ICMS) : 0);
			total_st				+= (parseFloat(objeto.ST) ? parseFloat(objeto.ST) : 0);
			total					+= parseFloat(objeto.preco_venda * objeto.quantidade);
			total_geral				+= parseFloat(objeto.preco_venda * objeto.quantidade);
			total_peso				+= parseFloat(objeto.peso_total);
		});
		
		total_desconto 			= (total_preco_unitario - total_preco_venda) * 100 / total_preco_unitario;
		total_desconto			= (total_desconto > 0 ? total_desconto : 0);
		
		var descricao_tipo_pedido = obter_descricao_pedido('upper');
		
		var html = '';
		html += '<td colspan="2">TOTAIS DOS ITENS</td>';
		html += '<td align="right">' + number_format(total_preco_unitario, 3, ',', '.') + '</td>';
		html += '<td align="right">' + number_format(total_desconto, 3, ',', '.') + '</td>';
		html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
		html += '<td align="right">' + number_format(total_valor_unidade, 3, ',', '.') + '</td>';
		html += '<td></td>';
		html += '<td align="right">' + total_quantidade + '</td>';
		html += '<td align="right">' + number_format(total_peso, 3, '.', '.') + '</td>';
		
		//Total sem impostos
		html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
		
		//Impostos
		html += '<td align="right">' + number_format(total_ipi, 3, ',', '.')  + '</td>';
		html += '<td align="right">' + number_format(total_icms, 3, ',', '.')  + '</td>';
		html += '<td align="right">' +  number_format(total_st, 3, ',', '.')  + '</td>';
		
		//Total com impostos
		html += '<td align="right">' + number_format(total_geral + (total_ipi + total_st), 3, ',', '.') + '</td>';
		
		if(tipo_produto != 'troca') {
			$(classe_itens).append('<tr class="novo_grid_rodape">' + html + '</tr>');
		}
		
		//Analitico
		var html = '';
		html += '<td colspan="2">ANALÍTICO</td>';
		html += '<td colspan="10"></td>';
		html += '<td colspan="2" align="right">R$ ' + number_format(obter_valor_sessao('analitico_reais'), 3, ',', '.')  + '<br />(' + number_format(obter_valor_sessao('analitico_porcentagem'), 3, ',', '.')  + '%)</td>';
		
		if(tipo_produto != 'troca') {
			$(classe_itens).append('<tr class="novo_grid_rodape">' + html + '</tr>');
		}
		
		//Totais do pedido
		var html = '';
		html += '<td colspan="2">TOTAIS DO PEDIDO</td>';
		html += '<td colspan="10"></td>';
		var total_sem_desconto = total_geral + (total_ipi + total_st);
		var total_com_desconto = total_sem_desconto;
		
		var desconto_cliente = obter_valor_sessao('desconto_cliente');
		if(desconto_cliente) {
			total_com_desconto = total_sem_desconto - (total_sem_desconto * (desconto_cliente / 100));
		}
		
		var desconto_condicao_pagamento = obter_valor_sessao('desconto_condicao_pagamento');
		if(desconto_condicao_pagamento) {
			total_com_desconto = total_sem_desconto - (total_sem_desconto * (desconto_condicao_pagamento / 100));
		}
		
		var regra_desconto = obter_valor_sessao('regra_desconto');
		if(regra_desconto) {
			total_com_desconto = total_sem_desconto - (total_sem_desconto * (regra_desconto / 100));
		}
		
		html += '<td colspan="2" align="right">R$ ' + number_format(total_com_desconto, 3, ',', '.')  + '</td>';
		
		if(tipo_produto != 'troca') {
			$(classe_itens).append('<tr class="novo_grid_rodape">' + html + '</tr>');
		}
		alterarCabecalhoListagem('#itens_pedido_finalizar_tabela');
		alterarCabecalhoListagem('#itens_pedido_troca_finalizar_tabela');
	}
	
}

function exibir_outras_informacoes() {
	var sessao_pedido = [];
	
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	$('.pedido_cliente').html(sessao_pedido.pedido_cliente ? sessao_pedido.pedido_cliente : 'N/A');
	$('.data_entrega').html(sessao_pedido.data_entrega ? sessao_pedido.data_entrega : 'N/A');
	obter_evento(sessao_pedido.evento);
	$('.tipo_frete').html(sessao_pedido.tipo_frete == 'F' ? 'FOB' : 'CIF');
	obter_transportadora(sessao_pedido.codigo_transportadora);
	
	$('.cliente_entrega').html(sessao_pedido.descricao_cliente_entrega ? sessao_pedido.descricao_cliente_entrega : 'N/A');
	
	$('.mennota').html(sessao_pedido.mennota ? sessao_pedido.mennota : 'N/A');
	$('.observacao_comercial').html(sessao_pedido.observacao_comercial ? sessao_pedido.observacao_comercial : 'N/A');
}

function obter_evento(codigo) {
	if(codigo) {
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM eventos WHERE id = ?', [codigo], function(x, dados) {
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					$('.evento').html(dado.id + ' - ' + dado.nome);
				} else {
					$('.evento').html('N/A');
				}
			});
		});
	} else {
		$('.evento').html('N/A');
	}
}

function obter_transportadora(codigo) {
	if(codigo) {
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM transportadoras WHERE codigo = ?', [codigo], function(x, dados) {
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					$('.transportadora').html(dado.codigo + ' - ' + dado.nome);
				} else {
					$('.transportadora').html('N/A');
				}
			});
		});
	} else {
		$('.transportadora').html('N/A');
	}
}

function salvar_pedido() {

	var sessao_pedido = [];

	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	sessionStorage['pagina_itens']					= 1;
	sessionStorage['pagina_itens_troca']			= 1;
	sessionStorage['pagina_itens_finalizar']		= 1
	sessionStorage['pagina_itens_troca_finalizar']	= 1;
	
	
	//Definindo Tabela de pedidos ou or�amentos
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento') {
		var tabela 	= 'orcamentos';
		var tipo 	= 'O';
	} else {
		var tabela 	= 'pedidos_pendentes';
		var tipo 	= 'P';
	}

	// Juntando Itens do Pedido com os Itens de Troca
	//var sessao_produtos = $.extend({}, sessao_pedido.produtos, sessao_pedido.produtos_troca);
	//Alterado para permitir que seja adicionado um item como venda e/ou troca no mesmo pedido 
	var itens_troca = new Array();
	if(sessao_pedido.produtos_troca) {
		$.each(sessao_pedido.produtos_troca, function(key, item){
			itens_troca['T' + key] = item;
		});
	}
	
	var sessao_produtos = $.extend({}, sessao_pedido.produtos, itens_troca);
	
	if(sessao_produtos) {
		var total_produtos 		= Object.keys(sessao_produtos).length;
	} else {
		var total_produtos 		= 0;
	}
	
	var dados_cliente 			= sessao_pedido.dados_cliente;
	var dados_prospect			= sessao_pedido.dados_prospect;
	var dados_transportadoras 	= sessao_pedido.dados_transportadoras;
	var dados_forma_pagamento	= sessao_pedido.dados_forma_pagamento;
	var numero_itens 			= 1;
	
	if(total_produtos > 0) {
	
		var editar = sessao_pedido.editar;
	
		if(editar) {
			var id_pedido 		= sessao_pedido.id_pedido;
		} else {
			var id_pedido 		= uniqid();
		}
		
		if(sessao_pedido.converter_pedido_orcamento) {
			id_pedido 		= sessao_pedido.id_pedido;
		}
		
		var data_emissao 	= date('Ymd');
		var time_emissao 	= time();
		var produtos		= [];
		
		var analitico_reais = (obter_valor_sessao('analitico_reais') ? obter_valor_sessao('analitico_reais') : '0.00');
		var analitico_porcentagem = (obter_valor_sessao('analitico_porcentagem') ? obter_valor_sessao('analitico_porcentagem') : '0.00');
		var gps_latitude = localStorage.getItem('gps_latitude');
		var gps_longitude = localStorage.getItem('gps_longitude');
		var versao_dwfdv = config.versao;//localStorage.getItem('versao');
		
		var mensagem_nota = utf8_encode(str_replace("\n", " ", sessao_pedido.mennota));
		var observacao_comercial = utf8_encode(str_replace("\n", " ", sessao_pedido.observacao_comercial));
		var pedido_cliente = removeSpecialChar(sessao_pedido.pedido_cliente);
		
		var log_pedido = '';
		
		$.each(sessao_produtos, function(key, item){
			var campos 			= [];
			var interrogacoes  	= [];
			var valores		 	= [];
			
			campos.push('converter_pedido_orcamento');					interrogacoes.push('?');
			
			campos.push('pedido_autorizado');							interrogacoes.push('?');
			
			campos.push('empresa');										interrogacoes.push('?');
			campos.push('filial');										interrogacoes.push('?');
			campos.push('pedido_filial');								interrogacoes.push('?');
			campos.push('pedido_numero_item');							interrogacoes.push('?');
			campos.push('pedido_id_pedido'); 							interrogacoes.push('?');
			campos.push('pedido_data_emissao'); 						interrogacoes.push('?');
			campos.push('pedido_time_emissao'); 						interrogacoes.push('?');
			campos.push('pedido_id_usuario'); 							interrogacoes.push('?');
			campos.push('pedido_codigo_representante'); 				interrogacoes.push('?');
			campos.push('pedido_tabela_precos');						interrogacoes.push('?');
			campos.push('pedido_id_feira');								interrogacoes.push('?');
			campos.push('pedido_codigo_cliente'); 						interrogacoes.push('?');
			campos.push('pedido_loja_cliente');							interrogacoes.push('?');
			campos.push('pedido_condicao_pagamento');					interrogacoes.push('?');
			campos.push('pedido_tipo_frete');							interrogacoes.push('?');
			campos.push('pedido_codigo_transportadora');				interrogacoes.push('?');
			campos.push('pedido_tipo_venda');							interrogacoes.push('?');
			campos.push('pedido_mensagem_nota');						interrogacoes.push('?');
			campos.push('pedido_observacao_comercial');					interrogacoes.push('?');			
			campos.push('pedido_cliente_entrega');						interrogacoes.push('?');
			campos.push('pedido_loja_entrega');							interrogacoes.push('?');
			campos.push('pedido_data_entrega');							interrogacoes.push('?');
			campos.push('pedido_pedido_cliente');						interrogacoes.push('?');
			
			//---
			campos.push('pedido_latitude');								interrogacoes.push('?');
			campos.push('pedido_longitude');							interrogacoes.push('?');
			campos.push('pedido_versao');								interrogacoes.push('?');
			//---
			
			//-- Cliente/Prospect
			if(dados_prospect) {
				campos.push('pedido_id_prospects');							interrogacoes.push('?');
				campos.push('prospect_codigo');								interrogacoes.push('?');				
				campos.push('prospect_codigo_loja');						interrogacoes.push('?');
				campos.push('prospect_nome');								interrogacoes.push('?');
				campos.push('prospect_cgc');								interrogacoes.push('?');
				
				campos.push('prospect_nome_contato');						interrogacoes.push('?');
				campos.push('prospect_cargo_contato');						interrogacoes.push('?');
				campos.push('prospect_email_contato');						interrogacoes.push('?');
				campos.push('prospect_telefone_contato');					interrogacoes.push('?');
				
				campos.push('prospect_endereco');							interrogacoes.push('?');
				campos.push('prospect_bairro');								interrogacoes.push('?');
				campos.push('prospect_cep');								interrogacoes.push('?');
				campos.push('prospect_codigo_municipio');					interrogacoes.push('?');
				campos.push('prospect_estado');								interrogacoes.push('?');
				campos.push('prospect_telefone');							interrogacoes.push('?');
				campos.push('prospect_email');								interrogacoes.push('?');
			} else { //Cliente
				campos.push('cliente_codigo');								interrogacoes.push('?');				
				campos.push('cliente_loja');								interrogacoes.push('?');
				campos.push('cliente_nome');								interrogacoes.push('?');
				campos.push('cliente_cpf');									interrogacoes.push('?');
				campos.push('cliente_pessoa_contato');						interrogacoes.push('?');
				campos.push('cliente_endereco');							interrogacoes.push('?');
				campos.push('cliente_bairro');								interrogacoes.push('?');
				campos.push('cliente_cep');									interrogacoes.push('?');
				campos.push('cliente_cidade');								interrogacoes.push('?');
				campos.push('cliente_estado');								interrogacoes.push('?');
				campos.push('cliente_telefone');							interrogacoes.push('?');
				campos.push('cliente_email');								interrogacoes.push('?');
				campos.push('pedido_tipo_cliente');							interrogacoes.push('?');
				campos.push('pedido_desconto1');							interrogacoes.push('?');
			}
			
			//Transportadoras
			campos.push('transportadora_codigo');						interrogacoes.push('?');		
			campos.push('transportadora_nome');							interrogacoes.push('?');
			
			//Condição de pagamento
			campos.push('forma_pagamento_codigo');						interrogacoes.push('?');
			campos.push('forma_pagamento_descricao');					interrogacoes.push('?');
			
			//Formas de pagamento
			campos.push('pedido_formas_de_pagamento');					interrogacoes.push('?');
			
			//Produtos
			campos.push('pedido_codigo_produto');						interrogacoes.push('?');
			campos.push('pedido_preco_unitario');						interrogacoes.push('?');
			campos.push('pedido_preco_venda');							interrogacoes.push('?');
			campos.push('pedido_quantidade');							interrogacoes.push('?');
			campos.push('pedido_desconto_item');						interrogacoes.push('?');
			campos.push('produto_descricao');							interrogacoes.push('?');
			campos.push('produto_ipi');									interrogacoes.push('?');
			campos.push('pedido_unidade_medida');						interrogacoes.push('?');
			campos.push('pedido_local');								interrogacoes.push('?');
			campos.push('pedido_peso_total');							interrogacoes.push('?');
			campos.push('item_troca');									interrogacoes.push('?');
			campos.push('produto_tipo_converter');						interrogacoes.push('?');
			campos.push('produto_converter');							interrogacoes.push('?');
			campos.push('produto_unidade_medida');						interrogacoes.push('?');
			campos.push('produto_segunda_unidade_medida');				interrogacoes.push('?');
			
			//Impostos
			campos.push('pedido_tipo_entrada_saida');					interrogacoes.push('?');
			campos.push('pedido_codigo_fiscal');						interrogacoes.push('?');
			campos.push('pedido_st');									interrogacoes.push('?');
			campos.push('pedido_ipi');									interrogacoes.push('?');
			campos.push('pedido_icms');									interrogacoes.push('?');
			
			campos.push('produto_grupo_tributacao');					interrogacoes.push('?');
			
			// Valores Fixos
			campos.push('pedido_status');								interrogacoes.push('?');
			campos.push('pedido_codigo_empresa');						interrogacoes.push('?');
			
			// Descontos
			campos.push('pedido_desconto2');							interrogacoes.push('?');
			
			//Valor da Unidade
			campos.push('pedido_valor_unidade');						interrogacoes.push('?');
			
			//Tipo de Movimentação
			campos.push('pedido_tipo_movimentacao');					interrogacoes.push('?');
			
			//Analiticos
			campos.push('pedido_analitico_valor');						interrogacoes.push('?');
			campos.push('pedido_analitico_perc');						interrogacoes.push('?');
			
			//------------------------
			
			valores.push(sessao_pedido.converter_pedido_orcamento);
			
			valores.push(sessao_pedido.autorizado);
			
			valores.push(info.empresa);
			valores.push(sessao_pedido.filial);
			valores.push(sessao_pedido.filial);
			valores.push(String(numero_itens));
			valores.push(id_pedido);
			valores.push(data_emissao);
			valores.push(time_emissao);
			valores.push(info.id_rep);
			valores.push(info.cod_rep);
			valores.push(sessao_pedido.tabela_precos);
			valores.push(sessao_pedido.evento);
			valores.push(sessao_pedido.codigo_cliente);
			valores.push(sessao_pedido.loja_cliente);
			valores.push(sessao_pedido.condicao_pagamento);
			valores.push(sessao_pedido.tipo_frete);
			valores.push(sessao_pedido.codigo_transportadora);
			valores.push(sessao_pedido.tipo_pedido);
			
			valores.push(mensagem_nota);
			valores.push(observacao_comercial);
			
			valores.push(sessao_pedido.codigo_cliente_entrega);
			valores.push(sessao_pedido.loja_cliente_entrega);
			valores.push(data_normal2protheus(sessao_pedido.data_entrega));
			valores.push(pedido_cliente);
			
			//Posição da venda
			valores.push(gps_latitude)
			valores.push(gps_longitude)
			//Versão			
			valores.push(versao_dwfdv);
			
			//-- Cliente/Prospect
			if(dados_prospect) {
				valores.push(dados_prospect.cgc);
				valores.push(dados_prospect.codigo);
				valores.push(dados_prospect.codigo_loja);
				valores.push(dados_prospect.nome);
				valores.push(dados_prospect.cgc);
				
				valores.push(dados_prospect.nome_contato);
				valores.push(dados_prospect.cargo_contato);
				valores.push(dados_prospect.email_contato);
				valores.push(dados_prospect.telefone_contato);
				
				valores.push(dados_prospect.endereco);
				valores.push(dados_prospect.bairro);
				valores.push(dados_prospect.cep);
				valores.push(dados_prospect.codigo_municipio);
				valores.push(dados_prospect.estado);
				valores.push(dados_prospect.telefone);
				valores.push(dados_prospect.email);
			} else { //Cliente
				valores.push(dados_cliente.codigo);
				valores.push(dados_cliente.loja);
				valores.push(dados_cliente.nome);
				valores.push(dados_cliente.cpf);
				valores.push(dados_cliente.pessoa_contato);
				valores.push(dados_cliente.endereco);
				valores.push(dados_cliente.bairro);
				valores.push(dados_cliente.cep);
				valores.push(dados_cliente.cidade);
				valores.push(dados_cliente.estado);
				valores.push(dados_cliente.telefone);
				valores.push(dados_cliente.email);
				valores.push(dados_cliente.tipo);
				valores.push(sessao_pedido.desconto_cliente);
			}
			
			//Transportadoras
			valores.push(dados_transportadoras.codigo);
			valores.push(dados_transportadoras.nome);
			
			//Formas Pagamento
			valores.push(dados_forma_pagamento.codigo);
			valores.push(dados_forma_pagamento.descricao);
			
			//Formas de pagamento
			valores.push(sessao_pedido.formas_de_pagamento);
			
			//Produtos
			valores.push(item.codigo);
			if(item.preco_tabela) {
				valores.push(item.preco_tabela);
			} else {
				valores.push(item.preco_unitario);
			}			
			valores.push(item.preco_venda);
			valores.push(item.quantidade);
			valores.push(item.desconto);
			valores.push(item.descricao);
			valores.push(item.ipi);
			//valores.push(item.unidade_medida);
			valores.push(item.unidade_medida_venda);
			valores.push(item.local);
			valores.push(item.peso_total);
			valores.push(item.item_troca);
			valores.push(item.tipo_converter);
			valores.push(item.valor_produto_converter);
			valores.push(item.unidade_medida);
			valores.push(item.segunda_unidade_medida);
			
			//Impostos
			valores.push(item.TES);
			valores.push(item.CF);
			valores.push(item.ST);
			valores.push(item.IPI);
			valores.push(item.ICMS);
			
			valores.push(item.grupo_tributacao);
			
			// Valores Fixos
			valores.push('A');
			valores.push(info.empresa);
			
			// Descontos
			valores.push(sessao_pedido.desconto_condicao_pagamento);
			
			//Valor da Unidade
			valores.push(item.valor_unidade);
			
			//Tipo de Movimentação
			valores.push(sessao_pedido.tipo_movimentacao);
			
			//Analiticos
			valores.push(analitico_reais);
			valores.push(analitico_porcentagem);
			
			//------------------------------------
			//---- PRODUTOS
			produtos.push(item.codigo);
			//------------------------------------
			
			if(!log_pedido) {
				log_pedido += campos.join('||');
				log_pedido += "\n";
			}
			
			log_pedido += valores.join('||');
			log_pedido += "\n";	
			
			
			if(editar) {
				db.transaction(function(x) {
					x.executeSql('UPDATE ' + tabela + ' SET ' + campos.join(' = ?, ') + ' = ? WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto = "' + item.codigo + '" AND item_troca = "' + item.item_troca+ '"', valores, function(e, resultado){
						
						if(!resultado.rowsAffected) {
							
							x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
								
								total_produtos--;
								
								if(total_produtos == 0) {
									gerar_arquivo_log(log_pedido, id_pedido);
									
									finalizar_pedido_editado(id_pedido, sessao_pedido.filial, produtos);
								}
							},
							function (e, t){
								console.log(e);
								console.log(t);
							});
						} else {
							total_produtos--;
							
							if(total_produtos == 0) {
								gerar_arquivo_log(log_pedido, id_pedido);
								
								finalizar_pedido_editado(id_pedido, sessao_pedido.filial, produtos);
							}
						}
					},
					function (e, t){
						console.log(e);
						console.log(t);
					});
				});
			} else {
				db.transaction(function(x) {
					x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
						total_produtos--;
						
						if(total_produtos == 0) {
							plugins.waitingDialog.fecharDialog();
							
							gerar_arquivo_log(log_pedido, id_pedido);
							
							// Se o sistema estiver transformando orçamento em pedido, exluir orçamento apos 
							if(sessao_pedido.converter_pedido_orcamento) {
								x.executeSql('DELETE FROM orcamentos WHERE pedido_id_pedido = "' + id_pedido + '"', [], function(){
									
									// Apagando pedido da sessao
									location.href="pedidos_espelho.html#" + id_pedido + '|' + sessao_pedido.filial + '|' + tipo;
									sessionStorage[sessionStorage['sessao_tipo']] = '';
								});
								
							} else {
								
								// Apagando pedido da sessao
								location.href="pedidos_espelho.html#" + id_pedido + '|' + sessao_pedido.filial + '|' + tipo;
								sessionStorage[sessionStorage['sessao_tipo']] = '';
							}
						}
					},
					function (e, t){
						console.log(JSON.stringify(e));
						console.log(JSON.stringify(t));
					});
				});
			}
			numero_itens++;
		});
	}
}

function gerar_arquivo_log(dados, codigo_pedido){
	var log = new LogPedidos();
	
	if(window.navigator.onLine) {
		$.ajax({
			url: config.ws_url + 'logs/importar',
			type: 'POST',
			data: {
				log: json_encode(dados)
			},
			success: function(dados) {
				console.log('Log enviado');
			}
		});
	}
	
	log.salvar_arquivo(dados, codigo_pedido);
}

function finalizar_pedido_editado(id_pedido, filial, produtos) {

	//Definindo Tabela de pedidos ou or�amentos
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento') {
		var tabela = 'orcamentos';
		var tipo = 'O';
	} else {
		var tabela = 'pedidos_pendentes';
		var tipo = 'P';
	}

	db.transaction(function(x) {
		x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto NOT IN ("' + produtos.join('", "') + '")', [], function(){
			plugins.waitingDialog.fecharDialog();
			// Apagando pedido da sessao
			location.href="pedidos_espelho.html#" + id_pedido + '|' + filial + '|' + tipo;
			sessionStorage[sessionStorage['sessao_tipo']] = '';
		
		});
	});
}

function montar_paginacao_finalizar(tipo_produto){
	if(tipo_produto == 'troca'){
		var produtos = obter_produtos_troca_sessao();
	} else {
		var produtos = obter_produtos_sessao();
	}
	
	if(produtos){
		var total = Object.keys(produtos).length;
	}else{
		var total = 0;
	}
	
	var html = '';
	html += '<div class="pagination" id="pagination">';
	for(i = 0; i < ceil(total / por_pagina); i++){
		
		var classe = '';
		if(tipo_produto == 'troca')
		{
			if((i+1) == sessionStorage['pagina_itens_troca_finalizar'])
			{
				classe = 'active';
			}
		}
		else
		{
			if((i+1) == sessionStorage['pagina_itens_finalizar'])
			{
				classe = 'active';
			}
		}
		
		html += '<ul>';
		html += '<li><a href="" class="pagina-finalizar ' + classe + '" data-tipo="' + tipo_produto + '" data-page="' + (i+1) + '">' + (i+1) + '</a></li>';
		html += '</ul>';
	}
	html += '</div>';
	
	if(tipo_produto == 'troca')
	{
		$('#page-selection-troca-finalizar').html(html);
	}
	else
	{
		$('#page-selection-finalizar').html(html);
	}
}

/**
 * CUSTOM E0184/2014 - Calculo de impostos offline
 */
function simular_impostos(callback){
	
	//Iniciando simulação de impostos
	$('#msgImpostos').addClass('info');
	$('#msgImpostos').html('Aguarde simulação impostos em andamento...');
	$('#botao_impostos').attr('disabled', 'disabled');
	
	
	//Sessão do pedido
	var sessao_pedido = [];

	//Obter sessão do pedido
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	var produtos = sessao_pedido['produtos'];
	
	if(produtos) {
		var total_produtos 		= Object.keys(produtos).length;
	}
	var indice = 0;
	
	
	//Realiza os filtros de filial, grupo de tributação do cliente e estado do cliente na tabela de exceção fiscal
	//Filtro padrão para todos os itens
	var where  = 'filial = "' + sessao_pedido.filial + '"';
		where += ' AND tributacao_cliente = "' + sessao_pedido.grupo_tributacao_cliente + '"';
		where += ' AND estado_cliente = "' + sessao_pedido.estado_cliente + '"';
		
	//Varre os itens da sessão para obter o registro de exceção fiscal de acordo com o grupo de tributação do produto
	$.each(produtos, function(key, produto){
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM excecao_fiscal WHERE ' + where + ' AND tributacao_produto = "' + produto.grupo_tributacao + '" LIMIT 1', [], function(x, dados) {
				console.log('SELECT * FROM excecao_fiscal WHERE ' + where + ' AND tributacao_produto = "' + produto.grupo_tributacao + '" LIMIT 1');
				if (dados.rows.length) {
					var excecao_fiscal = dados.rows.item(0);
					
					//Variaveis que serão utilizadas para o calculo de impostos
					var margem				= excecao_fiscal.margem;
					var aliquota_interna	= excecao_fiscal.aliquota_interna;
					var aliquota_externa	= excecao_fiscal.aliquota_externa;
					var base_reducao		= (excecao_fiscal.base_reducao > 0 ? 100 - excecao_fiscal.base_reducao : 0);
					var estado_filial		= sessao_pedido.estado_filial;
					var estado_cliente		= sessao_pedido.estado_cliente;
					var valor_total_produto	= produto.quantidade * produto.preco_venda;
					var ipi					= produto.ipi;
					
					//Verifica se a venda é interestadual ou externa para obter a aliquota de base do ICMS
					var aliquota = 0;
					if (estado_filial == estado_cliente) {
						aliquota = aliquota_interna;
				    } else {
				    	aliquota = aliquota_externa;
				    }
				    
				    /**
				     * Base de ICMS
				     * 
				     * Valor total do produto - percentual da base de redução
				     */
				    var base_icms =  valor_total_produto - ((base_reducao * valor_total_produto) / 100);
				    
				    /**
				     * Valor ICMS
				     * 
				     * Base de icms + percentual de aliquota (interna para venda interestadual e externa para vendas fora do estado)
				     */
				    var valor_icms = number_format(((base_icms * aliquota) / 100), 2);
				    
				    /**
				     * Base de ST
				     * 
				     * Valor total do produto + percentual de MVA
				     */
				    var base_st =  ((margem * valor_total_produto) / 100) + valor_total_produto;
				    
				    /**
				     * Valor ST
				     * 
				     * (Base de ST * percentual de Aliquota Interna) - Valor do ICMS
				     */
				    if(margem > 0) {
				    	var valor_st = number_format(((aliquota_interna * base_st) / 100) - valor_icms, 2);
				    } else {
				    	var valor_st = 0;
				    }
				    
				    /**
				     * Valor IPI
				     * 
				     * Valor total do produto + percentual de IPI
				     */
				    var valor_ipi =  number_format(((ipi * valor_total_produto) / 100), 2);
				    
					var codigo_produto 	= remover_zero_esquerda(produto.codigo);
					
					var produtos = new Array();
						produtos[codigo_produto] = new Array();
						produtos[codigo_produto]['codigo'] 			= produto.codigo;
						produtos[codigo_produto]['descricao'] 		= produto.descricao;
						produtos[codigo_produto]['preco_tabela'] 	= produto.preco_tabela;
						produtos[codigo_produto]['preco_unitario'] 	= produto.preco_unitario;
						produtos[codigo_produto]['preco_venda'] 	= produto.preco_venda
						produtos[codigo_produto]['quantidade'] 		= produto.quantidade;
						produtos[codigo_produto]['desconto'] 		= produto.desconto;
						produtos[codigo_produto]['segunda_unidade_medida']	= produto.segunda_unidade_medida;
						produtos[codigo_produto]['unidade_medida_venda']	= produto.unidade_medida_venda
						produtos[codigo_produto]['unidade_medida'] 	= produto.unidade_medida;
						produtos[codigo_produto]['tipo_converter']	= produto.tipo_converter;
						produtos[codigo_produto]['local'] 			= produto.local;
						//Impostos
						produtos[codigo_produto]['TES'] 			= 0;
						produtos[codigo_produto]['CF'] 				= 0;
						produtos[codigo_produto]['ST'] 				= valor_st;
						produtos[codigo_produto]['IPI'] 			= valor_ipi;
						produtos[codigo_produto]['ICMS'] 			= valor_icms;
						
						produtos[codigo_produto]['valor_unidade']			= produto.valor_unidade;
						produtos[codigo_produto]['valor_produto_converter']	= produto.valor_produto_converter;
						
						produtos[codigo_produto]['peso_total'] 		= produto.peso_total;
						produtos[codigo_produto]['peso_unitario'] 	= produto.peso_unitario;
						produtos[codigo_produto]['item_troca'] 		= produto.item_troca;
						
						produtos[codigo_produto]['grupo_tributacao'] = produto.grupo_tributacao;
					
					var produtos = serialize(produtos);
					
					salvar_produto_sessao(produtos);
					
					indice++;
					
					if(indice == total_produtos) {
						callback();
					}
				} else {
					
					$('#msgImpostos').addClass('warning');
					$('#msgImpostos').html('Não foi possível realziar simulação de impostos.');
					$('#botao_impostos').removeAttr('disabled');
					
					callback();
				}
			});
		});
	});
}