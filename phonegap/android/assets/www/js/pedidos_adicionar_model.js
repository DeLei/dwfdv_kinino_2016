var tabela = 'pedidos_pendentes';

var Pedidos = function(){

	var that = this;
	
	this.setClienteProspect = function(value){
		$("#cliente_prospect").val(value);
	}
	
	this.getClienteProspect = function(){
		return $("#cliente_prospect").val();
	}
	
	this.gerarTxt = function(cabecalho, callback){
		var codigo_pedido = cabecalho.pedido_id_pedido;
		if(cabecalho.editado){
			codigo_pedido = cabecalho.editado;
		} else if(cabecalho.converter_pedido_orcamento != '0'){
			codigo_pedido = cabecalho.converter_pedido_orcamento;
		}
		
		db.transaction(function(x){
			x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ?', [codigo_pedido], function(x, dados){
				var total = dados.rows.length;
				
				if(total){
					var campos	 = new Array();
					var valores	 = new Array();
					var conteudo = null;
					for(i = 0; i < total; i++){
						var registro = dados.rows.item(i);
						
						$.each(registro, function(campo, valor){
							if(i == 0){
								campos.push(campo);
								conteudo = campos.join("||");
								conteudo += "\n";
							}
							valores.push(valor);
							if(Object.keys(registro).length == valores.length){
								conteudo += valores.join("||");
								conteudo += "\n";
								
								valores = new Array();
							}
						});
					}
					
					var log = new LogPedidos();
					log.salvar_arquivo(conteudo, codigo_pedido);
					
					callback(codigo_pedido);
				}
			});
		});
	}
	
	this.finalizar = function(callback){
		that.getCabecalho(function(cabecalho){
			if(cabecalho.editado){
				that.finalizarEdicao(cabecalho, function(retorno){
					callback(retorno);
				});
			} else if(cabecalho.converter_pedido_orcamento != '0'){
				that.finalizarConversaoOrcamento(cabecalho, function(retorno){
					callback(retorno);
				});
			} else {
				that.finalizarNovo(cabecalho, function(retorno){
					callback(retorno);
				});
			}
		});
	}
	
	this.finalizarConversaoOrcamento = function(cabecalho, callback){
		that.getTotaisPedido(function(totais){
			var valor_analitico = totais.valor_analitico;
			var percentual_analitico = parseFloat(totais.percentual_analitico) - parseFloat(cabecalho.pedido_desconto2);
			
			var orcamento_convertido	= cabecalho.converter_pedido_orcamento;
			var codigo_temporario		= cabecalho.pedido_id_pedido;
			
			db.transaction(function(x){
				x.executeSql('DELETE FROM pedidos_pendentes WHERE (pedido_status = ? AND pedido_id_pedido = ? AND pedido_codigo_produto IS NULL) OR (pedido_id_pedido = ?)', ["R", codigo_temporario, orcamento_convertido], function(x, dados){
					x.executeSql('DELETE FROM orcamentos WHERE pedido_id_pedido = ?', [orcamento_convertido], function(x, dados){
						x.executeSql('UPDATE pedidos_pendentes SET pedido_id_pedido = ?, converter_pedido_orcamento = ?, pedido_status = ?, pedido_analitico_perc = ?, pedido_analitico_valor = ? WHERE pedido_status = ? AND pedido_id_pedido = ?', [orcamento_convertido, "1", "A", percentual_analitico, valor_analitico, "R", codigo_temporario], function(x, dados){
							localStorage.removeItem('sequencial_item');
							callback(cabecalho);
						}, function(){
							callback(false);
						});
					}, function(){
						callback(false);
					});
				}, function(){
					callback(false);
				});
			});
		});
	}
	
	this.finalizarEdicao = function(cabecalho, callback){
		that.getTotaisPedido(function(totais){
			var valor_analitico = totais.valor_analitico;
			var percentual_analitico = parseFloat(totais.percentual_analitico) - parseFloat(cabecalho.pedido_desconto2);
			
			var codigo_editado		= cabecalho.editado;
			var codigo_temporario	= cabecalho.pedido_id_pedido;
			
			db.transaction(function(x){
				x.executeSql('DELETE FROM ' + tabela + ' WHERE (pedido_status = ? AND pedido_id_pedido = ? AND pedido_codigo_produto IS NULL) OR (pedido_id_pedido = ?)', ["R", codigo_temporario, codigo_editado], function(x, dados){
					x.executeSql('UPDATE ' + tabela + ' SET pedido_id_pedido = ?, editado = ?, pedido_status = ?, pedido_analitico_perc = ?, pedido_analitico_valor = ? WHERE pedido_status = ? AND pedido_id_pedido = ?', [codigo_editado, "1", "A", percentual_analitico, valor_analitico, "R", codigo_temporario], function(x, dados){
						localStorage.removeItem('sequencial_item');
						callback(cabecalho);
					}, function(){
						callback(false);
					});
				}, function(){
					callback(false);
				});
			});
		});
	}
	
	this.finalizarNovo = function(cabecalho, callback){
		that.getTotaisPedido(function(totais){
			var valor_analitico = totais.valor_analitico;
			var percentual_analitico = parseFloat(totais.percentual_analitico) - parseFloat(cabecalho.pedido_desconto2);
			
			db.transaction(function(x){
				x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_status = ? AND pedido_id_pedido = ? AND pedido_codigo_produto IS NULL', ['R', cabecalho.pedido_id_pedido], function(x, dados){
					x.executeSql('UPDATE ' + tabela + ' SET pedido_status = ?, pedido_analitico_perc = ?, pedido_analitico_valor = ? WHERE pedido_status = ? AND pedido_id_pedido = ?', ['A', percentual_analitico, valor_analitico, 'R', cabecalho.pedido_id_pedido], function(x, dados){
						localStorage.removeItem('sequencial_item');
						callback(cabecalho);
					}, function(){
						callback(false);
					});
				}, function(){
					callback(false);
				});
			});
		});
	}
	
	this.getDataEntregaDefault = function(){
		return date('d/m/Y', strtotime('+14 days'));
	}
	
	this.getDataEntrega = function(){
		return $("#data_entrega").val();
	}
	
	this.setDataEntrega = function(value){
		$("#data_entrega").val(value);
	}
	
	this.getEdicao = function(){
		return $("#edicao").val();
	}
	
	this.setEdicao = function(value){
		$("#edicao").val(value);
	}
	
	this.getTotaisPedido = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT COUNT(*) AS qtd_itens, ROUND(SUM((pedido_preco_venda - pedido_preco_unitario) * pedido_quantidade), 2) AS valor_analitico, ROUND((SUM(pedido_preco_venda * pedido_quantidade) - SUM(pedido_preco_unitario * pedido_quantidade) ) * 100 / SUM(pedido_preco_unitario * pedido_quantidade), 2) AS percentual_analitico, ROUND(SUM(pedido_preco_venda * pedido_quantidade), 2) AS total_sem_impostos, ROUND(SUM((pedido_preco_venda * pedido_quantidade) + pedido_st + pedido_ipi), 2) AS total_com_impostos FROM ' + tabela + ' WHERE pedido_status = ? AND pedido_codigo_produto != ? AND item_troca = ?', ['R', '', '0'], function(x, dados){
				if(dados.rows.length){
					var Condicao = new CondicoesPagamento();
					var desconto_condicao = Condicao.getValorDescontoCabecalho();
					
					var dado = dados.rows.item(0);
						dado.desconto_condicao = parseFloat(desconto_condicao);
						
					callback(dado);
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.getTotaisPedidoAnalitico = function(somarDesconto, callback)
	{
		//mensagem(somarDesconto+' - somar desconto');
		db.transaction(function(x)
		{
			x.executeSql('SELECT COUNT(*) AS qtd_itens, ROUND(SUM((pedido_preco_venda - pedido_preco_unitario) * pedido_quantidade), 2) AS valor_analitico, ROUND((SUM(pedido_preco_venda * pedido_quantidade) - SUM(pedido_preco_unitario * pedido_quantidade) ) * 100 / SUM(pedido_preco_unitario * pedido_quantidade), 2) AS percentual_analitico, ROUND(SUM(pedido_preco_venda * pedido_quantidade), 2) AS total_sem_impostos, ROUND(SUM((pedido_preco_venda * pedido_quantidade) + pedido_st + pedido_ipi), 2) AS total_com_impostos FROM ' + tabela + ' WHERE pedido_status = ? AND pedido_codigo_produto != ? AND item_troca = ?', ['R', '', '0'], function(x, dados){
				if(dados.rows.length)
				{
					var Condicao = new CondicoesPagamento();
					var desconto_condicao = Condicao.getValorDescontoCabecalho();
					
					var dado = dados.rows.item(0);
					if(somarDesconto == 'N'){
						dado.valor_analitico = parseFloat(0);
						dado.percentual_analitico = parseFloat(0);
					}else{
						dado.desconto_condicao = parseFloat(desconto_condicao);
					}
						callback(dado);	
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.getTotaisTroca = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT COUNT(*) AS qtd_itens, SUM(pedido_preco_venda * pedido_quantidade) AS total_sem_impostos, SUM((pedido_preco_venda * pedido_quantidade) + pedido_st + pedido_ipi) AS total_com_impostos FROM ' + tabela + ' WHERE pedido_status = ? AND pedido_codigo_produto != ? AND item_troca = ?', ['R', '', '1'], function(x, dados){
				if(dados.rows.length){
					var dado = dados.rows.item(0);
					
					callback(dado);
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.getPedidoTemporario = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_status = ?', ['R'], function(x, dados){
				if(dados.rows.length){
					callback(dados);
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.getCabecalho = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_status = ? LIMIT 1', ['R'], function(x, dados){
				if(dados.rows.length){
					var dado = dados.rows.item(0);
					
					callback(dado);
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.removerPedidoTemporario = function(callback){
		db.transaction(function(x){
			x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_status = ?', ['R'], function(x, dados){
				localStorage.removeItem('sequencial_item');
				callback();
			});
		});
	}
	
	this.getArrayDadosCabecalho = function(cabecalho, pedidoTemporario, callback){
		
		var campos			= [];
		var valores			= [];
		var interrogacoes	= [];
		
		if(pedidoTemporario.pedido_id_pedido){ //Edição
			var id_pedido	=	pedidoTemporario.pedido_id_pedido;
		} else {
			var id_pedido	= uniqid();
		}
		var time_emissao	= time();
		var latitude		= localStorage.getItem('gps_latitude');
		var longitude		= localStorage.getItem('gps_longitude');
		
		var converter = cabecalho.converter;
		if(pedidoTemporario.converter_pedido_orcamento) {
			converter = pedidoTemporario.converter_pedido_orcamento;
		}
		if(!converter){
			converter = '0';
		} else if(converter != '0'){
			cabecalho.tipo_pedido.codigo = 'N';
			cabecalho.converter_pedido_orcamento = id_pedido;
		}
		
		if(cabecalho.edicao.editado){
			campos.push('editado');						interrogacoes.push('?');		valores.push(cabecalho.edicao.codigo_pedido);
		}
		campos.push('converter_pedido_orcamento');		interrogacoes.push('?');		valores.push(converter);
		campos.push('pedido_id_pedido');				interrogacoes.push('?');		valores.push(id_pedido);
		campos.push('pedido_status');					interrogacoes.push('?');		valores.push('R');
		campos.push('pedido_codigo_empresa');			interrogacoes.push('?');		valores.push(info.empresa);
		campos.push('empresa');							interrogacoes.push('?');		valores.push(info.empresa);
		campos.push('pedido_filial');					interrogacoes.push('?');		valores.push(cabecalho.filial.codigo);
		campos.push('filial');							interrogacoes.push('?');		valores.push(cabecalho.filial.codigo);
		campos.push('pedido_data_emissao');				interrogacoes.push('?');		valores.push(date('Ymd'));
		campos.push('pedido_time_emissao');				interrogacoes.push('?');		valores.push(time_emissao);
		campos.push('pedido_id_usuario');				interrogacoes.push('?');		valores.push(info.id_rep);
		campos.push('pedido_codigo_representante');		interrogacoes.push('?');		valores.push(info.cod_rep);
		campos.push('pedido_tabela_precos');			interrogacoes.push('?');		valores.push(cabecalho.tabela_precos.codigo);
		campos.push('pedido_tipo_venda');				interrogacoes.push('?');		valores.push(cabecalho.tipo_pedido.codigo);
		campos.push('pedido_pedido_cliente');			interrogacoes.push('?');		valores.push(cabecalho.ordem_compra);
		campos.push('pedido_latitude');					interrogacoes.push('?');		valores.push(latitude);
		campos.push('pedido_longitude');				interrogacoes.push('?');		valores.push(longitude);
		campos.push('pedido_versao');					interrogacoes.push('?');		valores.push(config.versao);
		campos.push('pedido_observacao_comercial');		interrogacoes.push('?');		valores.push(cabecalho.observacao_comercial);
		campos.push('pedido_tipo_movimentacao');		interrogacoes.push('?');		valores.push(cabecalho.tipo_movimentacao.codigo);
		campos.push('pedido_autorizado');				interrogacoes.push('?');		valores.push(cabecalho.autorizado);
		campos.push('pedido_analitico_perc');			interrogacoes.push('?');		valores.push(cabecalho.analitico.percentual);
		campos.push('pedido_analitico_valor');			interrogacoes.push('?');		valores.push(cabecalho.analitico.valor);
		campos.push('pedido_id_feira');					interrogacoes.push('?');		valores.push(cabecalho.evento.id);
		campos.push('pedido_tipo_frete');				interrogacoes.push('?');		valores.push(cabecalho.frete.codigo);
		campos.push('pedido_dirigido_redespacho');		interrogacoes.push('?');		valores.push(cabecalho.frete.dirigido_redespacho);
		campos.push('pedido_codigo_transportadora');	interrogacoes.push('?');		valores.push(cabecalho.transportadora.codigo);
		campos.push('transportadora_codigo');			interrogacoes.push('?');		valores.push(cabecalho.transportadora.codigo);
		campos.push('transportadora_nome');				interrogacoes.push('?');		valores.push(cabecalho.transportadora.nome);
		campos.push('pedido_data_entrega');				interrogacoes.push('?');		valores.push(data_normal2protheus(cabecalho.data_entrega));
		
		//Forma de Pagamento
		campos.push('pedido_formas_de_pagamento');		interrogacoes.push('?');		valores.push(cabecalho.forma_pagamento.codigo);
		
		//Condição de Pagamento
		campos.push('forma_pagamento_codigo');			interrogacoes.push('?');		valores.push(cabecalho.condicao_pagamento.codigo);
		campos.push('forma_pagamento_descricao');		interrogacoes.push('?');		valores.push(cabecalho.condicao_pagamento.descricao);
		campos.push('pedido_condicao_pagamento');		interrogacoes.push('?');		valores.push(cabecalho.condicao_pagamento.codigo);
		campos.push('pedido_desconto2');				interrogacoes.push('?');		valores.push(cabecalho.condicao_pagamento.desconto);
		
		if(cabecalho.prospect){
			campos.push('pedido_id_prospects');			interrogacoes.push('?');	valores.push(cabecalho.prospect.cgc);
			campos.push('prospect_codigo');				interrogacoes.push('?');	valores.push(cabecalho.prospect.codigo);
			campos.push('prospect_codigo_loja');		interrogacoes.push('?');	valores.push(cabecalho.prospect.codigo_loja);
			campos.push('prospect_nome');				interrogacoes.push('?');	valores.push(cabecalho.prospect.nome);
			campos.push('prospect_cgc');				interrogacoes.push('?');	valores.push(cabecalho.prospect.cgc);
			
			campos.push('prospect_nome_contato');		interrogacoes.push('?');	valores.push(cabecalho.prospect.nome_contato);
			campos.push('prospect_cargo_contato');		interrogacoes.push('?');	valores.push(cabecalho.prospect.cargo_contato);
			campos.push('prospect_email_contato');		interrogacoes.push('?');	valores.push(cabecalho.prospect.email_contato);
			campos.push('prospect_telefone_contato');	interrogacoes.push('?');	valores.push(cabecalho.prospect.telefone_contato);
			
			campos.push('prospect_endereco');			interrogacoes.push('?');	valores.push(cabecalho.prospect.endereco);
			campos.push('prospect_bairro');				interrogacoes.push('?');	valores.push(cabecalho.prospect.bairro);
			campos.push('prospect_cep');				interrogacoes.push('?');	valores.push(cabecalho.prospect.cep);
			campos.push('prospect_codigo_municipio');	interrogacoes.push('?');	valores.push(cabecalho.prospect.codigo_municipio);
			campos.push('prospect_estado');				interrogacoes.push('?');	valores.push(cabecalho.prospect.estado);
			campos.push('prospect_telefone');			interrogacoes.push('?');	valores.push(cabecalho.prospect.telefone);
			campos.push('prospect_email');				interrogacoes.push('?');	valores.push(cabecalho.prospect.email);
		} else {
			//Cliente
			campos.push('cliente_codigo');				interrogacoes.push('?');		valores.push(cabecalho.cliente.codigo);
			campos.push('cliente_loja');				interrogacoes.push('?');		valores.push(cabecalho.cliente.loja);
			campos.push('cliente_filial');				interrogacoes.push('?');		valores.push(cabecalho.cliente.filial);
			campos.push('cliente_nome');				interrogacoes.push('?');		valores.push(cabecalho.cliente.nome);
			campos.push('cliente_cpf');					interrogacoes.push('?');		valores.push(cabecalho.cliente.cpf);
			campos.push('cliente_pessoa_contato');		interrogacoes.push('?');		valores.push(cabecalho.cliente.pessoa_contato);
			campos.push('cliente_endereco');			interrogacoes.push('?');		valores.push(cabecalho.cliente.endereco);
			campos.push('cliente_site');				interrogacoes.push('?');		valores.push(cabecalho.cliente.site);
			campos.push('cliente_bairro');				interrogacoes.push('?');		valores.push(cabecalho.cliente.bairro);
			campos.push('cliente_cep');					interrogacoes.push('?');		valores.push(cabecalho.cliente.cep);
			campos.push('cliente_cidade');				interrogacoes.push('?');		valores.push(cabecalho.cliente.cidade);
			campos.push('cliente_estado');				interrogacoes.push('?');		valores.push(cabecalho.cliente.estado);
			campos.push('cliente_codigo_municipio');	interrogacoes.push('?');		valores.push(cabecalho.cliente.codigo_municipio);
			campos.push('cliente_ddd');					interrogacoes.push('?');		valores.push(cabecalho.cliente.ddd);
			campos.push('cliente_telefone');			interrogacoes.push('?');		valores.push(cabecalho.cliente.telefone);
			campos.push('cliente_email');				interrogacoes.push('?');		valores.push(cabecalho.cliente.email);
			campos.push('cliente_grupo_tributacao');	interrogacoes.push('?');		valores.push(cabecalho.cliente.grupo_tributacao);
			campos.push('pedido_codigo_cliente');		interrogacoes.push('?');		valores.push(cabecalho.cliente.codigo);
			campos.push('pedido_loja_cliente');			interrogacoes.push('?');		valores.push(cabecalho.cliente.loja);
			campos.push('pedido_desconto1');			interrogacoes.push('?');		valores.push(parseFloat(cabecalho.cliente.desconto));
			campos.push('pedido_tipo_cliente');			interrogacoes.push('?');		valores.push(cabecalho.cliente.tipo);
			campos.push('pedido_cliente_entrega');		interrogacoes.push('?');		valores.push(cabecalho.cliente_entrega.codigo);
			campos.push('pedido_loja_entrega');			interrogacoes.push('?');		valores.push(cabecalho.cliente_entrega.loja);
		}
		
		var retorno = {
			campos: campos,
			valores: valores,
			interrogacoes: interrogacoes
		};
		callback(retorno);
	}
	
	this.setCabecalho = function(cabecalho, callback){
		
		that.getCabecalho(function(pedidoTemporario){
			
			that.getArrayDadosCabecalho(cabecalho, pedidoTemporario, function(retorno){
				
				if(cabecalho.converter == '0'){
					tabela = (cabecalho.tipo_pedido.codigo == '*' ? 'orcamentos' : 'pedidos_pendentes');
				}
				
				if(cabecalho.edicao.editado){
					that.setEdicao(cabecalho.edicao.codigo_pedido);
				}
				
				if(pedidoTemporario.pedido_id_pedido){
					db.transaction(function(x){
						x.executeSql('UPDATE ' + tabela + ' SET ' + retorno.campos.join(' = ?, ') + ' = ? WHERE pedido_id_pedido = "' + pedidoTemporario.pedido_id_pedido + '"', retorno.valores, function(e, resultado){
							callback('update');
						});
					});
				} else {
					db.transaction(function(x){
						x.executeSql('INSERT INTO ' + tabela + ' (' + retorno.campos.join(', ') + ') VALUES (' + retorno.interrogacoes.join(', ') + ')', retorno.valores, function(){
							callback('insert');
						});
					});
				}
				
			});
			
		});
	}
	
	this.getUltimoPedidoCliente = function(codigo, loja, callback){
		db.transaction(function(x){
			x.executeSql('SELECT SUM(ip_valor_total_item) AS valor_pedido, SUM(ip_total_desconto_item) AS valor_desconto, * FROM pedidos_processados WHERE cliente_codigo = ? AND cliente_loja = ? GROUP BY pedido_codigo ORDER BY pedido_data_emissao DESC', [codigo, loja], function(x, dados){
				if(dados.rows.length){
					var dado = dados.rows.item(0);
					callback(dado);
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.validarVerbaIntrodutoria = function(codigo, loja, callback){
		
		if(!!!codigo){			
			callback({});
		}
		
		db.transaction(function(x){
			x.executeSql('SELECT nome, verba_introdutoria FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados){
				if(dados.rows.length){
					var dado = dados.rows.item(0);
					
					callback(dado);
				}
			});
		});
	}
	
	this.validarVerbaMensal = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT verba_mensal FROM representante WHERE codigo = ?', [info.cod_rep], function(x, dados){
				if(dados.rows.length){
					var dado = dados.rows.item(0);
					
					callback(dado);
				}
			});
		});
	}
	
	this.setAutorizado = function(value){
		$("#autorizado").val(value);
	}
	
	this.getAutorizado = function(){
		return $("#autorizado").val();
	}
	
	this.setPercentualAnalitico = function(){
		//TODO
	}
	
	this.getPercentualAnalitico = function(){
		//TODO
		return 'Percentual Analitico';
	}
	
	this.setValorAnalitico = function(){
		//TODO
	}
	
	this.getValorAnalitico = function(){
		//TODO
		return 'Valor Analitico';
	}
	
	this.getCorAnalitico = function(valor){
		var cor = '#000000';	//Preto para valor 0
		if(valor > 0){
			cor = '#179817';	//Verde para valor > 0
		}else if(valor < 0){
			cor = '#FF0000';	//Vermelho para valor < 0
		}
		
		return cor;
	}
	
	this.setOrdemCompra = function(value){
		$("#pedido_cliente").val(value);
	}
	
	this.getOrdemCompra = function(){
		return $("#pedido_cliente").val();
	}
	
	this.setObservacaoComercial = function(value){
		$("#observacao_comercial").val(value);
	}
	
	this.getObservacaoComercial = function(){
		return $("#observacao_comercial").val();
	}
	
};

var Filiais = function(){
	
	var that = this;
	
	this.filialPadrao = '01';
	
	this.getFiliais = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT codigo, razao_social, estado FROM filiais', [], function(x, dados){
				var total = dados.rows.length;
				
				$("#filial").html('');
				$("#filial").append('<option value="">SELECIONE...</option>');
				
				for(i = 0; i < total; i++){
					var dado = dados.rows.item(i);
					$("#filial").append('<option value="' + dado.codigo + '" data-estado="' + dado.estado + '">' + dado.codigo + ' - ' + dado.razao_social.toUpperCase() + '</option>');
				}
				
				callback();
			});
		});
	}
	
	this.setDefault = function(){
		$("#filial option[value='" + that.filialPadrao + "']").attr('selected', 'selected');
	}
	
	this.getValue = function(){
		return $("#filial").val();
	}
	
	this.getEstado = function(){
		return $("#filial option[value='" + that.getValue() + "']").data('estado');
	}
	
	this.setValue = function(value){
		$("#filial").val(value);
	}
	
	this.getLabel = function(){
		return $("#filial option[value='" + that.getValue() + "']").html();
	}
	
};

var TiposPedido = function(){
	
	var that = this;
	
	this.getValue = function(){
		return $("#tipo_pedido").val();
	}
	
	this.getLabel = function(){
		return $("#tipo_pedido option[value='" + that.getValue() + "']").html();
	}
	
	this.setValue = function(value){
		$("#tipo_pedido").val(value);
	}
	
}

var Clientes = function(){
	
	var that = this;
	
	this.getClientes = function(codigo, loja, callback){
		var Filial = new Filiais();
		var codigo_filial = Filial.getValue();
		
		if(codigo_filial){
			var where = "";
			
			if(info.empresa){
				where += " AND empresa = '" + info.empresa + "'";
			}
			
			if(codigo && loja){
				where += " AND codigo = '" + codigo + "' AND loja = '" + loja + "'";
			}
			
			db.transaction(function(x){
				x.executeSql('SELECT * FROM clientes WHERE (filial = "' + codigo_filial + '" OR filial = "") ' + where, [], function(x, dados){
					if(dados.rows.length){
						
						if(codigo && loja){
							var dado = dados.rows.item(0);
							
							callback(dado);
							
						} else {
							
							var clientes = [];
							
							for(i = 0; i < dados.rows.length; i++){
								var dado = dados.rows.item(i);
								
								clientes.push({ label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf, codigo: dado.codigo, loja: dado.loja, tabela_preco: dado.tabela_preco, desconto: dado.desconto, condicao_pagamento: dado.condicao_pagamento, estado: dado.estado, grupo_tributacao: dado.grupo_tributacao});
							}
							
							that.autocomplete(clientes);
							
							callback();
						}
					} else {
						callback();
					}
				});
			});
		}
	}
	
	this.autocomplete = function(clientes){
		$("#cliente").autocomplete({
			minLength: 3,
			source: clientes,
			position : { my : "left bottom", at: "left top", collision : "none" },
			select: function( event, ui ){
				
				that.setLabel(ui.item.label);
				that.setCodigo(ui.item.codigo);
				that.setLoja(ui.item.loja);
				that.setDesconto(ui.item.desconto);
				that.setEstado(ui.item.estado);
				that.setGrupoTributacao(ui.item.grupo_tributacao);
				that.getInformacoesCliente(ui.item.codigo, ui.item.loja);
				
				var tabela = new TabelaPrecos();
					tabela.regraTabelasPreco(ui.item.tabela_preco);
					
				// chamado 68
				var CondicaoPagamento	= 	new CondicoesPagamento();
					CondicaoPagamento.regraCondicaoPagamento(ui.item.condicao_pagamento);
				// fim chamado 68
				
				$("#cliente").attr('disabled', 'disabled');
				$("#trocar_cliente").show();
				
				// chamado 558
					var valor = $('select[name=tipo_pedido]').val();
					
					if(valor == 'I' || valor == 'B') {
						$('select[name="condicao_pagamento"] option[value="099"]').attr('selected', 'selected');
						$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
					} else {
						$('select[name="condicao_pagamento"] option[value="'+ui.item.condicao_pagamento+'"]').attr('selected', 'selected');
						
						if(obter_valor_sessao('tabela_precos')) 
						{
							$('select[name="condicao_pagamento"]').removeAttr('disabled');
						}						
					}
					
					$('select[name="condicao_pagamento"]').change();
					
				// fim chamado 558
				return false;
			}
		});
	}
	
	/**
	 * Exibe os detalhes do cliente durante a inclusão do pedido
	 */
	this.getInformacoesCliente = function(codigo, loja){
		that.getClientes(codigo, loja, function(cliente){
			$(".info_nome").html(cliente.nome);
			$(".info_cpf").html(cliente.cpf);
			$(".info_limite_credito").html(number_format(cliente.limite_credito, 3, ',', '.'));
			$(".info_titulos_aberto").html(number_format(cliente.total_titulos_aberto, 3, ',', '.'));
			$(".info_titulos_vencidos").html(number_format(cliente.total_titulos_ventidos, 3, ',', '.'));
			$(".info_credito_disponivel").html(number_format(cliente.limite_credito - cliente.total_titulos_aberto, 3, ',', '.'));
			$(".info_endereco").html(cliente.endereco);
			$(".info_bairro").html(cliente.bairro);
			$(".info_cidade").html(cliente.cidade);
			$(".info_estado").html(cliente.estado);
			$(".info_telefone").html(cliente.ddd + ' ' + cliente.telefone);
			that.exibirUltimoPedidoCliente(cliente.codigo, cliente.loja);
			
			$("#info_cli").show();
		});
	}
	
	/**
	 * Exibe os dados do último pedido realizado para o cliente selecionado
	 */
	this.exibirUltimoPedidoCliente = function(codigo, loja){
		var pedido = new Pedidos();
		
		pedido.getUltimoPedidoCliente(codigo, loja, function(cliente){
			if(cliente){
				$(".info_pedido_data_emissao").html(protheus_data2data_normal(cliente.pedido_data_emissao));
				$(".info_pedido_codigo").html(cliente.pedido_codigo);
				$(".info_pedido_codigo").attr('href', 'pedidos_espelho.html#' + cliente.pedido_codigo + '|' + cliente.pedido_filial);
				$(".info_forma_pagamento").html(cliente.forma_pagamento_descricao);
				$(".info_valor_pedido").html('R$ ' + number_format(cliente.valor_pedido, 3, ',', '.'));
				$(".info_valor_desconto").html('R$ ' + number_format(cliente.valor_desconto, 3, ',', '.'));
				$("#conteudo_ultimo_pedido ul").show();
				$("#conteudo_ultimo_pedido div").hide();
			} else {
				$("#conteudo_ultimo_pedido ul").hide();
				$("#conteudo_ultimo_pedido div").show();
			}
		});
	}
	
	/**
	 * Realiza a validação de títulos vencidos do cliente
	 *
	 * 		Não realiza a regra caso o cliente possuir a autorização de emergencia habilitada (campo autorizado = 'S' na tabela de clientes)
	 */
	this.validarTitulosVencidos = function(param, callback){
		if(param.pedido_orcamento == 'O'){
			
			callback(false);
			
		} else {
			
			db.transaction(function(x){
				x.executeSql('SELECT nome, data_titulo_vencido, autorizado FROM clientes WHERE codigo = ? AND loja = ?', [param.codigo, param.loja], function(x, dados){
					if(dados.rows.length){
						var dado = dados.rows.item(0);
						
						var pedido = new Pedidos();
						var validar_bloqueio = true;
						var data_bloqueio = date('Ymd', diminuir_dias_uteis(time(), 5));
						
						/*
						Solicitação 002936 - Bloqueio títulos vencidos
						
						//Nunca realiza o bloqueio para compras com A VISTA e BONIFICAÇÃO
						if(in_array(param.forma_pagamento, ['VIS', 'BON'])){
							validar_bloqueio = false;
						}else if(in_array(param.forma_pagamento, ['BOL', 'CAR', 'VA', 'CH'])){
							//Realiza o bloqueio somente 5 dias úteis após o vencimento
							if(data_bloqueio < dado.data_titulo_vencido){
								validar_bloqueio = false;
							}
						}
						*/
						if(data_bloqueio < dado.data_titulo_vencido){
							validar_bloqueio = false;
						}
						
						//Verifica se o cliente/pedido passa pela validação de bloqueio
						if(validar_bloqueio){
							if(!dado.data_titulo_vencido){
								pedido.setAutorizado('N');
								
								callback(false);
							} else if(dado.autorizado == 'S'){ //Autorização de Emergência
								pedido.setAutorizado('S');
								
								callback(false);
							} else {
								callback(dado);
							}
							
						} else {
							pedido.setAutorizado('N');
							callback(false);
						}
					}	
				});
			});
		}
	}
	
	this.getCodigo = function(){
		return $("#codigo_cliente").val();
	}
	
	this.setCodigo = function(value){
		$("#codigo_cliente").val(value);
	}
	
	this.getLoja = function(){
		return $("#loja_cliente").val();
	}
	
	this.setLoja = function(value){
		$("#loja_cliente").val(value);
	}
	
	this.getDesconto = function(){
		return $("#desconto_cliente").val();
	}
	
	this.setDesconto = function(value){
		$("#desconto_cliente").val(value);
	}
	
	this.getEstado = function(){
		return $("#estado_cliente").val();
	}
	
	this.setEstado = function(value){
		$("#estado_cliente").val(value);
	}
	
	this.getGrupoTributacao = function(){
		return $("#grupo_tributacao_cliente").val();
	}
	
	this.setGrupoTributacao = function(value){
		$("#grupo_tributacao_cliente").val(value);
	}
	
	this.setLabel = function(value){
		$("#cliente").val(value);
		if(value){
			$("#cliente").attr('disabled', 'disabled');
			$("#trocar_cliente").show();
		}
	}
	
	this.trocarCliente = function(){
		var tabelaPrecos = new TabelaPrecos();
		var CondicaoPagamento	= 	new CondicoesPagamento();
		
		that.setLabel('');
		that.setCodigo('');
		that.setLoja('');
		that.setDesconto('');
		that.setEstado('');
		that.setGrupoTributacao('');
		$("#info_cli").hide();
		$("#trocar_cliente").hide();
		$("#cliente").removeAttr('disabled');
		$("#cliente").focus();
		tabelaPrecos.regraTabelasPreco();
		// chamado 68
			CondicaoPagamento.regraCondicaoPagamento();
		// fim chamado 68
	}
	
	this.getLabel = function(){
		return $("#cliente").val();
	}
	
	this.exibirClienteEntrega = function(codigo_cliente, loja_cliente){
		that.getClientes(codigo_cliente, loja_cliente, function(cliente_entrega){
			$("#cliente_entrega").val(cliente_entrega.codigo + '/' + cliente_entrega.loja + ' - ' + cliente_entrega.nome + ' - ' + cliente_entrega.cpf);
			$(".endereco_entrega").html(cliente_entrega.endereco ? cliente_entrega.endereco : 'N/A');
			$(".bairro_entrega").html(cliente_entrega.bairro ? cliente_entrega.bairro : 'N/A');
			$(".cep_entrega").html(cliente_entrega.cep ? cliente_entrega.cep : 'N/A');
			$(".cidade_entrega").html(cliente_entrega.cidade ? cliente_entrega.cidade : 'N/A');
			$(".estado_entrega").html(cliente_entrega.estado ? cliente_entrega.estado : 'N/A');	
		});
	}
	
	this.getLimiteDesconto = function(codigo, loja, callback){
		db.transaction(function(x){
			x.executeSql('SELECT limite_desconto, limite_acrescimo FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados){
				if(dados.rows.length){
					callback(dados.rows.item(0));
				} else {
					callback(false);
				}
			});
		});
	}
};






var Prospects = function(){
	
	var that = this;
	
	this.getProspects = function(codigo, loja, callback){
		var Filial = new Filiais();
		var codigo_filial = Filial.getValue();
		
		if(codigo_filial){
			var where = "";
			
			if(info.empresa){
				where += " AND empresa = '" + info.empresa + "'";
			}
			
			if(codigo && loja){
				where += " AND codigo = '" + codigo + "' AND codigo_loja = '" + loja + "'";
			}
			
			db.transaction(function(x){
				x.executeSql('SELECT * FROM prospects WHERE (filial = "' + codigo_filial + '" OR filial = "") ' + where, [], function(x, dados){
					if(dados.rows.length){
						
						if(codigo && loja){
							var dado = dados.rows.item(0);
							
							callback(dado);
							
						} else {
							
							var prospects = [];
							
							for(i = 0; i < dados.rows.length; i++){
								var dado = dados.rows.item(i);
								
								prospects.push({ label: dado.codigo + '/' + dado.codigo_loja + ' - ' + dado.nome + ' - ' + dado.cgc, codigo: dado.codigo, loja: dado.codigo_loja, estado: dado.estado});
							}
							
							that.autocomplete(prospects);
							
							callback();
						}
						
					} else {
						
						callback();
						
					}
				});
			});
		}
	}
	
	this.autocomplete = function(prospects){
		$("#prospect").autocomplete({
			minLength: 3,
			source: prospects,
			position : { my : "left bottom", at: "left top", collision : "none" },
			select: function( event, ui ){
				
				that.setLabel(ui.item.label);
				that.setCodigo(ui.item.codigo);
				that.setLoja(ui.item.loja);
				that.setEstado(ui.item.estado);
				that.getInformacoesProspect(ui.item.codigo, ui.item.loja);
				
				$("#prospect").attr('disabled', 'disabled');
				$("#trocar_prospect").show();
				return false;
			}
		});
	}
	
	/**
	 * Exibe os detalhes do cliente durante a inclusão do pedido
	 */
	this.getInformacoesProspect = function(codigo, loja){
		that.getProspects(codigo, loja, function(prospect){
			$(".info_nome").html(prospect.nome);
			$(".info_cpf").html(prospect.cgc);
			$(".info_endereco").html(prospect.endereco);
			$(".info_bairro").html(prospect.bairro);
			$(".info_cidade").html(prospect.nome_municipio);
			$(".info_estado").html(prospect.estado);
			$(".info_cep").html(prospect.cep);
			$(".info_telefone").html(prospect.ddd + ' ' + prospect.telefone);
			
			$("#info_pro").show();
		});
	}
	
	this.getCodigo = function(){
		return $("#codigo_prospect").val();
	}
	
	this.setCodigo = function(value){
		$("#codigo_prospect").val(value);
	}
	
	this.getLoja = function(){
		return $("#loja_prospect").val();
	}
	
	this.setLoja = function(value){
		$("#loja_prospect").val(value);
	}
	
	this.getEstado = function(){
		return $("#estado_prospect").val();
	}
	
	this.setEstado = function(value){
		$("#estado_prospect").val(value);
	}
	
	this.setLabel = function(value){
		$("#prospect").val(value);
		if(value){
			$("#prospect").attr('disabled', 'disabled');
			$("#trocar_prospect").show();
		}
	}
	
	this.trocarProspect = function(){
		that.setLabel('');
		that.setCodigo('');
		that.setLoja('');
		that.setEstado('');
		$("#info_pro").hide();
		$("#trocar_prospect").hide();
		$("#prospect").removeAttr('disabled');
		$("#prospect").focus();
	}
	
	this.getLabel = function(){
		return $("#prospect").val();
	}
	
	this.exibirProspectEntrega = function(codigo_prospect, loja_prospect){
		that.getProspects(codigo_prospect, loja_prospect, function(prospect_entrega){
			$("#cliente_entrega").val(prospect_entrega.codigo + '/' + prospect_entrega.codigo_loja + ' - ' + prospect_entrega.nome + ' - ' + prospect_entrega.cgc);
			$(".endereco_entrega").html(prospect_entrega.endereco ? prospect_entrega.endereco : 'N/A');
			$(".bairro_entrega").html(prospect_entrega.bairro ? prospect_entrega.bairro : 'N/A');
			$(".cep_entrega").html(prospect_entrega.cep ? prospect_entrega.cep : 'N/A');
			$(".cidade_entrega").html(prospect_entrega.nome_municipio ? prospect_entrega.nome_municipio : 'N/A');
			$(".estado_entrega").html(prospect_entrega.estado ? prospect_entrega.estado : 'N/A');	
		});
	}
	
};

var TabelaPrecos = function(){
	
	var that = this;
	
	this.getTabelasPreco = function(callback){
		var where = " WHERE codigo > 0 ";
		
		if(info.empresa){
			where += " AND empresa = '" + info.empresa + "' ";
		}
		
		where += " AND vigencia_inicio <= '" + date('Ymd') + "'";
		where += " AND (vigencia_final >= '" + date('Ymd') + "' OR vigencia_final = '')";
		
		db.transaction(function(x){
			x.executeSql('SELECT * FROM tabelas_preco ' + where, [], function(x, dados){
				var total = dados.rows.length;
				
				$("#tabela_precos").append('<option value="">SELECIONE...</option>');
				
				for(i = 0; i < total; i++){
					var dado = dados.rows.item(i);
					$("#tabela_precos").append('<option value="' + dado.codigo + '" data-condicao_pagamento="' + dado.condicao_pagamento + '">' + dado.codigo + ' - ' + dado.descricao.toUpperCase() + '</option>');
				}
				
				callback();
			});
		});
	}
	
	/**
	 * Fixa a tabela de preços que retornar do cadastro de clientes, não permite alteração
	 */
	this.regraTabelasPreco = function(codigo_tabela){
		if(codigo_tabela){
			$("#tabela_precos option[value='" + codigo_tabela + "']").attr('selected', 'selected');
			$("#tabela_precos").attr('disabled', 'disabled');
		} else {
			$("#tabela_precos option[value='']").attr('selected', 'selected');
			$("#tabela_precos").removeAttr('disabled');
		}
	}
	
	this.getValue = function(){
		return $("#tabela_precos").val();
	}
	
	this.getLabel = function(){
		return $("#tabela_precos option[value='" + that.getValue() + "']").html();
	}
	
	this.setValue = function(value){
		$("#tabela_precos").val(value);
	}
	
};

var CondicoesPagamento = function(){
	
	var that = this;
	
	this.getCondicoesPagamento = function(callback){
		var tipoPedido = new TiposPedido();
		
		var where = '';
		
		if(info.empresa){
			where += " WHERE empresa = '" + info.empresa + "'";
		}
		
		if(tipoPedido.getValue() == 'N'){
			where = (where ? where + ' AND ' : ' ');
			where += 'codigo != "099"';
		}
		
		db.transaction(function(x){
			x.executeSql('SELECT * FROM formas_pagamento ' + where + ' AND codigo IN (select condicao_pagamento from vendedor_condicao_pagamento)', [], function(x, dados){
				
				var total = dados.rows.length;
				
				$("#condicao_pagamento").html('<option value="">SELECIONE...</option>');
				
				for(i = 0; i < total; i++){
					var dado = dados.rows.item(i);
					$("#condicao_pagamento").append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
				
				callback();
			});
		});
		
	}
	
	// chamado 68
	/**
	 * Fixa a tabela de formas de pagamentos que retornar do cadastro de clientes, não permite alteração
	 */
	this.regraCondicaoPagamento = function(codigo){
		if(codigo)
		{
			if($('select[name=tipo_pedido]').val() == 'N')
			{
				$("#condicao_pagamento option[value='" + codigo + "']").attr('selected', 'selected');
				$("#condicao_pagamento").attr('disabled', 'disabled');
			}
			
			if(codigo == '001' || codigo == '019' || codigo == '074')
			{
				$("#box_desconto_condicao_pagamento").show();
			} else {
				$("#desconto_condicao_pagamento").val(0);
				this.setValorDesconto('0');
				$("#box_desconto_condicao_pagamento").hide();
			}
			
		} else {
			$("#box_desconto_condicao_pagamento").hide();
			$("#condicao_pagamento option[value='']").attr('selected', 'selected');
			$("#condicao_pagamento").removeAttr('disabled');
		}
	}
	// fim chamado 68
	
	/**
	 * Liberado o campo desconto de condição de pagamento ao selecionar as seguintes condições:
	 * 		001 - A VISTA
	 * 		019 - 7 DIAS
	 * 		074 - 10 DIAS
	 * 
	 * Obs: O Representante poderá selecionar somente os valores 2% ou 0% como desconto de condição de pagamento
	 */
	this.getDescontoCabecalho = function(){
		var condicao = that.getValue();
		//mensagem(condicao+' - condicao');
		if(condicao == '001' || condicao == '019' || condicao == '074'){
			$("#box_desconto_condicao_pagamento").show();
		} else {
			$("#box_desconto_condicao_pagamento").hide();
			that.setValorDesconto('0');
		}
	}
	
	this.getLabel = function(){
		return $("#condicao_pagamento option[value='" + that.getValue() + "']").html();
	}
	
	this.getValorDescontoCabecalho = function(){
		return $("#desconto_condicao_pagamento").val();
	}
	
	this.getValue = function(){
		return $("#condicao_pagamento").val();
	}
	
	this.getValorDesconto = function(){
		return parseFloat($("#desconto_condicao_pagamento").val());
	}
	
	this.setValue = function(value, disabled){
		$("#condicao_pagamento").val(value);
		
		if(disabled){
			$("#condicao_pagamento").attr('disabled', 'disabled');
		} else {
			$("#condicao_pagamento").removeAttr('disabled');
		}
	}
	
	this.setValorDesconto = function(value){
		$("#desconto_condicao_pagamento").val(value);
	}
};

var TiposMovimentacao = function(){
	
	var that = this;
	
	this.getValue = function(){
		return $("#tipo_movimentacao").val();
	}
	
	this.setValue = function(value){
		$("#tipo_movimentacao").val(value);
	}
	
	this.getLabel = function(){
		return $("#tipo_movimentacao option[value='" + that.getValue() + "']").html();
	}
	
};

var FormasPagamento = function(){
	
	var that = this;
	
	/**
	 * Formas de pagamento disponíveis de acordo com o tipo de movimentação:
	 * 
	 *  	Para o tipo de movimentação 00:
	 *  		VIS	- A VISTA
	 *  		CH	- CHEQUE
	 *  		BON	- BONIFICAÇÃO
	 *  		VA	- VALE
	 *  
	 *  	Para o tipo de movimentação 01:
	 *  		VIS - A VISTA
	 *  		CH	- CHEQUE
	 *  		BOL	- BOLETO
	 *  		BON	- BONIFICAÇÃO
	 *  		CAR	- CARTEIRA
	 */
	this.getFormasPagamento = function(callback){
		var tipo_movimentacao = new TiposMovimentacao();
		
		if(info.empresa){
			var where = " WHERE empresa = '" + info.empresa + "'";
		}
		
		if(tipo_movimentacao.getValue() == '00'){
			where += ' AND chave IN ("VIS", "CH", "BON", "VA")';
		} else {
			where += ' AND chave IN ("VIS", "CH", "BOL", "BON", "CAR")';
		}
		
		db.transaction(function(x){
			x.executeSql('SELECT * FROM formas_pagamento_real ' + where, [], function(x, dados){
				var total = dados.rows.length;
				
				$("#forma_pagamento").html('<option value="">SELECIONE...</option>');
				
				for(i = 0; i < total; i++){
					var dado = dados.rows.item(i);
					$("#forma_pagamento").append('<option value="' + dado.chave + '">' + dado.descricao + '</option>');
				}
				
				callback();
			});
		});
	}
	
	this.getValue = function(){
		return $("#forma_pagamento").val();
	}
	
	this.getLabel = function(){
		return $("#forma_pagamento option[value='" + that.getValue() + "']").html();
	}
	
	this.setValue = function(value, disabled){
		$("#forma_pagamento").val(value);
		
		if(disabled){
			$("#forma_pagamento").attr('disabled', 'disabled');
		} else {
			$("#forma_pagamento").removeAttr('disabled');
		}
	}
	
};

var Eventos = function(){
	
	this.getIdEvento = function(){
		return $("#evento").val();
	}
	
	this.setIdEvento = function(){
		$("#evento option[value='" + value + "']").attr('selected', 'selected');
	}
	
	this.getLista = function(callback){
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM eventos', [], function(x, dados) {
				var total = dados.rows.length;
				
				$("#evento").html('<option value="">SELECIONE...</option>');
				
				for(i = 0; i < total; i++) {
					var dado = dados.rows.item(i);
					$("#evento").append('<option value="' + dado.id + '">' + dado.nome + '</option>');
				}
			});
		});
	}
};

var Fretes = function(){
	
	var that = this;
	
	this.getFrete = function(){
		return $("#tipo_frete").val();
	}
	
	this.getLabel = function(){
		return $("#tipo_frete option[value='" + that.getFrete() + "']").html();
	}
	
	this.setFrete = function(value){
		$("#tipo_frete option[value='" + value + "']").attr('selected', 'selected');
	}
	
	this.getDirigidoRedespacho = function(){
		return $("#dirigido_redespacho").val();
	}
	
	this.getLabelDirigidoRedespacho = function(){
		return $("#dirigido_redespacho option[value='" + that.getDirigidoRedespacho() + "']").html();
	}
	
	this.setDirigidoRedespacho = function(value){
		$("#dirigido_redespacho option[value='" + value + "']").attr('selected', 'selected');
	}
	
};

var Transportadoras = function(){
	
	this.getCodigo = function(){
		return $("#codigo_transportadora").val();
	}
	
	this.setCodigo = function(value){
		$("#codigo_transportadora").val(value);
	}
	
	this.getLabel = function(){
		return $("#auto-transportadora").val();
	}
	
	this.setLabel = function(value){
		$("#auto-transportadora").val(value);
	}
};



/// ----Etapa Produtos

var Produtos = function(){
	
	var that = this;
	
	this.getProdutosAutocomplete = function(codigo_filial, tabela_precos){
		$(".auto-produto").autocomplete({
			minLength: 2,
			source: function(request, response){
				$("#erro_item").hide();
				$("#carregando_produtos").show();
				
				var id = $(this)[0].element[0].id;
				var tamanho = request.term.length;
				var wheres = '';
				
				if(id == 'codigo_reduzido'){
					wheres += ' AND ((substr(produto_codigo_palm, 1, ' + tamanho + ') LIKE "' + request.term + '%" AND length(produto_codigo_palm) <= ' + tamanho + ') OR produto_descricao LIKE "' + request.term + '%" )';
				} else {
					wheres += ' AND produto_descricao LIKE "' + request.term + '%"';
				}
				
				if(codigo_filial && codigo_filial != 'undefined'){
					wheres += " AND (produto_filial = '" + codigo_filial + "' OR produto_filial = '')";
				}
				
				db.transaction(function(x){
					x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ?' + wheres, [tabela_precos], function(x, dados){
						if(dados.rows.length){
							
							if (dados.rows.length == 1){ //Código Reduzido
								var dado = dados.rows.item(0);
								
								var produto = {
									label:	dado.produto_codigo_palm + ' - ' + dado.produto_descricao,
									dados:	dado
								}
								
								that.setValoresProdutoSelecionado(produto);
								that.bloquearProdutoSelecionado();
								
							} else { //Autocomplete
								
								var produtos = [];
								
								for(i = 0; i < dados.rows.length; i++){
									var dado = dados.rows.item(i);
									
									produtos.push({
										label:	dado.produto_codigo_palm + ' - ' + dado.produto_descricao,
										dados:	dado
									});
								}
								
								$("#carregando_produtos").hide();
								
								response(produtos);
							}
							
						}
					});
				});
			},	
			position : { my : "left bottom", at: "left top", collision : "none"},
			select: function( event, ui ) {
				
				that.setValoresProdutoSelecionado(ui.item);
				that.bloquearProdutoSelecionado();
				
				return false;
			}
		});
	}
	
	this.setValoresProdutoSelecionado = function(produto){
		var item_troca = that.getItemTrocaProdutoSelecionado();
		
		that.verificarProdutoExiste(produto.dados.produto_codigo, item_troca, function(dadosProdutoAdicionado){
			
			var preco_unitario = produto.dados.ptp_preco * produto.dados.produto_converter;
			
			if(dadosProdutoAdicionado){
				var total = parseFloat(dadosProdutoAdicionado.pedido_preco_venda) * parseInt(dadosProdutoAdicionado.pedido_quantidade);
				that.setPrecoVendaProdutoSelecionado(dadosProdutoAdicionado.pedido_preco_venda);
				that.setValorUnidadeProdutoSelecionado(dadosProdutoAdicionado.pedido_valor_unidade);
				that.setPrecoUnitarioProdutoSelecionado(preco_unitario);
				that.getListaUnidadesMedida(dadosProdutoAdicionado);
				that.setUnidadeMedidaProdutoSelecionado(dadosProdutoAdicionado.pedido_unidade_medida);
				that.setQuantidadeProdutoSelecionado(dadosProdutoAdicionado.pedido_quantidade);
				that.setDescontoProdutoSelecionado(dadosProdutoAdicionado.pedido_desconto_item);
				that.setValorTotalProdutoSelecionado();
			} else {
				var preco = produto.dados.ptp_preco * produto.dados.produto_converter;
				
				that.setPrecoVendaProdutoSelecionado(preco);
				that.setValorUnidadeProdutoSelecionado(produto.dados.ptp_preco);
				that.setPrecoUnitarioProdutoSelecionado(preco_unitario);
				that.getListaUnidadesMedida(produto.dados);
				if(that.getItemTrocaProdutoSelecionado() == '1'){
					that.setUnidadeMedidaProdutoSelecionado(produto.dados.produto_unidade_medida);
				} else {
					that.setUnidadeMedidaProdutoSelecionado(produto.dados.produto_segunda_unidade_medida);
				}
				that.setQuantidadeProdutoSelecionado('1');
				that.setDescontoProdutoSelecionado('0');
				that.setValorTotalProdutoSelecionado();
			}
			
			that.setLabelProdutoSelecionado(produto.label);
			that.setCodigoReduzidoProdutoSelecionado(produto.dados.produto_codigo_palm);
			that.setCodigoProdutoSelecionado(produto.dados.produto_codigo);
			that.setFatorConversaoProdutoSelecionado(produto.dados.produto_converter);
			that.setTipoConversaoProdutoSelecionado(produto.dados.produto_tipo_converter);
		});
	}
	
	this.bloquearProdutoSelecionado = function(){
		$("#erro_item").hide();
		$("#auto-produto").attr('disabled', 'disabled');
		$("#auto-produto").css('background-color', '#F2F2F2');
		$("#trocar_produto").show();
	}
	
	this.limparProdutoSelecionado = function(limparCodigoReduzido){
		that.setLabelProdutoSelecionado('');
		if(limparCodigoReduzido){
			that.setCodigoReduzidoProdutoSelecionado('');
		}
		that.setPrecoVendaProdutoSelecionado('');
		that.setValorUnidadeProdutoSelecionado('');
		that.setDescontoProdutoSelecionado('');
		that.setValorTotalProdutoSelecionado('');
		that.setTipoConversaoProdutoSelecionado('');
		that.setFatorConversaoProdutoSelecionado('');
		that.setPrecoUnitarioProdutoSelecionado('');
		that.setUnidadeMedidaProdutoSelecionado('');
		that.setQuantidadeProdutoSelecionado('');
		that.setItemTrocaProdutoSelecionado('0');
		that.calcularValoresProdutoSelecionado();
		$("#trocar_produto").hide();
		$("#codigo_reduzido").trigger('click');
		$("#auto-produto").removeAttr('disabled');
		$("#auto-produto").css('background-color', '#FFF');
	}
	
	this.getValorImpostos = function(params, callback){
		var itemTroca = that.getItemTrocaProdutoSelecionado();
		
		//Não calula ST para itens de troca
		if(itemTroca == 1) {
			
			var impostos = {
				tes: 0,
				cf: 0,
				st: 0,
				ipi: 0,
				icms: 0
			};
			
			callback(impostos);
				
		} else {
			var where  = 'filial = "' + params.codigo_filial + '"';
				where += ' AND tributacao_cliente = "' + params.grupo_tributacao_cliente + '"';
				where += ' AND estado_cliente = "' + params.estado_cliente + '"';
			
			db.transaction(function(x) {
				
				
				
				x.executeSql('SELECT * FROM excecao_fiscal WHERE ' + where + ' AND tributacao_produto = "' + params.produto.produto_grupo_tributacao + '" LIMIT 1', [], function(x, dados) {
					
					if (dados.rows.length) {
						
						var excecao_fiscal = dados.rows.item(0);
						
						
						var margem				= excecao_fiscal.margem;
						var aliquota_destino	= excecao_fiscal.aliquota_destino;
						var aliquota_interna	= excecao_fiscal.aliquota_interna;
						var aliquota_externa	= excecao_fiscal.aliquota_externa;
						var base_reducao		= (excecao_fiscal.base_reducao > 0 ? 100 - excecao_fiscal.base_reducao : 0);
						var base_reducao_st		= (excecao_fiscal.base_reducao_st > 0 ? 100 - excecao_fiscal.base_reducao_st : 0);
						var estado_filial		= params.estado_filial;
						var estado_cliente		= params.estado_cliente;
						var valor_total_produto	= params.produto.quantidade * params.produto.preco_venda;
						var ipi					= params.produto.ipi;
						
						//Verifica se a venda é interestadual ou externa para obter a aliquota de base do ICMS
						var aliquota = 0;
						var aliquota_operacao = aliquota_interna;
						
						if(aliquota_destino > 0) {
							aliquota_operacao = aliquota_destino;
						}
						
						if (estado_filial == estado_cliente) {
							aliquota = aliquota_interna;
					    } else {
					    	aliquota = aliquota_externa;
					    }
						
					    /**
					     * Base de ICMS
					     * 
					     * Valor total do produto - percentual da base de redução
					     */
					    var base_icms =  valor_total_produto - ((base_reducao * valor_total_produto) / 100);
					    
					    /**
					     * Valor ICMS
					     * 
					     * Base de icms + percentual de aliquota (interna para venda interestadual e externa para vendas fora do estado)
					     */
					    var valor_icms = number_format(((base_icms * aliquota) / 100), 2);
					    
					    /**
					     * Base de ST
					     * 
					     * Valor total do produto + percentual de MVA
					     */
					    var base_st =  ((margem * valor_total_produto) / 100) + valor_total_produto;
					    
					    /**
					     * Se o produto possuir redução na ST o valor será abatido do valor da base de ST 
					     */
					    if(base_reducao_st > 0) {
					    	base_st = base_st - ((base_reducao_st * base_st) / 100);
					    }
					    
					    /**
					     * Valor ST
					     * 
					     * (Base de ST * percentual de Aliquota Interna) - Valor do ICMS
					     */
					    if(margem > 0) {
					    	var valor_st = number_format(((aliquota_operacao * base_st) / 100) - valor_icms, 2);
					    } else {
					    	var valor_st = 0;
					    }
					    
					    /**
					     * Valor IPI
					     * 
					     * Valor total do produto + percentual de IPI
					     */
					    var valor_ipi =  number_format(((ipi * valor_total_produto) / 100), 2);
					    
						var impostos = {
							tes: 0,
							cf: 0,
							st: parseFloat(valor_st),
							ipi: parseFloat(valor_ipi),
							icms: parseFloat(valor_icms)
						};
						
						callback(impostos);
						
					} else {
						
						var impostos = {
							tes: 0,
							cf: 0,
							st: 0,
							ipi: 0,
							icms: 0
						};
						
						callback(impostos);
						
					}
					
				});
			});
		}
	}
	
	this.getPrecoUnitarioProdutoSelecionado = function(){
		return converter_decimal($("#preco_unitario").val());
	}
	
	this.getCodigoSelecionado = function(){
		return $("#codigo_produto").val();
	}
	
	this.getPrecoVendaProdutoSelecionado = function(monetario){
		if(monetario){
			return $("#preco_venda").val();
		} else {
			return converter_decimal($("#preco_venda").val());
		}
	}
	
	this.getQuantidadeProdutoSelecionado = function(){
		return $("#quantidade").val();
	}
	
	this.getDescontoProdutoSelecionado = function(){
		return converter_decimal($("#desconto").val());
	}
	
	this.getItemTrocaProdutoSelecionado = function(){
		return $("#item_troca").val();
	}
	
	this.setItemTrocaProdutoSelecionado = function(value){
		$("#item_troca option[value='" + value + "']").attr('selected', 'selected');
	}
	
	this.getTipoConversaoProdutoSelecionado = function(){
		return $("#tipo_conversao").val();
	}
	
	this.getFatorConversaoProdutoSelecionado = function(){
		return $("#valor_conversao").val();
	}
	
	this.getUnidadeMedidaProdutoSelecionado = function(){
		if(that.getItemTrocaProdutoSelecionado() == '1'){ //Item troca retorna o valor do dropdown
			return $("#dropdown_unidade_medida").val();
		} else { //Item de venda retorna o valor do input
			return $("#input_unidade_medida").val();
		}
	}
	
	this.getValorUnidadeProdutoSelecionado = function(monetario){
		if(monetario) {
			return $("#valor_unidade").val();
		} else {
			return converter_decimal($("#valor_unidade").val());
		}
	}
	
	this.getDadosProduto = function(codigo_filial, codigo_produto, codigo_tabela_precos, callback){
		var where = '';
		
		if(codigo_filial && codigo_filial != 'undefined'){
			where += " AND (produto_filial = '" + codigo_filial + "' OR produto_filial = '')";
		}
		
		db.transaction(function(x){
			x.executeSql('SELECT * FROM produtos WHERE tb_codigo = ? AND produto_codigo = ?' + where, [codigo_tabela_precos, codigo_produto], function(x, dados){
				var dadosProduto = dados.rows.item(0);
				
				callback(dadosProduto);
			});
		});
	}
	
	this.getProduto = function(filial, codigo_produto, tabela_precos, callback){
		that.getDadosProduto(filial, codigo_produto, tabela_precos, function(dadosProduto){
			var filial	= new Filiais();
			var cliente	= new Clientes();
			var produto	= new Produtos();
			
			dadosProduto.item_troca				= produto.getItemTrocaProdutoSelecionado();
			dadosProduto.preco_venda			= produto.getPrecoVendaProdutoSelecionado();
			dadosProduto.valor_unidade			= produto.getValorUnidadeProdutoSelecionado();
			dadosProduto.quantidade				= produto.getQuantidadeProdutoSelecionado();
			dadosProduto.desconto				= produto.getDescontoProdutoSelecionado();
			dadosProduto.unidade_medida_venda	= produto.getUnidadeMedidaProdutoSelecionado();
			
			var params = {
				estado_filial				: filial.getEstado(),
				codigo_filial				: filial.getValue(),
				estado_cliente				: cliente.getEstado(),
				grupo_tributacao_cliente	: cliente.getGrupoTributacao(),
				produto						: dadosProduto
			};
			
			that.getValorImpostos(params, function(impostos){
				dadosProduto.cf			= impostos.cf;
				dadosProduto.tes		= impostos.tes;
				dadosProduto.valor_st	= impostos.st;
				dadosProduto.valor_ipi	= impostos.ipi;
				dadosProduto.valor_icms	= impostos.icms;
				
				callback(dadosProduto);
			});
		});
	}
	
	this.setProduto = function(cabecalho, produto, descontoPeriodo, somarDesconto, callback)
	{	
		var pedido = new Pedidos();
		//mensagem(descontoPeriodo+' - desconto Período, ' + somarDesconto+' - somar Desconto ');
		
		pedido.getCabecalho(function(pedidoTemporario)
		{
			pedido.getArrayDadosCabecalho(cabecalho, pedidoTemporario, function(retorno)
			{	
				var campos			= [];
				var valores			= [];
				var interrogacoes	= [];
				
				var numero_item = localStorage.getItem('sequencial_item');
				if(numero_item){
					numero_item = parseInt(numero_item) + 1;
				} else {
					numero_item = 1;
				}
				localStorage.setItem('sequencial_item', numero_item);
				
				var preco_tabela = produto.ptp_preco;
				
				if(produto.unidade_medida_venda != produto.produto_unidade_medida)
				{
					if(produto.produto_tipo_converter == 'D'){
						preco_tabela = parseFloat(produto.ptp_preco) * parseFloat(produto.produto_converter);
					} else {
						preco_tabela = parseFloat(produto.ptp_preco) / parseFloat(produto.produto_converter);
					}
				}
				if(descontoPeriodo > 0)
				{
					produto.desconto = 0;	
				}
				
				retorno.campos.push('pedido_numero_item');				retorno.interrogacoes.push('?');	retorno.valores.push(numero_item);
				retorno.campos.push('pedido_codigo_produto');			retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_codigo);
				retorno.campos.push('pedido_preco_unitario');			retorno.interrogacoes.push('?');	retorno.valores.push(preco_tabela);
				retorno.campos.push('pedido_preco_venda');				retorno.interrogacoes.push('?');	retorno.valores.push(produto.preco_venda);
				retorno.campos.push('pedido_quantidade');				retorno.interrogacoes.push('?');	retorno.valores.push(produto.quantidade);
				retorno.campos.push('pedido_desconto_item');			retorno.interrogacoes.push('?');	retorno.valores.push(produto.desconto);
				retorno.campos.push('produto_descricao');				retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_descricao);
				retorno.campos.push('produto_ipi');						retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_ipi);
				retorno.campos.push('pedido_unidade_medida');			retorno.interrogacoes.push('?');	retorno.valores.push(produto.unidade_medida_venda);
				retorno.campos.push('pedido_local');					retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_locpad);
				retorno.campos.push('pedido_peso_total');				retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_peso_total);
				retorno.campos.push('item_troca');						retorno.interrogacoes.push('?');	retorno.valores.push(produto.item_troca);
				retorno.campos.push('produto_tipo_converter');			retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_tipo_converter);
				retorno.campos.push('produto_converter');				retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_converter);
				retorno.campos.push('produto_unidade_medida');			retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_unidade_medida);
				retorno.campos.push('produto_segunda_unidade_medida');	retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_segunda_unidade_medida);
				retorno.campos.push('produto_grupo_tributacao');		retorno.interrogacoes.push('?');	retorno.valores.push(produto.produto_grupo_tributacao);
				retorno.campos.push('pedido_tipo_entrada_saida');		retorno.interrogacoes.push('?');	retorno.valores.push(produto.tes);
				retorno.campos.push('pedido_codigo_fiscal');			retorno.interrogacoes.push('?');	retorno.valores.push(produto.cf);
				retorno.campos.push('pedido_st');						retorno.interrogacoes.push('?');	retorno.valores.push(produto.valor_st);
				retorno.campos.push('pedido_ipi');						retorno.interrogacoes.push('?');	retorno.valores.push(produto.valor_ipi);
				retorno.campos.push('pedido_icms');						retorno.interrogacoes.push('?');	retorno.valores.push(produto.valor_icms);
				retorno.campos.push('pedido_valor_unidade');			retorno.interrogacoes.push('?');	retorno.valores.push(produto.valor_unidade);
				
				var where = 'WHERE pedido_id_pedido = "' + pedidoTemporario.pedido_id_pedido + '"';
					where += ' AND pedido_codigo_produto = "' + produto.produto_codigo + '"';
					where += ' AND item_troca = "' + produto.item_troca + '"';
				
				db.transaction(function(x) 
				{
					x.executeSql('UPDATE ' + tabela + ' SET ' + retorno.campos.join(' = ?, ') + ' = ? ' + where, retorno.valores, function(x, dados)
					{	
						if(dados.rowsAffected < 1) 
						{
							x.executeSql('INSERT INTO ' + tabela + ' (' + retorno.campos.join(', ') + ') VALUES (' + retorno.interrogacoes.join(', ') + ')', retorno.valores, function()
							{
								callback(true);
							}, function(){
								callback(false);
							});
						} else {
							callback(true);
						}
					}, function(){
						callback(false);
					});
				});
				
			});
		});
	}
	
	this.getSegundaUnidadeMedidaProdutoSelecionado = function(){
		return $("#input_unidade_medida").val();
	}
	
	this.calcularValoresItemVenda = function(campo){
		var preco_tabela	= that.getPrecoUnitarioProdutoSelecionado();
		var preco_venda		= that.getPrecoVendaProdutoSelecionado();
		var valor_unidade	= that.getValorUnidadeProdutoSelecionado();
		var quantidade		= that.getQuantidadeProdutoSelecionado();
		var desconto		= that.getDescontoProdutoSelecionado();
		
		var tipo_converter	= that.getTipoConversaoProdutoSelecionado();
		var fator_conversao	= that.getFatorConversaoProdutoSelecionado();
		
		if(campo == 'preco_venda'){
			
			if(tipo_converter == 'D'){
				valor_unidade = preco_venda / fator_conversao;
			} else {
				valor_unidade = preco_venda * fator_conversao;
			}
			
			that.setValorUnidadeProdutoSelecionado(valor_unidade);
			that.setDescontoProdutoSelecionado(that.calcularDesconto(campo));
			
		} else if(campo == 'valor_unidade'){

			if(tipo_converter == 'D'){
				preco_venda = valor_unidade * fator_conversao;
			} else {
				preco_venda = valor_unidade / fator_conversao;
			}
			
			that.setPrecoVendaProdutoSelecionado(preco_venda);
			that.setDescontoProdutoSelecionado(that.calcularDesconto(campo));
			
		} else if(campo == 'desconto'){
			
			preco_venda = preco_tabela - ((preco_tabela * desconto) / 100);
			
			if(tipo_converter == 'D'){
				valor_unidade = preco_venda / fator_conversao;
			} else {
				valor_unidade = preco_venda * fator_conversao;
			}
			
			that.setPrecoVendaProdutoSelecionado(preco_venda);
			that.setValorUnidadeProdutoSelecionado(valor_unidade);
			
		} else {
			
			if(campo != 'quantidade'){
				preco_venda = preco_tabela;
				
				if(tipo_converter == 'D'){
					valor_unidade = preco_venda / fator_conversao;
				} else {
					valor_unidade = preco_venda * fator_conversao;
				}
				
				that.setPrecoVendaProdutoSelecionado(preco_venda);
				that.setValorUnidadeProdutoSelecionado(valor_unidade);
				that.setDescontoProdutoSelecionado(that.calcularDesconto(campo));
			}
		}
		
		that.setValorTotalProdutoSelecionado();
	}
	
	this.calcularValoresItemTroca = function(campo){
		var preco_tabela	= that.getPrecoUnitarioProdutoSelecionado();
		var preco_venda		= that.getPrecoVendaProdutoSelecionado();
		var valor_unidade	= that.getValorUnidadeProdutoSelecionado();
		var quantidade		= that.getQuantidadeProdutoSelecionado();
		var desconto		= that.getDescontoProdutoSelecionado();
		
		var tipo_converter	= that.getTipoConversaoProdutoSelecionado();
		var fator_conversao	= that.getFatorConversaoProdutoSelecionado();
		
		if(campo == 'preco_venda'){
			
			valor_unidade = preco_venda;
			
			that.setValorUnidadeProdutoSelecionado(valor_unidade);
			that.setDescontoProdutoSelecionado(that.calcularDesconto(campo, true));
			
		} else if(campo == 'valor_unidade'){
			
			preco_venda = valor_unidade;
			
			that.setPrecoVendaProdutoSelecionado(preco_venda);
			that.setDescontoProdutoSelecionado(that.calcularDesconto(campo, true));
			
		} else if(campo == 'desconto'){
			
			preco_venda = preco_tabela - ((preco_tabela * desconto) / 100);
			if(tipo_converter = 'D'){
				valor_unidade = preco_venda / fator_conversao;
			} else {
				valor_unidade = preco_venda * fator_conversao;
			}
			preco_venda = valor_unidade;
			
			that.setPrecoVendaProdutoSelecionado(valor_unidade);
			that.setValorUnidadeProdutoSelecionado(valor_unidade);
			
		} else {
			
			if(campo != 'quantidade'){
				preco_venda = valor_unidade;
				
				that.setPrecoVendaProdutoSelecionado(preco_venda);
			}
			
		}
		
		that.setValorTotalProdutoSelecionado();
	}
	
	this.calcularValoresProdutoSelecionado = function(campo){
		if((that.getItemTrocaProdutoSelecionado() == '1') && (that.getSegundaUnidadeMedidaProdutoSelecionado() != that.getUnidadeMedidaProdutoSelecionado())){
			that.calcularValoresItemTroca(campo);
		} else {
			that.calcularValoresItemVenda(campo);
		}
	}
	
	this.calcularDesconto = function(campo, troca){
		var preco_tabela	= that.getPrecoUnitarioProdutoSelecionado();
		var preco_venda		= that.getPrecoVendaProdutoSelecionado();
		var valor_unidade	= that.getValorUnidadeProdutoSelecionado();
		var tipo_converter	= that.getTipoConversaoProdutoSelecionado();
		var fator_conversao	= that.getFatorConversaoProdutoSelecionado();
		
		if(campo == 'preco_venda'){
			if(troca){
				preco_venda = valor_unidade * fator_conversao;
			}
		} else if(campo == 'valor_unidade'){
			if(tipo_converter = 'D'){
				preco_venda = valor_unidade * fator_conversao;
			} else {
				preco_venda = valor_unidade / fator_conversao;
			}
		}
		
		var diferenca	= round(preco_tabela - preco_venda, 2);
		var desconto	= diferenca / preco_tabela * 100;
		
		if(desconto < 0){
			desconto = 0;
		}
		
		return desconto;
	}
	
	this.setPrecoVendaProdutoSelecionado = function(value){
		$("#preco_venda").val(number_format(value, 2, ',', '.'));
	}
	
	this.setValorUnidadeProdutoSelecionado = function(value){
		$("#valor_unidade").val(number_format(value, 2, ',', '.'));
	}
	
	this.setDescontoProdutoSelecionado = function(value){
		$("#desconto").val(number_format(value, 2, ',', '.'));
	}
	
	this.setValorTotalProdutoSelecionado = function(total){
		if(!total){
			var total = parseFloat(that.getPrecoVendaProdutoSelecionado()) * parseInt(that.getQuantidadeProdutoSelecionado());
		}
		$("#total_item").val(number_format(total, 2, ',', '.'));
	}
	
	this.setTipoConversaoProdutoSelecionado = function(value){
		$("#tipo_conversao").val(value);
	}
	
	this.setFatorConversaoProdutoSelecionado = function(value){
		$("#valor_conversao").val(value);
	}
	
	this.setCodigoReduzidoProdutoSelecionado = function(value){
		$("#codigo_reduzido").val(value);
	}
	
	this.getLabelProdutoSelecionado = function(){
		return $("#auto-produto").val();
	}
	
	this.setLabelProdutoSelecionado = function(value){
		$("#auto-produto").val(value);
	}
	
	this.setPrecoUnitarioProdutoSelecionado = function(value){
		$("#preco_unitario").val(number_format(value, 2, ',', '.'));
	}
	
	this.getListaUnidadesMedida = function(produto){
		$("#dropdown_unidade_medida").html('');
		$("#dropdown_unidade_medida").append('<option value="' + produto.produto_segunda_unidade_medida + '">' + produto.produto_segunda_unidade_medida + '</option>');
		$("#dropdown_unidade_medida").append('<option value="' + produto.produto_unidade_medida + '" selected="selected">' + produto.produto_unidade_medida + '</option>');
	}
	
	this.setUnidadeMedidaProdutoSelecionado = function(value){
		if(that.getItemTrocaProdutoSelecionado() == '1'){
			$("#dropdown_unidade_medida").val(value);
		} else {
			$("#input_unidade_medida").val(value);
		}
	}
	
	this.setQuantidadeProdutoSelecionado = function(value){
		$("#quantidade").val(value);
	}
	
	this.setCodigoProdutoSelecionado = function(value){
		$("#codigo_produto").val(value);
	}
	
	this.verificarProdutoExiste = function(codigo_produto, item_troca, callback){
		db.transaction(function(x){
			x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_status = ? AND pedido_codigo_produto = ? AND item_troca = ? LIMIT 1', ['R', codigo_produto, item_troca.toString()], function(x, dados){
				if(dados.rows.length){
					callback(dados.rows.item(0));
				} else {
					callback(false);
				}
			});
		});
	}
	
	
	this.getItensVenda = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_status = ? AND item_troca = ? ORDER BY CAST(pedido_numero_item AS INT) DESC', ['R', '0'], function(x, dados){
				if(dados.rows.length){
					callback(dados);
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.getItensTroca = function(callback){
		db.transaction(function(x){
			x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_status = ? AND item_troca = ? ORDER BY CAST(pedido_numero_item AS INT) DESC', ['R', '1'], function(x, dados){
				if(dados.rows.length){
					callback(dados);
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.getValoresProdutoItemTroca = function(){
		$("#unidade_medida_normal").hide();
		$("#unidade_medida_troca").show();
		
		that.calcularValoresProdutoSelecionado();
	}
	
	this.getValoresProdutoItemVenda = function(){
		$("#unidade_medida_normal").show();
		$("#unidade_medida_troca").hide();
		
		that.calcularValoresProdutoSelecionado();
	}
	
	this.excluir = function(codigo_filial, codigo_produto, item_troca, callback){
		db.transaction(function(x){
			x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_filial = "' + codigo_filial + '" AND pedido_codigo_produto = "' + codigo_produto + '" AND item_troca = "' + item_troca + '" AND pedido_status = "R"', [], function(x, dados){
				callback();
			});
		});
	}
	
	//TODO
	this.getLimiteDescontoAcrescimo = function(codigo_tabela, codigo_produto, callback) {
		db.transaction(function(x){
			x.executeSql('SELECT produto_limite_desconto AS limite_desconto, produto_limite_acrescimo AS limite_acrescimo FROM produtos WHERE produto_codigo = ? AND ptp_codigo_tabela_precos = ?', [codigo_produto, codigo_tabela], function(x, dados){
				if(dados.rows.length){
					callback(dados.rows.item(0));
				} else {
					callback(false);
				}
			});
		});
	};
	
	this.validarDesconto = function(limiteDesconto) {
		return (parseFloat(limiteDesconto) >= parseFloat(that.getDescontoProdutoSelecionado()));
	};
		
	this.validarAcrescimo = function(limiteAcrescimo) {
		var valido = true;
		
		var precoVenda = parseFloat(that.getPrecoVendaProdutoSelecionado());
		var precoTabela = parseFloat(that.getPrecoUnitarioProdutoSelecionado());
		
		if(precoVenda > precoTabela) {
			var acrescimo = ((precoVenda / precoTabela) - 1) * 100;
			
			if(parseFloat(acrescimo) > parseFloat(limiteAcrescimo)) {
				valido = false;
			}
		}
		
		return valido;
	};
	
};

var Representantes = function(){
	
	this.getInformacoesRepresentante = function(callback) {
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM representante', [], function(x, dados) {
				if(dados.rows.length){
					callback(dados.rows.item(0));
				} else {
					callback(false);
				}
			});
		});
	};
	
	this.getLimiteDesconto = function(callback){
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM representante', [], function(x, dados) {
				if(dados.rows.length){
					callback(dados.rows.item(0));
				} else {
					callback(false);
				}
			});
		});
	}
	
	this.getDescontoPeriodoGrupo = function(codigo_grupo, callback)
	{ 
		var d = new Date();
		var mes = d.getMonth() + 1;
		var dia = d.getDate();
		var dataAtual = d.getFullYear() +''+ (mes < 10 ? '0' + mes : '' + mes) +''+ (dia < 10 ? '0' + dia : '' + dia) ;
		var retorno = 0;

		db.transaction(function(x)
		{
			x.executeSql("SELECT * FROM desconto_periodo WHERE grupo_produto = '"+codigo_grupo+"' and data_vigencia >= '"+dataAtual+"' ", [], function(x, dados)
			{
				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
					callback(dado);
				}
				else 
				{
					callback(null);
				}
			});
		});
	}
	
	this.getDescontoPeriodoCliente = function(codigo_cliente, callback)
	{
		var d = new Date();
		var mes = d.getMonth() + 1;
		var dia = d.getDate();
		var dataAtual = d.getFullYear() +''+ (mes < 10 ? '0' + mes : '' + mes) +''+ (dia < 10 ? '0' + dia : '' + dia) ;
		var retorno = 0;

		db.transaction(function(x)
		{
			x.executeSql("SELECT * FROM desconto_periodo WHERE codigo_cliente = '"+codigo_cliente+"' and data_vigencia >= '"+ dataAtual +"' ", [], function(x, dados)
			{
				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
					callback(dado);
				}
				else 
				{
					callback(null);
				}
			});
		});
	} 
	
	this.getDescontoPeriodoRepresentante = function(codigo_representante, callback)
	{ 
		var d = new Date();
		var mes = d.getMonth() + 1;
		var dia = d.getDate();
		var dataAtual = d.getFullYear() +''+ (mes < 10 ? '0' + mes : '' + mes) +''+ (dia < 10 ? '0' + dia : '' + dia) ;

		db.transaction(function(x)
		{
			x.executeSql("SELECT * FROM desconto_periodo WHERE codigo_representante = '"+codigo_representante+"' and data_vigencia >= '"+ dataAtual +"' ", [], function(x, dados)
			{
				if(dados.rows.length)
				{
					var dado = dados.rows.item(0);
					callback(dado);
				}
				else 
				{
					callback(null);
				}
			});
		});
	}
	
	
	
};