package com.developweb.dwfdv;

import org.apache.cordova.Config;
import org.apache.cordova.DroidGap;

import android.os.Bundle;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends DroidGap {

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width = dm.widthPixels;
		int height = dm.heightPixels;
		
		Log.v(TAG, "width=" + width + "height= "+ height);
		
		
		super.onCreate(savedInstanceState);			
		this.getIntent().putExtra("loadUrlTimeoutValue", 90000);
		super.loadUrl(Config.getStartUrl());
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	//handle menu item selection
	 public boolean onOptionsItemSelected(MenuItem item)
	 {
		 
		 switch (item.getItemId()) 
		 {
		 	case R.id.btnHome: super.loadUrl("file:///android_asset/www/index.html");
		 	break;
		 	case R.id.btnInfo: super.loadUrl("file:///android_asset/www/info.html");
		 	break;
		 	case R.id.btnEmpresas: super.loadUrl("javascript: abrirEmpresas();");
		 	break;
		 	case R.id.btnSincronizar:  super.loadUrl("file:///android_asset/www/sincronizar.html");
		 	break;
		 }
		 return true;
	 }
	
	public void onConfigurationChanged(Configuration newConfig)
	{
	    super.onConfigurationChanged(newConfig);

	    DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width = dm.widthPixels;
		int height = dm.heightPixels;
		
	    
	  }

}
