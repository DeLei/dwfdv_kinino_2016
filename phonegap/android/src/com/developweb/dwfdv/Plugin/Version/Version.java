package com.developweb.dwfdv.Plugin.Version;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;

import org.json.JSONArray;
import org.json.JSONException;

public class Version extends CordovaPlugin 
{	
	public final String ACTION_GET_VERSION_CODE = "GetVersionCode";

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException 
	{            
		if(action.equals(ACTION_GET_VERSION_CODE)) 
		{
			this.retornarVersionCode(callbackContext);
			return true;
		}
		return false;
	}

	private void retornarVersionCode(CallbackContext callbackContext)
	{
		PackageManager packageManager = this.cordova.getActivity().getPackageManager();
		try 
		{
			PackageInfo packageInfo = packageManager.getPackageInfo(this.cordova.getActivity().getPackageName(), 0);
			Integer versionCode = packageInfo.versionCode;
			callbackContext.success(versionCode.toString());             
		}
		catch (NameNotFoundException nnfe) 
		{                
			callbackContext.success(nnfe.getMessage());             
		}
	}
}