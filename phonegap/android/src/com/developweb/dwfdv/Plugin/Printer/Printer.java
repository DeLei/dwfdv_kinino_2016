package com.developweb.dwfdv.Plugin.Printer;

/////////////////////////////////////////////
// LIB CORDOVA
/////////////////////////////////////////////
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.ComponentName;
/////////////////////////////////////////////
// LIB ANDROI
/////////////////////////////////////////////
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;

/////////////////////////////////////////////
// LIB CITIZEN
/////////////////////////////////////////////
import com.citizen.*;

public class Printer extends CordovaPlugin
{

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException 
    {
        if (action.equals("imprimir")) 
        {
        	
        	try {

	        	Intent intent = new Intent();
	            intent.setComponent(new ComponentName("com.citizen.app.escpos", "com.citizen.app.escpos.ESCPOSTester"));
	            this.cordova.getActivity().startActivity(intent);
	            callbackContext.success();
	            return true;
	            
        	} catch (Exception e) {
        		
        		callbackContext.error("Erro ao iniciar a aplicação de impressão.");
				return false;
				
			}
        }
        return false;
    }
    
}
